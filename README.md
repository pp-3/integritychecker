# IntegrityChecker #

IntegrityChecker provides an easy to use interface for creating integrity data (e.g. check sums) and use this data later on to verify files are unchanged / intact.
Supported data types are: MD5, SHA1, SHA256, SHA512, SHA3_256, and SHA3_512.

![Screenshot](doc/img/screenshot.jpg "Screenshot")

### Build ###
To build the software with cmake call from the source directory:

```bash
mkdir build
cd build
cmake ..
```

For this a default QT5 development package has to be installed. E.g. the qtbase5-dev in Debian.

If the QT libraries are not found, add the install path manually before calling cmake:

```bash
export CMAKE_PREFIX_PATH=.../path_to_qt_base_path
```
 
In case this error occurs: 'Failed to find "GL/gl.h" in "/usr/include/libdrm".' during the cmake run, install the "mesa-common-dev" package.

### Translations ###
The integritychecker binary in the build directory runs with an English GUI. To create the currently supported translation files call in the build directory:

```bash
make integritychecker_translations
```

### Target ###
On the target machine the Qt libraries "widgets", "core", and "gui" have to be available via the library
search paths.

### Developer documentation ###
For enabling the build of the developer documentation doxygen is required. In the build directory create the documentation with:

```bash
cmake .. -D BUILD_DOC=ON
make doc
```

The CI build version can be found here: [https://pp-3.gitlab.io/integritychecker/](https://pp-3.gitlab.io/integritychecker/).

For disabling the building of the tests set the cmake variable BUILD_TESTING to 'OFF':

```bash
cmake .. -D BUILD_TESTING=OFF
```
