add_compile_options(-fno-exceptions -Werror -Wall -Wextra -Wunused -Wformat -Wmissing-include-dirs)

if (BUILD_TESTING)

# add test define
add_compile_definitions(ENABLE_TEST_ADDONS)

endif (BUILD_TESTING)
