<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name></name>
    <message>
        <source>Integrity data exists</source>
        <translation type="vanished">Integritätsdaten gefunden</translation>
    </message>
    <message>
        <source>Integrity data is verified</source>
        <translation type="vanished">Integritätsdaten überprüft</translation>
    </message>
    <message>
        <source>Integrity data error</source>
        <translation type="vanished">Integritätsdaten fehlerhaft</translation>
    </message>
    <message>
        <source>Check sum file exists, but reading failed before writing: </source>
        <translation type="vanished">Checksumme existiert, konnte vor dem Schreiben aber nicht gelesen werden: </translation>
    </message>
    <message>
        <source>Writing check sum file failed: </source>
        <translation type="vanished">Fehler beim Schreiben der Checksumme: </translation>
    </message>
    <message>
        <source>Check sum back up file renaming failed: </source>
        <translation type="vanished">Umbennen der Checksummensicherung fehlgeschlagen: </translation>
    </message>
    <message>
        <source>Removing check sum back file failed: </source>
        <translation type="vanished">Löschen der Checksummensicherung fehlgeschlagen: </translation>
    </message>
    <message>
        <source>Error during parsing line in checksum file: </source>
        <translation type="vanished">Einlesen der Checksummendatei fehlgeschlagen: </translation>
    </message>
    <message>
        <source>Check sum line size shorter than required: </source>
        <translation type="vanished">Checksummenzeile kürzer als erwartet: </translation>
    </message>
    <message>
        <source>Check sum separator not found in line: </source>
        <translation type="vanished">Checksummentrenner in Zeile nicht gefunden: </translation>
    </message>
    <message>
        <source>Search integrity data</source>
        <translation type="vanished">Suche Integritätsdaten</translation>
    </message>
    <message>
        <source>Compare integrity data</source>
        <translation type="vanished">Vergleiche Integritätsdaten</translation>
    </message>
    <message>
        <source>Calculate integrity data</source>
        <translation type="vanished">Berechne Integritätsdaten</translation>
    </message>
    <message>
        <source>Scan files: </source>
        <translation type="vanished">Suche Dateien: </translation>
    </message>
    <message>
        <source>Find files: </source>
        <translation type="vanished">Suche nach Dateien: </translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">Hilfe</translation>
    </message>
    <message>
        <source>Error: unknown profile: </source>
        <translation type="vanished">Fehler: Unbekanntes Profil: </translation>
    </message>
    <message>
        <source>Available profiles:</source>
        <translation type="vanished">Verfügbare Profile:</translation>
    </message>
    <message>
        <source>Error: profile name missing</source>
        <translation type="vanished">Fehler: Profil Name fehlt</translation>
    </message>
    <message>
        <source>Error: language missing. Add 2 character abbreviation. E.g. en</source>
        <translation type="vanished">Fehler: Sprache fehlt. Fügen Sie eine 2 Buchstabenabkürzung an, z.B.: de</translation>
    </message>
    <message>
        <source>Unknown parameter:</source>
        <translation type="vanished">Unbekannter Parameter:</translation>
    </message>
    <message>
        <source>Usage:</source>
        <translation type="vanished">Anwendung:</translation>
    </message>
    <message>
        <source>language</source>
        <translation type="vanished">Sprache</translation>
    </message>
    <message>
        <source>profile</source>
        <translation type="vanished">Profil</translation>
    </message>
    <message>
        <source>Set 2 character language for GUI</source>
        <translation type="vanished">Setzen Sie die 2 Buchstabenabkürzung für die Oberflächensprache</translation>
    </message>
    <message>
        <source>Show help</source>
        <translation type="vanished">Zeige Hilfe</translation>
    </message>
    <message>
        <source>Profile name for user settings</source>
        <translation type="vanished">Profilname für die Benutzereinstellungen</translation>
    </message>
    <message>
        <source>Start without gui</source>
        <translation type="vanished">Ohne grafische Oberfläche starten</translation>
    </message>
</context>
<context>
    <name>ChooseProfileDialog</name>
    <message>
        <location filename="../src/view/ChooseProfileDialog.ui" line="33"/>
        <source>Choose Profile</source>
        <translation>Wählen Sie ein Profil</translation>
    </message>
    <message>
        <location filename="../src/view/ChooseProfileDialog.cpp" line="24"/>
        <source>Choose Profile to use</source>
        <translation>Profilauswahl</translation>
    </message>
</context>
<context>
    <name>FileInfoDialog</name>
    <message>
        <location filename="../src/view/FileInfoDialog.cpp" line="34"/>
        <source>File Details</source>
        <translation>Datei Details</translation>
    </message>
    <message>
        <location filename="../src/view/FileInfoDialog.cpp" line="82"/>
        <source> file(s): </source>
        <translation> Datei(en): </translation>
    </message>
    <message>
        <location filename="../src/view/FileInfoDialog.cpp" line="100"/>
        <source> file(s) without check sum</source>
        <translation> Datei(en) ohne Checksumme</translation>
    </message>
    <message>
        <source>file(s): </source>
        <translation type="obsolete">Datei(en): </translation>
    </message>
    <message>
        <source> file(s) without check sum.</source>
        <translation type="obsolete">Datei(en) ohne Checksumme.</translation>
    </message>
    <message>
        <location filename="../src/view/FileInfoDialog.cpp" line="112"/>
        <source>No check sums exist.</source>
        <translation>Keine Checksumme gefunden.</translation>
    </message>
</context>
<context>
    <name>FileInfoForm</name>
    <message>
        <location filename="../src/view/FileInfoForm.ui" line="52"/>
        <source>Path:</source>
        <translation>Pfad:</translation>
    </message>
</context>
<context>
    <name>FileViewModel</name>
    <message>
        <location filename="../src/presenter/FileViewModel.cpp" line="102"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
</context>
<context>
    <name>LanguageSelectionDialog</name>
    <message>
        <location filename="../src/view/LanguageSelectionDialog.cpp" line="27"/>
        <source>Language Selection</source>
        <translation>Sprachauswahl</translation>
    </message>
    <message>
        <location filename="../src/view/LanguageSelectionDialog.ui" line="20"/>
        <source>Select language</source>
        <translation>Wählen Sie eine Sprache aus</translation>
    </message>
</context>
<context>
    <name>OptionPresenter</name>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="143"/>
        <source>Can&apos;t parse worker thread count: </source>
        <translation>Einlesefehler Arbeitsthreads: </translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="174"/>
        <source>Can&apos;t parse cache size: </source>
        <translation>Einlesefehler Cachegröße: </translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="214"/>
        <source>Separate output path?</source>
        <translation>Separater Ausgabepfad?</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="214"/>
        <source>Are you sure you want to enable / diable the use of a separate check sum output directory?</source>
        <translation>Sind Sie sicher, daß Sie einen separaten Ausgabepfad  aktivieren / deaktivieren möchten?</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="243"/>
        <source>Change input directory?</source>
        <translation>Eingangsverzeichnis ändern?</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="243"/>
        <source>Are you sure you want to change the base directory for the check sum files?</source>
        <translation>Sind Sie sicher, daß Sie das Eingangsverzeichnis ändern möchten?</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="249"/>
        <source>Chosen input directory does not exist: </source>
        <translation>Gewähltes Eingangsverzeichnis existiert nicht: </translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="249"/>
        <source>Invalid input directory!</source>
        <translation>Ungültiges Eingangsverzeichnis!</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="276"/>
        <source>Change output directory?</source>
        <translation>Ausgangsverzeichnis ändern?</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="276"/>
        <source>Are you sure you want to change the output directory for the check sum files?</source>
        <translation>Sind Sie sicher, daß Sie das Ausgangsverzeichnis für die Checksummen ändern möchten?</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="312"/>
        <source>Chosen output directory does not exist: </source>
        <translation>Gewähltes Ausgangsverzeichnis existiert nicht: </translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="312"/>
        <source>Create it?</source>
        <translation>Verzeichnis anlegen?</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="312"/>
        <source>Invalid output directory</source>
        <translation>Ungültiges Ausgangsverzeichnis</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="318"/>
        <source>Directory creating failed: </source>
        <translation>Fehler beim Verzeichnis anlegen: </translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="318"/>
        <source>Check status log for more information.</source>
        <translation>Mehr Informationen im Ausgabefenster.</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="318"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/presenter/OptionPresenter.cpp" line="346"/>
        <source>Choose directory</source>
        <translation>Verzeichnis wählen</translation>
    </message>
</context>
<context>
    <name>PresenterManager</name>
    <message>
        <location filename="../src/presenter/PresenterManager.cpp" line="146"/>
        <source>IntegrityChecker Setup</source>
        <translation>IntegrityChecker Einrichten</translation>
    </message>
</context>
<context>
    <name>SetupWizardCalc</name>
    <message>
        <location filename="../src/view/SetupWizardCalc.ui" line="20"/>
        <source>Select the type of integrity data to show</source>
        <oldsource>Select the type of check sums to enable</oldsource>
        <translation>Auswahl der anzuzeigenden Integritätstypen</translation>
    </message>
    <message>
        <location filename="../src/view/SetupWizardCalc.cpp" line="41"/>
        <source>Integrity data types</source>
        <translation>Integritätstypen</translation>
    </message>
</context>
<context>
    <name>SetupWizardFinal</name>
    <message>
        <location filename="../src/view/SetupWizardFinal.ui" line="20"/>
        <source>Finish</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="../src/view/SetupWizardFinal.cpp" line="25"/>
        <source>Configuration finished</source>
        <translation>Konfiguration beendet</translation>
    </message>
    <message>
        <location filename="../src/view/SetupWizardFinal.cpp" line="27"/>
        <source>IntegrityChecker is now configured for checking and calculating integrity data.
All settings may be changed in the &apos;Options&apos; tab.</source>
        <oldsource>IntegrityChecker is now configured for checking and calculating check sums.
All settings may be changed in the Option tab.</oldsource>
        <translation>IntegrityChecker ist jetzt soweit konfiguriert, um Integritätsdaten zu vergleichen und zu erzeugen.\nDiese Einstellungen können im Reiter &apos;Einstellungen&apos; wieder geändert werden.</translation>
    </message>
</context>
<context>
    <name>SetupWizardIntro</name>
    <message>
        <location filename="../src/view/SetupWizardIntro.ui" line="29"/>
        <source>Intro</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/view/SetupWizardIntro.cpp" line="25"/>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location filename="../src/view/SetupWizardIntro.cpp" line="27"/>
        <source>It seems IntegrityChecker is not configured properly.
Please select a base path for looking up existing integrity data files and the type of integrity data to show in the main window.

All settings may be changed later on in the &apos;Options&apos; tab.</source>
        <oldsource>It seems IntegrityChecker is not configured properly.
Please select a base path for looking up existing check sums and the type of check sums to show in the main window.

All settings may be changed later on in the Options tab.</oldsource>
        <translation>IntegrityChecker ist bisher nicht konfiguriert.\nBitte wählen Sie einen Ursprungspfad für die Suche nach den Integritätsdaten und die anzuzeigenden Integritätstypen aus.\n\nAlle Einstellungen können später im Reiter &apos;Einstellungen&apos; wieder geändert werden.</translation>
    </message>
</context>
<context>
    <name>SetupWizardPath</name>
    <message>
        <location filename="../src/view/SetupWizardPath.ui" line="20"/>
        <source>Select the base path for all integrity data files</source>
        <oldsource>Select the base path for all check sum files</oldsource>
        <translation>Wählen Sie bitte den Ursprungspfad für die Integritätsdateien aus</translation>
    </message>
    <message>
        <location filename="../src/view/SetupWizardPath.ui" line="52"/>
        <source>Browse</source>
        <translation>Durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/view/SetupWizardPath.cpp" line="42"/>
        <source>Input path</source>
        <translation>Ursprungspfad</translation>
    </message>
    <message>
        <location filename="../src/view/SetupWizardPath.cpp" line="83"/>
        <source>Choose directory</source>
        <translation>Verzeichnis wählen</translation>
    </message>
</context>
<context>
    <name>StatusOutputModel</name>
    <message>
        <location filename="../src/presenter/StatusOutputModel.cpp" line="118"/>
        <source>Task</source>
        <translation>Tätigkeit</translation>
    </message>
    <message>
        <source>Sub Tasks</source>
        <translation type="vanished">Unteraufgaben</translation>
    </message>
    <message>
        <source>Open Sub Tasks</source>
        <translation type="vanished">Offene Unteraufgaben</translation>
    </message>
    <message>
        <location filename="../src/presenter/StatusOutputModel.cpp" line="119"/>
        <source>Total Tasks</source>
        <translation>Alle Tätigkeiten</translation>
    </message>
    <message>
        <location filename="../src/presenter/StatusOutputModel.cpp" line="120"/>
        <source>Open Tasks</source>
        <translation>Offene Tätigkeiten</translation>
    </message>
    <message>
        <location filename="../src/presenter/StatusOutputModel.cpp" line="121"/>
        <source>Started</source>
        <translation>Startzeit</translation>
    </message>
    <message>
        <location filename="../src/presenter/StatusOutputModel.cpp" line="122"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
</context>
<context>
    <name>TaskLifeCycle</name>
    <message>
        <location filename="../src/tasks/TaskLifeCycle.cpp" line="166"/>
        <source>Waiting</source>
        <translation>Warten</translation>
    </message>
    <message>
        <location filename="../src/tasks/TaskLifeCycle.cpp" line="167"/>
        <source>Running</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="../src/tasks/TaskLifeCycle.cpp" line="168"/>
        <source>Finished</source>
        <translation>Beendet</translation>
    </message>
    <message>
        <location filename="../src/tasks/TaskLifeCycle.cpp" line="169"/>
        <source>Canceled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <source>Init</source>
        <translation type="obsolete">Init</translation>
    </message>
</context>
<context>
    <name>Tr</name>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="78"/>
        <source>Expected data vanished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="93"/>
        <source>Check sum file exists, but reading failed before writing: </source>
        <translation>Checksumme existiert, konnte vor dem Schreiben aber nicht gelesen werden: </translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="117"/>
        <source>Creating path failed for: </source>
        <translation>Pfaderstellung fehlgeschlagen für: </translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="147"/>
        <source>Check sum file open failed: </source>
        <translation>Öffnen der Checksumme fehlgeschlagen: </translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="158"/>
        <source>Writing check sum file failed: </source>
        <translation>Fehler beim Schreiben der Checksumme: </translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="177"/>
        <source>Check sum back up file renaming failed: </source>
        <translation>Umbennen der Checksummensicherung fehlgeschlagen: </translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="190"/>
        <source>Removing check sum back file failed: </source>
        <translation>Löschen der Checksummensicherung fehlgeschlagen: </translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="244"/>
        <source>Error during parsing line in checksum file: </source>
        <translation>Einlesen einer Zeile der Checksummendatei fehlgeschlagen: </translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="279"/>
        <source>Check sum line size shorter than required: </source>
        <translation>Checksummenzeile kürzer als erwartet: </translation>
    </message>
    <message>
        <location filename="../src/sum_storage/TwoColumnSumFile.cpp" line="292"/>
        <source>Check sum separator not found in line: </source>
        <translation>Checksummentrenner in Zeile nicht gefunden: </translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="65"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="69"/>
        <source>Shows this help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="74"/>
        <source>Closes the application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="100"/>
        <source>File system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="104"/>
        <source>Lists the files in the current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="109"/>
        <source>Shows the current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="114"/>
        <source>Changes directory to dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="175"/>
        <source>Checks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="179"/>
        <source>Shows the integrity status of the current directory or the given file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="184"/>
        <source>Scans the given directory for new files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="189"/>
        <source>Checks the files in the current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="194"/>
        <source>Creates the integrity data for the files in the current directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="269"/>
        <source>Tasks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="273"/>
        <source>Shows the current active tasks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="311"/>
        <source>Unknown id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="278"/>
        <source>Cancels an active task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="339"/>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="437"/>
        <source>Call a sub command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="362"/>
        <source>Input path is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="389"/>
        <source>Setting enabled types failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="331"/>
        <source>Reads or sets the enabled check types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="409"/>
        <source>Shows the current profile name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="414"/>
        <source>Switches current profile to &apos;name&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="424"/>
        <source>Renames current profile to &apos;new_name&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="429"/>
        <source>Deletes the existing profile &apos;name&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="326"/>
        <source>Reads or sets the input path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="111"/>
        <source>profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="404"/>
        <source>Lists available profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="419"/>
        <source>Copies current profile to another profile &apos;new_name&apos; and switches to it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="581"/>
        <location filename="../src/console/io/ConsoleIo.cpp" line="67"/>
        <source>Error: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="586"/>
        <source>No parameters expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="590"/>
        <source>Minimum required parameters: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="595"/>
        <location filename="../src/console/cmds/CommandAdapter.cpp" line="599"/>
        <source>Required parameters: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ifc/CommandHandlerConstants.cpp" line="22"/>
        <source>Bye.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ifc/CommandHandlerConstants.cpp" line="20"/>
        <source>Unknown sub command: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ifc/CommandHandlerConstants.cpp" line="19"/>
        <source>Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ifc/CommandHandlerConstants.cpp" line="21"/>
        <source>Unknown command: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ifc/CommandHandlerConstants.cpp" line="23"/>
        <source>Command line interpreter. Type &apos;help&apos; for all commands.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/io/ConsoleIo.cpp" line="77"/>
        <source>Couldn&apos;t open stdin: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/io/ConsoleIo.cpp" line="85"/>
        <source>Couldn&apos;t open stdout: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ConsoleModel.cpp" line="58"/>
        <source>Checksum data invalid!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ConsoleModel.cpp" line="90"/>
        <source>Not existing path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ConsoleModel.cpp" line="128"/>
        <source>Not existing directory: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ConsoleModel.cpp" line="256"/>
        <source>Invalid path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ConsoleModel.cpp" line="266"/>
        <source>Directory not found for path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ConsoleModel.cpp" line="193"/>
        <location filename="../src/console/ConsoleModel.cpp" line="245"/>
        <location filename="../src/console/ConsoleModel.cpp" line="285"/>
        <location filename="../src/console/ConsoleModel.cpp" line="331"/>
        <source>Path Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/console/ConsoleUtils.cpp" line="160"/>
        <source>Unknown file: </source>
        <translation>Unbekannte Datei: </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="67"/>
        <source>Error: unknown profile: </source>
        <translation>Fehler: Unbekanntes Profil: </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="68"/>
        <source>Available profiles:</source>
        <translation>Verfügbare Profile:</translation>
    </message>
    <message>
        <location filename="../src/presenter/HelpLink.cpp" line="54"/>
        <source>help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../src/presenter/HelpLink.cpp" line="55"/>
        <source>Open related help page</source>
        <translation>Öffne zugehörige Hilfeseite</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="55"/>
        <source>Error: profile name missing</source>
        <translation>Fehler: Profilname fehlt</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="68"/>
        <source>Error: language missing. Add 2 character abbreviation. E.g. en</source>
        <translation>Fehler: Sprache fehlt. Fügen Sie eine 2 Buchstabenabkürzung an, z.B.: de</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="82"/>
        <source>Unknown parameter:</source>
        <translation>Unbekannter Parameter:</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="107"/>
        <source>Usage:</source>
        <translation>Anwendung:</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="109"/>
        <source>language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="117"/>
        <source>Set 2 character language for GUI</source>
        <translation>Setzen Sie die 2 Buchstabenabkürzung für die Oberflächensprache</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="118"/>
        <source>Show help</source>
        <translation>Zeige Hilfe</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="119"/>
        <source>Profile name for user settings</source>
        <translation>Profilname für die Benutzereinstellungen</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="120"/>
        <source>Start without gui</source>
        <translation>Ohne grafische Oberfläche starten</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="121"/>
        <source>Start an interactive shell</source>
        <translation>Ein interaktive Eingabemaske starten</translation>
    </message>
    <message>
        <location filename="../src/shared/ParameterParser.cpp" line="122"/>
        <source>Shortcut for -nogui plus -i</source>
        <translation>Abkürzung für -nogui und -i</translation>
    </message>
    <message>
        <location filename="../src/sum_shared/CheckSumState.h" line="67"/>
        <source>Integrity data exists</source>
        <translation>Integritätsdaten gefunden</translation>
    </message>
    <message>
        <location filename="../src/sum_shared/CheckSumState.h" line="68"/>
        <source>Integrity data is verified</source>
        <translation>Integritätsdaten überprüft</translation>
    </message>
    <message>
        <location filename="../src/sum_shared/CheckSumState.h" line="69"/>
        <source>Integrity data error</source>
        <translation>Integritätsdaten fehlerhaft</translation>
    </message>
    <message>
        <location filename="../src/tasks/CheckForCheckSum.cpp" line="52"/>
        <source>Search integrity data</source>
        <translation>Suche Integritätsdaten</translation>
    </message>
    <message>
        <location filename="../src/tasks/CheckForCheckSum.cpp" line="54"/>
        <source>Compare integrity data</source>
        <translation>Vergleiche Integritätsdaten</translation>
    </message>
    <message>
        <location filename="../src/tasks/CheckForCheckSum.cpp" line="56"/>
        <source>Calculate integrity data</source>
        <translation>Berechne Integritätsdaten</translation>
    </message>
    <message>
        <source>Scan files: </source>
        <translation type="vanished">Suche Dateien: </translation>
    </message>
    <message>
        <location filename="../src/tasks/ScanFsTask.cpp" line="36"/>
        <source>Find files: </source>
        <translation>Suche nach Dateien: </translation>
    </message>
    <message>
        <location filename="../src/checksums/SumFileMd5Manager.cpp" line="154"/>
        <location filename="../src/checksums/SumFileShaManager.cpp" line="185"/>
        <source>Check sum path not found: </source>
        <translation>Pfad zur Checksumme nicht gefunden: </translation>
    </message>
    <message>
        <location filename="../src/checksums/SumFileMd5Manager.cpp" line="209"/>
        <location filename="../src/checksums/SumFileShaManager.cpp" line="242"/>
        <source>Separate output directory missing: </source>
        <translation>Separates Ausgabeverzeichnis nicht vorhanden: </translation>
    </message>
    <message>
        <location filename="../src/checksums/SumFileMd5Manager.cpp" line="235"/>
        <location filename="../src/checksums/SumFileShaManager.cpp" line="270"/>
        <source>Writing check sum failed in: </source>
        <translation>Schreiben der Checksummendatei ist fehlgeschlagen in: </translation>
    </message>
    <message>
        <location filename="../src/checksums/SumMd5Type.cpp" line="93"/>
        <location filename="../src/checksums/SumShaType.cpp" line="90"/>
        <source>Comparison failed, because check sum doesn&apos;t exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/checksums/SumMd5Type.cpp" line="115"/>
        <location filename="../src/checksums/SumShaType.cpp" line="112"/>
        <source>Comparison failed, because file doesn&apos;t exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/checksums/SumMd5Type.cpp" line="138"/>
        <location filename="../src/checksums/SumShaType.cpp" line="135"/>
        <source>Calculation failed, because file doesn&apos;t exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/checksums/SumMd5Type.cpp" line="169"/>
        <location filename="../src/checksums/SumShaType.cpp" line="166"/>
        <source>Calculation failed, because file isn&apos;t readable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/checksums/SumMd5Type.cpp" line="182"/>
        <location filename="../src/checksums/SumShaType.cpp" line="179"/>
        <source>Calculation failed, because algorithm failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/tasks/ReScanFsTask.cpp" line="45"/>
        <source>Re-scan files: </source>
        <translation>Wiederhole Dateisuche: </translation>
    </message>
    <message>
        <location filename="../src/tasks/ReScanFsTask.cpp" line="117"/>
        <location filename="../src/tasks/ScanFsTask.cpp" line="74"/>
        <source>Directory scan failed for: </source>
        <translation>Verzeichnissuche fehlgeschlagen für: </translation>
    </message>
    <message>
        <location filename="../src/console/cmds/Command.cpp" line="56"/>
        <source>This command is not executable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkerInterface</name>
    <message>
        <source>IntegrityChecker Setup</source>
        <translation type="obsolete">IntegrityChecker Einrichten</translation>
    </message>
</context>
<context>
    <name>WorkerSettings</name>
    <message>
        <location filename="../src/worker/WorkerSettings.cpp" line="157"/>
        <source>Input directory invalid: </source>
        <translation>Ungültiges Eingangsverzeichnis: </translation>
    </message>
    <message>
        <location filename="../src/worker/WorkerSettings.cpp" line="168"/>
        <source>Output directory invalid: </source>
        <translation>Ungültiges Ausgangsverzeichnis: </translation>
    </message>
    <message>
        <location filename="../src/worker/WorkerSettings.cpp" line="196"/>
        <source>Output directory not set</source>
        <translation>Ausgabeverzeichnis ist nicht gesetzt</translation>
    </message>
</context>
<context>
    <name>mainWnd</name>
    <message>
        <location filename="../src/view/MainWindow.ui" line="61"/>
        <source>Files</source>
        <translation>Dateien</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="244"/>
        <source>Show Details</source>
        <translation>Zeige Details</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="251"/>
        <source>Comapre stored checksum with current</source>
        <translation>Vergleiche Checksumme</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="254"/>
        <source>Compare Integrity Data</source>
        <oldsource>Check Integrity Data</oldsource>
        <translation>Vergleiche Intigritätsdaten</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="261"/>
        <source>Calculates a new check sum</source>
        <translation>Berechnet neue Checksumme</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="264"/>
        <source>Calculate Integrity Data</source>
        <translation>Berechne Integritätsdaten</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="284"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Cancel selected task</source>
        <translation type="obsolete">Breche selektierte Aufgabe ab</translation>
    </message>
    <message>
        <source>Cancel Task</source>
        <translation type="obsolete">Breche Aufgabe ab</translation>
    </message>
    <message>
        <source>Cancels all tasks</source>
        <translation type="obsolete">Bricht alle Aufgaben ab</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="348"/>
        <source>Cancel All Tasks</source>
        <translation>Breche all Aufgaben ab</translation>
    </message>
    <message>
        <source>Remove Selected Tasks</source>
        <oldsource>Remove Selected</oldsource>
        <translation type="obsolete">Entferne selektierte Aufgaben</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="385"/>
        <source>Remove Finished Tasks</source>
        <oldsource>Remove Finished</oldsource>
        <translation>Entferne beendete Aufgaben</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="392"/>
        <source>Remove All</source>
        <translation>Entferne alle</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="412"/>
        <source>Options</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="425"/>
        <source>Paths</source>
        <translation>Pfade</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="452"/>
        <source>Select the input root directory:</source>
        <oldsource>Select root input directory:</oldsource>
        <translation>Auswahl Ursprungspfad:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="497"/>
        <location filename="../src/view/MainWindow.ui" line="611"/>
        <source>Browse</source>
        <translation>Durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="522"/>
        <source>Select the integrity data output directory:</source>
        <oldsource>Select output directory:</oldsource>
        <translation>Auswahl Verzeichnis für die Integritätsdaten:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="576"/>
        <source>Same as input directory</source>
        <oldsource>As input directory</oldsource>
        <translation>Gleich wie Eingangsverzeichnis</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="811"/>
        <source>Calculation</source>
        <translation>Berechnung</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="832"/>
        <source>Maximum used parallel worker count:</source>
        <oldsource>Maximum used worker count:</oldsource>
        <translation>Maximal parallel ausgeführte Aufgaben:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="895"/>
        <source>Count of parallel worker threads</source>
        <translation>Anzahl der parallelen Bearbeitungsthreads</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="905"/>
        <source>Cores</source>
        <translation>Kerne</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="931"/>
        <source>Maximum used cache size:</source>
        <oldsource>Cache Size</oldsource>
        <translation>Maximal benutzte Cache Größe:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1001"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1059"/>
        <source>Current Profile:</source>
        <translation>Aktuelles Profil:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1125"/>
        <source>Optional profile description:</source>
        <translation>Optionale Profilbeschreibung:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1159"/>
        <source>Administration:</source>
        <translation>Verwaltung:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1202"/>
        <source>Delete Profile</source>
        <translation>Profil löschen</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1209"/>
        <location filename="../src/view/MainWindow.ui" line="1233"/>
        <source>to</source>
        <translation>zu</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1216"/>
        <source>Rename Profile</source>
        <translation>Profil umbenennen</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1223"/>
        <source>Copy Profile</source>
        <translation>Profil kopieren</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1370"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1363"/>
        <source>Qt Compile Version:</source>
        <translation>Qt Übersetzungsversion:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1411"/>
        <source>Qt Library Version:</source>
        <translation>Qt Bibliothekversion:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="641"/>
        <source>View</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="338"/>
        <source>Cancel Selected Task</source>
        <translation>Breche selektierte Aufgabe ab</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="378"/>
        <source>Remove Selected Task</source>
        <translation>Entferne selektierte Aufgabe</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="662"/>
        <source>Show this integrity types in view:</source>
        <oldsource>Show this summary types:</oldsource>
        <translation>Zeige diese Integritätstypen:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="722"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="774"/>
        <location filename="../src/view/MainWindow.ui" line="1102"/>
        <source>Change</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1038"/>
        <source>Profiles</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1314"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1326"/>
        <source>IntegrityChecker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1437"/>
        <source>Licenses</source>
        <translation>Lizenzen</translation>
    </message>
    <message>
        <location filename="../src/view/MainWindow.ui" line="1292"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
</context>
</TS>
