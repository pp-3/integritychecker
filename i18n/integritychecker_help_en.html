<!--
Copyright 2016 Peter Peters

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>IntegrityChecker</title>

	<style>
		body {
			background: white;
			counter-reset: h2;
		}
		h2 {
			counter-reset: h3;
		}
		h3 {
			counter-reset: h4;
		}
		h2:before { counter-increment: h2; content: counter(h2) " "; }
		h3:before { counter-increment: h3; content: counter(h2) "." counter(h3) " "; }
		h4:before { counter-increment: h4; content: counter(h2) "." counter(h3) "." counter(h4) " "; }
		code {
			display: block;
			background: #FFA;
			padding: 0.5em;
		}
	</style>
</head>

<body>
	<h1>IntegrityChecker</h1>

	<ul>
		<li><a href="#files">Files Tab</a></li>
		<li><a href="#status">Status Tab</a></li>
		<li><a href="#options">Options Tab</a></li>
	</ul>

	<h2>Overview</h2>
	<p>
	The IntegrityChecker provides an easy interface for creating integrity data (e.g. check sums) and use
	this data later on to verify files are unchanged / intact. Checking backed up data on a repeated basis
	for example may ensure the data is not altered by a yet undetected cause.
	</p>
	<p>
	The integrity data used for the checks are widely used and known check sums like MD5 or SHA. This data
	may also be generated and used interchangeable by other tools. During the initial setup the type of
	integrity data to show in the GUI has to be chosen. Only the selected types are shown and used for the
	data verification.
	</p>

	<h2 id="files">Files</h2>
	<p>
	During the initial setup a root directory has to be set. The "Files" tab shows only directories and files
	in the chosen root directory, in order to reduce the search time in the file system. By selecting a
	directory on the left hand side the files and sub directories are shown on the right hand side.
	</p>

	<h3 id="dirview">Directory View</h3>
	<p>
	The directory view on the left hand side shows an overview of the sub directory tree. Select a directory
	in this view to see all files and sub directories in the file view on the right side.
	</p>

	<h3 id="fileview">File View</h3>
	<p>
	The file view on the right hand side shows the content of the directory selected in the directory view
	on the left side. All sub directories and files in this directory are listed vertically. For each
	enabled data integrity type a separate column is shown in this list. The enabled integrity types may be
	changed in the "Options" tab.
	</p>
	<p>
	Each cell shows the current status for the file or directory and the according integrity data type. The
	status information is divided into four parts representing the four possible check states for a file: not available,
	untested, verified, and errored. For a file exactly one of this states applies. A directory shows the summary count
	for each state of the sub directories and files it contains.
	</p>
	<p>
	The first entry in each cell has no icon and represents the "not available" state. For a file this means there is no
	integrity data according to the type column. A directory shows the count of files with no integrity data in all sub
	directories.
	</p>
	<p>
	The second entry in each cell shows a circle icon and represents the state "untested". For a file this means the
	according integrity data exists, but the file was not yet checked whether the integrity data matches or not. A
	directory shows the count of files with available, but untested integrity data in the full sub tree.
	</p>
	<p>
	The third entry in each cell represents the "verified" state. This state is entered either via comparing a
	found integrity data with a fresh calculation and a match between both or via calculating and storing the
	integrity data, which automatically means the integrity data matches. Directories show the summary count of files in
	this state. The icon for this state is a check mark.
	</p>
	<p>
	The fourth entry, a symbolized red 'x', is shown when the comparison of the stored integrity data and a fresh calculation
	mismatches. This means, since the integrity data has been calculated and stored either the file or the integrity data
	has been altered.
	</p>

	<h3 id="sumcount">Summary Count</h3>
	<p>
	After start up all existing integrity data files are searched under the root directory, but no verification is done
	automatically, as it may be time consuming. After the user started the comparison of files with the related
	integrity data the summary count in the bottom left corner shows the summary count for succeeded and failed
	verification tasks.
	</p>

	<h3 id="calcbuttons">Actions</h3>
	<p>
	On the bottom of the "Files" tab several actions may be started by clicking the related button. The selected action
	is executed for the selected files or directories in the directory and file view. If a directory is selected the
	action is executed on all sub directories and files in this directory.
	</p>
	<p>
	The directory view on the left allows to select a single directory only. The selected action is executed for all
	files and directories in this directory. For each file the action is executed for all enabled integrity data types,
	as shown in file view.
	</p>
	<p>
	Adding a selection in the file view after having a directory selected in the directory view narrows the executed
	actions down to the selected files and integrity data types. Multiple selections are possible. Holding the CTRL key
	while selecting adds or removes to or from the current selection. This selection types are supported:
	</p>
	<ul>
		<li>Clicking into a single cell adds or removes the related file in combination with the related integrity
		data type to list of files to operate on.</li>
		<li>Clicking onto a row header on the left of the file name selects / deselects the full row. This means all
		shown integrity data types are added or removed for this one file to or from the list of files to operate on.</li>
		<li>Clicking onto a column header with the name of an integrity data type adds or removes this data type
		for all files and directories.</li>
		<li>Removing the last selection in the file view makes the last selection in the directory view the valid one
		again meaning all files and directories with all integrity data types are included when an action is executed.</li>
	</ul>
	<p>
	The "Show Details" button opens an additional window and shows the current state of the selected files and directories
	in a more detailed manner.
	</p>
	<p>
	The "Compare Integrity Data" button starts to verify the integrity data for all selected files and integrity data
	type combinations. After the verification finishes the state in the file view is updated accordingly, either to
	"verified" or "errored". For files where the selected data type doesn't exist the verification is skipped. The state
	in the file view stays in "not available". As the verification of the integrity data may take some time, the progress
	is shown in the "Status" tab. Also, the verification process may be canceled there.
	</p>
	<p>
	The "Calculate Integrity Data" starts the calculation of the integrity data for all selected files / directories and
	integrity data type combinations. The calculated integrity data is added to the according integrity data files, if it
	didn't exist, or it overwrites the existing data with the freshly calculated one.
	</p>

	<h2 id="status">Status</h2>
	The "Status" shows all the actions started in the past and their current state. For some lengthy operations a
	progress is shown until the execution is finished. At the bottom several buttons allow the user to interact with
	the running tasks or the window output:
	<ul>
		<li>The buttons on the left cancel running tasks. After a task is canceled it shown in the log view as canceled.</li>
		<li>The buttons on the right clean up the log window. They don't interact with running tasks.</li>
	</ul>
	<p>
	The log view adds one entry for each started task, e.g. a integrity data verification or calculation. Still running
	tasks may be canceled. If a task contains more than one file, e.g. verify all files in one directory, the task
	cancellation is done on a file granularity basis. That means, all files which have been already processed are kept
	in their new state and only not yet processed files are skipped.
	</p>

	<h3 id="log">Log View</h3>
	The log view shows the current state of each started task divided into several columns.
	<ul>
		<li>The "Task" column shows a short description of the task.</li>
		<li>The "Sub Tasks" shows the count of sub tasks the task is divided up. This are usually separate
		file / integrity data type combinations.</li>
		<li>The "Open Sub Tasks" shows the count of not yet processed sub tasks. During the task execution this count
		decreases to zero and thus gives a hint of the task progress. If a task is canceled only the still open
		sub tasks are canceled.</li>
		<li>The "Started" column shows the start time of the task. Sorting the full table for this column show the tasks
		in the order they have been started.</li>
		<li>The "State" column depicts the current task state, which may be "not started", "running", "finished",
		or "canceled".
	</ul>

	<h3 id="taskbuttons">Task Actions</h3>
	<p>
	The task actions on the bottom left allow to cancel running or not yet started tasks. "Cancel Task" cancels
	only the selected task in the log view. "Cancel All Tasks" stops all not yet finished tasks.
	</p>

	<h3 id="logbuttons">Log Actions</h3>
	The log actions on the bottom right clean up the log view. "Remove Selected Tasks" removes the currently selected task
	in the log view. "Remove Finished Tasks" removes all tasks from the log view that finished their executions.
	"Remove All" clears the log view. Neither of the log actions cancels a running tasks.

	<h2 id="options">Options</h2>
	<p>
	The "Options" tab shows all the settings which are used for comparing and calculating the integrity data. The required
	settings were requested after the first software start in a separate dialog. Other settings got a meaningful default value.
	</p>
	<p>
	The settings are grouped into separate topics which are shown in vertical sub tabs. All chosen values are stored in a
	config file and restored after the next software start.
	</p>
	
	<h3 id="paths">Paths</h3>
	The "Paths" sub tab shows the root paths in the file system to look for integrity files. This paths are shown in the
	"Files" tab and changing this values will discard all evaluated status data.

	<h4 id="inputpath">Input Path</h4>
	The input root directory sets the top directory in the file system to look for integrity data files. The directory view in
	the "Files" tab will only show files and directories in this directory and all sub directories, respectively. Selecting a
	specific directory will reduce the search time after start up and restrict the software to access only files in this
	directory.

	<h4 id="outputpath">Output Path</h4>
	<p>
	The output path sets the root directory for integrity data.
	</p>
	<p><b>Note:</b></p>
	<p>
	The standard tools for creating integrity data, e.g. MD5 or SHA store the integrity data in the same directory as the
	original file. Deviating from this behavior will break the compatibility with the standard tools. In any doubt select
	the "Same as input directory" setting here.
	</p>
	<h5>Same as input directory</h5>
	Selecting the first option looks for and creates the integrity data in the same directory as the original file. This is
	the standard behavior. If, for example, the root input directory is set to /a and there is a file f in a sub directory b
	<div><code>
	/a/b/f
	</code></div>
	the MD5 hash sum is stored in the MD5SUMS file in the original's file directory /a/b
	<div><code>
	/a/b/MD5SUMS
	</code></div>

	<h5>Separate output directory</h5>
	<p>
	The second options allows to set a different root directory for the integrity data in comparison to the root directory
	for the input files. This is non standard, but may be useful in some circumstances. E.g.:
	</p>
	<ul>
		<li>The input directory is read-only for the user, because of missing rights or security reasons. Writing the
		    integrity data to a different directory is the only option in this case.</li>
		<li>Separating the integrity data into a separate directory / file system by own choice may be useful, if the
		    separate directory has a different backup plan or just for the case to keep the input directories clean
			from the integrity data files.</li>
	</ul>
	If, for example, the input root is /a and a file f is in a sub directory b
	<div><code>
	/a/b/f
	</code></div>
	selecting a different root /z/myhashes for the output directory expects the MD5SUMS file for the MD5 hashes in
	<div><code>
	/z/myhashes/b/MD5SUMS
	</code></div>
	Thus, the sub hierarchy is kept as in the input directory. Only, the root directory is exchanged.

	<h3 id="view">View</h3>
	In the "View" sub tab the the way the main window looks like may be configured.

	<h4 id="types">Data Types</h4>
	The list of integrity data types shows all supported types. Selecting only the one of interest changes the
	software behavior twofold. First, in the file view on the "Files" tab only the selected data types are shown
	as separate columns. Second, if a data action (e.g. integrity data comparison) is started for a directory, the
	action is only executed for the enabled data types. Not selected data types are not shown, compared, nor calculated.

	<h4 id="language">Language</h4>
	The language all texts in the software's user interface are shown in may be changed here. Available are only
	languages with an existing translation file in the "i18n" sub directory.

	<h3 id="calculation">Calculation</h3>
	The "Calculation" sub tab allows to tweak the comparison / calculation of the integrity data.

	<h4 id="workercount">Worker Count</h4>
	The worker count sets the maximum count of parallel tasks. Especially, the count of files the integrity data is
	calculated for at the same time. To get the maximum performance out of the computer the count should be around
	the number of cores in the processor. A lower count reduces the load on the computer. A higher count is usually
	not helpful.

	<h4 id="cachesize">Cache Size</h4>
	With the maximum cache size (in MB) the amount of memory is set which may be used by the software for increasing
	the performance by caching often used data. Main usage is for files containing the integrity data.

	<h3 id="profiles">Profiles</h3>
	All the settings in the "Options" tab are stored in a settings file and are restored when the application
	is restarted. Different settings may be managed via profiles, with a profile being a distinct set of
	settings. Switching between profiles changes all settings to the values stored in the according profile.
	After the application starts the last chosen profile will be restored. The initial "---" profile is just
	a placeholder for a not yet named profile.

	<h4 id="changeprofile">Change Profile</h4>
	<p>
	If more than one settings profile is stored, the "Change" button offers to choose between them. Selecting
	another profile than the current one, will change all the settings and discard all the calculated status
	information for the old settings.
	</p>
	<p>
	The optional profile descripton is intended for a short summary of the current profile.
	</p>

	<h4 id="adminprofile">Manage Profiles</h4>
	<p>
	The "Administration" section offers to duplicate, rename, or delete profiles.
	</p>
	<p>
	With the "Copy Profile" button the current settings are copied to a new profile with the name provided
	in the "to" input field. The current profile is automatically changed to the new one, taking over all
	settings. Any change in the settings value will now stored in the new profile. The former one stays
	unchanged.
	</p>
	<p>
	The "Rename Profile" changes the name of the current settings profile to the one provided in the "to"
	input field. The setting values stay unchanged.
	</p>
	<p>
	The "Delete Profile" deletes the profile with the name set in the drop down box on the left. All values
	stored in the chosen profile are deleted. Deleting the current active profile will switch over to the
	default "---" profile. If no default profile is available, a new one is created with default values.
	</p>

	<h2 id="help">Help</h2>
	The Help tab shows a copy of this help. If a translation is available in the i18n sub folder the translated
	version is shown here. Also, the help file may be browsed in a Web Browser by opening the help file from the
	i18n sub folder.

	<h2 id="about">About</h2>
	The "About" sub tab shows some software information and the licenses of the IntegrityChecker software and
	the licenses of the used tools.
</body>

