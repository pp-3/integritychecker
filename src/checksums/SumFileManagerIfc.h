// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMFILEMANAGERIFC_H
#define SUMFILEMANAGERIFC_H

#include <QSharedPointer>

#include "sum_tree/FileInfo.h"

class CheckSumIfc;

/**
 * Base class / interface for all managers of a specific integrity data file type (e.g. MD5SUMS for MD5 check sums).
 * This class and all sub classes are intended to decouple a integrity data type ( SumTypeIfc ) from the storage
 * of the integrity data. The data type instances have be singletons, as they represent a specific data type. The
 * related file managers may be also singleton, but don't have to be.
 * The interface is very generic in order to support different storage types.
 */
class SumFileManagerIfc
{
public:
    /**
     * The possible return values of several methods in this interface:
     */
    enum CHECK_FILE_RET
    {
        FILE_FOUND,     ///< File found / successfully read or written. In general it means success.
        FILE_NOT_FOUND, ///< The provided integrity data file doesn't exist or is not readable.
        PARSING_ERROR,  ///< The integrity data file is readable, but it's content couldn't be parsed / understood.
        ACCESS_ERROR    ///< The provided information passed into a method doesn't make sense. In general any unspecific error.
    };

    virtual ~SumFileManagerIfc() = default;

    /**
     * Checks, if the provided file is itself an integrity data file for the specific data type.
     * @param file The file to look for.
     * @return True, if it's an integrity data file. False, otherwise.
     */
    virtual bool isCheckSumFile(const FileInfo::weak_ptr &file) const = 0;

    /**
     * Checks, whether the integrity data file related to the provided file contains integrity data for the given file.
     * @param file The file to look for.
     * @return Return enum. FILE_FOUND is the only value meaning the integrity data is readable in the integrity data file.
     */
    virtual SumFileManagerIfc::CHECK_FILE_RET hasCheckSum(const FileInfo::weak_ptr &file) = 0;
    /**
     * Reads the integrity data from the integrity data file related to the provided file.
     * @param file The file to read the integrity data for.
     * @return Pointer to a CheckSumIfc instance. The specific type is related to the integrity data. nullptr, if the integrity data doesn't exist or isn't readable. Any errors should be directly posted to the error collector.
     */
    virtual QSharedPointer<CheckSumIfc> readCheckSum(const FileInfo::weak_ptr &file) = 0;
    /**
     * Writing of integrity data to the integrity data file related to the provided file has to be done in two steps. First, with startCheckSumWrite() the writing has to be initiated. If it succeeds, it has to be finished with the endCheckSumWrite() call.
     * @param file The file to write the integrity data for.
     * @param checksum The integrity data instance to write to the integrity data file.
     * @return Return enum. FILE_FOUND is the only value meaning success. All others mean writing fails.
     */
    virtual SumFileManagerIfc::CHECK_FILE_RET startCheckSumWrite(const FileInfo::weak_ptr &file, const QSharedPointer<CheckSumIfc>& checksum) = 0;
    /**
     * After the startCheckSumWrite() succeeds the endCheckSumWrite() has to be called after all integrity data has been provided and the integrity data file may be closed.
     * @param file The file to write the integrity data for. Has to be the same as in startCheckSumWrite().
     * @return Return enum. FILE_FOUND is the only value meaning success. If startCheckSumWrite() succeeded, but endCheckSumWrite() failed, the integrity data file could be ended corrupted.
     */
    virtual SumFileManagerIfc::CHECK_FILE_RET endCheckSumWrite(const FileInfo::weak_ptr &file) = 0;

protected:
    SumFileManagerIfc() = default;
};

#endif // SUMFILEMANAGERIFC_H
