// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMFILEMD5MANAGER_H
#define SUMFILEMD5MANAGER_H

#include <QRegExp>
#include <QString>

#include "shared/DefaultDataCacheType.h"
#include "sum_storage/MemoryStoredCheckSum.h"

#include "SumFileManagerIfc.h"

class WorkerSettingsIfc;
class SumFileAccessLock;
class ErrorPostIfc;

/**
 * The SumFileMd5Manager class implements the SumFileManagerIfc interface for the MD5 integrity data type and the related MD5SUMS
 * integrity data files.
 * The CheckSumIfc instances are expected to be of the type MemoryStoredCheckSum.
 * The MD5SUMS files are read / written by the TwoColumnSumFile class.
 */
class SumFileMd5Manager : public SumFileManagerIfc
{
    friend class SumMd5Type;

public:
    /**
     * Constructor.
     * @param worker_settings The WorkerSettings instance to read the root settings from.
     * @param lock            The integrity data file access lock instance.
     * @param cache           The global cache for storing read MD5 check sums.
     * @param errors          The ErrorCollector interface.
     */
    SumFileMd5Manager(WorkerSettingsIfc    *worker_settings,
                      SumFileAccessLock    &lock,
                      DefaultDataCacheType *cache,
                      ErrorPostIfc         *errors);
    virtual ~SumFileMd5Manager() = default;

    bool isCheckSumFile(const FileInfo::weak_ptr &file) const override;

    SumFileManagerIfc::CHECK_FILE_RET hasCheckSum(const FileInfo::weak_ptr &file) override;
    QSharedPointer<CheckSumIfc> readCheckSum(const FileInfo::weak_ptr &file) override;
    SumFileManagerIfc::CHECK_FILE_RET startCheckSumWrite(const FileInfo::weak_ptr &file, const QSharedPointer<CheckSumIfc>& checksum) override;
    SumFileManagerIfc::CHECK_FILE_RET endCheckSumWrite(const FileInfo::weak_ptr &file) override;

protected:
    SumFileManagerIfc::CHECK_FILE_RET hasCheckSumInFile(const QString &checksum_directory, const QString &file_to_check);
    QSharedPointer<CheckSumIfc> readCheckSumInFile(const QString &checksum_directory, const QString &file_to_check);
    SumFileManagerIfc::CHECK_FILE_RET startCheckSumWriteInFile(const QString &checksum_directory, const QString &file_to_check, const MemoryStoredCheckSum& checksum);

private:
    WorkerSettingsIfc    *pWorkerSettings;
    DefaultDataCacheType *pDataCache;
    ErrorPostIfc         *theErrorCollector;
    SumFileAccessLock    &theFileLock;

    static const QString SUM_FILENAME;
    static const int     CHECKSUM_LEN;
    static const QRegExp FILE_SPARATOR;
    static const QString WRITE_SPARATOR;
};

#endif // SUMFILEMD5MANAGER_H
