// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumFileShaManager.h"

#include <QDir>
#include <QMutexLocker>

#include "ifc/ErrorPostIfc.h"
#include "ifc/WorkerSettingsIfc.h"
#include "shared/Tr.h"
#include "sum_storage/CheckSumIfc.h"
#include "sum_storage/SumFileAccessLock.h"
#include "sum_storage/TwoColumnSumFile.h"
#include "sum_tree/FileInfo.h"
#include "sum_tree/TreeUtil.h"


const QRegExp SumFileShaManager::FILE_SPARATOR(" [ | \\*]");
const QString SumFileShaManager::WRITE_SPARATOR("  ");

SumFileShaManager::SumFileShaManager(QCryptographicHash::Algorithm algorithm,
                                     WorkerSettingsIfc            *worker_settings,
                                     SumFileAccessLock            &lock,
                                     DefaultDataCacheType         *cache,
                                     ErrorPostIfc                 *errors) :
    SumFileManagerIfc(),
    theAlgorithm(algorithm),
    pWorkerSettings(worker_settings),
    pDataCache(cache),
    theErrorCollector(errors),
    theFileLock(lock)
{
}

QString SumFileShaManager::getFileNameForAlgorithm() const
{
    switch (theAlgorithm)
    {
    case QCryptographicHash::Sha1     : return "SHA1SUMS";
    case QCryptographicHash::Sha256   : return "SHA256SUMS";
    case QCryptographicHash::Sha512   : return "SHA512SUMS";
    case QCryptographicHash::Sha3_256 : return "SHA3_256SUMS";
    case QCryptographicHash::Sha3_512 : return "SHA3_512SUMS";
    default                           : Q_ASSERT(false); std::abort();
    }
}

int SumFileShaManager::getHashCharLenForAlgorithm() const
{
    switch (theAlgorithm)
    {
    case QCryptographicHash::Sha1     : return 40;
    case QCryptographicHash::Sha256   : return 64;
    case QCryptographicHash::Sha512   : return 128;
    case QCryptographicHash::Sha3_256 : return 64;
    case QCryptographicHash::Sha3_512 : return 128;
    default                           : Q_ASSERT(false); std::abort();
    }
}

bool SumFileShaManager::isCheckSumFile(const FileInfo::weak_ptr &file) const
{
    Q_ASSERT(!file.isNull());
    FileInfo::ptr fi = file.toStrongRef();

    const QString filename(getFileNameForAlgorithm());

    return !filename.isEmpty() && fi && fi->getName().compare(filename, Qt::CaseInsensitive)==0;
}

SumFileManagerIfc::CHECK_FILE_RET SumFileShaManager::hasCheckSum(const FileInfo::weak_ptr &file)
{
    Q_ASSERT(!file.isNull());

    const std::optional<QString> parent_sub_path = TreeUtil::getParentSubPath(file);
    if (!parent_sub_path)
    {
        return SumFileManagerIfc::ACCESS_ERROR;
    }

    QString filename_to_check;
    {
        FileInfo::ptr fi = file.toStrongRef();
        if (!fi)
            return SumFileManagerIfc::ACCESS_ERROR;

        filename_to_check = fi->getName();
    }

    const WorkerSettingsIfc::OUTPUT_SELECTION where = pWorkerSettings->getOutputDirSelection();

    switch (where)
    {
        case WorkerSettingsIfc::AS_INPUT:
        {
            QString d(QDir::cleanPath(pWorkerSettings->getInputDir()+"/"+parent_sub_path.value()));
            return hasCheckSumInFile(d, filename_to_check);
        }
        case WorkerSettingsIfc::OTHER_DIR:
        {
            QString d(QDir::cleanPath(pWorkerSettings->getOutputDir()+"/"+parent_sub_path.value()));
            return hasCheckSumInFile(d, filename_to_check);
        }
    }

    Q_ASSERT(false);
    std::abort();
}

SumFileManagerIfc::CHECK_FILE_RET SumFileShaManager::hasCheckSumInFile(const QString &checksum_directory, const QString &file_to_check)
{
    QDir d(checksum_directory);
    if (!d.exists() || !d.isReadable())
    {
        return SumFileManagerIfc::FILE_NOT_FOUND;
    }

    const QString filename(getFileNameForAlgorithm());
    TwoColumnSumFile t(d, filename, theFileLock, pDataCache, theErrorCollector, getHashCharLenForAlgorithm(), FILE_SPARATOR);
    if (!t.isValid())
    {
        return SumFileManagerIfc::PARSING_ERROR;
    }
    return t.readCheckSum(file_to_check).isEmpty() ?
                SumFileManagerIfc::FILE_NOT_FOUND :
                SumFileManagerIfc::FILE_FOUND;
}

QSharedPointer<CheckSumIfc> SumFileShaManager::readCheckSum(const FileInfo::weak_ptr &file)
{
    Q_ASSERT(!file.isNull());

    const std::optional<QString> parent_sub_path = TreeUtil::getParentSubPath(file);
    if (!parent_sub_path)
    {
        return QSharedPointer<CheckSumIfc>();
    }

    QString filename_to_check;
    {
        FileInfo::ptr fi = file.toStrongRef();
        if (!fi)
            return QSharedPointer<CheckSumIfc>();

        filename_to_check = fi->getName();
    }

    const WorkerSettingsIfc::OUTPUT_SELECTION where = pWorkerSettings->getOutputDirSelection();

    switch (where)
    {
        case WorkerSettingsIfc::AS_INPUT:
        {
            QString d(QDir::cleanPath(pWorkerSettings->getInputDir()+"/"+parent_sub_path.value()));
            return readCheckSumInFile(d, filename_to_check);
        }
        case WorkerSettingsIfc::OTHER_DIR:
        {
            QString d(QDir::cleanPath(pWorkerSettings->getOutputDir()+"/"+parent_sub_path.value()));
            return readCheckSumInFile(d, filename_to_check);
        }
    }

    Q_ASSERT(false);
    std::abort();
}

QSharedPointer<CheckSumIfc> SumFileShaManager::readCheckSumInFile(const QString &checksum_directory, const QString &file_to_check)
{
    QDir d(checksum_directory);
    if (!d.exists() || !d.isReadable())
    {
        theErrorCollector->addError(Tr::tr("Check sum path not found: ")+d.absolutePath());
        return QSharedPointer<CheckSumIfc>();
    }

    const QString filename(getFileNameForAlgorithm());
    TwoColumnSumFile t(d, filename, theFileLock, pDataCache, theErrorCollector, getHashCharLenForAlgorithm(), FILE_SPARATOR);
    if (!t.isValid())
    {
        return QSharedPointer<CheckSumIfc>();
    }
    const QByteArray b = t.readCheckSum(file_to_check);
    if (b.isEmpty())
    {
        return QSharedPointer<CheckSumIfc>();
    }
    return QSharedPointer<MemoryStoredCheckSum>::create(b);
}

SumFileManagerIfc::CHECK_FILE_RET SumFileShaManager::startCheckSumWrite(const FileInfo::weak_ptr &file, const QSharedPointer<CheckSumIfc> &checksum)
{
    Q_ASSERT(!file.isNull());

    const std::optional<QString> parent_sub_path = TreeUtil::getParentSubPath(file);
    if (!parent_sub_path)
    {
        return SumFileManagerIfc::ACCESS_ERROR;
    }

    QSharedPointer<MemoryStoredCheckSum> m = checksum.dynamicCast<MemoryStoredCheckSum>();
    if (m.isNull())
    {
        return SumFileManagerIfc::ACCESS_ERROR;
    }

    QString filename_to_check;
    {
        FileInfo::ptr fi = file.toStrongRef();
        if (!fi)
            return SumFileManagerIfc::ACCESS_ERROR;

        filename_to_check = fi->getName();
    }

    const WorkerSettingsIfc::OUTPUT_SELECTION where = pWorkerSettings->getOutputDirSelection();

    switch (where)
    {
        case WorkerSettingsIfc::AS_INPUT:
        {
            QString d(QDir::cleanPath(pWorkerSettings->getInputDir()+"/"+parent_sub_path.value()));
            return startCheckSumWriteInFile(d, filename_to_check, *m);
        }
        case WorkerSettingsIfc::OTHER_DIR:
        {
            QDir d(pWorkerSettings->getOutputDir());
            if (!d.exists() || !d.isReadable())
            {
                theErrorCollector->addError(Tr::tr("Separate output directory missing: ")+d.absolutePath());
                return SumFileManagerIfc::FILE_NOT_FOUND;
            }
            QString d2(QDir::cleanPath(pWorkerSettings->getOutputDir()+"/"+parent_sub_path.value()));
            return startCheckSumWriteInFile(d2, filename_to_check, *m);
        }
    }

    Q_ASSERT(false);
    std::abort();
}


SumFileManagerIfc::CHECK_FILE_RET SumFileShaManager::startCheckSumWriteInFile(const QString &checksum_directory,
                                                                              const QString &file_to_check,
                                                                              const MemoryStoredCheckSum& checksum)
{
    QDir d;
    d.setPath(checksum_directory);

    const QString filename(getFileNameForAlgorithm());
    TwoColumnSumFile t(d, filename, theFileLock, pDataCache, theErrorCollector, getHashCharLenForAlgorithm(), FILE_SPARATOR);
    if (t.writeCheckSum(file_to_check, checksum.toByteArray(), WRITE_SPARATOR))
    {
        return SumFileManagerIfc::FILE_FOUND;
    }
    else
    {
        theErrorCollector->addError(Tr::tr("Writing check sum failed in: ")+checksum_directory + " / " + filename);
        return SumFileManagerIfc::ACCESS_ERROR;
    }
}

SumFileManagerIfc::CHECK_FILE_RET SumFileShaManager::endCheckSumWrite(const FileInfo::weak_ptr &/*file*/)
{
    return SumFileManagerIfc::FILE_FOUND;
}
