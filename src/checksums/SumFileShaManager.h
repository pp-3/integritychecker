// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMFILESHA1MANAGER_H
#define SUMFILESHA1MANAGER_H

#include <QCryptographicHash>
#include <QString>

#include "shared/DefaultDataCacheType.h"
#include "sum_storage/MemoryStoredCheckSum.h"

#include "SumFileManagerIfc.h"

class WorkerSettingsIfc;
class SumFileAccessLock;
class ErrorPostIfc;

/**
 * The SumFileShaManager class implements the SumFileManagerIfc interface for the SHA* integrity data types and the related SHA*SUMS
 * integrity data files.
 * The CheckSumIfc instances are expected to be of the type MemoryStoredCheckSum.
 * The SHA*SUMS files are read / written by the TwoColumnSumFile class.
 */
class SumFileShaManager : public SumFileManagerIfc
{
    friend class SumShaType;

public:
    /**
     * Constructor.
     * @param algorithm       The SHA algorithm.
     * @param worker_settings The WorkerSettingsIfc instance to read the root settings (and other) from.
     * @param lock            The integrity data file access lock instance.
     * @param cache           The global cache for storing read MD5 check sums.
     * @param errors          The ErrorCollector interface.
     */
    SumFileShaManager(QCryptographicHash::Algorithm algorithm,
                      WorkerSettingsIfc            *worker_settings,
                      SumFileAccessLock            &lock,
                      DefaultDataCacheType         *cache,
                      ErrorPostIfc                 *errors);
    virtual ~SumFileShaManager() = default;

    bool isCheckSumFile(const FileInfo::weak_ptr &file) const override;

    SumFileManagerIfc::CHECK_FILE_RET hasCheckSum(const FileInfo::weak_ptr &file) override;
    QSharedPointer<CheckSumIfc> readCheckSum(const FileInfo::weak_ptr &file) override;
    SumFileManagerIfc::CHECK_FILE_RET startCheckSumWrite(const FileInfo::weak_ptr &file, const QSharedPointer<CheckSumIfc>& checksum) override;
    SumFileManagerIfc::CHECK_FILE_RET endCheckSumWrite(const FileInfo::weak_ptr &file) override;

protected:
    QString getFileNameForAlgorithm() const;
    int getHashCharLenForAlgorithm() const;
    SumFileManagerIfc::CHECK_FILE_RET hasCheckSumInFile(const QString &checksum_directory, const QString &file_to_check);
    QSharedPointer<CheckSumIfc> readCheckSumInFile(const QString &checksum_directory, const QString &file_to_check);
    SumFileManagerIfc::CHECK_FILE_RET startCheckSumWriteInFile(const QString &checksum_directory, const QString &file_to_check, const MemoryStoredCheckSum& checksum);

private:
    static const QRegExp FILE_SPARATOR;
    static const QString WRITE_SPARATOR;

    QCryptographicHash::Algorithm theAlgorithm;
    WorkerSettingsIfc            *pWorkerSettings;
    DefaultDataCacheType         *pDataCache;
    ErrorPostIfc                 *theErrorCollector;
    SumFileAccessLock            &theFileLock;
};

#endif // SUMFILESHA1MANAGER_H
