// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumMd5Type.h"

#include <QCryptographicHash>
#include <QDir>

#include <sum_storage/MemoryStoredCheckSum.h>
#include <sum_tree/DirInfoRoot.h>
#include <sum_tree/FileInfo.h>
#include <sum_tree/InfoBase.h>
#include <sum_tree/InfoParent.h>

#include "SumFileMd5Manager.h"
#include "SumTypeManager.h"

static const char *MODULE_NAME = "MD5";

SumMd5Type::SumMd5Type() :
    SumTypeIfc()
{
}

void SumMd5Type::createInstance()
{
    static SumMd5Type instance;
}

SumTypeHash SumMd5Type::getTypeHash() const
{
    return (SumTypeHash) this;
}

QString SumMd5Type::getName() const
{
    return QString(MODULE_NAME);
}

QSharedPointer<SumTypeCalculatorIfc> SumMd5Type::createCalculator(const QString &input_path, const FileInfo::weak_ptr &file) const
{
    return QSharedPointer<SumTypeCalculatorIfc>(
        new SumMd5Calculator(input_path,
                             file,
                             getSumFileManager(),
                             SumTypeManager::getInstance().getErrorCollector()));
}

QSharedPointer<SumFileManagerIfc> SumMd5Type::getSumFileManager() const
{
    return QSharedPointer<SumFileManagerIfc>(
        new SumFileMd5Manager(SumTypeManager::getInstance().getSettings(),
                              SumTypeManager::getInstance().getFileLock(),
                              SumTypeManager::getInstance().getDataCache(),
                              SumTypeManager::getInstance().getErrorCollector()));
}







SumMd5Calculator::SumMd5Calculator(const QString                           &input_path,
                                   const FileInfo::weak_ptr                &file,
                                   const QSharedPointer<SumFileManagerIfc> &sum_file,
                                         ErrorPostIfc                      *errors) :
    SumTypeCalculatorIfc(input_path, file, sum_file),
    theErrorCollector(errors)
{
}

bool SumMd5Calculator::existsCheckSum() const
{
    return theSumFileManager->hasCheckSum(pFile) == SumFileManagerIfc::FILE_FOUND;
}

bool SumMd5Calculator::compareCheckSum() const
{
    if (!existsCheckSum())
    {
        theErrorCollector->addError(Tr::tr("Comparison failed, because check sum doesn't exist"), MODULE_NAME);
        return false;
    }

    QSharedPointer<CheckSumIfc> stored = theSumFileManager->readCheckSum(pFile);
    if (stored.isNull())
    {
        return false;
    }

    QString filepath_to_check;
    {
        FileInfo::ptr fi = pFile.toStrongRef();
        if (!fi)
            return false;

        filepath_to_check = theInputPath + "/" + fi->getSubPath();
    }

    QFile f(filepath_to_check);
    if (!f.exists())
    {
        theErrorCollector->addError(Tr::tr("Comparison failed, because file doesn't exist"), MODULE_NAME);
        return false;
    }

    MemoryStoredCheckSum actual(calculateSum(f, theErrorCollector));

    return stored->compare(actual);
}

bool SumMd5Calculator::createCheckSum(bool addToCheckSumFile)
{
    QString filepath_to_check;
    {
        FileInfo::ptr fi = pFile.toStrongRef();
        if (!fi)
            return false;

        filepath_to_check = theInputPath + "/" + fi->getSubPath();
    }

    QFile f(filepath_to_check);
    if (!f.exists())
    {
        theErrorCollector->addWarning(Tr::tr("Calculation failed, because file doesn't exist"), MODULE_NAME);
        return false;
    }

    QSharedPointer<CheckSumIfc> actual(new MemoryStoredCheckSum(calculateSum(f, theErrorCollector)));

    if (addToCheckSumFile)
    {
        if (theSumFileManager->startCheckSumWrite(pFile, actual) != SumFileManagerIfc::FILE_FOUND)
        {
            return false;
        }
        if (theSumFileManager->endCheckSumWrite(pFile) != SumFileManagerIfc::FILE_FOUND)
        {
            return false;
        }
    }

    return true;
}

QByteArray SumMd5Calculator::calculateSum(const QFile &file, ErrorPostIfc *errors)
{
    if (!file.exists())
    {
        return QByteArray();
    }

    QFile f(file.fileName());
    if (!f.open(QFile::ReadOnly))
    {
        errors->addError(Tr::tr("Calculation failed, because file isn't readable"), MODULE_NAME);
        return QByteArray();
    }

    QByteArray ret;

    QCryptographicHash h(QCryptographicHash::Md5);
    if (h.addData(&f))
    {
        ret = h.result();
    }
    else
    {
        errors->addError(Tr::tr("Calculation failed, because algorithm failed"), MODULE_NAME);
    }

    f.close();
    return ret;
}
