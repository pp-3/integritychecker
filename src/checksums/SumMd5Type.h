// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMMD5TYPE_H
#define SUMMD5TYPE_H

#include <QByteArray>
#include <QFile>
#include <QString>

#include "ifc/ErrorPostIfc.h"
#include "sum_shared/CheckSumState.h"

#include "SumTypeCalculatorIfc.h"
#include "SumTypeIfc.h"

/**
 * The SumMd5Type class represents the MD5 checksum.
 */
class SumMd5Type : public SumTypeIfc
{
public:
    virtual ~SumMd5Type() override = default;

    static void createInstance();

    SumTypeHash getTypeHash() const override;
    QString getName() const override;
    QSharedPointer<SumTypeCalculatorIfc> createCalculator(const QString            &input_path,
                                                          const FileInfo::weak_ptr &file) const override;
    QSharedPointer<SumFileManagerIfc> getSumFileManager() const override;

private:
    SumMd5Type();
};


/**
 * The SumMd5Calculator class implements the calculation of MD5 hashes.
 */
class SumMd5Calculator : public SumTypeCalculatorIfc
{
public:
    /**
     * Constructor.
     * @param input_path The root path for the file the integrity data is calculated for.
     * @param file The file the integrity data is calculated for.
     * @param sum_file The integrity data file manager instance for reading / writing the integrity data.
     * @param errors The ErrorCollector interface.
     */
    explicit SumMd5Calculator(const QString                           &input_path,
                              const FileInfo::weak_ptr                &file,
                              const QSharedPointer<SumFileManagerIfc> &sum_file,
                                    ErrorPostIfc                      *errors);
    virtual ~SumMd5Calculator() override = default;

protected:
    bool existsCheckSum() const override;
    bool compareCheckSum() const override;
    bool createCheckSum(bool addToCheckSumFile) override;

    static QByteArray calculateSum(const QFile &file, ErrorPostIfc *errors);

private:
    ErrorPostIfc *theErrorCollector;
};

#endif // SUMMD5TYPE_H
