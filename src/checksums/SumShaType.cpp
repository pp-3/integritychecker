// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumShaType.h"

#include "sum_storage/MemoryStoredCheckSum.h"
#include "sum_tree/FileInfo.h"

#include "SumFileShaManager.h"
#include "SumTypeManager.h"

static const char *MODULE_NAME = "SHA";

void createShaInstances()
{
    SumSha1Type::createInstance();
    SumSha256Type::createInstance();
    SumSha512Type::createInstance();
    SumSha3_256Type::createInstance();
    SumSha3_512Type::createInstance();
}

SumShaType::SumShaType() :
    SumTypeIfc()
{
}

SumTypeHash SumShaType::getTypeHash() const
{
    return (SumTypeHash) this;
}

QSharedPointer<SumTypeCalculatorIfc> SumShaType::createCalculator(const QString            &input_path,
                                                                  const FileInfo::weak_ptr &file) const
{
    return QSharedPointer<SumTypeCalculatorIfc>(
        new SumShaCalculator(input_path,
                             file,
                             getSumFileManager(),
                             getAlgorithm(),
                             SumTypeManager::getInstance().getErrorCollector()));
}

QSharedPointer<SumFileManagerIfc> SumShaType::getSumFileManager() const
{
    return QSharedPointer<SumFileManagerIfc>(
        new SumFileShaManager(getAlgorithm(),
                              SumTypeManager::getInstance().getSettings(),
                              SumTypeManager::getInstance().getFileLock(),
                              SumTypeManager::getInstance().getDataCache(),
                              SumTypeManager::getInstance().getErrorCollector()));
}






SumShaCalculator::SumShaCalculator(const QString                           &input_path,
                                   const FileInfo::weak_ptr                &file,
                                   const QSharedPointer<SumFileManagerIfc> &sum_file,
                                         QCryptographicHash::Algorithm      algorithm,
                                         ErrorPostIfc                      *errors) :
    SumTypeCalculatorIfc(input_path, file, sum_file),
    theAlgorithm(algorithm),
    theErrorCollector(errors)
{
}

bool SumShaCalculator::existsCheckSum() const
{
    return theSumFileManager->hasCheckSum(pFile) == SumFileManagerIfc::FILE_FOUND;
}

bool SumShaCalculator::compareCheckSum() const
{
    if (!existsCheckSum())
    {
        theErrorCollector->addError(Tr::tr("Comparison failed, because check sum doesn't exist"), MODULE_NAME);
        return false;
    }

    QSharedPointer<CheckSumIfc> stored = theSumFileManager->readCheckSum(pFile);
    if (stored.isNull())
    {
        return false;
    }

    QString filepath_to_check;
    {
        FileInfo::ptr fi = pFile.toStrongRef();
        if (!fi)
            return false;

        filepath_to_check = theInputPath + "/" + fi->getSubPath();
    }

    QFile f(filepath_to_check);
    if (!f.exists())
    {
        theErrorCollector->addError(Tr::tr("Comparison failed, because file doesn't exist"), MODULE_NAME);
        return false;
    }

    MemoryStoredCheckSum actual(calculateSum(f, theAlgorithm, theErrorCollector));

    return stored->compare(actual);
}

bool SumShaCalculator::createCheckSum(bool addToCheckSumFile)
{
    QString filepath_to_check;
    {
        FileInfo::ptr fi = pFile.toStrongRef();
        if (!fi)
            return false;

        filepath_to_check = theInputPath + "/" + fi->getSubPath();
    }

    QFile f(filepath_to_check);
    if (!f.exists())
    {
        theErrorCollector->addWarning(Tr::tr("Calculation failed, because file doesn't exist"), MODULE_NAME);
        return false;
    }

    QSharedPointer<CheckSumIfc> actual(new MemoryStoredCheckSum(calculateSum(f, theAlgorithm, theErrorCollector)));

    if (addToCheckSumFile)
    {
        if (theSumFileManager->startCheckSumWrite(pFile, actual) != SumFileManagerIfc::FILE_FOUND)
        {
            return false;
        }
        if (theSumFileManager->endCheckSumWrite(pFile) != SumFileManagerIfc::FILE_FOUND)
        {
            return false;
        }
    }

    return true;
}

QByteArray SumShaCalculator::calculateSum(const QFile &file, QCryptographicHash::Algorithm algorithm, ErrorPostIfc *errors)
{
    if (!file.exists())
    {
        return QByteArray();
    }

    QFile f(file.fileName());
    if (!f.open(QFile::ReadOnly))
    {
        errors->addError(Tr::tr("Calculation failed, because file isn't readable"), MODULE_NAME);
        return QByteArray();
    }

    QByteArray ret;

    QCryptographicHash h(algorithm);
    if (h.addData(&f))
    {
        ret = h.result();
    }
    else
    {
        errors->addError(Tr::tr("Calculation failed, because algorithm failed"), MODULE_NAME);
    }

    f.close();
    return ret;
}

void SumSha1Type::createInstance()
{
  static SumSha1Type instance;
} 
void SumSha256Type::createInstance()
{
  static SumSha256Type instance;
} 
void SumSha512Type::createInstance()
{
  static SumSha512Type instance;
} 
void SumSha3_256Type::createInstance()
{
  static SumSha3_256Type instance;
} 
void SumSha3_512Type::createInstance()
{
  static SumSha3_512Type instance;
} 
