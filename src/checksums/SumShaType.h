// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMSHA1TYPE_H
#define SUMSHA1TYPE_H

#include <QByteArray>
#include <QCryptographicHash>
#include <QFile>
#include <QSharedPointer>

#include "ifc/ErrorPostIfc.h"
#include "sum_shared/CheckSumState.h"

#include "SumTypeCalculatorIfc.h"
#include "SumTypeIfc.h"

void createShaInstances();

/**
 * The SumShaType class is the base class for all classes representing SHA hashes.
 */
class SumShaType : public SumTypeIfc
{
public:
    virtual ~SumShaType() override = default;

    SumTypeHash getTypeHash() const override;
    QSharedPointer<SumTypeCalculatorIfc> createCalculator(const QString            &input_path,
                                                          const FileInfo::weak_ptr &file) const override;
    QSharedPointer<SumFileManagerIfc> getSumFileManager() const override;

protected:
    SumShaType();
    virtual QCryptographicHash::Algorithm getAlgorithm() const = 0;
};

/**
 * The SumSha1Type class represents the SHA1 hash.
 */
class SumSha1Type : public SumShaType
{
public:
    virtual ~SumSha1Type() override = default;
    static void createInstance();
protected:
    QString getName()                            const override { return "SHA1"; }
    QCryptographicHash::Algorithm getAlgorithm() const override { return QCryptographicHash::Sha1; }
private:
    SumSha1Type() = default;
};

/**
 * The SumSha256Type class represents the SHA2_256 hash.
 */
class SumSha256Type : public SumShaType
{
public:
    virtual ~SumSha256Type() override = default;
    static void createInstance();
protected:
    QString getName()                            const override { return "SHA256"; }
    QCryptographicHash::Algorithm getAlgorithm() const override { return QCryptographicHash::Sha256; }
private:
    SumSha256Type() = default;
};

/**
 * The SumSha512Type class represents the SHA2_512 hash.
 */
class SumSha512Type : public SumShaType
{
public:
    virtual ~SumSha512Type() override = default;
    static void createInstance();
protected:
    QString getName()                            const override { return "SHA512"; }
    QCryptographicHash::Algorithm getAlgorithm() const override { return QCryptographicHash::Sha512; }
private:
    SumSha512Type() = default;
};

/**
 * The SumSha3_256Type class represents the SHA3_256 hash.
 */
class SumSha3_256Type : public SumShaType
{
public:
    virtual ~SumSha3_256Type() override = default;
    static void createInstance();
protected:
    QString getName()                            const override { return "SHA3_256"; }
    QCryptographicHash::Algorithm getAlgorithm() const override { return QCryptographicHash::Sha3_256; }
private:
    SumSha3_256Type() = default;
};

/**
 * The SumSha3_512Type class represents the SHA3_512 hash.
 */
class SumSha3_512Type : public SumShaType
{
public:
    virtual ~SumSha3_512Type() override = default;
    static void createInstance();
protected:
    QString getName()                            const override { return "SHA3_512"; }
    QCryptographicHash::Algorithm getAlgorithm() const override { return QCryptographicHash::Sha3_512; }
private:
    SumSha3_512Type() = default;
};



/**
 * The SumShaCalculator class implements the calculation of SHA hashes.
 */
class SumShaCalculator : public SumTypeCalculatorIfc
{
public:
    /**
     * Constructor.
     * @param input_path The root path for the file the integrity data is calculated for.
     * @param file The file the integrity data is calculated for.
     * @param sum_file The integrity data file manager instance for reading / writing the integrity data.
     * @param algorithm The SHA algorithm to use for the calculation.
     * @param errors The ErrorCollector interface.
     */
    explicit SumShaCalculator(const QString                           &input_path,
                              const FileInfo::weak_ptr                &file,
                              const QSharedPointer<SumFileManagerIfc> &sum_file,
                                    QCryptographicHash::Algorithm      algorithm,
                                    ErrorPostIfc                      *errors);
    virtual ~SumShaCalculator() override = default;

protected:
    bool existsCheckSum() const override;
    bool compareCheckSum() const override;
    bool createCheckSum(bool addToCheckSumFile) override;

    static QByteArray calculateSum(const QFile &file, QCryptographicHash::Algorithm algorithm, ErrorPostIfc *errors);

protected:
    QCryptographicHash::Algorithm theAlgorithm;

private:
    ErrorPostIfc *theErrorCollector;
};

#endif // SUMSHA1TYPE_H
