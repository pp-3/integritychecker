// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumTypeCalculatorIfc.h"

#include "SumFileManagerIfc.h"

SumTypeCalculatorIfc::SumTypeCalculatorIfc(const QString                           &input_path,
                                           const FileInfo::weak_ptr                &file,
                                           const QSharedPointer<SumFileManagerIfc> &sum_file) :
    theInputPath(input_path),
    pFile(file),
    theSumFileManager(sum_file)
{
    Q_ASSERT(!input_path.isEmpty());
    Q_ASSERT(file);
    Q_ASSERT(!theSumFileManager.isNull());
}
