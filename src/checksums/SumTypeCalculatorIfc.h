// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMTYPECALCULATORIFC_H
#define SUMTYPECALCULATORIFC_H

#include <QSharedPointer>
#include <QString>

#include <sum_tree/FileInfo.h>

class SumFileManagerIfc;

/**
 * Interface for all integrity data calculation implementations.
 */
class SumTypeCalculatorIfc
{
public:
    virtual ~SumTypeCalculatorIfc() = default;

    /**
     * Convenience method. Queries the integrity data file manager for the existence of integrity data for the provided file.
     * @return True, if integrity data exists in the file system. False, if not.
     */
    virtual bool existsCheckSum() const = 0;
    /**
     * Calculates the integrity data for the given file and compares it to the already stored integrity data (provided from the integrity data file manager).
     * @return True, if the comparison found matching integrity data. False, otherwise.
     */
    virtual bool compareCheckSum() const = 0;
    /**
     * Creates the integrity data for the given file and stores it via the provided integrity data file manager, if required.
     * @param addToCheckSumFile If true, the calculated integrity data is written to the integrity data file manager. If false, it's not stored (dry run).
     * @return True, if successful. False, otherwise.
     */
    virtual bool createCheckSum(bool addToCheckSumFile) = 0;

protected:
    /**
     * Constructor.
     * @param input_path The root path for the file the integrity data is calculated for.
     * @param file The file the integrity data is calculated for.
     * @param sum_file The integrity data file manager instance for reading / writing the integrity data.
     */
    SumTypeCalculatorIfc(const QString                           &input_path,
                         const FileInfo::weak_ptr                &file,
                         const QSharedPointer<SumFileManagerIfc> &sum_file);

protected:
    QString                           theInputPath;
    FileInfo::weak_ptr                pFile;
    QSharedPointer<SumFileManagerIfc> theSumFileManager;
};

#endif // SUMTYPECALCULATORIFC_H
