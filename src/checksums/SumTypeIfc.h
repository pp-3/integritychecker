// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMTYPEIFC_H
#define SUMTYPEIFC_H

#include <QSharedPointer>
#include <QString>

#include <sum_shared/CheckSumState.h>

#include "SumFileManagerIfc.h"
#include "SumTypeCalculatorIfc.h"

/**
 * Base class for integrity data types. Sub classes should be created statically and exactly once, as they are automatically
 * registered at the SumTypeManager. All information shown in the GUI and all information needed to create and compare
 * integrity data is drawn from this instances. They have to be multi threading safe.
 */
class SumTypeIfc
{
public:
    virtual ~SumTypeIfc() = default;

    /**
     * Each integrity data type has to provide an unique hash value. It's used for storing or passing through the computation without the need to handle pointers. The SumTypeManager will return the real instance for a given hash, if needed.
     * @return The unique hash for this integrity data type instance.
     */
    virtual SumTypeHash getTypeHash() const = 0;
    /**
     * Returns the name of the integrity data type this instance relates to.
     * @return The integrity data type name.
     */
    virtual QString getName() const = 0;
    /**
     * Returns the calculator instance intended for creating and comparing integrity data of the integrity data type this instance relates to.
     * @param input_path The input root path of file this computation instance is related to.
     * @param file The file to calculate the integrity data for.
     * @return The calculation instance.
     */
    virtual QSharedPointer<SumTypeCalculatorIfc> createCalculator(const QString &input_path, const FileInfo::weak_ptr &file) const = 0;
    /**
     * Returns the integrity data files manager for the integrity data type this instance relates to.
     * @return The integrity data files manager.
     */
    virtual QSharedPointer<SumFileManagerIfc> getSumFileManager() const = 0;

protected:
    SumTypeIfc();

private:
    Q_DISABLE_COPY(SumTypeIfc)
};

#endif // SUMTYPEIFC_H
