// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumTypeManager.h"

#include "shared/ExitManager.h"

std::unique_ptr<SumTypeManager> SumTypeManager::m_pInstance;


SumTypeManager::SumTypeManager() :
    pWorkerSettings(nullptr),
    pDataCache(nullptr),
    m_Types()
{
    ExitManager::getInstance().addExitListener(
        []()
        {
            m_pInstance.reset();
        });

    StaticInitializer::updateMe(static_cast<WorkerSettingsUpdate *>(this));
    StaticInitializer::udpateMe(static_cast<DataCacheUpdate *>(this));
    StaticInitializer::updateMe(static_cast<ErrorCollectorUpdate *>(this));
}

SumTypeManager &SumTypeManager::getInstance()
{
    if (!m_pInstance)
    {
        m_pInstance.reset(new SumTypeManager);
    }

    return *m_pInstance;
}

void SumTypeManager::addSumType(SumTypeIfc *pType)
{
    Q_ASSERT(pType);
    m_Types.append(pType);
}

const QList<SumTypeIfc *> &SumTypeManager::getRegisteredTypes() const
{
    return m_Types;
}

SumTypeHashList SumTypeManager::getRegisteredHashes() const
{
    SumTypeHashList ret;

    for (SumTypeIfc *it : m_Types)
    {
        ret << it->getTypeHash();
    }

    return ret;
}

SumTypeIfc *SumTypeManager::fromName(const QString &name) const
{
    for (SumTypeIfc *it : m_Types)
    {
        if (it->getName().compare(name, Qt::CaseInsensitive) == 0)
        {
            return it;
        }
    }

    return nullptr;
}

SumTypeIfc *SumTypeManager::fromHash(const SumTypeHash &hash) const
{
    for (SumTypeIfc *it : m_Types)
    {
        if (it->getTypeHash() == hash)
        {
            return it;
        }
    }

    return nullptr;
}
