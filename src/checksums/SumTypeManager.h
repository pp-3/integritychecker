// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMTYPEMANAGER_H
#define SUMTYPEMANAGER_H

#include <memory>

#include <QList>
#include <QSet>

#include "shared/DefaultDataCacheType.h"
#include "sum_shared/CheckSumState.h"
#include "sum_storage/SumFileAccessLock.h"
#include "worker/StaticInitializer.h"

#include "SumTypeIfc.h"

class WorkerSettingsIfc;
class ErrorPostIfc;

/**
 * The SumTypeManager class keeps track of all available integrity data types. In order to ensure all types are available
 * during the application start up, the initialization has to follow a strict order:
 * - Static initialization:
 *   - All data types should be created as static instance. The SumTypeIfc base class registers them here. Don't access this instance in any other way in the constructor.
 * - Run time initialization:
 *   - During the application startup all the internal references are updated.
 * - Run time
 *   - After the application started the integrity data type instances may access all the getter methods in this manager. Thus, once the data instances are accessed from the application, it is safe to access the SumTypeManager.
 */
class SumTypeManager : public WorkerSettingsUpdate,
                       public DataCacheUpdate,
                       public ErrorCollectorUpdate
{
public:
    /**
     * Returns the single instance. Pointer is valid until process ends.
     * @return The SumTypeManager.
     */
    static SumTypeManager& getInstance();
    virtual ~SumTypeManager() override = default;

    WorkerSettingsIfc    *getSettings()       const { return pWorkerSettings; }
    SumFileAccessLock    &getFileLock()             { return SumFileAccessLock::getInstance(); }
    DefaultDataCacheType *getDataCache()      const { return pDataCache; }
    ErrorPostIfc         *getErrorCollector() const { return theErrorCollector; }

    void addSumType(SumTypeIfc *pType);
    const QList<SumTypeIfc *> &getRegisteredTypes() const;
    SumTypeHashList getRegisteredHashes() const;
    SumTypeIfc *fromName(const QString &name) const;
    SumTypeIfc *fromHash(const SumTypeHash &hash) const;

protected:
    void workerSettingsUpdate(WorkerSettingsIfc *w) override { pWorkerSettings = w; }
    void dataCacheUpdate(DefaultDataCacheType *d) override   { pDataCache = d; }
    void errorCollectorUpdate(ErrorPostIfc *e) override      { theErrorCollector = e; }

private:
    Q_DISABLE_COPY(SumTypeManager)

    SumTypeManager();

private:
    static std::unique_ptr<SumTypeManager> m_pInstance;
    WorkerSettingsIfc    *pWorkerSettings;
    DefaultDataCacheType *pDataCache;
    ErrorPostIfc         *theErrorCollector;

    QList<SumTypeIfc *> m_Types;
};

#endif // SUMTYPEMANAGER_H
