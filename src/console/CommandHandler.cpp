// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "CommandHandler.h"

#include <QMutexLocker>

#include "Version.h"

#include "ConsoleModel.h"
#include "ConsoleUtils.h"
#include "ifc/CommandHandlerConstants.h"

CommandHandler::CommandHandler(const CommandFactory &cmd_factory, ConsoleModel *console_model) :
    theCommands(cmd_factory.getCommands()),
    theConsoleModel(console_model)
{
}

CommandHandler::~CommandHandler()
{
    CommandHandler::close();
}

void CommandHandler::processCommand(const QString &command_line)
{
    QMutexLocker ml(&m_Lock);

    if (!command_line.isEmpty())
    {
        QStringList command_list = ConsoleUtils::parseCommandLine(command_line);

        if (!command_list.isEmpty())
        {
            const QString cmd = command_list.takeFirst();
            if (theCommands.contains(cmd))
            {
                QString next_cmd = cmd;

                QSharedPointer<CommandIfc> c = theCommands.value(cmd);
                while (!c.isNull() && c->hasSubCommands() && !command_list.isEmpty())
                {
                    next_cmd = command_list.takeFirst();
                    c = c->getSubCommand(next_cmd);
                }

                if (!c.isNull())
                {
                    Q_ASSERT(theOutput);
                    if (!c->execute(command_list, theOutput))
                    {
                        sendResponse(CommandHandlerConstants::GENERAL_ERROR);
                    }
                }
                else
                {
                    sendResponse(CommandHandlerConstants::UNKNOWN_SUBCOMMAND + next_cmd);
                }
            }
            else
            {
                sendResponse(CommandHandlerConstants::UNKNOWN_COMMAND + cmd);
            }
        }
    }

    writePrompt();
}

void CommandHandler::setResponseIfc(const CommandResponseFunction &callback)
{
    QMutexLocker ml(&m_Lock);

    theOutput = callback;

    writeHeader();
}

void CommandHandler::setQueryIfc(const CommandResponseFunction &callback)
{
    QMutexLocker ml(&m_Lock);

    theQueryIfc = callback;

    writePrompt();
}

void CommandHandler::close()
{
    QMutexLocker ml(&m_Lock);

    sendResponse(CommandHandlerConstants::QUIT_RESPONSE);
    theQueryIfc = {};
    theOutput = {};
    theCommands.clear();
}

void CommandHandler::sendResponse(const QString &response) const
{
    if (theOutput)
        theOutput(response);
}

void CommandHandler::writeHeader() const
{
    QString title(CommandHandlerConstants::CMDLINE_TITLE);
    title += Version::getStrVersion();
    sendResponse(title);

    sendResponse(CommandHandlerConstants::CMDLINE_HELP);
    sendResponse(QString());
}

void CommandHandler::writePrompt() const
{
    if (theQueryIfc)
        theQueryIfc(theConsoleModel->pwd() + CommandHandlerConstants::CMDLINE_PROMPT);
}
