// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMMANDHANDLER_H
#define COMMANDHANDLER_H

#include <QMutex>
#include <QSharedPointer>
#include <QString>

#include "cmds/CommandFactory.h"
#include "ifc/CommandHandlerIfc.h"
#include "ifc/CommandIfc.h"

class ConsoleModel;

/**
 * @brief The CommandHandler class translates an incoming command line into the related command from the command
 * tree and writes out the command's responses to the CommandResponseIfc.
 */
class CommandHandler : public CommandHandlerIfc
{
public:
    /**
     * @param cmd_factory The factory to get the command tree from.
     * @param console_model The console model to operate the commands on.
     */
    CommandHandler(const CommandFactory &cmd_factory, ConsoleModel *console_model);
    ~CommandHandler() override;

protected:
    void processCommand(const QString &command_line) override;
    void setResponseIfc(const CommandResponseFunction &callback) override;
    void setQueryIfc(const CommandResponseFunction &callback) override;
    void close() override;
    void sendResponse(const QString &response) const;
    void writeHeader() const;
    void writePrompt() const;

protected:
    QHash<QString, QSharedPointer<CommandIfc>> theCommands;
    CommandResponseFunction                    theOutput;
    CommandResponseFunction                    theQueryIfc;
    ConsoleModel                              *theConsoleModel;
    QMutex                                     m_Lock;
};

#endif // COMMANDHANDLER_H
