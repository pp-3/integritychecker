// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ConsoleModel.h"

#include "checksums/SumTypeIfc.h"
#include "checksums/SumTypeManager.h"
#include "model/IntegrityModel.h"
#include "model/RunningTasks.h"
#include "shared/UserSettings.h"
#include "sum_tree/DirInfoRoot.h"
#include "thread_util/AsyncExec.h"
#include "worker/WorkerSettings.h"

#include "ConsoleUtils.h"

ConsoleModel::ConsoleModel(IntegrityModel *model) :
    pModel(model),
    m_Cwd(),
    m_RunningTasksId()
{
}

ConsoleModel::~ConsoleModel()
{
    close();
}

void ConsoleModel::close()
{
    m_RunningTasksId.cancelAll();
    for (const TaskLifeCycle::ptr &task : pModel->getRunningTasks()->currentTasks())
    {
        task->waitForTaskFinished();
    }
    m_Cwd.clear();
}

QList<ConsoleModel::FileType> ConsoleModel::ls(const QString &path, QString *error) const
{
    QList<ConsoleModel::FileType> ret;

    const DirInfo::ptr start = getCwd();
    if (!start)
    {
        if (error)
            *error = Tr::tr("Checksum data invalid!");
        return ret;
    }
    DirInfoRoot::ptr root = pModel->getCurrentRoot();

    const ConsoleUtils::PathWithContent pc = ConsoleUtils::relPath(root->getRoot(), start, path);
    const InfoBase::ptr &ib = pc.first;
    const bool is_content = pc.second;

    if (ib)
    {
        // `ls` returns current folder
        // `ls dir` returns dir summary
        // `ls dir/` returns dir entries
        if (ib->isDir() && is_content)
        {
            DirInfo::ptr di = DirInfo::cast(ib);
            const QList<InfoBase::ptr> kids = di->getChildren();

            for (const InfoBase::ptr &it : kids)
            {
                ret << FileType(it->getName(), it->isDir());
            }
        }
        else
        {
            ret << FileType(ib->getName(), ib->isDir());
        }
    }
    else
    {
        if (error)
            *error = Tr::tr("Not existing path: ") + path;
    }

    return ret;
}

QString ConsoleModel::pwd() const
{
    if (m_Cwd.isEmpty())
    {
        return ConsoleUtils::PATH_SEP;
    }

    return m_Cwd.join(ConsoleUtils::PATH_SEP) + ConsoleUtils::PATH_SEP;
}

bool ConsoleModel::cd(const QString &dir, QString *error)
{
    if (dir.isEmpty())
    {
        m_Cwd.clear();
        return true;
    }

    const DirInfo::ptr start = getCwd();
    if (!start)
    {
        m_Cwd.clear();
        return false;
    }

    const DirInfo::ptr root = pModel->getCurrentRoot()->getRoot();

    const ConsoleUtils::PathWithContent pc = ConsoleUtils::relPath(root, start, dir);
    InfoBase *ib = pc.first.get();
    if (!ib)
    {
        if (error)
            *error = Tr::tr("Not existing directory: ") + dir;
        return false;
    }

    m_Cwd.clear();
    if (ib == root.get())
    {
        return true;
    }

    while (ib && ib != root.get())
    {
        m_Cwd.push_front(ib->getName());
        ib = ib->getParent()->getInfoBase();
    }

    return true;
}

DirInfo::ptr ConsoleModel::getCwd() const
{
    DirInfoRoot::ptr root = pModel->getCurrentRoot();
    if (root.isNull())
    {
        return nullptr;
    }

    DirInfo::ptr di = root->getRoot();
    if (m_Cwd.isEmpty() || !di)
    {
        return di;
    }

    for (QStringList::iterator it=m_Cwd.begin() ; it!=m_Cwd.end() ; ++it)
    {
        InfoBase::ptr ib = ConsoleUtils::getChild(di, *it);
        DirInfo::ptr old = di;

        if (ib && ib->isDir())
        {
            di = DirInfo::cast(ib);
        }
        else
        {
            di = nullptr;
        }

        if (!di)
        {
            m_Cwd.erase(it);
            return old;
        }
    }

    return di;
}

QList<ConsoleModel::STATUS> ConsoleModel::status(QStringList params, QString *error)
{
    QList<ConsoleModel::STATUS> ret;

    DirInfo::ptr di = getCwd();
    if (!di)
    {
        if (error)
            *error = Tr::tr("Path Error");
        return ret;
    }

    // get types from params
    QList<ConsoleModel::CheckSumData> enabled = extractTypesFromParams(params);
    if (enabled.isEmpty())
        enabled = getEnabledTypes();

    // remaining params are files
    QList<InfoBase::ptr> files;

    if (params.isEmpty())
    {
        files = di->getChildren();  // all children
    }
    else
    {
        files = ConsoleUtils::getChildren(di, params, error);  // selected ones
    }

    // build status list
    const QList<CheckSumState::States> states = CheckSumState::getStateList();
    for (const InfoBase::ptr &ib : std::as_const(files))
    {
        FileType ls(ib->getName(), ib->isDir());
        STATUS st(ls);

        for (const CheckSumData &it : std::as_const(enabled))
        {
            for (const CheckSumState::States &it2 : states)
            {
                const unsigned long count = ib->getSumCount(it.Hash, it2);
                if (count > 0)
                {
                    st.States << ConsoleModel::STATUS::STATE(it, it2);
                }
            }
        }

        ret << st;
    }

    return ret;
}

void ConsoleModel::scan(const QString &path, QString *error) const
{
    DirInfo::ptr di = getCwd();
    if (!di)
    {
        if (error)
            *error = Tr::tr("Path Error");
        return;
    }

    const DirInfo::ptr root = pModel->getCurrentRoot()->getRoot();

    const ConsoleUtils::PathWithContent pc = ConsoleUtils::relPath(root, di, path);
    const InfoBase::ptr &ib = pc.first;
    if (!ib)
    {
        if (error)
            *error = Tr::tr("Invalid path: ") + path;
        return;
    }

    di = ib->isDir() ?
             DirInfo::cast(ib) :
             DirInfo::cast(root->searchChild(ib->getParent()->getInfoBase()));
    if (!di)
    {
        if (error)
            *error = Tr::tr("Directory not found for path: ") + path;
        return;
    }

    // scan files in directory
    AsyncExec::execInMain(
        [di, this]()
        {
            pModel->updateFs(di, true);
            return true;
        });
}

void ConsoleModel::check(QStringList params, QString *error)
{
    DirInfo::ptr di = getCwd();
    if (!di)
    {
        if (error)
            *error = Tr::tr("Path Error");
        return;
    }

    // get types from params
    QList<ConsoleModel::CheckSumData> enabled = extractTypesFromParams(params);
    if (enabled.isEmpty())
        enabled = getEnabledTypes();

    SumTypeHashList check_types;
    for (const CheckSumData &it : std::as_const(enabled))
        check_types.insert(it.Hash);

    // remained params are files
    QList<InfoBase::weak_ptr> files;

    if (params.isEmpty())
    {
        const QList<InfoBase::ptr> kids = di->getChildren();
        for (const InfoBase::ptr &it : kids)
            files << it.toWeakRef();
    }
    else
    {
        const QList<InfoBase::ptr> kids = ConsoleUtils::getChildren(di, params, error);
        for (const InfoBase::ptr &it : kids)
            files << it.toWeakRef();
    }

    // check all files
    AsyncExec::execInMain(
        [files, check_types, this]()
        {
            for (const InfoBase::weak_ptr &ib : files)
                pModel->compareCheckSum(ib, true, check_types);

            return true;
        });
}

void ConsoleModel::create(QStringList params, QString *error)
{
    DirInfo::ptr di = getCwd();
    if (!di)
    {
        if (error)
            *error = Tr::tr("Path Error");
        return;
    }

    // get types from params
    QList<ConsoleModel::CheckSumData> enabled = extractTypesFromParams(params);
    if (enabled.isEmpty())
        enabled = getEnabledTypes();

    SumTypeHashList check_types;
    for (const CheckSumData &it : std::as_const(enabled))
        check_types.insert(it.Hash);

    // remained params are files
    QList<InfoBase::weak_ptr> files;

    if (params.isEmpty())
    {
        const QList<InfoBase::ptr> kids = di->getChildren();
        for (const InfoBase::ptr &it : kids)
            files << it.toWeakRef();
    }
    else
    {
        const QList<InfoBase::ptr> kids = ConsoleUtils::getChildren(di, params, error);
        for (const InfoBase::ptr &it : kids)
            files << it.toWeakRef();
    }

    // check all files
    AsyncExec::execInMain(
        [files, check_types, this]()
        {
            for (const InfoBase::weak_ptr &ib : files)
                pModel->createCheckSum(ib, true, check_types);

            return true;
        });
}

QList<ConsoleModel::TASK> ConsoleModel::tasks() const
{
    QList<ConsoleModel::TASK> ret;

    const QList<TaskLifeCycle::ptr> tasks = pModel->getRunningTasks()->currentTasks();

    for (const TaskLifeCycle::ptr &task : tasks)
    {
        ConsoleModel::TASK t;
        t.Id = m_RunningTasksId.getId(task);
        t.Name = task->getName();
        t.State = task->getStateStr();
        t.StartTime = task->getStartTime();
        t.CurrentProgress = task->getProgress().first;
        t.MaxProgress = task->getProgress().second;

        ret << t;
    }

    return ret;
}

bool ConsoleModel::cancel(const QString &task_id)
{
    bool bOk;
    const unsigned int id = task_id.toUInt(&bOk);
    if (!bOk)
        return false;

    return m_RunningTasksId.cancelId(id);
}

QString ConsoleModel::getWorkingPath() const
{
    return pModel->getWorkerSettings()->getInputDir();
}

bool ConsoleModel::setWorkingPath(const QString &aPath)
{
    return AsyncExec::execInMain(
               [this, aPath]() -> bool
               {
                   pModel->getWorkerSettings()->setInputDir(aPath);
                   return pModel->getWorkerSettings()->isInputDirValid();
               })
        .result();
}

QList<ConsoleModel::CheckSumData> ConsoleModel::getAllTypes()
{
    QList<ConsoleModel::CheckSumData> ret;

    SumTypeManager& stm = SumTypeManager::getInstance();
    const QList<SumTypeIfc *> &types = stm.getRegisteredTypes();

    for (SumTypeIfc *it : types)
    {
        ret << CheckSumData(it->getName(), it->getTypeHash());
    }

    return ret;
}

QList<ConsoleModel::CheckSumData> ConsoleModel::getEnabledTypes() const
{
    const QList<ConsoleModel::CheckSumData> all = getAllTypes();
    const SumTypeHashList en = pModel->getWorkerSettings()->getEnabledCalcHashes();

    QList<ConsoleModel::CheckSumData> ret;

    std::copy_if(all.cbegin(),
                 all.cend(),
                 std::back_inserter(ret),
                 [en](const CheckSumData &it) -> bool
                 {
                     return en.contains(it.Hash);
                 });

    return ret;
}

bool ConsoleModel::setEnabledTypes(QStringList types)
{
    const QList<ConsoleModel::CheckSumData> all = getAllTypes();

    QHash<SumTypeHash, bool> enabled_states;
    for (const CheckSumData &it : all)
    {
        const SumTypeHash hash = it.Hash;
        const bool enabled = types.contains(it.Name, Qt::CaseInsensitive);
        enabled_states.insert(hash, enabled);

        types.removeOne(it.Name);
    }

    const bool ret = AsyncExec::execInMain(
                         [this, enabled_states]()
                         {
                             const auto B = enabled_states.constKeyValueBegin();
                             const auto E = enabled_states.constKeyValueEnd();
                             for (auto it = B ; it != E ; ++it)
                             {
                                 pModel->getWorkerSettings()->setEnabledType(it->first, it->second);
                             }
                             return true;
                         })
                         .result();

    return ret && types.isEmpty();
}

QStringList ConsoleModel::getProfileNames() const
{
    return AsyncExec::execInMain(
               [this]() -> QStringList
               {
                   return pModel->getUserSettingsManager()->getProfiles();
               }).result();
}

QString ConsoleModel::getCurrentProfileName() const
{
    return AsyncExec::execInMain(
        [this]() -> QString
        {
            return pModel->getUserSettingsManager()->getCurrentUserSettings()->profileName();
        }).result();
}

bool ConsoleModel::setCurrentProfileName(const QString &name)
{
    return AsyncExec::execInMain(
               [this, name]() -> bool
               {
                   if (!pModel->getUserSettingsManager()->existsProfile(name))
                       return false;

                   return (bool)pModel->getUserSettingsManager()->setCurrentUserProfile(name);
               })
        .result();
}

bool ConsoleModel::cpCurrentProfile(const QString &new_name)
{
    return AsyncExec::execInMain(
               [this, new_name]() -> bool
               {
                   return (bool)pModel->getUserSettingsManager()->copyProfile(new_name);
               })
        .result();
}

bool ConsoleModel::renameCurrentProfile(const QString &new_name)
{
    return AsyncExec::execInMain(
               [this, new_name]() -> bool
               {
                   return (bool)pModel->getUserSettingsManager()->renameProfile(new_name);
               })
        .result();
}

bool ConsoleModel::deleteProfile(const QString &name)
{
    return AsyncExec::execInMain(
               [this, name]() -> bool
               {
                   if (!pModel->getUserSettingsManager()->existsProfile(name))
                       return false;

                   pModel->getUserSettingsManager()->deleteProfile(name);
                   return true;
               })
        .result();
}

QList<ConsoleModel::CheckSumData> ConsoleModel::extractTypesFromParams(QStringList &params) const
{
    const QList<ConsoleModel::CheckSumData> all = getAllTypes();
    const QList<ConsoleModel::CheckSumData> enabled = getEnabledTypes();
    QList<ConsoleModel::CheckSumData> ret;

    bool searching = true;
    while (searching && !params.isEmpty())
    {
        const QString type = params.first();

        if (type == TYPE_FILE_SEP)
        {
            // end of type list indicated
            params.removeFirst();
            searching = false;
        }
        else if (type.compare(All_TYPES, Qt::CaseInsensitive) == 0)
        {
            // all types
            params.removeFirst();
            ret = enabled;
            searching = false;
            if (!params.isEmpty() && params.first() == TYPE_FILE_SEP)
            {
                params.removeFirst();
            }
        }
        else
        {
            bool found = false;
            for (const CheckSumData &it : all)
            {
                if (it.Name.compare(type, Qt::CaseInsensitive) == 0)
                {
                    found = true;
                    params.removeFirst();
                    if (enabled.contains(it))
                    {
                        ret << it;
                    }
                    break;
                }
            }

            searching = found;
        }
    }

    return ret;
}

bool ConsoleModel::CheckSumData::operator ==(const CheckSumData &r) const
{
    const bool ret = Hash==r.Hash;
    Q_ASSERT((Name==r.Name) == ret);

    return ret;
}
