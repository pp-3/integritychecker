// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CONSOLEMODEL_H
#define CONSOLEMODEL_H

#include <QDateTime>
#include <QList>
#include <QString>
#include <QStringList>

#include <sum_shared/CheckSumState.h>
#include <sum_tree/DirInfo.h>

#include "RunningTasksId.h"

class IntegrityModel;

/**
 * @brief The ConsoleModel class holds additional data, which is only necessary for the operation of a console
 * interface. Also, it provides helper functions for translating between internal states and the string interface.
 */
class ConsoleModel
{
public:
    /**
     * @brief The FileType class stores a name in folder together with the information, if it's a file or folder.
     */
    struct FileType
    {
        FileType(const QString &filename, bool dir) :
            FileName(filename), IsDir(dir)
        {}
        QString FileName; ///< The file or folder name (without path)
        bool    IsDir;    ///< Flag, if it's a directory
    };

    /**
     * @brief The CheckSumData class stores a single checksum's hash and it's name.
     */
    struct CheckSumData
    {
        CheckSumData(const QString &name, const SumTypeHash &hash) :
            Name(name), Hash(hash)
        {}
        bool operator ==(const CheckSumData &r) const;
        QString     Name; ///< The checksum's name
        SumTypeHash Hash; ///< The checksum's hash
    };

    /**
     * @brief The STATUS class stores for a single entry (file or folder) the list of checksums and their current
     * evaluation state.
     */
    struct STATUS
    {
        /**
         * @brief The STATE class stores the state of a checksum;
         */
        struct STATE
        {
            STATE(const CheckSumData &type, const CheckSumState::States &state) :
                Type(type), State(state)
            {}
            CheckSumData          Type; ///< The checksum
            CheckSumState::States State; ///< the current state for the checksum
        };

        STATUS(const FileType &file) :
            File(file), States()
        {}
        FileType                           File; ///< The file or folder
        QList<ConsoleModel::STATUS::STATE> States; ///< The (enabled) checksums and their states
    };

    /**
     * @brief The TASK class stores all data related to a single running task.
     */
    struct TASK
    {
        RunningTasksId::Id Id;
        QString            Name;
        QString            State;
        int                CurrentProgress;
        int                MaxProgress;
        QDateTime          StartTime;
    };

public:
    ConsoleModel(IntegrityModel *model);
    ~ConsoleModel();

    void close();

    QList<ConsoleModel::FileType> ls(const QString &path, QString *error) const;
    static QStringList lsParamsHelp() { return QStringList() << "[path]"; }

    QString pwd() const;
    static QStringList pwdParamsHelp() { return QStringList(); }

    bool cd(const QString &dir, QString *error);
    static QStringList cdParamsHelp() { return QStringList() << "[dir]"; }

    QList<ConsoleModel::STATUS> status(QStringList params, QString *error);
    static QStringList statusParamsHelp() { return QStringList() << "[[check_type1 [check_type2]] | all]" << "[-]" << "[filename]"; }

    void scan(const QString &path, QString *error) const;
    static QStringList scanParamsHelp() { return QStringList() << "[path]"; }

    void check(QStringList params, QString *error);
    static QStringList checkParamsHelp() { return QStringList() << "[[check_type1 [check_type2]] | all]" << "[-]" << "[filename]"; }

    void create(QStringList params, QString *error);
    static QStringList createParamsHelp() { return QStringList() << "[[check_type1 [check_type2]] | all]" << "[-]" << "[filename]"; }

    QList<TASK> tasks() const;
    static QStringList tasksParamsHelp() { return QStringList(); }

    bool cancel(const QString &task_id);
    static QStringList cancelParamsHelp() { return QStringList() << "task ID"; }

    QString getWorkingPath() const;
    bool setWorkingPath(const QString &aPath);

    static QList<ConsoleModel::CheckSumData> getAllTypes();
    QList<ConsoleModel::CheckSumData> getEnabledTypes() const;
    bool setEnabledTypes(QStringList types);

    QStringList getProfileNames() const;
    QString getCurrentProfileName() const;
    bool setCurrentProfileName(const QString &name);
    bool cpCurrentProfile(const QString &new_name);
    bool renameCurrentProfile(const QString &new_name);
    bool deleteProfile(const QString &name);

private:
    DirInfo::ptr getCwd() const;
    QList<ConsoleModel::CheckSumData> extractTypesFromParams(QStringList &params) const;

private:
    IntegrityModel     *pModel;
    mutable QStringList m_Cwd;
    RunningTasksId      m_RunningTasksId;

    static constexpr const char *TYPE_FILE_SEP = "-";
    static constexpr const char *All_TYPES = "all";
};

#endif // CONSOLEMODEL_H
