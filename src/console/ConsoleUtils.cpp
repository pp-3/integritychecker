// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ConsoleUtils.h"

#include "shared/PathUtils.h"

QStringList ConsoleUtils::parseCommandLine(const QString &line)
{
    QStringList l = line.trimmed().split(PARAM_SEP);
    l = PathUtils::combinePaths(l);

    QStringList ret;
    for (QString s : std::as_const(l))
    {
        s = s.trimmed();
        if (!s.isEmpty())
            ret << std::move(s);
    }

    return ret;
}


ConsoleUtils::PathWithContent ConsoleUtils::relPath(const DirInfo::ptr &root, const DirInfo::ptr &start, const QString &path)
{
    if (!start)
    {
        return PathWithContent(nullptr, false);
    }
    if (path.isEmpty())
    {
        return PathWithContent(start, true);
    }
    if (path == PATH_SEP)
    {
        return PathWithContent(root, true);
    }

    QStringList sub_paths = path.split(PATH_SEP);
    const bool is_absolute = path.startsWith(PATH_SEP);
    if (is_absolute)
    {
        Q_ASSERT(sub_paths.first().isEmpty());
        sub_paths = sub_paths.mid(1);
    }
    bool is_content = path.endsWith(PATH_SEP);
    if (is_content)
    {
        Q_ASSERT(sub_paths.last().isEmpty());
        sub_paths.removeLast();
    }
    InfoBase::ptr ib_walker = is_absolute ? root : start;

    while (!sub_paths.isEmpty())
    {
        const QString entry = sub_paths.takeFirst();

        if (entry == CUR_DIR)
        {
            if (sub_paths.isEmpty())
                is_content = true;
            continue;
        }

        if (entry == PAREN_DIR)
        {
            InfoParent *const ip = ib_walker->getParent();
            if (!ip)
                return PathWithContent(nullptr, false);
            InfoParent *const ip_parent = ip->getParent();
            if (!ip_parent)
                return PathWithContent(nullptr, false);

            ib_walker = ip_parent->searchChild(ip->getInfoBase());
            if (!ib_walker)
                return PathWithContent(nullptr, false);

            if (sub_paths.isEmpty())
                is_content = true;
            continue;
        }

        if (!ib_walker->isDir())
        {
            if (sub_paths.isEmpty())
                return PathWithContent(ib_walker, false);  // last part may be a file

            return PathWithContent(nullptr, false);  // error: more path after file
        }

        DirInfo::ptr di = DirInfo::cast(ib_walker);
        if (!di)
            return PathWithContent(nullptr, false);

        ib_walker = getChild(di, entry);
        if (!ib_walker)
            return PathWithContent(nullptr, false);
    }

    return PathWithContent(ib_walker, is_content);
}

InfoBase::ptr ConsoleUtils::getChild(const InfoParent::ptr &dir, const QString &name)
{
    Q_ASSERT(dir);

    const QList<InfoBase::ptr> kids = dir->getChildren();
    for (const InfoBase::ptr &ib : kids)
    {
        if (ib->getName() == name)
        {
            return ib;
        }
    }

    return {};
}

QList<InfoBase::ptr> ConsoleUtils::getChildren(const DirInfo::ptr &dir, const QStringList &names, QString *error)
{
    QList<InfoBase::ptr> ret;

    for (const QString &it : names)
    {
        const PathWithContent pc = relPath(dir, dir, it);
        InfoBase::ptr ib = pc.first;
        const bool is_content = pc.second;

        if (ib)
        {
            if (ib->isDir() && is_content)
            {
                DirInfo::ptr ip = DirInfo::cast(ib);
                Q_ASSERT(ip);

                // add children of directory
                const QList<InfoBase::ptr> kids = ip->getChildren();
                ret.append(kids);
            }
            else
            {
                ret << ib;
            }
        }
        else
        {
            if (error)
                *error += Tr::tr("Unknown file: ") + it + "\n";
        }
    }

    return ret;
}
