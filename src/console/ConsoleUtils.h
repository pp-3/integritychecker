// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CONSOLEUTILS_H
#define CONSOLEUTILS_H

#include <QStringList>

#include "sum_tree/DirInfo.h"

class ConsoleUtils
{
public:
    static QStringList parseCommandLine(const QString &line);
    /**
     *  First: pointer to a path. May null, if not found.
     *  Second: if pointer points to a directory, the boolean depicts, if the content of the directory is meant (true)
     *          or just the directory (false)
     */
    using PathWithContent = QPair<InfoBase::ptr, bool>;
    static PathWithContent relPath(const DirInfo::ptr &root, const DirInfo::ptr &start, const QString& path);
    static InfoBase::ptr getChild(const InfoParent::ptr &dir, const QString &name);
    static QList<InfoBase::ptr> getChildren(const DirInfo::ptr &dir, const QStringList &names, QString *error);

    static constexpr const char *PARAM_SEP = " ";
    static constexpr const char *PATH_SEP  = "/";
    static constexpr const char *CUR_DIR   = ".";
    static constexpr const char *PAREN_DIR = "..";
};

#endif // CONSOLEUTILS_H
