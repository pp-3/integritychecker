// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "IntegrityConsole.h"

#include "cmds/CommandFactory.h"
#include "model/IntegrityModel.h"

#include "CommandHandler.h"

IntegrityConsole::IntegrityConsole(IntegrityModel *model) :
    pModel(model),
    m_ConsoleModel(pModel),
    m_pConsoleIo()
{
    initialize();
}

IntegrityConsole::~IntegrityConsole()
{
    close();
}

void IntegrityConsole::initialize()
{
    CommandFactory factory(&m_ConsoleModel);
    m_pCommandHandler = QSharedPointer<CommandHandler>::create(factory, &m_ConsoleModel);
    m_pConsoleIo = std::make_unique<ConsoleIo>(m_pCommandHandler, pModel);
}

void IntegrityConsole::close()
{
    m_ConsoleModel.close();

    m_pCommandHandler->close();
    m_pCommandHandler.reset();

    m_pConsoleIo->close();
    m_pConsoleIo.reset();
}
