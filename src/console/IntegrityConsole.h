// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INTEGRITYCONSOLE_H
#define INTEGRITYCONSOLE_H

#include <memory>

#include <QSharedPointer>

#include "ConsoleModel.h"
#include "ifc/CommandHandlerIfc.h"
#include "io/ConsoleIo.h"

class IntegrityModel;

/**
 * @brief The IntegrityConsole class is the main class to create and connect all necessary entities to provide a
 * console operation on a given IntegrityModel:
 * - it creates a ConsoleModel to hold additional data only needed for the console operation (e.g. the current path)
 * - it creates a CommandFactory to create the command tree
 * - it creates a CommandHandler to parse commands from a string input
 * - it sets up ConsoleIo to connect stdin / stdout with the CommandHandler
 */
class IntegrityConsole
{
public:
    explicit IntegrityConsole(IntegrityModel *model);
    ~IntegrityConsole();

private:
    void initialize();
    void close();

private:
    IntegrityModel                   *pModel;
    ConsoleModel                      m_ConsoleModel;
    QSharedPointer<CommandHandlerIfc> m_pCommandHandler;
    std::unique_ptr<ConsoleIo>        m_pConsoleIo;
};

#endif // INTEGRITYCONSOLE_H
