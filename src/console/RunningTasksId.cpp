// Copyright 2020 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "RunningTasksId.h"

RunningTasksId::Id RunningTasksId::getId(const TaskLifeCycle::ptr &task) const
{
    const auto B = m_TaskIds.begin();

    for (auto it = B ; it != m_TaskIds.end() ; )
    {
        const TaskLifeCycle::ptr task2 = it.value().toStrongRef();

        if (!task2)
        {
            it = m_TaskIds.erase(it);
            continue;
        }

        if (task == task2)
            return it.key();

        ++it;
    }

    ++m_LastUsedId;
    m_TaskIds.insert(m_LastUsedId, task);

    return m_LastUsedId;
}

bool RunningTasksId::cancelId(Id id)
{
    QWeakPointer<TaskLifeCycle> task = m_TaskIds.take(id);
    if (!task)
        return false;

    TaskLifeCycle::ptr task2 = task.toStrongRef();
    if (task2)
        task2->cancelTask();

    return true;
}

void RunningTasksId::cancelAll()
{
    const auto B = m_TaskIds.begin();

    for (auto it = B ; it != m_TaskIds.end() ; )
    {
        const TaskLifeCycle::ptr task = it.value().toStrongRef();

        if (task)
            task->cancelTask();

        it = m_TaskIds.erase(it);
    }
}
