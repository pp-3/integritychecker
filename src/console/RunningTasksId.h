// Copyright 2020 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef RUNNINGTASKSID_H
#define RUNNINGTASKSID_H

#include <QHash>
#include <QWeakPointer>

#include <tasks/TaskLifeCycle.h>

/**
 * @brief The RunningTasksId class maps an unique ID to a TaskLifeCycle. With this ID the
 * user may manage the list of current running tasks via the console interface.
 */
class RunningTasksId
{
public:
    /**
     * Type of a task Id.
     */
    using Id = unsigned int;

    /**
     * @brief Returns or creates an unique ID for the given task. If the task has already an ID assigned to it's
     * returned. Otherwise, a new ID is created.
     * @param task The task to get an ID for.
     * @return The (unique) ID.
     */
    Id getId(const TaskLifeCycle::ptr &task) const;
    /**
     * @brief Cancels a task represented by the given ID.
     * @param id The ID of the task to cancel.
     * @return True, if task was found and canceled. False, if task ID is unknown.
     */
    bool cancelId(Id id);
    /**
     * @brief Cancels all known tasks.
     */
    void cancelAll();

private:
    mutable QHash<Id, QWeakPointer<TaskLifeCycle>> m_TaskIds;
    mutable Id m_LastUsedId = 0;
};

#endif // RUNNINGTASKSID_H
