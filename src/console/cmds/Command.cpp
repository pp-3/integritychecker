// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "Command.h"

#include <shared/Tr.h>

Command::Command(CommandFunction cmd_func) :
    theFunction(cmd_func)
{
}

QSharedPointer<CommandIfc> Command::getSubCommand(const QString & /*command*/) const
{
    Q_ASSERT(false);
    std::abort(); // asking a non-parent command for a sub command is always wrong
}

bool Command::execute(const QStringList &params, CommandResponseFunction &output)
{
    return theFunction(params, output);
}



ParentCommand::ParentCommand(const QHash<QString, QSharedPointer<CommandIfc>> &sub_commands,
                             const QSharedPointer<CommandIfc>                 &default_command) :
    theCommands(sub_commands),
    theDefaultCommand(default_command)
{
}

QSharedPointer<CommandIfc> ParentCommand::getSubCommand(const QString &command) const
{
    return theCommands.value(command);
}

bool ParentCommand::execute(const QStringList &params, CommandResponseFunction &output)
{
    if (!theDefaultCommand.isNull())
    {
        return theDefaultCommand->execute(params, output);
    }

    output(Tr::tr("This command is not executable"));
    return false;
}
