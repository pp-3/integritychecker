// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMMAND_H
#define COMMAND_H

#include <QHash>

#include "console/ifc/CommandIfc.h"

/**
 * @brief The Command class executes a single function when executed.
 */
class Command : public CommandIfc
{
public:
    /**
     * Wrapper for the actual function to execute.
     */
    using CommandFunction = std::function<bool(const QStringList &params, ///< The user parameters
                                                     CommandResponseFunction &output)>; ///< Interface for returning messages to the user

    /**
     * @param cmd_func The function to execute.
     */
    Command(CommandFunction cmd_func);

protected:
    bool hasSubCommands() const override { return false; }
    QSharedPointer<CommandIfc> getSubCommand(const QString & /*command*/) const override;
    bool execute(const QStringList &params, CommandResponseFunction &output) override;

private:
    CommandFunction theFunction;
};

/**
 * @brief The ParentCommand class executes a default command, if set. Otherwise, the execution fails.
 */
class ParentCommand : public CommandIfc
{
public:
    /**
     * @param sub_commands All sub commands in this parent command. Key in the hash is the sub command name. The
     * value is the related sub command.
     * @param default_command An optional default command. It's executed if this parent command gets executed.
     */
    ParentCommand(const QHash<QString, QSharedPointer<CommandIfc>> &sub_commands,
                  const QSharedPointer<CommandIfc>                 &default_command);

protected:
    bool hasSubCommands() const override { return true; }
    QSharedPointer<CommandIfc> getSubCommand(const QString &command) const override;
    bool execute(const QStringList &params, CommandResponseFunction &output) override;

private:
    QHash<QString, QSharedPointer<CommandIfc>> theCommands;
    QSharedPointer<CommandIfc>                 theDefaultCommand;
};

#endif // COMMAND_H
