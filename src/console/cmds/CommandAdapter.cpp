// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "CommandAdapter.h"

#include <QCoreApplication>

#include "shared/Tr.h"
#include "sum_shared/CheckSumState.h"

#include "Command.h"
#include "console/ConsoleUtils.h"
#include "console/ifc/ConsoleConstants.h"

static QString typeAndName(const ConsoleModel::FileType &file)
{
    QString s;
    s += file.IsDir ? ConsoleConstants::DIR_TAG : ConsoleConstants::FILE_TAG;
    s += " " + file.FileName;

    return s;
}

static QString filler(int count)
{
    if (count <= 0)
    {
        return QString();
    }
    else
    {
        return QString(count, ' ');
    }
}

static QString rightPad(const QString &text, int len)
{
    return text + filler(len - text.size());
}

static QString leftPad(const QString &text, int len)
{
    return filler(len - text.size()) + text;
}

QPair<CommandAdapter::AdapterCommandGroup, CommandAdapter::CreateHelpStringFunction> CommandAdapter::createGeneralCommandGroup()
{
    QSharedPointer<QString> fullHelpStr = QSharedPointer<QString>::create();
    const auto create_help_callback = [fullHelpStr](const QList<AdapterCommandGroup> &all_commands)
    {
        fullHelpStr->operator=(fullHelp(all_commands));
    };

    AdapterCommandGroup general(Tr::tr("General"), QSharedPointer<CommandIfc>());
    general << AdapterCommand(
        "help",
        QSharedPointer<CommandIfc>(new Command(std::bind(&generalHelpCommand, std::placeholders::_2, fullHelpStr))),
        Tr::tr("Shows this help"),
        QStringList());
    general << AdapterCommand(
        "quit",
        QSharedPointer<CommandIfc>(new Command(generalQuitCommand)),
        Tr::tr("Closes the application"),
        QStringList());

    return QPair<CommandAdapter::AdapterCommandGroup, CommandAdapter::CreateHelpStringFunction>(general, create_help_callback);
}

bool CommandAdapter::generalHelpCommand(CommandResponseFunction &output, const QSharedPointer<QString> &fullHelpStr)
{
    output(*fullHelpStr);
    return true;
}

bool CommandAdapter::generalQuitCommand(const QStringList &params,
                                        CommandResponseFunction &output)
{
    if (!params.isEmpty())
    {
        CommandAdapter::outputWrongParameter(output, 0, -1);
        return false;
    }
    QCoreApplication::instance()->quit();
    return true;
}

CommandAdapter::AdapterCommandGroup CommandAdapter::createFsCommandGroup(ConsoleModel *model)
{
    AdapterCommandGroup fs(Tr::tr("File system"), QSharedPointer<CommandIfc>());
    fs << AdapterCommand(
        "ls",
        QSharedPointer<CommandIfc>(new Command(std::bind(&fsLsCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Lists the files in the current directory"),
        model->lsParamsHelp());
    fs << AdapterCommand(
        "pwd",
        QSharedPointer<CommandIfc>(new Command(std::bind(&fsPwdCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Shows the current directory"),
        model->pwdParamsHelp());
    fs << AdapterCommand(
        "cd",
        QSharedPointer<CommandIfc>(new Command(std::bind(&fsCdCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Changes directory to dir"),
        model->cdParamsHelp());

    return fs;
}

bool CommandAdapter::fsLsCommand(ConsoleModel *model,
                                 const QStringList &params,
                                 CommandResponseFunction &output)
{
    if (params.size() > 1)
    {
        outputWrongParameter(output, 0, 1);
        return false;
    }
    QString err;
    const QList<ConsoleModel::FileType> ls = model->ls(params.isEmpty() ? QString() : params.first(), &err);
    if (!err.isEmpty())
    {
        output(err);
    }
    for (const ConsoleModel::FileType &it : ls)
    {
        output(typeAndName(it));
    }
    return true;
}

bool CommandAdapter::fsPwdCommand(ConsoleModel *model,
                                  const QStringList &params,
                                  CommandResponseFunction &output)
{
    if (!params.isEmpty())
    {
        outputWrongParameter(output, 0, -1);
        return false;
    }
    output(model->pwd());
    return true;
}

bool CommandAdapter::fsCdCommand(ConsoleModel *model,
                                 const QStringList &params,
                                 CommandResponseFunction &output)
{
    if (params.size() > 1)
    {
        outputWrongParameter(output, 0, 1);
        return false;
    }
    QString err;
    const bool b = model->cd(params.isEmpty() ? QString() : params.first(), &err);
    if (!err.isEmpty())
    {
        output(err);
    }
    return b;
}

CommandAdapter::AdapterCommandGroup CommandAdapter::createChecksCommandGroup(ConsoleModel *model)
{
    AdapterCommandGroup ch(Tr::tr("Checks"), QSharedPointer<CommandIfc>());
    ch << AdapterCommand(
        "status",
        QSharedPointer<CommandIfc>(new Command(std::bind(&checksStatusCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Shows the integrity status of the current directory or the given file"),
        model->statusParamsHelp());
    ch << AdapterCommand(
        "scan",
        QSharedPointer<CommandIfc>(new Command(std::bind(&checksScanCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Scans the given directory for new files"),
        model->scanParamsHelp());
    ch << AdapterCommand(
        "check",
        QSharedPointer<CommandIfc>(new Command(std::bind(&checksCheckCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Checks the files in the current directory"),
        model->checkParamsHelp());
    ch << AdapterCommand(
        "create",
        QSharedPointer<CommandIfc>(new Command(std::bind(&checksCreateCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Creates the integrity data for the files in the current directory"),
        model->createParamsHelp());

    return ch;
}

bool CommandAdapter::checksStatusCommand(ConsoleModel *model,
                                         const QStringList &params,
                                         CommandResponseFunction &output)
{
    QString err;
    const QList<ConsoleModel::STATUS> l = model->status(params, &err);
    if (!err.isEmpty())
    {
        output(err);
        return false;
    }

    return buildStatusOutput(l, output);
}

bool CommandAdapter::checksScanCommand(ConsoleModel *model,
                                       const QStringList &params,
                                       CommandResponseFunction &output)
{
    if (params.size() > 1)
    {
        outputWrongParameter(output, 0, 1);
        return false;
    }
    const QString path = params.empty() ? QString() : params.first();

    QString err;
    model->scan(path, &err);
    if (!err.isEmpty())
    {
        output(err);
        return false;
    }

    return true;
}

bool CommandAdapter::checksCheckCommand(ConsoleModel *model,
                                        const QStringList &params,
                                        CommandResponseFunction &output)
{
    QString err;
    model->check(params, &err);
    if (!err.isEmpty())
    {
        output(err);
        return false;
    }

    return true;
}

bool CommandAdapter::checksCreateCommand(ConsoleModel *model,
                                         const QStringList &params,
                                         CommandResponseFunction &output)
{
    QString err;
    model->create(params, &err);
    if (!err.isEmpty())
    {
        output(err);
        return false;
    }

    return true;
}

CommandAdapter::AdapterCommandGroup CommandAdapter::createTasksCommandGroup(ConsoleModel *model)
{
    AdapterCommandGroup tasks(Tr::tr("Tasks"), QSharedPointer<CommandIfc>());
    tasks << AdapterCommand(
        "tasks",
        QSharedPointer<CommandIfc>(new Command(std::bind(&tasksTasksCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Shows the current active tasks"),
        model->tasksParamsHelp());
    tasks << AdapterCommand(
        "cancel",
        QSharedPointer<CommandIfc>(new Command(std::bind(&tasksCancelCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Cancels an active task"),
        model->cancelParamsHelp());

    return tasks;
}

bool CommandAdapter::tasksTasksCommand(ConsoleModel *model,
                                       const QStringList &params,
                                       CommandResponseFunction &output)
{
    if (!params.isEmpty())
    {
        outputWrongParameter(output, 0, -1);
        return false;
    }

    const QList<ConsoleModel::TASK> t = model->tasks();

    return buildTaskOutput(t, output);
}

bool CommandAdapter::tasksCancelCommand(ConsoleModel *model,
                                        const QStringList &params,
                                        CommandResponseFunction &output)
{
    if (params.size() != 1)
    {
        outputWrongParameter(output, 1, 1);
        return false;
    }

    if (!model->cancel(params.first()))
    {
        output(Tr::tr("Unknown id"));
        return false;
    }

    return true;
}

CommandAdapter::AdapterCommandGroup CommandAdapter::createSetupCommandGroup(ConsoleModel *model)
{
    AdapterCommandGroup sett(
        "setup",
        QSharedPointer<CommandIfc>(new Command(std::bind(&setupDefaultCommand, std::placeholders::_2))));
    sett << AdapterCommand(
        "input",
        QSharedPointer<CommandIfc>(new Command(std::bind(&setupInputCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Reads or sets the input path"),
        QStringList() << "[path]");
    sett << AdapterCommand(
        "checks",
        QSharedPointer<CommandIfc>(new Command(std::bind(&setupChecksCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Reads or sets the enabled check types"),
        QStringList() << "[check_list]");

    return sett;
}

bool CommandAdapter::setupDefaultCommand(CommandResponseFunction &output)
{
    output(Tr::tr("Call a sub command"));
    return true;
}

bool CommandAdapter::setupInputCommand(ConsoleModel *model,
                                       const QStringList &params,
                                       CommandResponseFunction &output)
{
    if (params.size() > 1)
    {
        outputWrongParameter(output, 0, 1);
        return false;
    }

    if (params.size() == 0)
    {
        output(model->getWorkingPath());
        return true;
    }

    // else params.size() == 1
    const bool ret = model->setWorkingPath(params.first());
    if (!ret)
        output(Tr::tr("Input path is invalid"));

    return ret;
}

bool CommandAdapter::setupChecksCommand(ConsoleModel *model,
                                        const QStringList &params,
                                        CommandResponseFunction &output)
{
    if (params.size() == 0)
    {
        const QList<ConsoleModel::CheckSumData> types = model->getEnabledTypes();
        QStringList out;
        std::transform(types.cbegin(),
                       types.cend(),
                       std::back_inserter(out),
                       [](const ConsoleModel::CheckSumData &it) -> QString
                       {
                           return it.Name;
                       });
        output(out.join(ConsoleUtils::PARAM_SEP));
        return true;
    }

    // else params.size() > 0
    if (!model->setEnabledTypes(params))
    {
        output(Tr::tr("Setting enabled types failed"));
        return false;
    }

    return true;
}

CommandAdapter::AdapterCommandGroup CommandAdapter::createProfileCommandGroup(ConsoleModel *model)
{
    CommandAdapter::AdapterCommandGroup prof(
        "profile",
        QSharedPointer<CommandIfc>(new Command(std::bind(&profileDefaultCommand, std::placeholders::_2))));
    prof << AdapterCommand(
        "list",
        QSharedPointer<CommandIfc>(new Command(std::bind(&profileListCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Lists available profiles"),
        QStringList());
    prof << AdapterCommand(
        "current",
        QSharedPointer<CommandIfc>(new Command(std::bind(&profileCurrentCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Shows the current profile name"),
        QStringList());
    prof << AdapterCommand(
        "switch",
        QSharedPointer<CommandIfc>(new Command(std::bind(&profileSwitchCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Switches current profile to 'name'"),
        QStringList() << "name");
    prof << AdapterCommand(
        "cp",
        QSharedPointer<CommandIfc>(new Command(std::bind(&profileCpCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Copies current profile to another profile 'new_name' and switches to it"),
        QStringList() << "new_name");
    prof << AdapterCommand(
        "rename",
        QSharedPointer<CommandIfc>(new Command(std::bind(&profileRenameCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Renames current profile to 'new_name'"),
        QStringList() << "new_name");
    prof << AdapterCommand(
        "delete",
        QSharedPointer<CommandIfc>(new Command(std::bind(&profileDeleteCommand, model, std::placeholders::_1, std::placeholders::_2))),
        Tr::tr("Deletes the existing profile 'name'"),
        QStringList() << "name");

    return prof;
}

bool CommandAdapter::profileDefaultCommand(CommandResponseFunction &output)
{
    output(Tr::tr("Call a sub command"));
    return true;
}

bool CommandAdapter::profileListCommand(ConsoleModel *model,
                                        const QStringList &params,
                                        CommandResponseFunction &output)
{
    if (!params.isEmpty())
    {
        outputWrongParameter(output, 0, -1);
        return false;
    }
    output(model->getProfileNames().join(' '));
    return true;
}

bool CommandAdapter::profileCurrentCommand(ConsoleModel *model,
                                           const QStringList &params,
                                           CommandResponseFunction &output)
{
    if (!params.isEmpty())
    {
        outputWrongParameter(output, 0, -1);
        return false;
    }
    output(model->getCurrentProfileName());
    return true;
}

bool CommandAdapter::profileSwitchCommand(ConsoleModel *model,
                                          const QStringList &params,
                                          CommandResponseFunction &output)
{
    if (params.size() != 1)
    {
        outputWrongParameter(output, 1, 1);
        return false;
    }

    if (!model->setCurrentProfileName(params.first()))
    {
        output("Profile setting failed");
        return false;
    }
    return true;
}

bool CommandAdapter::profileCpCommand(ConsoleModel *model,
                                      const QStringList &params,
                                      CommandResponseFunction &output)
{
    if (params.size() != 1)
    {
        outputWrongParameter(output, 1, 1);
        return false;
    }

    if (!model->cpCurrentProfile(params.first()))
    {
        output("Profile copy failed");
        return false;
    }
    return true;
}

bool CommandAdapter::profileRenameCommand(ConsoleModel *model,
                                          const QStringList &params,
                                          CommandResponseFunction &output)
{
    if (params.size() != 1)
    {
        outputWrongParameter(output, 1, 1);
        return false;
    }

    if (!model->renameCurrentProfile(params.first()))
    {
        output("Profile renaming failed");
        return false;
    }
    return true;
}

bool CommandAdapter::profileDeleteCommand(ConsoleModel *model,
                                          const QStringList &params,
                                          CommandResponseFunction &output)
{
    if (params.size() != 1)
    {
        outputWrongParameter(output, 1, 1);
        return false;
    }

    if (!model->deleteProfile(params.first()))
    {
        output("Profile delete failed");
        return false;
    }
    return true;
}

QString CommandAdapter::fullHelp(const QList<AdapterCommandGroup> &all_commands)
{
    QString ret;

    for (const AdapterCommandGroup &it : std::as_const(all_commands))
    {
        ret += it.Name + CommandResponseIfc::NEWLINE;
        const bool is_parent = !it.Command.isNull();

        int len = it.Name.size();
        if (!is_parent)
        {
            ret += QString(len, '-') + CommandResponseIfc::NEWLINE;
        }

        if (!it.cmds().isEmpty())
        {
            const int maxSubLen = std::max_element(it.cmds().cbegin(),
                                                   it.cmds().cend(),
                                                   [](const AdapterCommand &l, const AdapterCommand &r) -> bool
                                                   {
                                                       return l.Name.size() < r.Name.size();
                                                   })
                                      ->Name.size();

            len = is_parent ? it.Name.size()+1 : 0;
            for (const AdapterCommand &it2 : it.cmds())
            {
                ret += QString(len, ' ') +
                       rightPad(it2.Name, maxSubLen) + " " + it2.ParamList.join(' ') +
                       "  | " + it2.Help + CommandResponseIfc::NEWLINE;
            }
        }

        ret += CommandResponseIfc::NEWLINE;
    }

    return ret;
}

void CommandAdapter::outputWrongParameter(CommandResponseFunction &output, int min, int max)
{
    QString err(Tr::tr("Error: "));
    if (max < min)
    {
        if (min == 0)
        {
            err += Tr::tr("No parameters expected.");
        }
        else
        {
            err += Tr::tr("Minimum required parameters: ") + QString::number(min);
        }
    }
    else if (min == max)
    {
        err += Tr::tr("Required parameters: ") + QString::number(min);
    }
    else	// max > min
    {
        err += Tr::tr("Required parameters: ") + QString::number(min) + QLatin1String(" - ") + QString::number(max);
    }

    output(err);
}

static QString getStatus(const CheckSumState::States &state)
{
    switch (state)
    {
    case CheckSumState::UNKNOWN:     return ConsoleConstants::STATE_UNKNOWN;
    case CheckSumState::EXISTS:      return ConsoleConstants::STATE_EXISTS;
    case CheckSumState::VERIFIED:    return ConsoleConstants::STATE_VERIFIED;
    case CheckSumState::CHECK_ERROR: return ConsoleConstants::STATE_CHECK_ERROR;
    default:                         return ConsoleConstants::STATE_NOT_EXISTING;
    }
}

bool CommandAdapter::buildStatusOutput(const QList<ConsoleModel::STATUS> &status_list,
                                             CommandResponseFunction     &output)
{
    QList<ConsoleModel::CheckSumData> tested_types;
    for (const ConsoleModel::STATUS &status : status_list)
        for (const ConsoleModel::STATUS::STATE &state : status.States)
            if (!tested_types.contains(state.Type))
                tested_types << state.Type;

    if (tested_types.isEmpty())
        return true;

    // longest file name
    const int name_len = 4 + std::max_element(status_list.constBegin(),
                                              status_list.constEnd(),
                                              [](const ConsoleModel::STATUS &l, const ConsoleModel::STATUS &r) -> bool
                                              {
                                                  return l.File.FileName.size() < r.File.FileName.size();
                                              })
                             ->File.FileName.size();

    // longest type
    const int type_len = 1 + std::max_element(tested_types.cbegin(),
                                              tested_types.cend(),
                                              [](const ConsoleModel::CheckSumData &l, const ConsoleModel::CheckSumData &r) -> bool
                                              {
                                                  return l.Name.size() < r.Name.size();
                                              })
                             ->Name.size();

    {
        // check sum row
        QString ret = filler(name_len);
        for (const ConsoleModel::CheckSumData &it : std::as_const(tested_types))
        {
            ret += rightPad(it.Name, type_len);
        }
        output(ret);
    }

    {
        // under line row
        QString ret = filler(name_len);
        for (const ConsoleModel::CheckSumData &it : std::as_const(tested_types))
        {
            ret += rightPad(QString(it.Name.size(), '-'), type_len);
        }
        output(ret);
    }

    // add states for each file
    const QList<CheckSumState::States> all_states = CheckSumState::getStateList();

    for (const ConsoleModel::STATUS &it1 : status_list)
    {
        QString stat = rightPad(typeAndName(it1.File), name_len);

        for (const ConsoleModel::CheckSumData &it2 : std::as_const(tested_types))
        {
            const SumTypeHash hash = it2.Hash;
            QString type_str;

            for (const CheckSumState::States &it3 : all_states)
            {
                QList<ConsoleModel::STATUS::STATE>::const_iterator ind = std::find_if(
                    it1.States.cbegin(),
                    it1.States.cend(),
                    [hash, it3](const ConsoleModel::STATUS::STATE &val) -> bool
                    {
                        return val.Type.Hash==hash && val.State==it3;
                    });

                if (ind != it1.States.cend())
                {
                    type_str += getStatus(it3);
                }
                else
                {
                    type_str += " ";
                }

            }

            stat += rightPad(type_str, type_len);
        }

        output(stat);
        stat.clear();
    }

    return true;
}

bool CommandAdapter::buildTaskOutput(const QList<ConsoleModel::TASK> &task_list,
                                           CommandResponseFunction   &output)
{
    if (task_list.isEmpty())
        return true;

    QList<QVector<QString>> lines;

    // build list of columns
    for (const ConsoleModel::TASK &t : task_list)
    {
        QVector<QString> line {
            QString::number(t.Id),
            t.State,
            QStringLiteral("%1 (%2)").arg(t.CurrentProgress).arg(t.MaxProgress),
            t.StartTime.toString(Qt::ISODate),
            t.Name
        };

        lines << line;
    }

    const static QStringList COLUMN_HEADERS = {
        "Id", "State", "Progress", "Started", "Task"
    };

    QVector<unsigned int> max_width;  // printed column widths
    // initial max width per column
    for (const QString &header : COLUMN_HEADERS) {
        max_width.push_back(header.size());
    }

    // max width per column for printed lines
    for (const QVector<QString> &line : std::as_const(lines))
    {
        Q_ASSERT(line.size() == max_width.size());
        for (int i=0 ; i<line.size() ; ++i)
            max_width[i] = std::max(max_width[i],
                                    static_cast<unsigned int>(line[i].size()));
    }

    // header
    {
        QString ret;
        for (int i=0 ; i<COLUMN_HEADERS.size() ; ++i)
        {
            ret.append(rightPad(COLUMN_HEADERS.at(i), 1+max_width.at(i)));
        }
        output(ret);
    }

    // under lines
    {
        QString ret;
        for (unsigned int w : std::as_const(max_width))
            ret.append(QString(w, '-') + " ");
        ret.resize(ret.size() - 1);  // remove trailing space
        output(ret);
    }

    // lines
    for (const QVector<QString> &line : std::as_const(lines))
    {
        QString ret;
        int i = 0;
        for (const QString &col : line)
        {
            if (i < max_width.size() - 1)
                ret.append(leftPad(col, max_width[i])).append(" ");
            else
                ret.append(rightPad(col, max_width[i]));

            ++i;
        }
        output(ret);
    }

    return true;
}


CommandAdapter::AdapterCommand::AdapterCommand(const QString &name,
                                               const QSharedPointer<CommandIfc> &cmd,
                                               const QString &help,
                                               const QStringList &params) :
    Name(name),
    Command(cmd),
    Help(help),
    ParamList(params)
{
}

CommandAdapter::AdapterCommandGroup::AdapterCommandGroup(const QString &name, const QSharedPointer<CommandIfc> &cmd) :
    AdapterCommand(name, cmd, QString(), QStringList())
{
}

CommandAdapter::AdapterCommandGroup &CommandAdapter::AdapterCommandGroup::operator <<(const AdapterCommand &cmd)
{
    SubCommands << cmd;
    return *this;
};
