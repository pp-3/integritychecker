// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMMANDADAPTER_H
#define COMMANDADAPTER_H

#include <functional>

#include <QList>
#include <QSharedPointer>
#include <QString>
#include <QStringList>

#include "console/ConsoleModel.h"
#include "console/ifc/CommandIfc.h"

/**
 * @brief The CommandAdapter class provides the adapter between the console commands and the ConsoleModel. Each top
 * command is returned as a AdapterCommandGroup. Acutal commands are stored in a AdapterCommand instance
 */
class CommandAdapter
{
public:
    struct AdapterCommand
    {
        AdapterCommand(const QString &name, const QSharedPointer<CommandIfc> &cmd, const QString &help, const QStringList &params);

        QString                    Name;      ///< The command name
        QSharedPointer<CommandIfc> Command;   ///< The related command instance
        QString                    Help;      ///< The help string
        QStringList                ParamList; ///< The accepted parameter list help
    };

    struct AdapterCommandGroup : public AdapterCommand
    {
        AdapterCommandGroup(const QString &name, const QSharedPointer<CommandIfc> &cmd);

        AdapterCommandGroup &operator <<(const AdapterCommand &cmd);
        const QList<AdapterCommand> &cmds() const { return SubCommands; }

    private:
        QList<AdapterCommand> SubCommands;
    };

    using CreateHelpStringFunction = std::function<void(const QList<AdapterCommandGroup> &all_commands)>;
    static QPair<AdapterCommandGroup, CreateHelpStringFunction> createGeneralCommandGroup();
private:
    static bool generalHelpCommand(CommandResponseFunction &output, const QSharedPointer<QString> &fullHelpStr);
    static bool generalQuitCommand(const QStringList &params, CommandResponseFunction &output);

public:
    static AdapterCommandGroup createFsCommandGroup(ConsoleModel *model);
private:
    static bool fsLsCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool fsPwdCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool fsCdCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);

public:
    static AdapterCommandGroup createChecksCommandGroup(ConsoleModel *model);
private:
    static bool checksStatusCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool checksScanCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool checksCheckCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool checksCreateCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);

public:
    static AdapterCommandGroup createTasksCommandGroup(ConsoleModel *model);
private:
    static bool tasksTasksCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool tasksCancelCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);

public:
    static AdapterCommandGroup createSetupCommandGroup(ConsoleModel *model);
private:
    static bool setupDefaultCommand(CommandResponseFunction &output);
    static bool setupInputCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool setupChecksCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);

public:
    static AdapterCommandGroup createProfileCommandGroup(ConsoleModel *model);
private:
    static bool profileDefaultCommand(CommandResponseFunction &output);
    static bool profileListCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool profileCurrentCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool profileSwitchCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool profileCpCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool profileRenameCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);
    static bool profileDeleteCommand(ConsoleModel *model, const QStringList &params, CommandResponseFunction &output);

private:
    static QString fullHelp(const QList<CommandAdapter::AdapterCommandGroup> &all_commands);
    static void outputWrongParameter(CommandResponseFunction &output, int min, int max);
    static bool buildStatusOutput(const QList<ConsoleModel::STATUS> &status_list, CommandResponseFunction &output);
    static bool buildTaskOutput(const QList<ConsoleModel::TASK> &task_list, CommandResponseFunction &output);
};

#endif // COMMANDADAPTER_H
