// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "CommandFactory.h"

#include "Command.h"

CommandFactory::CommandFactory(ConsoleModel *model)
{
    buildCommands(model);
}

QHash<QString, QSharedPointer<CommandIfc>> CommandFactory::getCommands() const
{
    QHash<QString, QSharedPointer<CommandIfc>> ret;

    for (const CommandAdapter::AdapterCommandGroup &it : std::as_const(theCommands))
    {
        QSharedPointer<CommandIfc> parent(it.Command);

        QHash<QString, QSharedPointer<CommandIfc>> sub_commands;

        QHash<QString, QSharedPointer<CommandIfc>> &tmp = !parent ? ret : sub_commands;
        for (const CommandAdapter::AdapterCommand &it2 : it.cmds())
        {
            if (it2.Command)
            {
                tmp.insert(it2.Name, it2.Command);
            }
        }

        if (!sub_commands.isEmpty())
        {
            ret.insert(it.Name, QSharedPointer<CommandIfc>(new ParentCommand(sub_commands, parent)));
        }
     }

    return ret;
}

void CommandFactory::buildCommands(ConsoleModel *model)
{
    if (!theCommands.isEmpty())
        return;

    // general
    QPair<CommandAdapter::AdapterCommandGroup, CommandAdapter::CreateHelpStringFunction> general =
        CommandAdapter::createGeneralCommandGroup();
    theCommands << general.first;

    // file system
    theCommands << CommandAdapter::createFsCommandGroup(model);

    // checks
    theCommands << CommandAdapter::createChecksCommandGroup(model);

    // tasks
    theCommands << CommandAdapter::createTasksCommandGroup(model);

    // setup
    theCommands << CommandAdapter::createSetupCommandGroup(model);

    // profiles
    theCommands << CommandAdapter::createProfileCommandGroup(model);

    // create help command with all commands
    general.second(theCommands);
}
