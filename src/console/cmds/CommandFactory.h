// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMMANDFACTORY_H
#define COMMANDFACTORY_H

#include <QHash>
#include <QList>
#include <QSharedPointer>
#include <QString>

#include "CommandAdapter.h"

/**
 * @brief The CommandFactory class creates adapters between the console string commands and the related calls into
 * the ConsoleModel. It provides the command tree for the command dispatcher to execute.
 */
class CommandFactory
{
public:
    /**
     * @param model The model the commands operate on when executed.
     */
    CommandFactory(ConsoleModel *model);

    /**
     * @brief Returns all supported commands as a command tree, where the hash key consists out of the command name
     * and the mapped CommandIfc either is the next level parent or an actual command to execute.
     * @return The command tree.
     */
    QHash<QString, QSharedPointer<CommandIfc>> getCommands() const;

private:
    Q_DISABLE_COPY(CommandFactory)

    void buildCommands(ConsoleModel *model);

private:
    QList<CommandAdapter::AdapterCommandGroup> theCommands;
};

#endif // COMMANDFACTORY_H
