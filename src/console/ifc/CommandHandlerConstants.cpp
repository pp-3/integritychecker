// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "CommandHandlerConstants.h"

#include "shared/Tr.h"

const QString CommandHandlerConstants::GENERAL_ERROR = Tr::tr("Error!");
const QString CommandHandlerConstants::UNKNOWN_SUBCOMMAND = Tr::tr("Unknown sub command: ");
const QString CommandHandlerConstants::UNKNOWN_COMMAND = Tr::tr("Unknown command: ");
const QString CommandHandlerConstants::QUIT_RESPONSE = Tr::tr("Bye.");
const QString CommandHandlerConstants::CMDLINE_HELP = Tr::tr("Command line interpreter. Type 'help' for all commands.");
