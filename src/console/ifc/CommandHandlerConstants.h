// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMMANDHANDLERCONSTANTS_H
#define COMMANDHANDLERCONSTANTS_H

#include <QString>

class CommandHandlerConstants
{
public:
    static const QString GENERAL_ERROR; // = Tr::tr("Error!")
    static const QString UNKNOWN_SUBCOMMAND; // = Tr::tr("Unknown sub command: ")
    static const QString UNKNOWN_COMMAND; // = Tr::tr("Unknown command: ")
    static const QString QUIT_RESPONSE; // = Tr::tr("Bye.")
    static constexpr const char *CMDLINE_TITLE = "Integrity Checker ";
    static const QString CMDLINE_HELP; // = Tr::tr("Command line interpreter. Type 'help' for all commands.")
    static constexpr const char *CMDLINE_PROMPT = " > ";
};

#endif // COMMANDHANDLERCONSTANTS_H
