// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMMANDHANDLERIFC_H
#define COMMANDHANDLERIFC_H

#include <QString>

#include "CommandResponseIfc.h"

/**
 * @brief The CommandHandlerIfc class provides the interface to a command handler, which takes a string and parses it
 * into the command and parameters.
 */
class CommandHandlerIfc
{
public:
    virtual ~CommandHandlerIfc() = default;

    /**
     * @brief Processes a command from the user.
     * @param command The command as entered from the user without any endline characters.
     */
    virtual void processCommand(const QString &command) = 0;
    /**
     * @brief Sets the interface for returning the command output to the user console.
     * @param callback The interface to pass in the command responses.
     */
    virtual void setResponseIfc(const CommandResponseFunction &callback) = 0;
    /**
     * @brief Sets the interface for allowing a command to query an user input. The only (expected) difference to
     * setResponseIfc() will be, that there is no end line added to the output.
     * @param callback The interface to pass in queries / requests to the user.
     */
    virtual void setQueryIfc(const CommandResponseFunction &callback) = 0;
    /**
     * @brief Stops processing further commands. If a command is currently processed this function will block until
     * that command is finished.
     */
    virtual void close() = 0;

protected:
    CommandHandlerIfc() = default;
};

#endif // COMMANDHANDLERIFC_H
