// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMMANDIFC_HPP
#define COMMANDIFC_HPP

#include <QSharedPointer>
#include <QStringList>

#include "console/ifc/CommandResponseIfc.h"

class ConsoleModel;

/**
 * @brief The CommandIfc class is the interface for all commands an user may execute from the console.
 */
class CommandIfc
{
public:
    virtual ~CommandIfc() = default;

    /**
     * @brief Checks, whether this command is a parent one (storing sub commands) or an actual command with a function
     * behind it.
     * @return True, if this command is a parent command. False, if it's an actual command.
     */
    virtual bool hasSubCommands() const = 0;
    /**
     * @brief For parent commands (hasSubCommands() == true) this function resolves the sub command for the given
     * command name.
     * @param command The command name to resolve.
     * @return The sub command pointer, if this command is a parent command having a sub command with the given name.
     *         nullptr, if this is not a parent command or if the given command name is unknown.
     */
    virtual QSharedPointer<CommandIfc> getSubCommand(const QString &command) const = 0;
    /**
     * @brief Executes this command. For actual commands the related function is executed. For parent commands it's up
     * to the command what to do. E.g. show a help message or execute a default command.
     * @param params The parameters passed in from the user for the command.
     * @param output The interface to return the command output.
     * @return True, if the command succeeded. False, if the command failed.
     */
    virtual bool execute(const QStringList &params, CommandResponseFunction &output) = 0;

protected:
    CommandIfc() = default;
};

#endif // COMMANDIFC_HPP
