// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMMANDRESPONSEIFC_H
#define COMMANDRESPONSEIFC_H

#include <functional>

#include <QString>

/**
 * Function interface provided to the command implementations for returning output to the console.
 * @param output The string to output on the console.
 */
using CommandResponseFunction = std::function<void(const QString &output)>;

struct CommandResponseIfc
{
    static constexpr const char NEWLINE = '\n';
};

#endif // COMMANDRESPONSEIFC_H
