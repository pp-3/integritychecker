// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CONSOLECONSTANTS_H
#define CONSOLECONSTANTS_H

/**
 * @brief The ConsoleConstants class keeps all the constants for the console output.
 */
class ConsoleConstants
{
public:
    static constexpr const char *DIR_TAG = "d";
    static constexpr const char *FILE_TAG = "f";

    static constexpr const char *STATE_UNKNOWN = "-";
    static constexpr const char *STATE_EXISTS = "X";
    static constexpr const char *STATE_VERIFIED = "V";
    static constexpr const char *STATE_CHECK_ERROR = "E";
    static constexpr const char *STATE_NOT_EXISTING = " ";
};

#endif // CONSOLECONSTANTS_H
