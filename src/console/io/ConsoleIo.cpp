// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ConsoleIo.h"

#include <iostream>

#include <QTimer>

#include <model/IntegrityModel.h>
#include <shared/ErrorCollector.h>

ConsoleIo::ConsoleIo(const QSharedPointer<CommandHandlerIfc> &command_handler, IntegrityModel *model) :
    QObject(),
    m_pCommandHandler(command_handler),
    pModel(model),
    m_pThread(new QThread),
    m_pInput(),
    m_pOutput()
{
    m_pThread->setObjectName("ConsoleIo");
    moveToThread(m_pThread);

    connect(m_pThread, &QThread::started,  this, &ConsoleIo::threadInit);
    connect(m_pThread, &QThread::finished, this, &ConsoleIo::threadClose);
    connect(m_pThread, &QThread::finished, m_pThread, &QThread::deleteLater);

    QTimer::singleShot(0, m_pThread, std::bind(&QThread::start, m_pThread, QThread::NormalPriority));
}

void ConsoleIo::close()
{
    if (m_pThread)
    {
        if (m_pInput)
            m_pInput->close();

        m_pThread->quit();

        if (!m_pThread->wait(1000))
        {
            m_pThread->terminate();
            threadClose();
        }
        m_pThread = nullptr;
    }
}

void ConsoleIo::threadInit()
{
    ErrorCollector *const error_ifc = pModel->getErrorInstance();

    connect(error_ifc, &ErrorCollector::newError,
            this, [this](const QString& err, const QString &module, const QDateTime &timestamp) -> void
            {
                const QString s = timestamp.toString() + " : " + Tr::tr("Error: ") + module + " : " + err;
                if (m_pOutput->isOpen())
                    writeOutput(s, true);
                else
                    std::cerr << s.toStdString() << std::endl;
            });

    m_pInput.reset(new QFile);
    if (!m_pInput->open(stdin, QIODevice::ReadOnly | QIODevice::Text))
    {
        error_ifc->addError(Tr::tr("Couldn't open stdin: ") + m_pInput->errorString());
        m_pInput.reset(nullptr);
        return;
    }

    m_pOutput.reset(new QFile());
    if (!m_pOutput->open(stdout, QIODevice::WriteOnly | QIODevice::Text))
    {
        error_ifc->addError(Tr::tr("Couldn't open stdout: ") + m_pOutput->errorString());
    }

    m_pCommandHandler->setResponseIfc(std::bind(&ConsoleIo::writeOutput, this, std::placeholders::_1, true));
    m_pCommandHandler->setQueryIfc(std::bind(&ConsoleIo::writeOutput, this, std::placeholders::_1, false));

    QTimer::singleShot(20, this, &ConsoleIo::checkForInputLine);
}

void ConsoleIo::threadClose()
{
    m_pCommandHandler.clear();

    if (m_pInput)
    {
        m_pInput->close();
        m_pInput.reset();
    }

    if (m_pOutput)
    {
        m_pOutput->close();
        m_pOutput.reset();
    }
}

void ConsoleIo::checkForInputLine()
{
    if (m_pInput && m_pInput->isOpen() && m_pInput->isReadable())
    {
        // blocks until first character
        m_pInput->peek(1);

        if (m_pInput->isOpen() && m_pInput->canReadLine())
        {
            const QByteArray b = m_pInput->readLine();
            const QString s = QString::fromUtf8(b);

            if (!m_pCommandHandler.isNull())
            {
                m_pCommandHandler->processCommand(s);
            }

            QTimer::singleShot(20, this, &ConsoleIo::checkForInputLine);
            return;
        }
    }

    QTimer::singleShot(200, this, &ConsoleIo::checkForInputLine);
}

void ConsoleIo::writeOutput(const QString &output, bool add_newline)
{
    if (m_pOutput->isOpen() && m_pOutput->isWritable())
    {
        m_pOutput->write(output.toUtf8());
        if (add_newline)
            m_pOutput->write(QByteArray().append(CommandResponseIfc::NEWLINE));
        m_pOutput->flush();
    }
}
