// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CONSOLEIO_H
#define CONSOLEIO_H

#include <QFile>
#include <QObject>
#include <QScopedPointer>
#include <QSharedPointer>
#include <QThread>

#include "console/ifc/CommandHandlerIfc.h"

class IntegrityModel;

class ConsoleIo : public QObject
{
    Q_OBJECT

public:
    explicit ConsoleIo(const QSharedPointer<CommandHandlerIfc> &command_handler, IntegrityModel *model);

    void close();

signals:

private slots:
    void threadInit();
    void threadClose();
    void checkForInputLine();
    void writeOutput(const QString &output, bool add_newline);

private:
    QSharedPointer<CommandHandlerIfc> m_pCommandHandler;
    IntegrityModel                   *pModel;
    QThread                          *m_pThread;
    QScopedPointer<QFile>             m_pInput;
    QScopedPointer<QFile>             m_pOutput;
};

#endif // CONSOLEIO_H
