// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef APPLICATIONPARAMETERPARSERIFC_H
#define APPLICATIONPARAMETERPARSERIFC_H

#include <QString>

/**
 * @brief The ParameterParserIfc class provides the interface for accessing the parameters set to the application at
 * start up.
 */
class ApplicationParameterParserIfc
{
public:
    virtual ~ApplicationParameterParserIfc() = default;

    /**
     * Returns, whether the GUI should be shown.
     * @return True, for GUI. False, for no GUI.
     */
    virtual bool showGui() const = 0;
    /**
     * Returns the profile name to use from the config.
     * @return Profile name, if not empty. If empty, profile name was not set.
     */
    virtual QString getProfile() const = 0;
    /**
     * Returns, whether the language was set via parameter.
     * @return True, for set. False, for not.
     */
    virtual bool isLangSet() const = 0;
    /**
     * Returns, whether the user requested an interactive shell.
     * @return True, if interactive. False, otherwise.
     */
    virtual bool isInteractive() const = 0;
};

#endif // APPLICATIONPARAMETERPARSERIFC_H
