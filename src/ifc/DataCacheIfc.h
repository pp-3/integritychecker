// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DATACACHEIFC_H
#define DATACACHEIFC_H

#include <QSharedPointer>

/**
 * @brief The DataCacheIfc class provides the interface for storing generic key value pairs.
 */
template <typename KEY,
          typename VALUE>
class DataCacheIfc
{
public:
    using key_type = KEY;
    using mapped_type = VALUE;
    using value_ptr = QSharedPointer<VALUE>;
    using const_value_ptr = QSharedPointer<const VALUE>;

    virtual ~DataCacheIfc() = default;

    /**
     * Adds a new item to the cache. addData() expects there is no previous stored data with the same key. If there is, use replaceData().
     * @param key  The related (unique) key to the data.
     * @param data The data to store.
     */
    virtual void addData(const key_type &key, const DataCacheIfc::value_ptr &data) = 0;
    /**
     * Replaces the data for the given key in the cache.
     * @param key  The related (unique) key to the data.
     * @param data The data to store.
     * @return The data previously stored with the given key. May be nullptr, if there wasn't any data stored.
     */
    virtual DataCacheIfc::value_ptr replaceData(const key_type &key, const DataCacheIfc::value_ptr &data) = 0;
    /**
     * Checks, if there is any data stored for the given key.
     * @param key  The related (unique) key to the data.
     * @return True, for data exists. False, otherwise.
     */
    virtual bool hasData(const key_type &key) const = 0;
    /**
     * Returns the data stored for the given key. Pointer is const, as it shouldn't be changed, because the stored pointer is returned and not a copy of the data.
     * Returned cache item stays unchanged even after the data for the given key changes in the cache. Thus users may (read-)access the data as long as needed without locking.
     * @param key  The related (unique) key to the data.
     * @return The stored data item, or nullptr, if it doesn't exist.
     */
    virtual DataCacheIfc::const_value_ptr getData(const key_type &key) const = 0;
    /**
     * Returns the data stored for the given key and casts the generic DataCacheItem type to the specific type T. Checking, if the cast is valid is only done in debug code. Returned instance is const and shouldn't be changed.
     * Returned cache item stays unchanged even after the data for the given key changes in the cache. Thus users may (read-)access the data as long as needed without locking.
     * @param key  The related (unique) key to the data.
     * @return The stored data item, or nullptr, if it doesn't exist.
     */
    template <typename T>
    QSharedPointer<const T> getTypedData(const key_type &key) const
    {
        const DataCacheIfc::const_value_ptr d = getData(key);
        if (!d.isNull())
        {
            Q_ASSERT(!d.template dynamicCast<const T>().isNull());
        }
        return d.template staticCast<const T>();
    }
    /**
     * Removes the data associated with the given key and returns it.
     * @param key  The related (unique) key to the data.
     * @return The stored data item, or nullptr, if it doesn't exist.
     */
    virtual DataCacheIfc::value_ptr removeData(const key_type &key) = 0;
    /**
     * Removes the data associated with the given key.
     * @param key  The related (unique) key to the data.
     * @return True, if data existed for the given key. False, otherwise.
     */
    virtual bool clearData(const key_type &key) = 0;

    /**
     * Returns the currently used count of bytes in the cache.
     * @return The used bytes.
     */
    virtual size_t getUsedBytes() const = 0;
};

#endif // DATACACHEIFC_H
