// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKEXECUTIONIFC_HPP
#define TASKEXECUTIONIFC_HPP

#include <QRunnable>

/**
 * @brief The TaskExecutionIfc class describes the interface for different execution strategies of a task wrapped into
 * a QRunnable. Each implementation is requested to take over the ownership of the QRunnable pointer passed in
 * executeTask() and delete the instance. This resembles the behavior of QThreadPool.
 */
class TaskExecutionIfc
{
public:
    virtual ~TaskExecutionIfc() = default;

    /**
     * @brief Takes the ownership of r. Thus the caller shouldn't access r after it's passed in here. Then executes
     * r according to the own execution strategy (e.g. in a thread pool) and deletes the pointer afterwards.
     * @param r The QRunnable to execute. Pointer is always deleted afterwards.
     */
    virtual void executeTask(QRunnable *r) = 0;

    /**
     * @brief setMaxThreadPoolSize limits the amount of parallel worker threads. If the implementation of this interface
     * uses threads it limits the count of parallel threads to this value. If it doesn't use additional threads, then
     * this value is ignored. If the value is reduced then then already running tasks are finished before this value
     * is taken into account. Thus, no task is canceled, because of a changed value.
     * @param s The maximum parallel threads to use.
     */
    virtual void setMaxThreadPoolSize(size_t s) = 0;

    /**
     * @brief Shuts the execution instance down. If tasks are still running shutdown() will block until they are
     * finished. Not yet started tasks are deleted.
     */
    virtual void shutdown() = 0;
};

#endif // TASKEXECUTIONIFC_HPP
