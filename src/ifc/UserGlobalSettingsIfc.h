// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef USERGLOBALSETTINGSIFC_HPP
#define USERGLOBALSETTINGSIFC_HPP

#include <QByteArray>
#include <QPoint>
#include <QSize>

/**
 * @brief The UserGlobalSettingsIfc class describes the interface to the profile independent user settings.
 */
class UserGlobalSettingsIfc
{
public:
    virtual ~UserGlobalSettingsIfc() = default;

    virtual QSize getMainWndSize() const = 0;
    virtual void setMainWndSize(const QSize &size) = 0;

    virtual QPoint getMainWndPos() const = 0;
    virtual void setMainWndPos(const QPoint &pos) = 0;

    virtual QByteArray getSplitPos() const = 0;
    virtual void setSplitPos(const QByteArray &pos) = 0;

    virtual QByteArray getFileTableColumWidths() const = 0;
    virtual void setFileTableColumWidths(const QByteArray &widths) = 0;

    virtual QByteArray getStatusColumWidths() const = 0;
    virtual void setStatusColumWidths(const QByteArray &widths) = 0;
};

#endif // USERGLOBALSETTINGSIFC_HPP
