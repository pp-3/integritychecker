// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef USERPROFILESETTINGSIFC_HPP
#define USERPROFILESETTINGSIFC_HPP

#include <QString>
#include <QStringList>

/**
 * @brief The UserProfileSettingsIfc class describes the interface to the profile specific user settings.
 */
class UserProfileSettingsIfc
{
public:
    virtual ~UserProfileSettingsIfc() = default;

    virtual const QString &profileName() const = 0;

    virtual QString getInputDir() const = 0;
    virtual void setInputDir(const QString &dir) = 0;

    virtual QString getOutputDirSelection() const = 0;
    virtual void setOutputDirSelection(const QString &sel) = 0;

    virtual QString getOutputDir() const = 0;
    virtual void setOutputDir(const QString &dir) = 0;

    virtual unsigned int getWorkerThreads() const = 0;
    virtual void setWorkerThreads(unsigned int w) = 0;

    virtual uint getCacheSize() const = 0;
    virtual void setCacheSize(uint c) = 0;

    virtual QStringList getEnabledTypes() const = 0;
    virtual void setEnabledTypes(const QStringList &types) = 0;

    virtual QString getLanguage() const = 0;
    virtual void setLanguage(const QString &lang) = 0;

    virtual QString getProfileDescription() const = 0;
    virtual void setProfileDescription(const QString &desc) = 0;
};

#endif // USERPROFILESETTINGSIFC_HPP
