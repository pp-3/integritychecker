// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef USERSETTINGSMANAGERIFC_HPP
#define USERSETTINGSMANAGERIFC_HPP

#include <QSharedPointer>
#include <QString>
#include <QStringList>

#include "UserGlobalSettingsIfc.h"
#include "UserProfileSettingsIfc.h"

/**
 * @brief The UserSettingsManagerIfc class describes the interface to the UserSettingsManager.
 */
class UserSettingsManagerIfc
{
public:
    virtual ~UserSettingsManagerIfc() = default;

    /**
     * Returns the global settings. Pointer is valid until process ends.
     * @return The global settings.
     */
    virtual const UserGlobalSettingsIfc *getGlobalSettings() const = 0;
    virtual       UserGlobalSettingsIfc *getGlobalSettings() = 0;

    /**
     * Returns the list of stored user profiles.
     * @return The user profiles in the config file.
     */
    virtual QStringList getProfiles() const = 0;
    /**
     * Checks, if a given profile name exists in the config file.
     * @param profile The profile name to check for.
     * @return True, if available. False, otherwise.
     */
    virtual bool existsProfile(const QString &profile) const = 0;
    /**
     * Returns the current user settings. If the shared pointer is stored, it's necessary to listen to the profileChanged() signal, as profile may change during run time.
     * @return The currently chosen profile / user settings.
     */
    virtual QSharedPointer<UserProfileSettingsIfc> getCurrentUserSettings() = 0;
    /**
     * Changes the user settings to a new profile. If profile does not exist, falls back to default.
     * @param profile The profile name to set user settings to.
     * @return The new current user settings.
     */
    virtual QSharedPointer<UserProfileSettingsIfc> setCurrentUserProfile(QString profile) = 0;
    /**
     * Copies current user settings to a new profile. Current profile is kept. New one is returned.
     * @param to The profile name to copy the current user settings to.
     * @return The new profile settings instance.
     */
    virtual QSharedPointer<UserProfileSettingsIfc> copyProfile(const QString &to) = 0;
    /**
     * Renames the current profile to another name. Returns the user settings instance with the new name. Old instance shouldn't be used anymore.
     * @param to The profile name to rename the current user settings to.
     * @return The renamed user settings instance.
     */
    virtual QSharedPointer<UserProfileSettingsIfc> renameProfile(const QString &to) = 0;
    /**
     * Deletes an user settings profile. If it's the current one, the current one gets invalid and the default one is returned.
     * @param profile The profile to delete.
     * @return The current one, if not deleted. The default one, if the current one got deleted.
     */
    virtual QSharedPointer<UserProfileSettingsIfc> deleteProfile(const QString &profile) = 0;
};

#endif // USERSETTINGSMANAGERIFC_HPP
