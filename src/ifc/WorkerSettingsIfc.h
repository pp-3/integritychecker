// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef WORKERSETTINGSIFC_H
#define WORKERSETTINGSIFC_H

#include <QObject>
#include <QPair>
#include <QString>

#include "sum_shared/CheckSumState.h"

/**
 * The WorkerSettingsIfc interface provides access to all settings needed in the worker threads.
 * Source of this information are the UserSettings.
 */
class WorkerSettingsIfc : public QObject
{
    Q_OBJECT

public:
    ~WorkerSettingsIfc() override = default;

    enum OUTPUT_SELECTION { AS_INPUT, OTHER_DIR };

    virtual OUTPUT_SELECTION getOutputDirSelection() const = 0;
    virtual QString getInputDir() const = 0;
    virtual QString getOutputDir() const = 0;
    virtual bool isInputDirValid() const = 0;
    virtual bool isOutputDirValid() const = 0;
    virtual bool areDirsValid() const = 0;
    virtual bool createOutputDir() = 0;
    virtual unsigned int getWorkerThreads() const = 0;
    virtual QPair<unsigned int, unsigned int> getMinMaxWorkerThreads() const = 0;
    virtual size_t getCacheSize() const = 0;
    virtual SumTypeHashList getEnabledCalcHashes() const = 0;

    static QString toString(OUTPUT_SELECTION output_selection)
    {
        switch (output_selection)
        {
        case OUTPUT_SELECTION::AS_INPUT: return "AS_INPUT";
        case OUTPUT_SELECTION::OTHER_DIR: return "OTHER_DIR";
        }

        Q_ASSERT(false);
        std::abort();
    }
    static OUTPUT_SELECTION fromString(const QString &s, bool *found)
    {
        OUTPUT_SELECTION ret = OUTPUT_SELECTION::AS_INPUT;
        bool valid = false;

        if (s.compare(toString(OUTPUT_SELECTION::AS_INPUT), Qt::CaseInsensitive) == 0)
        {
            ret = OUTPUT_SELECTION::AS_INPUT;
            valid = true;
        }
        else if (s.compare(toString(OUTPUT_SELECTION::OTHER_DIR), Qt::CaseInsensitive) == 0)
        {
            ret = OUTPUT_SELECTION::OTHER_DIR;
            valid = true;
        }

        if (found)
            *found = valid;

        return ret;
    }

signals:
    void s_OutputDirSelectionChanged(WorkerSettingsIfc::OUTPUT_SELECTION sel);
    void s_InputDirChanged(const QString &dir);
    void s_OutputDirChanged(const QString &dir);
    void s_WorkerThreadsChanged(unsigned int w);
    void s_CacheSizeChanged(const uint &s);
    void s_EnabledTypeChanged();
};

#endif // WORKERSETTINGSIFC_H
