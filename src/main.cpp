// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>

#include <QApplication>
#include <QScopedPointer>

#include "checksums/InitializeInstances.h"
#include "console/IntegrityConsole.h"
#include "model/IntegrityModel.h"
#include "shared/ExitManager.h"
#include "shared/LanguageSettings.h"
#include "shared/ParameterParser.h"
#include "shared/Tr.h"
#include "shared/UserSettings.h"
#include "thread_util/threadcheck.h"
#include "view/LanguageSelectionDialog.h"
#include "view/MainWindow.h"

int main(int argc, char *argv[])
{
    // create singletons of compiled in check sum calculators
    initializeDefaultInstances();

    // Qt application
    QApplication a(argc, argv);
#ifdef ENABLE_TEST_ADDONS
    // tests are enabled, but main application is executed
    // the check for the additional main thread during testing needs to be informed about the default even loop thread
    const auto application_thread = setMainThread(&a);
#endif

    // read user settings
    QScopedPointer<UserSettingsManager> user_settings(new UserSettingsManager);

    // language
    QScopedPointer<LanguageSettings> lang(new LanguageSettings(user_settings.data()));

    // parse application parameters
    ParameterParser pp;
    if (!pp.parse(argc, argv, lang.data()))
    {
        return -1;
    }

    // change profile, if set in user settings
    if (!pp.getProfile().isEmpty() && !user_settings->existsProfile(pp.getProfile()))
    {
        if (pp.showGui())
        {
            // TODO: question profile
        }
        else
        {
            std::cerr << Tr::tr("Error: unknown profile: ").toStdString() << pp.getProfile().toStdString() << std::endl;
            std::cerr << Tr::tr("Available profiles:").toStdString() << user_settings->getProfiles().join(", ").toStdString() << std::endl;
            return -2;
        }
    }

    // create model
    QScopedPointer<IntegrityModel> pModel(new IntegrityModel(&pp, user_settings.data(), lang.data()));
    pModel->initModel();

    QScopedPointer<MainWindow> w;

    // create GUI
    if (pp.showGui())
    {
        // set language, if not found in user settings
        if (!pp.isLangSet() && !pModel->getLanguageSettings()->isUserConfigured())
        {
            LanguageSelectionDialog dlg(nullptr, pModel->getLanguageSettings());
            dlg.exec();
        }
        w.reset(new MainWindow(pModel.data()));
        w->show();
    }

    // create console
    QScopedPointer<IntegrityConsole> console;
    if (pp.isInteractive())
    {
        console.reset(new IntegrityConsole(pModel.data()));
    }

    // application loop
    const int ret = a.exec();

    // close view and model
    w.reset(nullptr);
    console.reset(nullptr);
    pModel->closeModel();
    pModel.reset(nullptr);
    lang.reset(nullptr);

    // call all exit listeners
    ExitManager::tearDown();

    // close user settings
    user_settings.reset(nullptr);

    return ret;
}
