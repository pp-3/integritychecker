// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @mainpage
 *
 * # IntegrityChecker #
 *
 * ## Introduction ##
 * The IntegrityChecker application's goal is to provide an easy way to create integrity
 * data of important files and use this data later on to verify the important files are
 * unchanged / intact.
 *
 * The application is divided into several parts with distinct topics.
 *
 * The sumtree/ sub directory contains the entities (FileInfo for files and DirInfo for
 * directories) representing the real file system in the application and keeping track
 * of the current application state for each of them.
 *
 * The checksums/ sub directory contains all the classes related to different integrity
 * data types (e.g. MD5 check sums). The SumTypeManager keeps track of all available
 * integrity data types (SumTypeIfc instances). For each integrity data type a
 * SumFileManagerIfc instance is provided which handles the reading and storing of the
 * integrity data to the according files and a SumTypeCalculatorIfc instance for
 * calculating the integrity data.
 *
 * All tasks (computation / file searching) are grouped in the tasks/ sub directory and
 * are intended for parallel execution in back ground threads. Each task (TaskIfc) is
 * managed via a TaskLifeCycle instance which informs users about the current state and
 * offers various Qt signals for the state changes.
 *
 * The single IntegrityModel instance wraps all the provided services ( WorkerSettings,
 * UserSettingsManager, ErrorCollector, TaskDispatcher, DataCache, RunningTasks ) and is
 * therefore the root of the application model.
 *
 * The view/ sub directory contains the main window and all application dialogues. The
 * connection with the model is done by several presenters in the presenter/ directory.
 * They are managed in the PresenterManager instance, which passes the required view
 * instances into each presenter.
 */
