// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "AsyncCleanUp.h"

#include <sum_tree/DirInfoRoot.h>
#include <tasks/TaskLifeCycle.h>

#include "IntegrityModel.h"

void AsyncCleanUp::releaseInstanceAfterFinished(const QList<TaskLifeCycle::ptr> &tasks, IntegrityModel *pModel)
{
    DirInfoRoot::ptr root = pModel->getCurrentRoot();
    if (root)
    {
        for (const TaskLifeCycle::ptr &lf : tasks)
        {
            if (lf)
            {
                lf->addFinishedCallback(
                    [lf](bool /*canceled*/) -> void
                    {
                        // holds lf instance until next event loop
                        deleteLater(lf);
                    },
                    pModel);
            }
        }
    }
}

void AsyncCleanUp::cancelAndReleaseInstanceAfterFinished(const QList<TaskLifeCycle::ptr> &tasks, IntegrityModel *pModel)
{
    for (const TaskLifeCycle::ptr &lf : tasks)
        if (lf)
            lf->cancelTask();

    releaseInstanceAfterFinished(tasks, pModel);
}
