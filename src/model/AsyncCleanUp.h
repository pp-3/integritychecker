// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ASYNCCLEANUP_H
#define ASYNCCLEANUP_H

#include <QList>
#include <QSharedPointer>

#include <tasks/TaskLifeCycle.h>

class IntegrityModel;

/**
 * Utility class which releases the given task life cycle instance after the event loop finishes.
 */
class DeleteTaskLater : QObject
{
    Q_OBJECT

    friend class AsyncCleanUp;

private:
    explicit DeleteTaskLater(const TaskLifeCycle::ptr &task) :
        QObject(nullptr),
        m_Task(task)
    {
        QObject::deleteLater();
    }
    virtual ~DeleteTaskLater()
    {
        m_Task.clear();
    }

private:
    TaskLifeCycle::ptr m_Task;
};

/**
 * The AsyncCleanUp class provides clean up functions for the tree which are run later. Either
 * after the current running tasks have finished or later in the event loop.
 */
class AsyncCleanUp
{
public:
    /**
     * Holds the TaskLifeCycle shared pointers until the managed tasks have finished.
     * @param tasks The tasks to wait for.
     * @param pModel The model.
     */
    static void releaseInstanceAfterFinished(const QList<TaskLifeCycle::ptr> &tasks, IntegrityModel *pModel);
    /**
     * Cancels all given tasks and releases the TaskLifeCycle shared pointers after the managed tasks have finished.
     * @param tasks The task list to cancel.
     * @param pModel The model with the current tree root.
     */
    static void cancelAndReleaseInstanceAfterFinished(const QList<TaskLifeCycle::ptr> &tasks, IntegrityModel *pModel);
    /**
     * Releases the given task cycle instance in the next event loop.
     * @param task The task to release.
     */
    static void deleteLater(const TaskLifeCycle::ptr &task)
    {
        new DeleteTaskLater(task);
    }
};

#endif // ASYNCCLEANUP_H
