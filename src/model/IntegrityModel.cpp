// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "IntegrityModel.h"

#include "ifc/ApplicationParameterParserIfc.h"
#include "shared/DataCache.h"
#include "shared/ErrorCollector.h"
#include "shared/LanguageSettings.h"
#include "shared/UserSettings.h"
#include "sum_tree/FileInfo.h"
#include "sum_tree/InfoBase.h"
#include "tasks/CheckForCheckSum.h"
#include "tasks/ReScanFsTask.h"
#include "tasks/ScanFsTask.h"
#include "tasks/ThreadPoolExecution.h"
#include "thread_util/threadcheck.h"
#include "worker/StaticInitializer.h"
#include "worker/WorkerSettings.h"

#include "AsyncCleanUp.h"
#include "RunningTasks.h"
#include "TaskDispatcher.h"

IntegrityModel::IntegrityModel(ApplicationParameterParserIfc *pp,
                               UserSettingsManager           *user_sett,
                               LanguageSettings              *lang) :
    pPP(pp),
    pUserSettingsManager(user_sett),
    pLanguageSettings(lang),
    m_pSettings(),
    m_pTaskDispatcher(),
    m_pErrorInstance(new ErrorCollector),
    m_pCache(),
    m_pRunningTasks(),
    m_FileSysRoot(),
    m_CurrentTasks(),
    m_pLastCheckExistenceTask()
{
    qRegisterMetaType<DirInfoRoot::ptr>("DirInfoRoot::ptr");
}

IntegrityModel::~IntegrityModel()
{
    Q_ASSERT(m_CurrentTasks.isEmpty());

    m_pTaskDispatcher.reset(nullptr);
    m_pRunningTasks.reset(nullptr);
    m_pCache.reset(nullptr);
    m_pSettings.reset(nullptr);
}

void IntegrityModel::initModel()
{
    // initialize settings
    if (!pPP->getProfile().isEmpty())
    {
        getUserSettingsManager()->setCurrentUserProfile(pPP->getProfile());
    }

    m_pSettings.reset(new WorkerSettings(getUserSettingsManager(), getErrorInstance()));

    m_pTaskDispatcher.reset(new TaskDispatcher(getWorkerSettings(), std::make_unique<ThreadPoolExecution>()));
    m_pCache.reset(new DataCache(getUserSettingsManager(), getTaskDispatcher()));

    StaticInitializer::getInstance().populateSettings(getWorkerSettings(), &getDataCache(), getErrorInstance());

    m_FileSysRoot = DirInfoRoot::ptr::create(getWorkerSettings()->getInputDir());
    m_pRunningTasks.reset(new RunningTasks(this));

    connect(m_pSettings.data(), &WorkerSettings::s_InputDirChanged,           this, &IntegrityModel::rebuildFsTree);
    connect(m_pSettings.data(), &WorkerSettings::s_OutputDirChanged,          this, &IntegrityModel::rebuildFsTree);
    connect(m_pSettings.data(), &WorkerSettings::s_OutputDirSelectionChanged, this, &IntegrityModel::rebuildFsTree);
    connect(m_pSettings.data(), &WorkerSettings::s_EnabledTypeChanged,        this, &IntegrityModel::findExistingChecksums);

    // start scan after gui is up
    QTimer::singleShot(100, this, &IntegrityModel::rebuildFsTree);
}

void IntegrityModel::closeModel()
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    m_pCache->close();

    // stop tasks
    m_pLastCheckExistenceTask.clear();
    getRunningTasks()->shutdown();
    m_pTaskDispatcher->shutdown();

    setRootDir(DirInfoRoot::ptr());
}

void IntegrityModel::setRootDir(const DirInfoRoot::ptr &new_root)
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    if (m_FileSysRoot != new_root)
    {
        m_FileSysRoot = new_root;

        emit dirRootChanged(m_FileSysRoot);

        if (m_FileSysRoot)
        {
            findExistingChecksums();
        }
    }
}

void IntegrityModel::rebuildFsTree()
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    AsyncCleanUp::cancelAndReleaseInstanceAfterFinished(m_CurrentTasks, this);
    m_CurrentTasks.clear();
    m_pLastCheckExistenceTask.clear();
    getRunningTasks()->cancelAll();

    setRootDir(DirInfoRoot::ptr());
    scanFs();
}

void IntegrityModel::findExistingChecksums()
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    DirInfoRoot::ptr root = getCurrentRoot();
    if (root)
    {
        root->cleanFromUnusedIntegrityTypes(getWorkerSettings()->getEnabledCalcHashes());

        checkForExistingCheckSum(m_FileSysRoot->getRoot(), true);
        // avoid initial check gets canceled
        m_pLastCheckExistenceTask.clear();
    }
}

void IntegrityModel::scanFs()
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    QSharedPointer<ScanFsTask> task(new ScanFsTask(getWorkerSettings()->getInputDir(),
                                                   getErrorInstance()));
    TaskResult<DirInfoRoot::ptr>::ptr ret = task->getTaskResult();
    TaskLifeCycle::ptr lf = m_pTaskDispatcher->addTask(task.dynamicCast<TaskIfc>());
    m_CurrentTasks << lf;

    lf->addFinishedCallback(
        [this, lf, ret](bool canceled) -> void
        {
            m_CurrentTasks.removeOne(lf);
            if (canceled || !ret->hasResult())
            {
                setRootDir(DirInfoRoot::ptr());
                return;
            }

            setRootDir(ret->getResult());
        },
        this);
}

TaskLifeCycle::ptr IntegrityModel::updateFs(const DirInfo::weak_ptr &dir, bool recursive)
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    {
        DirInfo::ptr di = dir.toStrongRef();
        if (!m_FileSysRoot || !di)
            return TaskLifeCycle::ptr();
    }

    QSharedPointer<ReScanFsTask> task(
        new ReScanFsTask(m_FileSysRoot,
                         getWorkerSettings()->getInputDir(),
                         dir,
                         recursive,
                         getWorkerSettings()->getEnabledCalcHashes(),
                         getErrorInstance()));
    AsyncTreeResultListResult ret = task->getTaskResult();
    TaskLifeCycle::ptr lf = m_pTaskDispatcher->addTask(task.dynamicCast<TaskIfc>());
    m_CurrentTasks << lf;

    emit dirContentScanStarted(m_FileSysRoot, dir, lf);

    const DirInfoRoot::ptr root(m_FileSysRoot);
    lf->addFinishedCallback(
        [this, root, dir, ret, recursive, lf](bool canceled) -> void
        {
            m_CurrentTasks.removeOne(lf);

            if (ret->hasResult() && root == m_FileSysRoot)
            {
                const AsyncTreeResultList res = ret->getResult();
                for (const AsyncTreeResult &it : res)
                {
                    InfoBase::ptr ib = !it->hasResult() ? nullptr : it->getResult().toStrongRef();
                    if (ib && ib->isDir())
                    {
                        DirInfo::ptr d = DirInfo::cast(ib);
                        emit dirContentChanged(root, d);

                        if (!canceled)
                        {
                            // directory changed -> check it for changed checksums
                            checkForExistingCheckSum(d, recursive);
                        }
                    }
                }
            }

            dirContentScanFinished(root, dir);
        },
        this);

    return lf;
}

IntegrityModel::AsyncTreeResultListQuery IntegrityModel::checkForExistingCheckSum(const DirInfo::weak_ptr &dir, bool recursive)
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    {
        DirInfo::ptr di = dir.toStrongRef();

        if (!m_FileSysRoot || !di)
            return AsyncTreeResultListQuery::createInvalid();
    }

    // interrupt an ongoing checksum search, if a new one is requested
    if (m_pLastCheckExistenceTask)
    {
        QList<TaskLifeCycle::ptr> l;
        l << std::move(m_pLastCheckExistenceTask);
        m_pLastCheckExistenceTask.clear();

        AsyncCleanUp::cancelAndReleaseInstanceAfterFinished(l, this);
    }

    QSharedPointer<CheckForCheckSum> task(
        new CheckForCheckSum(getWorkerSettings()->getInputDir(),
                             dir,
                             getWorkerSettings()->getEnabledCalcHashes(),
                             recursive,
                             CheckForCheckSum::EXISTS,
                             getErrorInstance()));
    AsyncTreeResultListResult ret = task->getTaskResult();
    TaskLifeCycle::ptr lf = m_pTaskDispatcher->addTask(task.dynamicCast<TaskIfc>());
    m_pLastCheckExistenceTask = lf;
    m_CurrentTasks << lf;

    emit checksumSearchStarted(m_FileSysRoot, dir, lf);

    const DirInfoRoot::ptr current = m_FileSysRoot;
    lf->addFinishedCallback(
        [this, ret, lf, current, dir](bool /*canceled*/) -> void
        {
            m_CurrentTasks.removeOne(lf);
            if (m_pLastCheckExistenceTask == lf)
            {
                m_pLastCheckExistenceTask.clear();
            }
            if (!ret->hasResult() || m_FileSysRoot!=current)
            {
                return;
            }

            emit checksumSearchFinished(current, dir);
        },
        this);

    return AsyncTreeResultListQuery(lf, ret);
}

IntegrityModel::AsyncTreeResultListQuery IntegrityModel::compareCheckSum(const InfoBase::weak_ptr &node,
                                                                         bool recursive,
                                                                         const SumTypeHashList &check_types)
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    {
        InfoBase::ptr ib = node.toStrongRef();

        if (!m_FileSysRoot || !ib)
            return AsyncTreeResultListQuery::createInvalid();
    }

    QSharedPointer<CheckForCheckSum> task(
        new CheckForCheckSum(getWorkerSettings()->getInputDir(),
                             node,
                             check_types,
                             recursive,
                             CheckForCheckSum::COMPARE,
                             getErrorInstance()));
    AsyncTreeResultListResult ret = task->getTaskResult();
    TaskLifeCycle::ptr lf = m_pTaskDispatcher->addTask(task.dynamicCast<TaskIfc>());
    m_CurrentTasks << lf;

    emit comparisonStarted(m_FileSysRoot, node, lf);

    const DirInfoRoot::ptr root(m_FileSysRoot);
    lf->addFinishedCallback(
        [this, root, node, lf](bool /*canceled*/) -> void
        {
            m_CurrentTasks.removeOne(lf);

            emit comparisonFinished(root, node);
        },
        this);

    return AsyncTreeResultListQuery(lf, ret);
}

IntegrityModel::AsyncTreeResultListQuery IntegrityModel::createCheckSum(const InfoBase::weak_ptr &node,
                                                                        bool recursive,
                                                                        const SumTypeHashList &check_types)
{
    Q_ASSERT(isInMainThread());   // call from main thread only

    {
        InfoBase::ptr ib = node.toStrongRef();

        if (!m_FileSysRoot || !ib)
            return AsyncTreeResultListQuery::createInvalid();
    }

    QSharedPointer<CheckForCheckSum> task(
        new CheckForCheckSum(getWorkerSettings()->getInputDir(),
                             node,
                             check_types,
                             recursive,
                             CheckForCheckSum::CALCULATE,
                             getErrorInstance()));
    AsyncTreeResultListResult ret = task->getTaskResult();
    TaskLifeCycle::ptr lf = m_pTaskDispatcher->addTask(task.dynamicCast<TaskIfc>());
    m_CurrentTasks << lf;

    emit calculationStarted(m_FileSysRoot, node, lf);

    const DirInfoRoot::ptr root(m_FileSysRoot);
    lf->addFinishedCallback(
        [this, root, node, lf](bool /*canceled*/) -> void
        {
            m_CurrentTasks.removeOne(lf);

            emit calculationFinished(root, node);
        },
        this);

    return AsyncTreeResultListQuery(lf, ret);
}
