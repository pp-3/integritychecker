// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INTEGRITYMODEL_H
#define INTEGRITYMODEL_H

#include <QScopedPointer>

#include "IntegrityModelIfc.h"

class IntegrityModel : public IntegrityModelIfc
{
    Q_OBJECT

public:
    explicit IntegrityModel(ApplicationParameterParserIfc *pp,
                            UserSettingsManager           *user_sett,
                            LanguageSettings              *lang);
    IntegrityModel(const IntegrityModel&) = delete;
    virtual ~IntegrityModel() override;

    void initModel();
    void closeModel();

    DirInfoRoot::ptr getCurrentRoot() const override { return m_FileSysRoot; }

    const ApplicationParameterParserIfc *getParameters() const override     { return pPP; }
    const WorkerSettings                *getWorkerSettings() const override { return m_pSettings.data(); }
          WorkerSettings                *getWorkerSettings() override       { return m_pSettings.data(); }
          UserSettingsManager           *getUserSettingsManager() override  { return pUserSettingsManager; }
          ErrorCollector                *getErrorInstance() override        { return m_pErrorInstance.data(); }
    const TaskDispatcher                *getTaskDispatcher() const override { return m_pTaskDispatcher.data(); }
          TaskDispatcher                *getTaskDispatcher() override       { return m_pTaskDispatcher.data(); }
          DataCache                     &getDataCache() override            { return *m_pCache; }
          RunningTasks                  *getRunningTasks() override         { return m_pRunningTasks.data(); }
          LanguageSettings              *getLanguageSettings() override     { return pLanguageSettings; }

    TaskLifeCycle::ptr updateFs(const DirInfo::weak_ptr &dir, bool recursive) override;
    AsyncTreeResultListQuery checkForExistingCheckSum(const DirInfo::weak_ptr &dir, bool recursive) override;
    AsyncTreeResultListQuery compareCheckSum(const InfoBase::weak_ptr &node,
                                             bool recursive,
                                             const SumTypeHashList &check_types) override;
    AsyncTreeResultListQuery createCheckSum(const InfoBase::weak_ptr &node,
                                            bool recursive,
                                            const SumTypeHashList &check_types) override;

private:
    void setRootDir(const DirInfoRoot::ptr &new_root);
    void scanFs();

private slots:
    void rebuildFsTree();
    void findExistingChecksums();

private:
    ApplicationParameterParserIfc      *pPP;
    UserSettingsManager                *pUserSettingsManager;
    LanguageSettings                   *pLanguageSettings;
    QScopedPointer<WorkerSettings>      m_pSettings;
    QScopedPointer<TaskDispatcher>      m_pTaskDispatcher;
    QScopedPointer<ErrorCollector>      m_pErrorInstance;
    QScopedPointer<DataCache>           m_pCache;
    QScopedPointer<RunningTasks>        m_pRunningTasks;
    QScopedPointer<LanguageSettings>    m_pLanguageSettings;
    DirInfoRoot::ptr                    m_FileSysRoot;

    QList<TaskLifeCycle::ptr> m_CurrentTasks;
    TaskLifeCycle::ptr        m_pLastCheckExistenceTask; // current task checking for existing checksum files
};

#endif // INTEGRITYMODEL_H
