// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INTEGRITYMODELIFC_H
#define INTEGRITYMODELIFC_H

#include <QObject>

#include "sum_shared/CheckSumState.h"
#include "sum_tree/DirInfoRoot.h"
#include "tasks/TaskLifeCycle.h"
#include "tasks/TaskResult.h"
#include "tasks/TaskResultQuery.h"
#include "worker/WorkerSettings.h"

class ApplicationParameterParserIfc;
class UserSettingsManager;
class SumTypeManager;
class TaskDispatcher;
class ErrorCollector;
class DataCache;
class RunningTasks;
class LanguageSettings;
class FileInfo;

/**
 * @brief The IntegrityModelIfc class provides the interface to the main model.
 */
class IntegrityModelIfc : public QObject
{
    Q_OBJECT

public:
    using AsyncTreeResult = TaskResult<InfoBase::weak_ptr>::ptr;
    using AsyncTreeResultList = QList<AsyncTreeResult>;
    using AsyncTreeResultListResult = TaskResult<AsyncTreeResultList>::ptr;
    using AsyncTreeResultListQuery = TaskResultQuery<AsyncTreeResultList>;

    ~IntegrityModelIfc() override = default;

    virtual DirInfoRoot::ptr getCurrentRoot() const = 0;

    virtual const ApplicationParameterParserIfc *getParameters() const = 0;
    virtual const WorkerSettings                *getWorkerSettings() const = 0;
    virtual       WorkerSettings                *getWorkerSettings() = 0;
    virtual       UserSettingsManager           *getUserSettingsManager() = 0;
    virtual       ErrorCollector                *getErrorInstance() = 0;
    virtual const TaskDispatcher                *getTaskDispatcher() const = 0;
    virtual       TaskDispatcher                *getTaskDispatcher() = 0;
    virtual       DataCache                     &getDataCache() = 0;
    virtual       RunningTasks                  *getRunningTasks() = 0;
    virtual       LanguageSettings              *getLanguageSettings() = 0;

    virtual TaskLifeCycle::ptr updateFs(const DirInfo::weak_ptr &dir,
                                        bool recursive) = 0;
    virtual AsyncTreeResultListQuery checkForExistingCheckSum(const DirInfo::weak_ptr &dir,
                                                              bool recursive) = 0;
    virtual AsyncTreeResultListQuery compareCheckSum(const InfoBase::weak_ptr &node,
                                                     bool recursive,
                                                     const SumTypeHashList &check_types) = 0;
    virtual AsyncTreeResultListQuery createCheckSum(const InfoBase::weak_ptr &node,
                                                    bool recursive,
                                                    const SumTypeHashList &check_types) = 0;

signals:
    void dirRootChanged(const DirInfoRoot::ptr &root);
    void dirContentScanStarted(const DirInfoRoot::ptr &root, const DirInfo::weak_ptr &dir, const TaskLifeCycle::ptr &lifecycle);
    void dirContentChanged(const DirInfoRoot::ptr &root, const DirInfo::weak_ptr &dir);
    void dirContentScanFinished(const DirInfoRoot::ptr &root, const DirInfo::weak_ptr &dir);
    void checksumSearchStarted(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node, const TaskLifeCycle::ptr &lifecycle);
    void checksumSearchFinished(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node);
    void comparisonStarted(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node, const TaskLifeCycle::ptr &lifecycle);
    void comparisonFinished(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node);
    void calculationStarted(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node, const TaskLifeCycle::ptr &lifecycle);
    void calculationFinished(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node);
};

#endif // INTEGRITYMODELIFC_H
