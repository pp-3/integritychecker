// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "RunningTasks.h"

#include <iostream>

#include <QMutexLocker>

#include <tasks/TaskLifeCycle.h>

#include "AsyncCleanUp.h"
#include "IntegrityModel.h"
#include "TaskDispatcher.h"

RunningTasks::RunningTasks(IntegrityModel *model) :
    QObject(nullptr),
    pModel(model)
{
    initializeController();
}

void RunningTasks::initializeController()
{
    connect(pModel->getTaskDispatcher(), &TaskDispatcher::taskAdded,
            this,                        &RunningTasks::taskAdded, Qt::DirectConnection);
}

QList<TaskLifeCycle::ptr> RunningTasks::currentTasks() const
{
    QMutexLocker ml(&m_Lock);
    return m_Tasks.keys();
}

void RunningTasks::shutdown()
{
    cancelAll();

    bool wait = true;
    do
    {
        QMutexLocker ml(&m_Lock);
        TaskLifeCycle::ptr t;
        if (!m_Tasks.isEmpty())
        {
            t = m_Tasks.firstKey();
            const std::optional<QMetaObject::Connection> connection = std::as_const(m_Tasks)[t];
            if (connection)
                QObject::disconnect(*connection);
            m_Tasks.remove(t);
        }
        ml.unlock();

        if (!t.isNull())
        {
            t->waitForTaskFinished(nullptr);
            Q_ASSERT(t->isFinishedOrCanceled(nullptr));
        }

        ml.relock();
        wait = !m_Tasks.isEmpty();
        ml.unlock();
    } while (wait);
}

void RunningTasks::cancelAll()
{
    for (const TaskLifeCycle::ptr &it : currentTasks())
        it->cancelTask();
}

void RunningTasks::taskAdded(const TaskLifeCycle::ptr &task)
{
    const auto finished_callback = [this, task](bool /*canceled*/)
    {
        taskFinished(task);
    };

    const TaskLifeCycle::BoolWithConnection ret = task->addFinishedCallback(finished_callback, this);
    QMutexLocker ml(&m_Lock);
    if (!ret.first)
        m_Tasks.insert(task, ret.second);
    ml.unlock();
}

void RunningTasks::taskFinished(const TaskLifeCycle::ptr &task)
{
    QMutexLocker ml(&m_Lock);
    const bool b = m_Tasks.remove(task) == 1;

#ifdef QT_DEBUG
    // race between a task finishing and shutdown() trying to cancel that task
    // TODO: clean up
    if (!b)
        std::cout << "task finished, but was already removed: " << task->getName().toStdString() << std::endl;
#else
    (void)b;
#endif

    ml.unlock();
    AsyncCleanUp::deleteLater(task);
}
