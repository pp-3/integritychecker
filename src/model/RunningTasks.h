// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef RUNNINGTASKS_H
#define RUNNINGTASKS_H

#include <QMap>
#include <QMutex>
#include <QObject>

#include <tasks/TaskLifeCycle.h>

class IntegrityModel;

/**
 * The RunningTasks class monitors all currently running background tasks.
 */
class RunningTasks : public QObject
{
    Q_OBJECT

public:
    explicit RunningTasks(IntegrityModel *model);

    QList<TaskLifeCycle::ptr> currentTasks() const;
    void shutdown();
    void cancelAll();

private:
    void initializeController();

signals:

private slots:
    void taskAdded(const TaskLifeCycle::ptr &task);
    void taskFinished(const TaskLifeCycle::ptr &task);

private:
    IntegrityModel *pModel;
    mutable QMutex  m_Lock;
    /**
     * Data stored for each running task:
     * - the TaskLifeCycle instance
     * - the task finished callback connection
     */
    QMap<TaskLifeCycle::ptr, std::optional<QMetaObject::Connection>> m_Tasks;
};

#endif // RUNNINGTASKS_H
