// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TaskDispatcher.h"

#include <QThread>

#include "ifc/WorkerSettingsIfc.h"
#include "tasks/ParentTaskIfc.h"
#include "tasks/TaskIfc.h"
#include "tasks/TaskLifeCycle.h"
#include "thread_util/threadcheck.h"

TaskDispatcher::TaskDispatcher(WorkerSettingsIfc *const sett, std::unique_ptr<TaskExecutionIfc> &&executor) :
    pSettings(sett),
    taskExecutor(std::move(executor))
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only
    Q_ASSERT(taskExecutor);

    initialize();
}

void TaskDispatcher::shutdown()
{
    taskExecutor->shutdown();
    taskExecutor.reset();
}

void TaskDispatcher::initialize()
{
    setThreadPoolSize(pSettings->getWorkerThreads());

    connect(this, &TaskDispatcher::t_taskAdded, this, &TaskDispatcher::taskAdded, Qt::QueuedConnection);
    connect(pSettings, &WorkerSettingsIfc::s_WorkerThreadsChanged, this, &TaskDispatcher::setThreadPoolSize);
}

void TaskDispatcher::setThreadPoolSize(size_t s)
{
    taskExecutor->setMaxThreadPoolSize(s);
}

TaskLifeCycle::ptr TaskDispatcher::addTask(const QSharedPointer<TaskIfc> &task)
{
    TaskLifeCycle::ptr ret(new TaskLifeCycle(task));
    addTask(ret);
    return ret;
}

void TaskDispatcher::addTask(const TaskLifeCycle::ptr &task_lf)
{
    Q_ASSERT(task_lf);
    if (!task_lf)
        return;

    const QSharedPointer<TaskIfc> &t = task_lf->getTask();

    if (!t->isInternal())
    {
        // ensure signal is sent via gui thread
        if (isInMainThread())
        {
            emit taskAdded(task_lf, t->getName());
        }
        else
        {
            emit t_taskAdded(task_lf, t->getName());
        }
    }

    ParentTaskSubTaskRunnerIfc *p = dynamic_cast<ParentTaskSubTaskRunnerIfc *>(t.data());
    if (p)
    {
        TaskLifeCycle::weak_ptr lf_weak(task_lf);
        // if task is a potential parent, set the callback for added sub tasks
        p->setSubTaskRunner(
            [lf_weak, this](const QSharedPointer<TaskIfc> &subtask) -> void
            {
                TaskLifeCycle::ptr tlc = lf_weak.toStrongRef();
                if (!tlc)
                    return;

                taskExecutor->executeTask(new TaskRunnable(subtask, tlc->getLifeCycleStateIfc()));
            });
    }

    taskExecutor->executeTask(new TaskRunnable(t, task_lf->getLifeCycleStateIfc()));
}



TaskRunnable::TaskRunnable(const QSharedPointer<TaskIfc> &task, const LifeCycleStateIfc::ptr &state_ifc) :
    QRunnable(),
    Runnable(task),
    State(state_ifc)
{
    setAutoDelete(true);
    State->gotoCreated();
}

TaskRunnable::~TaskRunnable()
{
    if (Runnable)
    {
        // task didn't start
        State->gotoFinished(true);
        Runnable.clear();
    }
}

void TaskRunnable::run()
{
    QThread::currentThread()->setPriority(QThread::LowestPriority);

    if (!Runnable->shouldCancel())
    {
        State->gotoStarted();

        Runnable->doTask();
    }

    State->gotoFinished(Runnable->shouldCancel());

    Runnable.clear();
}
