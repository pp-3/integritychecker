// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKDISPATCHER_H
#define TASKDISPATCHER_H

#include <memory>

#include <QObject>
#include <QRunnable>
#include <QSharedPointer>

#include "ifc/TaskExecutionIfc.h"
#include "tasks/TaskIfc.h"
#include "tasks/TaskLifeCycle.h"

class WorkerSettingsIfc;

/**
 * @brief The TaskDispatcher class adds a life cycle (started, running, finished) to a single class. The TaskLifeCycle
 * provides interfaces to monitor the current task state.
 */
class TaskDispatcher : public QObject
{
    Q_OBJECT

public:
    explicit TaskDispatcher(WorkerSettingsIfc *const sett, std::unique_ptr<TaskExecutionIfc> &&executor);
    virtual ~TaskDispatcher() override = default;

    void shutdown();

public slots:
    /**
     * Adds a task for execution in a background thread.
     * @param task The task to execute.
     * @return The task management instance.
     */
    TaskLifeCycle::ptr addTask(const QSharedPointer<TaskIfc> &task);

private:
    WorkerSettingsIfc                *pSettings;
    std::unique_ptr<TaskExecutionIfc> taskExecutor;

    void initialize();
    void addTask(const TaskLifeCycle::ptr &task_lf);

signals:
    /**
     * Signals a newly added task. Signal is always sent from the main thread.
     * @param task The newly added task.
     * @param task_name The task name.
     */
    void taskAdded(const TaskLifeCycle::ptr &task, const QString &task_name) const;
    /**
     * Signals a newly added task. Signal is only sent if the sender's thread is not the main thread.
     * Signal is then re-routed to taskAdded.
     * @param task The newly added task.
     * @param task_name The task name.
     */
    void t_taskAdded(const TaskLifeCycle::ptr &task, const QString &task_name) const;

private slots:
    void setThreadPoolSize(size_t s);
};


/**
 * The class TaskRunnable implements the QRunnable interface (e.g. for running it in a thread pool) and
 * calls back all task life cycle hooks in the LifeCycleStateIfc interface allowing the TaskDispatcher
 * to track the current task progress.
 */
class TaskRunnable : public QRunnable
{
public:
    TaskRunnable(const QSharedPointer<TaskIfc> &task, const LifeCycleStateIfc::ptr &state_ifc);
    TaskRunnable(const TaskRunnable &) = delete;
    virtual ~TaskRunnable();

private:
    QSharedPointer<TaskIfc> Runnable;
    LifeCycleStateIfc::ptr  State;

protected:
    void run();
};

#endif // TASKDISPATCHER_H
