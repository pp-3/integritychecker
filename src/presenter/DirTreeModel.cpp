// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DirTreeModel.h"

#include <QHash>
#include <QList>

#include "model/IntegrityModelIfc.h"
#include "sum_tree/InfoBase.h"
#include "sum_tree/TreeUtil.h"

#include "DirTreeItem.h"

DirTreeModel::DirTreeModel(QObject *parent, IntegrityModelIfc *const model) :
    QStandardItemModel(parent),
    pModel(model)
{
}

void DirTreeModel::setRoot(const QWeakPointer<DirInfoRoot> &root)
{
    clear();
    QStandardItem *view_root = invisibleRootItem();
    Q_ASSERT(view_root->rowCount() == 0);

    m_Root = root;

    DirInfoRoot::ptr sh_root = m_Root.toStrongRef();
    if (sh_root)
    {
        rebuildModel(view_root, sh_root->getRoot());
    }
}

void DirTreeModel::rebuildModel(QStandardItem* const view, const InfoBase::ptr &model)
{
    if (model && model->isDir())
    {
        DirTreeItem* it = new DirTreeItem(model);
        view->appendRow(it);

        DirInfo::ptr dir = DirInfo::cast(model);
        const QList<InfoBase::ptr> kids = dir->getChildren();

        for (const InfoBase::ptr& ib : kids)
        {
            rebuildModel(it, ib);
        }
    }
}

bool DirTreeModel::updateDir(DirInfo* dir)
{
    QList<DirInfo *> hier = TreeUtil::getDirHierarchy(dir);

    QStandardItem *view_root = invisibleRootItem();
    dir = nullptr;
    QStandardItem *last_v = nullptr;
    DirInfo       *last_d = nullptr;

    while (!hier.isEmpty() && view_root)
    {
        last_d = dir;
        last_v = view_root;
        dir = hier.takeLast();
        view_root = getChild(dir, view_root);
    }

    if (view_root && dir)
    {
        return syncDirs(dir, view_root);
    }
    else if (last_d && last_v)
    {
        return syncDirs(last_d, last_v);
    }

    Q_ASSERT(false);
    return false;
}

QModelIndex DirTreeModel::getIndex(DirInfo *dir)
{
    QList<DirInfo *> hier = TreeUtil::getDirHierarchy(dir);
    QStandardItem *view_root = invisibleRootItem();
    DirInfo *d = nullptr;

    while (!hier.isEmpty() && view_root)
    {
        d = hier.takeLast();
        view_root = getChild(d, view_root);

        if (dir==d)
        {
            if (view_root)
            {
                return view_root->index();
            }
            else
            {
                return QModelIndex();
            }
        }
    }

    return QModelIndex();
}

QStandardItem *DirTreeModel::getChild(DirInfo *child, QStandardItem *parent)
{
    const QString name = child->getName();

    for (int i=0 ; i<parent->rowCount() ; i++)
    {
        QStandardItem *r = parent->child(i, 0);
        if (r && r->text() == name)
        {
            return r;
        }
    }

    return nullptr;
}

bool DirTreeModel::syncDirs(DirInfo *dir, QStandardItem *node)
{
    bool ret = true;

    // dir kidz
    QHash<QString, DirInfo::weak_ptr> dir_ch;
    if (dir)
    {
        for (const InfoBase::ptr &ib : dir->getChildren())
        {
            if (ib && ib->isDir())
            {
                dir_ch.insert(ib->getName(), DirInfo::cast(ib));
            }
        }
    }

    // gui kidz
    QHash<int, QStandardItem *> gui_ch;
    for (int i=0 ; i<node->rowCount() ; i++)
    {
        gui_ch.insert(i, node->child(i, 0));
    }

    // remove equal entries
    for (QHash<int, QStandardItem *>::iterator it=gui_ch.begin() ; it!=gui_ch.end() ; )
    {
        const QString name = it.value()->text();
        if (dir_ch.contains(name))
        {
            DirInfo::weak_ptr diw = dir_ch.value(name);
            syncDirs(diw.toStrongRef().data(), it.value());
            dir_ch.remove(name);
            it = gui_ch.erase(it);
        }
        else
        {
            ++it;
        }
    }

    // delete left over gui items
    QList<int> del_rows = gui_ch.keys();
    gui_ch.clear();
    std::sort(del_rows.begin(), del_rows.end());

    while (!del_rows.isEmpty())
    {
        node->removeRow(del_rows.takeLast());
    }

    // add new children
    for (QHash<QString, DirInfo::weak_ptr>::const_iterator it=dir_ch.constBegin() ; it!=dir_ch.constEnd() ; ++it)
    {
        DirTreeItem* dti = new DirTreeItem(it.value());
        bool ins = false;
        for (int i=0 ; !ins && i<node->rowCount() ; i++)
        {
            if (it.key().compare(node->child(i, 0)->text()) < 0)
            {
                node->insertRow(i, dti);
                ins = true;
            }
        }
        if (!ins)
        {
            node->appendRow(dti);
        }
        ret = ret && syncDirs(it.value().toStrongRef().data(), dti);
    }

    return ret;
}
