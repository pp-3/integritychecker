// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DIRTREEMODEL_H
#define DIRTREEMODEL_H

#include <QModelIndex>
#include <QStandardItem>
#include <QStandardItemModel>

#include "sum_tree/DirInfo.h"
#include "sum_tree/DirInfoRoot.h"
#include "sum_tree/FileInfo.h"

class IntegrityModelIfc;

class DirTreeModel : public QStandardItemModel
{
    Q_OBJECT

public:
    explicit DirTreeModel(QObject *parent, IntegrityModelIfc *const model);
    void setRoot(const DirInfoRoot::weak_ptr &root);
    const QWeakPointer<DirInfoRoot> &getRoot() const { return m_Root; }
    bool updateDir(DirInfo* dir);
    QModelIndex getIndex(DirInfo *dir);

protected:
    virtual Qt::ItemFlags flags(const QModelIndex & index) const
    {
        return QStandardItemModel::flags(index) & ~Qt::ItemIsEditable;
    }

private:
    static void rebuildModel(QStandardItem* const view, const InfoBase::ptr &model);
    static QStandardItem *getChild(DirInfo *child, QStandardItem *parent);
    static bool syncDirs(DirInfo *dir, QStandardItem *node);

signals:

private:
    IntegrityModelIfc    *pModel;
    DirInfoRoot::weak_ptr m_Root;
};

#endif // DIRTREEMODEL_H
