// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DirTreePresenter.h"

#include <QMessageBox>
#include <QTreeView>
#include <Qt>

#include "model/IntegrityModelIfc.h"
#include "tasks/TaskLifeCycle.h"
#include "worker/WorkerSettings.h"

#include "DirTreeItem.h"
#include "DirTreeModel.h"

DirTreePresenter::DirTreePresenter(QTreeView *const dir, IntegrityModelIfc *const model) :
    QObject(),
    pDirView(dir),
    pModel(model)
{
    initialize();
}

void DirTreePresenter::initialize()
{
    DirTreeModel* model = new DirTreeModel(pDirView, pModel);
    pDirView->setModel(model);

    pDirView->setHeaderHidden(true);
    pDirView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    pDirView->setCursor(Qt::ArrowCursor);

    connect(pModel, &IntegrityModelIfc::dirRootChanged, this, &DirTreePresenter::dirRootChanged);
    connect(pModel, &IntegrityModelIfc::dirContentChanged, this, &DirTreePresenter::dirContentChanged);

    buildTreeView(pModel->getCurrentRoot());

    pDirView->setFocus();
}

void DirTreePresenter::dirRootChanged(const DirInfoRoot::ptr &root)
{
    buildTreeView(root);

    QTimer::singleShot(10, this, &DirTreePresenter::selectTop);
}

void DirTreePresenter::selectTop()
{
    DirTreeModel *m = qobject_cast<DirTreeModel*>(pDirView->model());
    if (m && !m->getRoot().isNull())
    {
        QModelIndex top = m->index(0, 0);
        if (top.isValid())
        {
            pDirView->selectionModel()->select(top, QItemSelectionModel::Clear | QItemSelectionModel::SelectCurrent);
        }
    }
}

void DirTreePresenter::dirContentChanged(const DirInfoRoot::ptr &root, const DirInfo::weak_ptr& dir)
{
    DirTreeModel *m = qobject_cast<DirTreeModel*>(pDirView->model());
    QSharedPointer<DirInfoRoot> view_root = m->getRoot().toStrongRef();

    if (view_root && root.data() == view_root.data())
    {
        m->updateDir(dir.toStrongRef().data());
    }
}

void DirTreePresenter::buildTreeView(const DirInfoRoot::ptr &root)
{
    pDirView->selectionModel()->clearSelection();

    DirTreeModel *m = qobject_cast<DirTreeModel*>(pDirView->model());
    m->setRoot(root);
    QModelIndex top = m->index(0, 0);
    pDirView->expand(top);
}

QList<DirInfo::weak_ptr> DirTreePresenter::getSelectedDirs() const
{
    QList<DirInfo::weak_ptr> ret;

    const QModelIndexList sel = pDirView->selectionModel()->selectedIndexes();
    if (sel.size()>0)
    {
        for (const QModelIndex &ind : sel)
        {
            QStandardItem *sit = (qobject_cast<const DirTreeModel *>(ind.model()))->itemFromIndex(ind);
            DirTreeItem *it = static_cast<DirTreeItem *>(sit);
            if (it)
            {
                InfoBase::weak_ptr ibw = it->getTreeData();
                InfoBase::ptr ib = ibw.toStrongRef();
                if (ib && ib->isDir())
                {
                    DirInfo::ptr di = DirInfo::cast(ib);
                    ret << di;
                }
            }
        }
    }
    return ret;
}

void DirTreePresenter::selectAndExpand(const DirInfo::weak_ptr &d)
{
    DirInfo::ptr di = d.toStrongRef();
    if (di)
    {
        DirTreeModel *m = qobject_cast<DirTreeModel*>(pDirView->model());
        QModelIndex ind = m->getIndex(di.data());
        if (ind.isValid())
        {
            pDirView->expand(ind);
            pDirView->selectionModel()->select(ind, QItemSelectionModel::Clear | QItemSelectionModel::SelectCurrent);
            pDirView->scrollTo(ind);
        }
    }
}
