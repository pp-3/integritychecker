// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DIRTREEPRESENTER_H
#define DIRTREEPRESENTER_H

#include <QList>
#include <QObject>
#include <QSharedPointer>
#include <QString>
#include <QTreeView>

#include "sum_tree/DirInfo.h"
#include "sum_tree/DirInfoRoot.h"

class IntegrityModelIfc;
class TaskLifeCycle;

class DirTreePresenter : public QObject
{
    Q_OBJECT

public:
    explicit DirTreePresenter(QTreeView* const dir, IntegrityModelIfc *const model);

    QList<DirInfo::weak_ptr> getSelectedDirs() const;
    void selectAndExpand(const DirInfo::weak_ptr &d);

private:
    void initialize();
    void buildTreeView(const DirInfoRoot::ptr &root);

private:
    QTreeView         *pDirView;
    IntegrityModelIfc *pModel;

signals:

public slots:

private slots:
    void dirRootChanged(const DirInfoRoot::ptr &root);
    void selectTop();
    void dirContentChanged(const DirInfoRoot::ptr &root, const DirInfo::weak_ptr &dir);
};

#endif // DIRTREEPRESENTER_H
