// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "FileDirSelectionManager.h"

#include <algorithm>

#include "model/IntegrityModelIfc.h"
#include "view/FileInfoDialog.h"
#include "worker/WorkerSettings.h"

#include "DirTreePresenter.h"
#include "FileViewPresenter.h"

FileDirSelectionManager::FileDirSelectionManager(QTableView        *const file_view,
                                                 QTreeView         *const dir_view,
                                                 QPushButton       *const info,
                                                 QPushButton       *const check_sum,
                                                 QPushButton       *const calc_sum,
                                                 DirTreePresenter  *const dirp,
                                                 FileViewPresenter *const filep,
                                                 IntegrityModelIfc *const model) :
    QObject(),
    pFileView(file_view),
    pDirView(dir_view),
    pInfoButton(info),
    pCheckSumButton(check_sum),
    pCalcSumButton(calc_sum),
    pDirPresenter(dirp),
    pFilePresenter(filep),
    pModel(model),
    m_CurrentSelections()
{
    initialize();
    initializeController();
}

void FileDirSelectionManager::initialize()
{
    clearSelection();
}

void FileDirSelectionManager::initializeController()
{
    connect(pDirView->selectionModel(),  &QItemSelectionModel::selectionChanged, this, &FileDirSelectionManager::dirTreeSelectionChanged);
    connect(pFileView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FileDirSelectionManager::fileViewSelectionChanged);
    connect(pCheckSumButton, &QPushButton::clicked, this, &FileDirSelectionManager::checkSelectedSum);
    connect(pCalcSumButton,  &QPushButton::clicked, this, &FileDirSelectionManager::calculateSelectedSum);
    connect(pInfoButton, &QPushButton::clicked,     this, &FileDirSelectionManager::showInfo);
    connect(pFileView, &QTableView::doubleClicked,  this, &FileDirSelectionManager::fileViewDoubleClicked);
}

void FileDirSelectionManager::clearSelection()
{
    m_CurrentSelections.clear();
    pInfoButton->setEnabled(false);
    pCheckSumButton->setEnabled(false);
    pCalcSumButton->setEnabled(false);
}

void FileDirSelectionManager::enableButtons()
{
    const bool any_sel_exist = !m_CurrentSelections.isEmpty();

    using val = QPair<InfoBase::weak_ptr, SumTypeHash>;
    const bool sel_with_sum_exist =
        std::any_of(m_CurrentSelections.constBegin(),
                    m_CurrentSelections.constEnd(),
                    [](const val &v) -> bool
                    {
                        InfoBase::ptr ib = v.first.toStrongRef();
                        if (!ib)
                            return false;

                        const SumTypeHash &check_sum_hash = v.second;

                        constexpr static std::array<CheckSumState::States, 3> STATES_WITH_SUM = {
                            CheckSumState::States::EXISTS, CheckSumState::States::VERIFIED, CheckSumState::States::CHECK_ERROR};

                        return std::any_of(STATES_WITH_SUM.cbegin(),
                                           STATES_WITH_SUM.cend(),
                                           [&ib, &check_sum_hash](const CheckSumState::States &state) -> bool
                                           {
                                               return ib->getSumCount(check_sum_hash, state) > 0;
                                           });
                    });

    pInfoButton->setEnabled(sel_with_sum_exist);
    pCheckSumButton->setEnabled(sel_with_sum_exist);
    pCalcSumButton->setEnabled(any_sel_exist);
}

void FileDirSelectionManager::dirTreeSelectionChanged()
{
    m_CurrentSelections.clear();

    for (const DirInfo::weak_ptr &it : pDirPresenter->getSelectedDirs())
    {
        rescanDir(it);

        for (const SumTypeHash it2 : pModel->getWorkerSettings()->getEnabledCalcHashes())
        {
            m_CurrentSelections << QPair<InfoBase::weak_ptr, SumTypeHash>(it,it2);
        }
    }

    enableButtons();
}

void FileDirSelectionManager::fileViewSelectionChanged()
{
    m_CurrentSelections = pFilePresenter->getSelectedFiles();

    if (m_CurrentSelections.isEmpty())
    {
        dirTreeSelectionChanged();
    }
    else
    {
        enableButtons();
    }
}

void FileDirSelectionManager::checkSelectedSum()
{
    for (const QPair<InfoBase::weak_ptr, SumTypeHash> &it : m_CurrentSelections)
    {
        SumTypeHashList check_types;
        check_types.insert(it.second);
        pModel->compareCheckSum(it.first, true, check_types);
    }
}

void FileDirSelectionManager::calculateSelectedSum()
{
    for (const QPair<InfoBase::weak_ptr, SumTypeHash> &it : m_CurrentSelections)
    {
        SumTypeHashList check_types;
        check_types.insert(it.second);
        pModel->createCheckSum(it.first, true, check_types);
    }
}

void FileDirSelectionManager::showInfo()
{
    FileInfoDialog dlg(m_CurrentSelections, pFileView);
    dlg.exec();
}

void FileDirSelectionManager::fileViewDoubleClicked(const QModelIndex &index)
{
    if (index.isValid() && index.column()==0 && !m_CurrentSelections.isEmpty())
    {
        // first column dives into directories
        InfoBase::weak_ptr ibw = m_CurrentSelections.first().first;
        InfoBase::ptr ib = ibw.toStrongRef();

        if (ib && ib->isDir())
        {
            pDirPresenter->selectAndExpand(DirInfo::cast(ib));
            return;
        }
    }

    showInfo();
}

void FileDirSelectionManager::rescanDir(const DirInfo::weak_ptr &dir)
{
    pModel->updateFs(dir, false);
}
