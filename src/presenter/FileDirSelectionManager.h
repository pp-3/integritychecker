// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FILEDIRSELECTIONMANAGER_H
#define FILEDIRSELECTIONMANAGER_H

#include <QObject>
#include <QPushButton>
#include <QTableView>
#include <QTreeView>

#include "sum_tree/DirInfo.h"
#include "sum_tree/InfoBase.h"

class DirTreePresenter;
class FileViewPresenter;
class IntegrityModelIfc;

class FileDirSelectionManager : public QObject
{
    Q_OBJECT

public:
    explicit FileDirSelectionManager(QTableView        *const file_view,
                                     QTreeView         *const dir_view,
                                     QPushButton       *const info,
                                     QPushButton       *const check_sum,
                                     QPushButton       *const calc_sum,
                                     DirTreePresenter  *const dirp,
                                     FileViewPresenter *const filep,
                                     IntegrityModelIfc *const model);

private:
    void initialize();
    void initializeController();
    void enableButtons();
    void rescanDir(const DirInfo::weak_ptr &dir);

private:
    QTableView        *pFileView;
    QTreeView         *pDirView;
    QPushButton       *pInfoButton;
    QPushButton       *pCheckSumButton;
    QPushButton       *pCalcSumButton;
    DirTreePresenter  *pDirPresenter;
    FileViewPresenter *pFilePresenter;
    IntegrityModelIfc *pModel;
    QList<QPair<InfoBase::weak_ptr, SumTypeHash>> m_CurrentSelections;

signals:

private slots:
    void clearSelection();
    void dirTreeSelectionChanged();
    void fileViewSelectionChanged();
    void checkSelectedSum();
    void calculateSelectedSum();
    void showInfo();
    void fileViewDoubleClicked(const QModelIndex &index);
};

#endif // FILEDIRSELECTIONMANAGER_H
