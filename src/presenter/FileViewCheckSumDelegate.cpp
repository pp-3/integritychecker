// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "FileViewCheckSumDelegate.h"

#include <QBrush>
#include <QPainter>

#include "sum_shared/CheckSumState.h"
#include "view/SumStateIcons.h"

constexpr int HEIGHT_MARGIN = 3;

FileViewCheckSumDelegate::FileViewCheckSumDelegate(QWidget *parent) :
    QStyledItemDelegate(parent)
{
}

void FileViewCheckSumDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    const FileData fd = getFileData(index);

    painter->save();
    if (option.state & QStyle::State_Selected)
    {
        painter->fillRect(option.rect, option.palette.highlight());
    }

    const int h = fd.isDir ? option.rect.height() / 2 : option.rect.height();
    const int w = option.rect.width() / 4;

    const SumStateIcons &ssi = SumStateIcons::global();
    const QList<CheckSumState::States> STATES = CheckSumState::getStateList();

    for (int i=0 ; i<=STATES.size() ; i++)
    {
        QPixmap p;
        int cnt;

        if (i == 0)
        {
            if (fd.isDir)
            {
                cnt = fd.theStateCount.value(CheckSumState::UNKNOWN, 0);
            }
            else
            {
                cnt = 0;
            }
        }
        else
        {
            p = ssi.getPixmap(STATES.at(i-1));
            cnt = fd.theStateCount.value(STATES.at(i-1), 0);

            // separator
            const bool not_check = fd.theStateCount.value(CheckSumState::NOT_CHECKING, 0)>0;
            if (fd.isDir || !not_check)
            {
                QBrush b = painter->brush();
                painter->setBrush(QBrush(QColor(Qt::lightGray).lighter()));
                const int x = option.rect.x() + (i*w);
                const int y = option.rect.y() + (h-5)/2;
                painter->drawLine(x, y+1, x, y+2);
                painter->drawLine(x, y+4, x, y+5);
                painter->setBrush(b);
            }
        }

        if (cnt>0)
        {
            const int x = option.rect.x() + (i*w) + std::max(0, w-p.width()) / 2;
            const int y = option.rect.y() + std::max(0, h-p.height()) / 2 + (fd.isDir ? HEIGHT_MARGIN : 0);

            painter->drawPixmap(x, y, p);

            if (fd.isDir)
            {
                painter->drawText(option.rect.x() + i*w,
                                  option.rect.y() + h,
                                  w, h,
                                  Qt::AlignHCenter | Qt::AlignVCenter, QString::number(cnt));
            }
        }
    }

    painter->restore();
}

QSize FileViewCheckSumDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QSize s = QStyledItemDelegate::sizeHint(option, index);
    int &h = s.rheight();

    const int iconHeight = SumStateIcons::global().height();
    h = std::max(h, iconHeight + (2*HEIGHT_MARGIN));

    if (getFileData(index).isDir)
    {
        h *= 2;
    }

    return s;
}

FileData FileViewCheckSumDelegate::getFileData(const QModelIndex &index) const
{
    Q_ASSERT(index.model());

    const QVariant v = index.model()->data(index);
    if (v.canConvert<FileData>())
    {
        return v.value<FileData>();
    }

    return FileData(false);
}
