// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FILEVIEWCHECKSUMDELEGATE_H
#define FILEVIEWCHECKSUMDELEGATE_H

#include <QStyledItemDelegate>

#include "FileViewModel.h"

/**
 * The FileViewCheckSumDelegate class paints the icons + counts for each checksum variant into a table.
 */
class FileViewCheckSumDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit FileViewCheckSumDelegate(QWidget *parent);
    virtual ~FileViewCheckSumDelegate() override = default;

protected:
    QWidget *createEditor(QWidget */*parent*/, const QStyleOptionViewItem &/*option*/, const QModelIndex &/*index*/) const override { return nullptr; }
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    FileData getFileData(const QModelIndex &index) const;

signals:

public slots:

private:
};

#endif // FILEVIEWCHECKSUMDELEGATE_H
