// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "FileViewModel.h"

#include "checksums/SumTypeIfc.h"
#include "checksums/SumTypeManager.h"
#include "model/IntegrityModelIfc.h"
#include "sum_tree/DirInfo.h"
#include "worker/WorkerSettings.h"

FileViewModel::FileViewModel(QObject *parent, IntegrityModelIfc *const model) :
    QAbstractTableModel(parent),
    pModel(model),
    m_Files()
{
    qRegisterMetaType<QVector<int>>("QVector<int>");
    readEnabledTypes();

    connect(pModel->getWorkerSettings(), &WorkerSettings::s_EnabledTypeChanged, this, &FileViewModel::enabledTypesChanged);
}

void FileViewModel::clear()
{
    beginResetModel();
    m_Files.clear();
    endResetModel();
}

void FileViewModel::showFiles(const QList<InfoBase::weak_ptr> &files)
{
    beginResetModel();
    m_Files = files;
    endResetModel();
}

void FileViewModel::sumDataChanged()
{
    // full table update
    const static QVector<int> DispRole { Qt::DisplayRole };
    emit dataChanged(index(0, 0), index(rowCount()-1, columnCount()-1), DispRole);
}

void FileViewModel::enabledTypesChanged()
{
    beginResetModel();
    readEnabledTypes();
    endResetModel();
}

void FileViewModel::readEnabledTypes()
{
    m_EnabledTypes.clear();

    SumTypeManager& stm = SumTypeManager::getInstance();
    const SumTypeHashList en = pModel->getWorkerSettings()->getEnabledCalcHashes();
    const QList<SumTypeIfc *> &t = stm.getRegisteredTypes();
    for (SumTypeIfc *it : t)
    {
        if (en.contains(it->getTypeHash()))
        {
            m_EnabledTypes << it->getTypeHash();
        }
    }
}

SumTypeHash FileViewModel::getHashTypeForColumn(int col) const
{
    col -= DESCRIPTION_COLUMNS;
    if (col>=0 && col<m_EnabledTypes.size())
    {
        return m_EnabledTypes.at(col);
    }

    return 0;
}

QVariant FileViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation != Qt::Horizontal)
    {
        return QVariant();
    }
    if (section<0 || section>=columnCount())
    {
        return QVariant();
    }

    if (section == 0)
    {
        return tr("Name");
    }
    if (section == 1)
    {
        return QString();
    }

    SumTypeHash hash = getHashTypeForColumn(section);
    SumTypeManager& stm = SumTypeManager::getInstance();
    SumTypeIfc *sti = stm.fromHash(hash);
    return sti ? sti->getName() : QVariant();
}

int FileViewModel::rowCount(const QModelIndex &/*parent*/) const
{
    return m_Files.size();
}

int FileViewModel::columnCount(const QModelIndex &/*parent*/) const
{
    return DESCRIPTION_COLUMNS + m_EnabledTypes.size();
}

QVariant FileViewModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        const int row = index.row();
        if (row<0 || row>=rowCount())
        {
            return QVariant();
        }
        InfoBase::ptr ib = m_Files.at(row).toStrongRef();
        if (!ib || ib->isShutDown())
        {
            return QVariant();
        }

        const int col = index.column();
        if (col<0 || col>=columnCount())
        {
            return QVariant();
        }

        if (col == 0)
        {
            return ib->getName();
        }
        if (col == 1)
        {
            return QVariant();
        }

        const SumTypeHash hash = m_EnabledTypes.at(col - DESCRIPTION_COLUMNS);

        FileData fd(ib->isDir());
        fd.theStateCount = ib->getSumStateCount(hash);

        QVariant v;
        v.setValue(fd);
        return v;
    }

    return QVariant();
}
