// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FILEVIEWMODEL_H
#define FILEVIEWMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QHash>
#include <QSet>

#include "sum_shared/CheckSumState.h"
#include "sum_tree/InfoBase.h"

class IntegrityModelIfc;

struct FileData
{
    explicit FileData(bool is_dir = false) :
        isDir(is_dir)
    {}

    bool                    isDir;
    InfoBase::SumStateCount theStateCount;
};

Q_DECLARE_METATYPE(FileData)


class FileViewModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit FileViewModel(QObject *parent, IntegrityModelIfc *const model);

    void clear();
    void showFiles(const QList<InfoBase::weak_ptr> &files);
    const QList<InfoBase::weak_ptr> &getFiles() const { return m_Files; }
    SumTypeHash getHashTypeForColumn(int col) const;

    static constexpr unsigned int DESCRIPTION_COLUMNS = 2; // columns without checksums to the left of the table

public slots:
    void sumDataChanged();

protected:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    void readEnabledTypes();

signals:

private slots:
    void enabledTypesChanged();

private:
    IntegrityModelIfc        *pModel;
    QList<InfoBase::weak_ptr> m_Files;
    QList<SumTypeHash>        m_EnabledTypes;
};

#endif // FILEVIEWMODEL_H
