// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "FileViewPresenter.h"

#include <QHeaderView>
#include <QList>
#include <QModelIndex>
#include <QScrollBar>
#include <QStandardItem>
#include <QTableView>
#include <QThread>
#include <QTimer>
#include <QTreeView>

#include "model/IntegrityModelIfc.h"
#include "shared/UserSettings.h"
#include "sum_tree/DirInfo.h"
#include "thread_util/threadcheck.h"
#include "view/FileSorter.h"

#include "DirTreePresenter.h"
#include "FileViewCheckSumDelegate.h"
#include "FileViewModel.h"

FileViewPresenter::FileViewPresenter(QTableView        *const file_view,
                                     QTreeView         *const dir_view,
                                     DirTreePresenter  *const dirp,
                                     IntegrityModelIfc *const model) :
    QObject(),
    pFileView(file_view),
    pDirView(dir_view),
    pDirPresenter(dirp),
    pModel(model),
    pCheckSumDelegate(new FileViewCheckSumDelegate(pFileView)),
    theRoot()
{
    initialize();
}

void FileViewPresenter::initialize()
{
    delete pFileView->model();
    FileViewModel* model = new FileViewModel(pFileView, pModel);
    pFileView->setModel(model);
    pFileView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    pFileView->setAlternatingRowColors(true);
    updateViewDelegates();

    const QByteArray b = pModel->getUserSettingsManager()->getGlobalSettings()->getFileTableColumWidths();
    if (!b.isEmpty())
    {
        pFileView->horizontalHeader()->restoreState(b);
    }
    else
    {
        QTimer::singleShot(50, this, &FileViewPresenter::setDefaultHeaderSize);
    }

    connect(pModel, &IntegrityModelIfc::dirRootChanged,         this, &FileViewPresenter::dirRootChanged);
    connect(pModel, &IntegrityModelIfc::dirContentChanged,      this, &FileViewPresenter::dirContentChanged);
    connect(pModel, &IntegrityModelIfc::checksumSearchFinished, this, &FileViewPresenter::taskFinished);
    connect(pModel, &IntegrityModelIfc::comparisonFinished,     this, &FileViewPresenter::taskFinished);
    connect(pModel, &IntegrityModelIfc::calculationFinished,    this, &FileViewPresenter::taskFinished);
    connect(pModel, &IntegrityModelIfc::dirContentScanStarted,  this, &FileViewPresenter::taskStarted);
    connect(pModel, &IntegrityModelIfc::checksumSearchStarted,  this, &FileViewPresenter::taskStarted);
    connect(pModel, &IntegrityModelIfc::comparisonStarted,      this, &FileViewPresenter::taskStarted);
    connect(pModel, &IntegrityModelIfc::calculationStarted,     this, &FileViewPresenter::taskStarted);
    connect(pDirView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this,                       &FileViewPresenter::dirSelectionChanged);
    connect(pFileView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this,                        &FileViewPresenter::fileSelectionChanged);
    connect(pFileView->horizontalHeader(), &QHeaderView::geometriesChanged,
            this,                          &FileViewPresenter::storeHeaderWidths);

    connect(model, &QAbstractItemModel::modelReset, this, &FileViewPresenter::updateViewDelegates);
}

void FileViewPresenter::clear()
{
    FileViewModel* model = static_cast<FileViewModel*>(pFileView->model());
    model->clear();
}

void FileViewPresenter::dirRootChanged(const DirInfoRoot::ptr &root)
{
    theRoot = root.toWeakRef();
    clear();
}

void FileViewPresenter::dirContentChanged(const DirInfoRoot::ptr &root, const DirInfo::ptr &dir)
{
    if (theRoot.toStrongRef() == root)   // ensure working on the same tree
    {
        for (const DirInfo::weak_ptr &it : pDirPresenter->getSelectedDirs())
        {
            DirInfo::ptr di = it.toStrongRef();

            if (di == dir)
            {
                // directory changed
                dirSelectionChanged();
                return;
            }
        }
    }
}

void FileViewPresenter::dirSelectionChanged()
{
    FileViewModel* model = static_cast<FileViewModel*>(pFileView->model());
    model->clear();
    pFileView->selectionModel()->clear();

    QList<InfoBase::weak_ptr> files;
    for (const DirInfo::weak_ptr &it : pDirPresenter->getSelectedDirs())
    {
        DirInfo::ptr di = it.toStrongRef();
        QList<InfoBase::ptr> ch = di->getChildren();
        FileSorter fs;
        fs.sort(ch);

        for (const auto &it2 : ch)
            files << it2.toWeakRef();
    }

    model->showFiles(files);
    pFileView->verticalScrollBar()->setValue(pFileView->verticalScrollBar()->minimum());
    pFileView->resizeRowsToContents();
}

void FileViewPresenter::taskStarted(const DirInfoRoot::ptr &root, const InfoBase::ptr& /*node*/, const TaskLifeCycle::ptr &lifecycle)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (theRoot != root)
        return;

    FileViewModel* model = static_cast<FileViewModel*>(pFileView->model());
    connect(lifecycle.get(), &TaskLifeCycle::t_TaskProgress, model, &FileViewModel::sumDataChanged, Qt::QueuedConnection);
}

void FileViewPresenter::taskFinished(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr & /*node*/)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (theRoot != root)
        return;

    FileViewModel* model = static_cast<FileViewModel*>(pFileView->model());
    model->sumDataChanged();
}

QList<QPair<InfoBase::weak_ptr, SumTypeHash>> FileViewPresenter::getSelectedFiles() const
{
    QList<QPair<InfoBase::weak_ptr, SumTypeHash>> ret;

    const QModelIndexList sel = pFileView->selectionModel()->selectedIndexes();
    if (sel.size()>0)
    {
        FileViewModel* model = (FileViewModel*)pFileView->model();
        const QList<InfoBase::weak_ptr> &files = model->getFiles();

        for (const QModelIndex &ind : sel)
        {
            SumTypeHash h = model->getHashTypeForColumn(ind.column());
            if (h > 0)
            {
                ret << QPair<InfoBase::weak_ptr, SumTypeHash>(files.at(ind.row()), h);
            }
        }
    }

    return ret;
}

void FileViewPresenter::fileSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    static bool looping = false;
    if (looping)
    {
        return;
    }
    looping = true;

    QItemSelectionModel *sel_model = pFileView->selectionModel();

    QList<QPair<QItemSelection, bool>> sels;
    sels << QPair<QItemSelection, bool>(selected, true)
         << QPair<QItemSelection, bool>(deselected, false);

    for (const QPair<QItemSelection, bool> &sel : sels)
    {
        for (const QModelIndex &it : sel.first.indexes())
        {
            if (it.isValid() && it.column()==0)
            {
                 sel_model->select(sel.first,
                                   QItemSelectionModel::Rows | (sel.second ? QItemSelectionModel::Select : QItemSelectionModel::Deselect));
            }
        }
    }

    if (sel_model->selectedIndexes().size() == 1 && sel_model->currentIndex().column() == 0)
    {
        sel_model->clear();
    }

    if (sel_model->selectedIndexes().isEmpty())
    {
        pDirView->setFocus();
    }

    looping = false;
}

void FileViewPresenter::setDefaultHeaderSize()
{
    if (pFileView->isVisible())
    {
        int min0 = 120;
        const int minb = 20;
        int minc = 80;
        const int cols = pFileView->model()->columnCount() - FileViewModel::DESCRIPTION_COLUMNS;
        const int w = pFileView->width() - pFileView->horizontalScrollBar()->width();
        const int len = min0 + minb + cols*minc;
        if (w > len)
        {
             const int add = (w-len) / (cols + FileViewModel::DESCRIPTION_COLUMNS);
             min0 += 2*add;
             minc += add;
        }

        QHeaderView *v = pFileView->horizontalHeader();
        v->resizeSection(0, min0);
        v->resizeSection(1, minb);
        for (int i=0 ; i<cols ; i++)
        {
            v->resizeSection(i + FileViewModel::DESCRIPTION_COLUMNS, minc);
        }
    }
}

void FileViewPresenter::storeHeaderWidths()
{
    pModel->getUserSettingsManager()->getGlobalSettings()->setFileTableColumWidths(pFileView->horizontalHeader()->saveState());
}

void FileViewPresenter::updateViewDelegates()
{
    for (int i=FileViewModel::DESCRIPTION_COLUMNS ; i<=pFileView->model()->columnCount() ; i++)
    {
        pFileView->setItemDelegateForColumn(i, pCheckSumDelegate);
    }
}
