// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FILEVIEWPRESENTER_H
#define FILEVIEWPRESENTER_H

#include <QItemSelection>
#include <QObject>
#include <QPair>
#include <QTableView>
#include <QTreeView>
#include <QWeakPointer>

#include "sum_shared/CheckSumState.h"
#include "sum_tree/DirInfoRoot.h"
#include "sum_tree/InfoBase.h"
#include "tasks/TaskLifeCycle.h"

class IntegrityModelIfc;
class DirTreePresenter;
class FileViewCheckSumDelegate;


class FileViewPresenter : public QObject
{
    Q_OBJECT

public:
    explicit FileViewPresenter(QTableView        *const file_view,
                               QTreeView         *const dir_view,
                               DirTreePresenter  *const dirp,
                               IntegrityModelIfc *const model);

    QList<QPair<InfoBase::weak_ptr, SumTypeHash>> getSelectedFiles() const;

private:
    QTableView               *pFileView;
    QTreeView                *pDirView;
    DirTreePresenter         *pDirPresenter;
    IntegrityModelIfc        *pModel;
    FileViewCheckSumDelegate *pCheckSumDelegate;
    DirInfoRoot::weak_ptr     theRoot;

    void initialize();

signals:

private slots:
    void clear();
    void dirRootChanged(const DirInfoRoot::ptr &root);
    void dirContentChanged(const DirInfoRoot::ptr &root, const DirInfo::ptr &dir);
    void dirSelectionChanged();
    void taskStarted(const DirInfoRoot::ptr &root, const InfoBase::ptr& node, const TaskLifeCycle::ptr &lifecycle);
    void taskFinished(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node);
    void fileSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void setDefaultHeaderSize();
    void storeHeaderWidths();
    void updateViewDelegates();
};

#endif // FILEVIEWPRESENTER_H
