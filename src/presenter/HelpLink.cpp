// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "HelpLink.h"

#include <shared/LanguageSettings.h>
#include <shared/Tr.h>

#include "HelpPresenter.h"

HelpLink::HelpLink(HelpPresenter    *help,
                   LanguageSettings *lang,
                   QPushButton      *link,
                   const QString    &topic) :
    QObject(help),
    pHelpLink(link),
    pHelpPresenter(help),
    m_Topic(topic)
{
    initialize(lang);
}

void HelpLink::initialize(LanguageSettings *lang)
{
    pHelpLink->setIcon(QIcon(":/q"));
    pHelpLink->setFlat(true);
    pHelpLink->setCursor(QCursor(Qt::WhatsThisCursor));
    QFont f = pHelpLink->font();
    f.setUnderline(true);
    pHelpLink->setFont(f);
    pHelpLink->setStyleSheet(QString("QPushButton { color: blue; }"));

    languageChanged();

    QObject::connect(pHelpLink, &QPushButton::clicked, pHelpPresenter,
                     [this]() { pHelpPresenter->showHelpTopic(m_Topic); });
    QObject::connect(lang, &LanguageSettings::languageChanged,
                     this, &HelpLink::languageChanged);
}

void HelpLink::languageChanged()
{
    pHelpLink->setText(QStringLiteral("→ ")+Tr::tr("help"));
    pHelpLink->setToolTip(Tr::tr("Open related help page"));
}
