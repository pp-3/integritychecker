// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef HELPLINK_H
#define HELPLINK_H

#include <QObject>
#include <QPushButton>
#include <QString>

class HelpPresenter;
class LanguageSettings;

/**
 * Connects a help push button with the related topic in the help tab.
 */
class HelpLink : public QObject
{
    Q_OBJECT

public:
    /**
     * Constructor.
     * @param help The help presenter instance.
     * @param lang The language settings.
     * @param link The help button.
     * @param topic The help topic to naviate to.
     */
    HelpLink(HelpPresenter    *help,
             LanguageSettings *lang,
             QPushButton      *link,
             const QString    &topic);

private:
    void initialize(LanguageSettings *lang);
    void languageChanged();

private:
    QPushButton   *pHelpLink;
    HelpPresenter *pHelpPresenter;
    QString        m_Topic;
};

#endif // HELPLINK_H
