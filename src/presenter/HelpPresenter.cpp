// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "HelpPresenter.h"

#include <QFile>

#include "shared/LanguageSettings.h"

HelpPresenter::HelpPresenter(QTextBrowser     *help_viewer,
                             QTabWidget       *tab,
                             QWidget          *help_tab_page,
                             LanguageSettings *lang_sett) :
    QObject(),
    pHelpViewer(help_viewer),
    pTabWidget(tab),
    pHelpTabPage(help_tab_page),
    pLanguageSettings(lang_sett)
{
    pHelpViewer->setFocus();
    connect(pTabWidget, &QTabWidget::currentChanged, this, &HelpPresenter::currentTabChanged);
}

void HelpPresenter::showHelp()
{
    pTabWidget->setCurrentWidget(pHelpTabPage);
}

void HelpPresenter::showHelpTopic(const QString& topic)
{
    showHelp();
    pHelpViewer->scrollToAnchor(topic);
}

void HelpPresenter::createHelp()
{
    QFile f(pLanguageSettings->getHelpPath());
    if (!f.exists())
    {
        f.setFileName(":/help_en.html");
    }

    if (f.exists())
    {
        if (f.open(QFile::ReadOnly))
        {
            const QString s = QString::fromUtf8(f.readAll());
            pHelpViewer->setHtml(s);
            f.close();
        }
    }
}

void HelpPresenter::removeHelp()
{
    pHelpViewer->clear();
}

void HelpPresenter::currentTabChanged()
{
    if (pTabWidget->currentWidget() == pHelpTabPage)
    {
        createHelp();
    }
    else
    {
        removeHelp();
    }
}
