// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef HELPPRESENTER_H
#define HELPPRESENTER_H

#include <QObject>
#include <QWidget>
#include <QString>
#include <QTabWidget>
#include <QTextBrowser>

class LanguageSettings;

class HelpPresenter : public QObject
{
    Q_OBJECT

public:
    explicit HelpPresenter(QTextBrowser     *help_viewer,
                           QTabWidget       *tab,
                           QWidget          *help_tab_page,
                           LanguageSettings *lang_sett);
    virtual ~HelpPresenter() override = default;

    void showHelp();
    void showHelpTopic(const QString& topic);

    static constexpr const char *FILES_TOPIC          = "files";
    static constexpr const char *FILE_DIR_TOPIC       = "dirview";
    static constexpr const char *FILE_FILES_TOPIC     = "fileview";
    static constexpr const char *FILEs_SUM_TOPIC      = "sumcount";
    static constexpr const char *FILEs_BUTTON_TOPIC   = "calcbuttons";

    static constexpr const char *STATUS_TOPIC         = "status";
    static constexpr const char *STATUS_LOG_TOPIC     = "log";
    static constexpr const char *STATUS_TASKB_TOPIC   = "taskbuttons";
    static constexpr const char *STATUS_LOGB_TOPIC    = "logbuttons";

    static constexpr const char *OPTION_TOPIC                = "options";
    static constexpr const char *OPTION_PATH_TOPIC           = "paths";
    static constexpr const char *OPTION_PATH_INP_TOPIC       = "inputpath";
    static constexpr const char *OPTION_PATH_OUT_TOPIC       = "outputpath";
    static constexpr const char *OPTION_CALC_TOPIC           = "calculation";
    static constexpr const char *OPTION_CALC_WORKER_TOPIC    = "workercount";
    static constexpr const char *OPTION_CALC_CACHE_TOPIC     = "cachesize";
    static constexpr const char *OPTION_VIEW_TOPIC           = "view";
    static constexpr const char *OPTION_VIEW_TYPE_TOPIC      = "types";
    static constexpr const char *OPTION_VIEW_LANG_TOPIC      = "language";
    static constexpr const char *OPTION_PROFILE_TOPIC        = "profiles";
    static constexpr const char *OPTION_CHANGE_PROFILE_TOPIC = "changeprofile";
    static constexpr const char *OPTION_ADMIN_PROFILE_TOPIC  = "adminprofile";
    static constexpr const char *OPTION_ABOUT_TOPIC          = "about";
    static constexpr const char *OPTION_HELP_TOPIC           = "help";


private:
    void createHelp();
    void removeHelp();

private slots:
    void currentTabChanged();

signals:

private:
    QTextBrowser     *pHelpViewer;
    QTabWidget       *pTabWidget;
    QWidget          *pHelpTabPage;
    LanguageSettings *pLanguageSettings;
};

#endif // HELPPRESENTER_H
