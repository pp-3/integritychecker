// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "IsDirValidator.h"

#include <QFileInfo>
#include <QLineEdit>

IsDirValidator::IsDirValidator(QLineEdit *line, bool rejectWrongInput) :
    QValidator(line),
    pEdit(line),
    m_RejectWrongInput(rejectWrongInput),
    m_OrigFont(pEdit->font())
{
}

QValidator::State IsDirValidator::validate(QString& input, int& /*pos*/) const
{
    QFileInfo fi(input);

    if (fi.isDir() && fi.isReadable())
    {
        pEdit->setFont(m_OrigFont);
        return QValidator::Acceptable;
    }

    QFont f = pEdit->font();
    f.setStyle(QFont::StyleItalic);
    pEdit->setFont(f);

    return m_RejectWrongInput ?
             QValidator::Intermediate :
             QValidator::Acceptable;
}
