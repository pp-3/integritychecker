// Copyright 2016 Peter Peters
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ISDIRVALIDATOR_H
#define ISDIRVALIDATOR_H

#include <QFont>
#include <QString>
#include <QValidator>

class QLineEdit;

/**
 * The IsDirValidator class validates a QLineEdit input for being a valid filesystem directory.
 */
class IsDirValidator : public QValidator
{
    Q_OBJECT

public:
    /**
     * @param line The QLineEdit this validator is attached to. Needed for changing the font.
     * @param rejectWrongInput If true, the input won't accepted if invalid.
     *                         If false, invalid input will be shown as invalid, but still being accepted.
     */
    explicit IsDirValidator(QLineEdit *line, bool rejectWrongInput = true);

protected:
    QValidator::State validate(QString& input, int& pos) const;

signals:

public slots:

private:
    QLineEdit *pEdit;
    bool       m_RejectWrongInput;
    QFont      m_OrigFont;
};

#endif // ISDIRVALIDATOR_H
