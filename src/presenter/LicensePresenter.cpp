// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "LicensePresenter.h"

#include <QDesktopServices>
#include <QLibraryInfo>
#include <QTextBrowser>
#include <QtGlobal>

#include "Version.h"
#include "shared/ResourceReader.h"

LicensePresenter::LicensePresenter(QLabel     *qt_compile_version,
                                   QLabel     *qt_lib_version,
                                   QLabel     *integrity_version,
                                   QLabel     *git_hash,
                                   QTabWidget *options_license_sub_tab) :
    QObject(nullptr),
    pQtCompileVersion(qt_compile_version),
    pQtLibVersion(qt_lib_version),
    pIntegrityVersion(integrity_version),
    pGitHash(git_hash),
    pLicenseSubTab(options_license_sub_tab)
{
    initialize();
}

void LicensePresenter::initialize()
{
    pQtCompileVersion->setText(QT_VERSION_STR);
#if QT_VERSION < QT_VERSION_CHECK(5, 8, 0)
    pLibVersion->setText(qVersion());
#else
    pQtLibVersion->setText(QLibraryInfo::version().toString());
#endif

    pIntegrityVersion->setText(Version::getStrVersion());
    pGitHash->setText(Version::GIT_HASH);

    addLicenses();
}

void LicensePresenter::addLicenses()
{
    // integritychecker
    addLicenseTab("IntegrityChecker", ":/integritychecker_license_url", ":/integritychecker_license");

    // qt5
    addLicenseTab("Qt 5", ":/qt5_license_url", ":/qt5_license");

    // google test
    addLicenseTab("google test", ":/googletest_license_url", ":/googletest_license");
}

void LicensePresenter::addLicenseTab(const QString &tab_name,
                                     const QString &url_resource,
                                     const QString &license_resource)
{
    QTextBrowser *tb = new QTextBrowser(pLicenseSubTab);
    pLicenseSubTab->addTab(tb, tab_name);
    tb->setOpenLinks(false);
    tb->setOpenExternalLinks(true);

    ResourceReader rr;
    const QString url = rr.readString(url_resource);
    const QString lic = rr.readString(license_resource);

    tb->setHtml("<a href=\""+url+"\">"+url+"</a>");
    connect(tb,
            &QTextBrowser::anchorClicked,
            this,
            [url]()
            {
                QDesktopServices::openUrl(QUrl(url));
            });
    tb->append("\n");
    tb->append(lic);
}
