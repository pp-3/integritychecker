// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef LICENSEPRESENTER_H
#define LICENSEPRESENTER_H

#include <QLabel>
#include <QObject>
#include <QString>
#include <QTabWidget>

class LicensePresenter : public QObject
{
    Q_OBJECT

public:
    explicit LicensePresenter(QLabel     *qt_compile_version,
                              QLabel     *qt_lib_version,
                              QLabel     *integrity_version,
                              QLabel     *git_hash,
                              QTabWidget *options_license_sub_tab);

private:
    void initialize();
    void addLicenses();
    void addLicenseTab(const QString &tab_name,
                       const QString &url_resource,
                       const QString &license_resource);

private:
    QLabel     *pQtCompileVersion;
    QLabel     *pQtLibVersion;
    QLabel     *pIntegrityVersion;
    QLabel     *pGitHash;
    QTabWidget *pLicenseSubTab;
};

#endif // LICENSEPRESENTER_H
