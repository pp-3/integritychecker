// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "OptionPresenter.h"

#include <QFileDialog>
#include <QIntValidator>
#include <QList>
#include <QMessageBox>
#include <QPair>
#include <QWidget>

#include "model/IntegrityModelIfc.h"
#include "shared/ErrorCollector.h"
#include "shared/LanguageSettings.h"
#include "view/LanguageSelectionDialog.h"
#include "view/MainWindow.h"

#include "../view/ui_MainWindow.h"
#include "HelpLink.h"
#include "HelpPresenter.h"
#include "IsDirValidator.h"
#include "PresenterManager.h"

OptionPresenter::OptionPresenter(Ui::mainWnd       *mainwnd,
                                 IntegrityModelIfc *model,
                                 PresenterManager  *workerifc,
                                 HelpPresenter     *help_presenter) :
    QObject(),
    pMainWindow(mainwnd),
    pModel(model),
    pWorkerIfc(workerifc),
    pHelpPresenter(help_presenter)
{
    initializeOptions();
}

void OptionPresenter::initializeOptions()
{
    initializeViews();
    setupConnections();

    connect(pMainWindow->topTab, &QTabWidget::currentChanged, this, &OptionPresenter::tabChanged);
    connect(pMainWindow->op_configTab, &QTabWidget::currentChanged, this, &OptionPresenter::tabChanged);
}

void OptionPresenter::initializeViews()
{
    // button group
    m_OutputDirSelection.setExclusive(true);
    m_OutputDirSelection.addButton(pMainWindow->op_sameDirCheckBox     , WorkerSettings::AS_INPUT);
    m_OutputDirSelection.addButton(pMainWindow->op_outputDifferentCheck, WorkerSettings::OTHER_DIR);

    // fill widgets
    outputSelectionChanged_model(pModel->getWorkerSettings()->getOutputDirSelection());
    pMainWindow->op_inputDir->setText(pModel->getWorkerSettings()->getInputDir());
    pMainWindow->op_outputDir->setText(pModel->getWorkerSettings()->getOutputDir());
    workerCountChanged(pModel->getWorkerSettings()->getWorkerThreads());
    cacheSizeChanged(pModel->getWorkerSettings()->getCacheSize());
    languageChanged_model();
}

void OptionPresenter::tabChanged()
{
    if (pMainWindow->topTab->currentWidget() == pMainWindow->optionPage)
    {
        initializeViews();
    }
}

void OptionPresenter::setupConnections()
{
    // register model listers
    connect(pModel->getWorkerSettings(), &WorkerSettings::s_OutputDirSelectionChanged, this,                      &OptionPresenter::outputSelectionChanged_model);
    connect(pModel->getWorkerSettings(), &WorkerSettings::s_InputDirChanged,           pMainWindow->op_inputDir,  &QLineEdit::setText);
    connect(pModel->getWorkerSettings(), &WorkerSettings::s_OutputDirChanged,          pMainWindow->op_outputDir, &QLineEdit::setText);

    connect(pModel->getWorkerSettings(), &WorkerSettings::s_WorkerThreadsChanged,   this, &OptionPresenter::workerCountChanged);
    connect(pModel->getWorkerSettings(), &WorkerSettings::s_CacheSizeChanged,       this, &OptionPresenter::cacheSizeChanged);

    connect(pModel->getLanguageSettings(), &LanguageSettings::languageChanged, this, &OptionPresenter::languageChanged_model, Qt::QueuedConnection);

    // register view listeners
    connect(&m_OutputDirSelection,     QOverload<QAbstractButton *, bool>::of(&QButtonGroup::buttonToggled), this, &OptionPresenter::outputSelectionChanged_gui);
    connect(pMainWindow->op_inputDir,  &QLineEdit::editingFinished, this, &OptionPresenter::inputDirChanged_gui);
    connect(pMainWindow->op_outputDir, &QLineEdit::editingFinished, this, &OptionPresenter::outputDirChanged_gui);

    connect(pMainWindow->op_workerCountInput,  &QLineEdit::editingFinished, this, &OptionPresenter::workerCountChanged_gui);
    connect(pMainWindow->op_cacheSizeInput,    &QLineEdit::editingFinished, this, &OptionPresenter::cacheSizeChanged_gui);

    // button listeners
    connect(pMainWindow->op_inputBrowseButton,  &QPushButton::clicked, this, &OptionPresenter::browseInputDir);
    connect(pMainWindow->op_outputBrowseButton, &QPushButton::clicked, this, &OptionPresenter::browseOutputDir);

    connect(pMainWindow->op_viewChangeLangButton, &QPushButton::clicked, this, &OptionPresenter::changeLanguage_gui);

    // validator
    pMainWindow->op_inputDir->setValidator(new IsDirValidator(pMainWindow->op_inputDir));
    pMainWindow->op_outputDir->setValidator(new IsDirValidator(pMainWindow->op_outputDir, false));
    QPair<unsigned int, unsigned int> minmax = pModel->getWorkerSettings()->getMinMaxWorkerThreads();
    pMainWindow->op_workerCountInput->setValidator(new QIntValidator(minmax.first, minmax.second, pMainWindow->op_workerCountInput));
    pMainWindow->op_cacheSizeInput->setValidator(new QIntValidator(pMainWindow->op_cacheSizeInput));

    // help links
    new HelpLink(pHelpPresenter, pModel->getLanguageSettings(), pMainWindow->fi_fileHelp,      HelpPresenter::FILES_TOPIC);
    new HelpLink(pHelpPresenter, pModel->getLanguageSettings(), pMainWindow->st_statusHelp,    HelpPresenter::STATUS_TOPIC);
    new HelpLink(pHelpPresenter, pModel->getLanguageSettings(), pMainWindow->op_inputHelp,     HelpPresenter::OPTION_PATH_INP_TOPIC);
    new HelpLink(pHelpPresenter, pModel->getLanguageSettings(), pMainWindow->op_outputHelp,    HelpPresenter::OPTION_PATH_OUT_TOPIC);
    new HelpLink(pHelpPresenter, pModel->getLanguageSettings(), pMainWindow->op_workerHelp,    HelpPresenter::OPTION_CALC_WORKER_TOPIC);
    new HelpLink(pHelpPresenter, pModel->getLanguageSettings(), pMainWindow->op_cacheHelp,     HelpPresenter::OPTION_CALC_CACHE_TOPIC);
    new HelpLink(pHelpPresenter, pModel->getLanguageSettings(), pMainWindow->op_viewTypesHelp, HelpPresenter::OPTION_VIEW_TYPE_TOPIC);
    new HelpLink(pHelpPresenter, pModel->getLanguageSettings(), pMainWindow->op_viewLangHelp,  HelpPresenter::OPTION_VIEW_LANG_TOPIC);

    pMainWindow->op_inputDir->setFocus();
}

void OptionPresenter::workerCountChanged_gui()
{
    const QString c = pMainWindow->op_workerCountInput->text();

    // gui update -> update model
    bool update_gui = false;
    bool ok;
    unsigned int qc = c.toUInt(&ok);
    if (ok)
    {
        pModel->getWorkerSettings()->setWorkerThreads(qc);
        update_gui = (qc != pModel->getWorkerSettings()->getWorkerThreads());
    }
    else
    {
        pModel->getErrorInstance()->addError(tr("Can't parse worker thread count: ").append(c));

        // update gui
        update_gui = true;
    }

    if (update_gui)
    {
        workerCountChanged(pModel->getWorkerSettings()->getWorkerThreads());
    }
}
void OptionPresenter::workerCountChanged(unsigned int c)
{
    // model update -> update gui
    pMainWindow->op_workerCountInput->setText(QString::number(c));
}
void OptionPresenter::cacheSizeChanged_gui()
{
    // gui update -> update model
    QString s = pMainWindow->op_cacheSizeInput->text();

    bool update_gui = false;
    bool ok;
    uint qs = s.toUInt(&ok);
    if (ok)
    {
        pModel->getWorkerSettings()->setCacheSize(qs);
        update_gui = (qs != pModel->getWorkerSettings()->getCacheSize());
    }
    else
    {
        pModel->getErrorInstance()->addError(tr("Can't parse cache size: ").append(s));

        // update gui
        update_gui = true;
    }

    if (update_gui)
    {
        cacheSizeChanged(pModel->getWorkerSettings()->getCacheSize());
    }
}
void OptionPresenter::cacheSizeChanged(const uint& s)
{
    // model update -> update gui
    pMainWindow->op_cacheSizeInput->setText(QString::number(s));
}

void OptionPresenter::outputSelectionChanged_model(WorkerSettings::OUTPUT_SELECTION sel)
{
    Q_ASSERT(sel>=0 && sel<m_OutputDirSelection.buttons().size());

    if (sel>=0 && sel<m_OutputDirSelection.buttons().size())
    {
        m_OutputDirSelection.button(sel)->setChecked(true);
    }
    else
    {
        for (QAbstractButton *it : m_OutputDirSelection.buttons())
        {
            it->setChecked(false);
        }
    }
}

void OptionPresenter::outputSelectionChanged_gui()
{
    const WorkerSettings::OUTPUT_SELECTION n = static_cast<WorkerSettings::OUTPUT_SELECTION>(m_OutputDirSelection.checkedId());
    const WorkerSettings::OUTPUT_SELECTION o = pModel->getWorkerSettings()->getOutputDirSelection();
    if (n != o)
    {
        if (getAck(tr("Separate output path?"), tr("Are you sure you want to enable / diable the use of a separate check sum output directory?")))
        {
            pModel->getWorkerSettings()->setOutputDirSelection(n);
            if (n == WorkerSettings::OTHER_DIR)
            {
                checkOutputDir();
            }
        }
        else
        {
            outputSelectionChanged_model(o);
        }
    }
}

void OptionPresenter::inputDirChanged_gui()
{
    static bool ENTERED = false;
    if (ENTERED)
    {
        return;
    }
    ENTERED = true;

    const QString n = pMainWindow->op_inputDir->text();
    const QString o = pModel->getWorkerSettings()->getInputDir();

    if (n != o)
    {
        if (getAck(tr("Change input directory?"), tr("Are you sure you want to change the base directory for the check sum files?")))
        {
            pModel->getWorkerSettings()->setInputDir(n);

            if (!pModel->getWorkerSettings()->isInputDirValid())
            {
                QMessageBox::warning(pMainWindow->centralWidget, tr("Invalid input directory!"), tr("Chosen input directory does not exist: ")+n);
            }
        }
        else
        {
            pMainWindow->op_inputDir->setText(o);
        }
    }

    ENTERED = false;
}

void OptionPresenter::outputDirChanged_gui()
{
    static bool ENTERED = false;
    if (ENTERED)
    {
        return;
    }
    ENTERED = true;

    const QString n = pMainWindow->op_outputDir->text();
    const QString o = pModel->getWorkerSettings()->getOutputDir();

    if (n != o)
    {
        if (pModel->getWorkerSettings()->getOutputDirSelection() != WorkerSettings::OTHER_DIR ||
           getAck(tr("Change output directory?"), tr("Are you sure you want to change the output directory for the check sum files?")))
        {
            pModel->getWorkerSettings()->setOutputDir(n);
            checkOutputDir();
        }
        else
        {
            pMainWindow->op_outputDir->setText(o);
        }
    }

    ENTERED = false;
}

void OptionPresenter::languageChanged_model()
{
    const QLocale cur = pModel->getLanguageSettings()->getCurrentLanguage();
    QString lang = cur.nativeLanguageName();
    if (lang.isEmpty())
    {
        lang = QLocale::languageToString(cur.language());
    }
    pMainWindow->op_viewLang->setText(lang);
}

void OptionPresenter::changeLanguage_gui()
{
    LanguageSelectionDialog dlg(pMainWindow->centralWidget, pModel->getLanguageSettings());
    dlg.exec();
}

void OptionPresenter::checkOutputDir()
{
    if (!pModel->getWorkerSettings()->isOutputDirValid())
    {
        const QString o = pModel->getWorkerSettings()->getOutputDir();
        const QMessageBox::StandardButton ret = QMessageBox::question(pMainWindow->centralWidget, tr("Invalid output directory"), tr("Chosen output directory does not exist: ")+o+" "+tr("Create it?"));
        if (ret == QMessageBox::Yes)
        {
            const bool ret = pModel->getWorkerSettings()->createOutputDir();
            if (!ret)
            {
                QMessageBox::critical(pMainWindow->centralWidget, tr("Error"), tr("Directory creating failed: ")+o+" "+tr("Check status log for more information."));
            }
        }
    }
}

void OptionPresenter::browseInputDir()
{
    bool ok = false;
    const QString dir = browseDir(pModel->getWorkerSettings()->getInputDir(), ok);
    if (ok)
    {
        pModel->getWorkerSettings()->setInputDir(dir);
    }
}

void OptionPresenter::browseOutputDir()
{
    bool ok = false;
    const QString dir = browseDir(pModel->getWorkerSettings()->getOutputDir(), ok);
    if (ok)
    {
        pModel->getWorkerSettings()->setOutputDir(dir);
    }
}

QString OptionPresenter::browseDir(const QString& current, bool& ok)
{
    const QString ret = QFileDialog::getExistingDirectory(qobject_cast<QWidget *>(parent()), tr("Choose directory"), current, QFileDialog::ShowDirsOnly);

    ok = !ret.isNull() && !ret.isEmpty();
    return ret;
}

bool OptionPresenter::getAck(const QString &title, const QString &ack_str)
{
    const QMessageBox::StandardButton ret = QMessageBox::question(pMainWindow->centralWidget, title, ack_str);
    return (ret == QMessageBox::Yes);
}
