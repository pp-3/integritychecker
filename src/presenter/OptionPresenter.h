// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef OPTIONPRESENTER_H
#define OPTIONPRESENTER_H

#include <QButtonGroup>
#include <QList>
#include <QObject>
#include <QSharedPointer>
#include <QString>

#include "worker/WorkerSettings.h"

class MainWindow;
class IntegrityModelIfc;
class PresenterManager;
class HelpPresenter;
class HelpLink;
namespace Ui
{
class mainWnd;
}

class OptionPresenter : public QObject
{
    Q_OBJECT

public:
    explicit OptionPresenter(Ui::mainWnd       *mainwnd,
                             IntegrityModelIfc *model,
                             PresenterManager  *workerifc,
                             HelpPresenter     *help_presenter);

private:
    Ui::mainWnd       *pMainWindow;
    IntegrityModelIfc *pModel;
    PresenterManager  *pWorkerIfc;
    HelpPresenter     *pHelpPresenter;
    QButtonGroup       m_OutputDirSelection;

    void initializeOptions();
    void initializeViews();
    void setupConnections();
    QString browseDir(const QString& current, bool& ok);

    void checkOutputDir();
    bool getAck(const QString &title, const QString &ack_str);

signals:

private slots:
    void tabChanged();
    void workerCountChanged_gui();
    void workerCountChanged(unsigned int c);
    void cacheSizeChanged_gui();
    void cacheSizeChanged(const uint &s);

    void outputSelectionChanged_model(WorkerSettings::OUTPUT_SELECTION sel);
    void outputSelectionChanged_gui();
    void inputDirChanged_gui();
    void outputDirChanged_gui();
    void languageChanged_model();
    void changeLanguage_gui();

    void browseInputDir();
    void browseOutputDir();
};

#endif // OPTIONPRESENTER_H
