// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "PresenterManager.h"

#include <QTimer>
#include <QWizard>

#include "model/IntegrityModelIfc.h"
#include "view/MainWindow.h"
#include "view/SetupWizardCalc.h"
#include "view/SetupWizardFinal.h"
#include "view/SetupWizardIntro.h"
#include "view/SetupWizardPath.h"
#include "worker/WorkerSettings.h"

#include "../view/ui_MainWindow.h"
#include "DirTreePresenter.h"
#include "FileDirSelectionManager.h"
#include "FileViewPresenter.h"
#include "HelpPresenter.h"
#include "LicensePresenter.h"
#include "OptionPresenter.h"
#include "ProfilePresenter.h"
#include "StatusOutputPresenter.h"
#include "SummaryCountPresenter.h"
#include "TitlePresenter.h"
#include "ViewSettings.h"

PresenterManager::PresenterManager(MainWindow *mainwnd, IntegrityModelIfc *model) :
    QObject(mainwnd),
    pMainWindow(mainwnd),
    pModel(model)
{
    Ui::mainWnd *const ui = pMainWindow->ui;

    m_Presenters.emplace(new TitlePresenter(
    	pModel->getUserSettingsManager(),
        pMainWindow));
    HelpPresenter *helpPresenter = new HelpPresenter(
    	ui->helpBrowser,
        ui->topTab,
        ui->helpTabPage,
        pModel->getLanguageSettings());
    m_Presenters.emplace(helpPresenter);
    m_Presenters.emplace(new ViewSettings(
    	ui->viewTypesGroup,
        ui->viewTypesGroupLayout,
        pModel));
    m_Presenters.emplace(new OptionPresenter(
    	ui,
        pModel,
        this,
        helpPresenter));
    m_Presenters.emplace(new ProfilePresenter(
    	pModel->getUserSettingsManager(),
        helpPresenter,
        pModel->getLanguageSettings(),
        ui->op_profCurrentName1,
        ui->op_profCurrentName2,
        ui->op_profCurrentName3,
        ui->op_profChangeProfButton,
        ui->op_profCurrentHelp,
        ui->op_profDescription,
        ui->op_profProfileListBox,
        ui->op_profCopyProfileButton,
        ui->op_profRenameProfileButton,
        ui->op_profDeleteProfileButton,
        ui->op_profNewNameEdit,
        ui->op_profAdminHelp));
    m_Presenters.emplace(new StatusOutputPresenter(
    	ui->st_statusView,
        ui->st_cancelTaskButton,
        ui->st_cancelAllButton,
        ui->st_clearFinishedButton,
        ui->st_clearAllButton,
        ui->st_removeSelectedButton,
        pModel));
    DirTreePresenter *dirTreePresenter = new DirTreePresenter(
    	ui->fi_dirView,
        pModel);
    m_Presenters.emplace(dirTreePresenter);
    FileViewPresenter *fileViewPresenter = new FileViewPresenter(
    	ui->fi_fileView,
        ui->fi_dirView,
        dirTreePresenter,
        pModel);
    m_Presenters.emplace(fileViewPresenter);
    m_Presenters.emplace(new FileDirSelectionManager(
    	ui->fi_fileView,
        ui->fi_dirView,
        ui->fi_InfoButton,
        ui->fi_compareButton,
        ui->fi_calcButton,
        dirTreePresenter,
        fileViewPresenter,
        pModel));
    m_Presenters.emplace(new SummaryCountPresenter(
    	ui->fi_checkedl,
        ui->fi_errorl,
        ui->fi_checked_cnt,
        ui->fi_error_cnt,
        pModel));
    m_Presenters.emplace(new LicensePresenter(
    	ui->qt_compile_version,
        ui->qt_library_version,
        ui->version,
        ui->git_hash,
        ui->op_aboutLicenseTabs));

    initialize();
}

PresenterManager::~PresenterManager()
{
    while (!m_Presenters.empty())
        m_Presenters.pop();
}

void PresenterManager::initialize()
{
    QTimer::singleShot(100, this, &PresenterManager::delayedIntialize);
}

void PresenterManager::delayedIntialize()
{
    if (!pModel->getWorkerSettings()->areDirsValid())
    {
        QWizard wizard(pMainWindow);
        wizard.addPage(new SetupWizardIntro(&wizard));
        wizard.addPage(new SetupWizardPath(&wizard, pModel->getWorkerSettings()));
        wizard.addPage(new SetupWizardCalc(&wizard, pModel->getWorkerSettings()));
        wizard.addPage(new SetupWizardFinal(&wizard));

        wizard.setWindowTitle(tr("IntegrityChecker Setup"));
        wizard.setPixmap(QWizard::LogoPixmap, QPixmap(":/icon"));
        const int ret = wizard.exec();

        if (ret == QDialog::Rejected)
        {
            const QList<int> page_ids = wizard.pageIds();
            for (int it : std::as_const(page_ids))
            {
                wizard.page(it)->cleanupPage();
            }
        }
    }
}
