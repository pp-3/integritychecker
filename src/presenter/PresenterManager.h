// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef WORKERINTERFACE_H
#define WORKERINTERFACE_H

#include <QObject>

#include <memory>
#include <stack>

class MainWindow;
class IntegrityModelIfc;

/**
 * Creates all the presenter instances for the view and passes the necessary model / widget instances into the
 * according presenters.
 */
class PresenterManager : public QObject
{
    Q_OBJECT

public:
    /**
     * Constructor.
     * @param mainwnd The main window instance.
     * @param model   The model instance.
     */
    explicit PresenterManager(MainWindow *mainwnd, IntegrityModelIfc *model);
    virtual ~PresenterManager();

private:
    MainWindow                          *pMainWindow;
    IntegrityModelIfc                   *pModel;
    std::stack<std::unique_ptr<QObject>> m_Presenters;

    void initialize();

signals:

public slots:

private slots:
    void delayedIntialize();
};

#endif // WORKERINTERFACE_H
