// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ProfilePresenter.h"

#include "shared/UserSettings.h"
#include "view/ChooseProfileDialog.h"

#include "HelpLink.h"
#include "HelpPresenter.h"

ProfilePresenter::ProfilePresenter(UserSettingsManager *user_manager,
                                   HelpPresenter       *help_presenter,
                                   LanguageSettings    *lang,
                                   QLabel              *current_name1,
                                   QLabel              *current_name2,
                                   QLabel              *current_name3,
                                   QPushButton         *change_current,
                                   QPushButton         *current_help,
                                   QLineEdit           *description,
                                   QComboBox           *profile_list,
                                   QPushButton         *copy_profile,
                                   QPushButton         *rename_profile,
                                   QPushButton         *delete_profile,
                                   QLineEdit           *new_profile_name,
                                   QPushButton         *admin_help) :
    QObject(),
    pUserManager(user_manager),
    pHelpPresenter(help_presenter),
    theCurrentNames(),
    pChangeCurrentButton(change_current),
    pDescription(description),
    pProfileList(profile_list),
    pCopyProfileButton(copy_profile),
    pRenameProfileButton(rename_profile),
    pDeleteProfileButton(delete_profile),
    pNewProfileName(new_profile_name),
    m_AvailableProfileNames()
{
    theCurrentNames << current_name1 << current_name2 << current_name3;

    initialize(lang, current_help, admin_help);
    initializeConnections();
}

ProfilePresenter::~ProfilePresenter()
{
    pNewProfileName->setValidator(nullptr);
}

void ProfilePresenter::initialize(LanguageSettings *lang, QPushButton *current_help, QPushButton *admin_help)
{
    pNewProfileName->setValidator(new SupressDoubleNamesValidator(this, pNewProfileName, m_AvailableProfileNames));

    new HelpLink(pHelpPresenter, lang, current_help, HelpPresenter::OPTION_CHANGE_PROFILE_TOPIC);
    new HelpLink(pHelpPresenter, lang, admin_help,   HelpPresenter::OPTION_ADMIN_PROFILE_TOPIC);

    profileChanged();
    profileListChanged(pUserManager->getProfiles());
    updateButtonStatus();
}

void ProfilePresenter::initializeConnections()
{
    connect(pUserManager, &UserSettingsManager::profileChanged,     this, &ProfilePresenter::profileChanged);
    connect(pUserManager, &UserSettingsManager::profileListChanged, this, &ProfilePresenter::profileListChanged);

    connect(pChangeCurrentButton, &QPushButton::clicked, this, &ProfilePresenter::changeProfileClicked);
    connect(pCopyProfileButton,   &QPushButton::clicked, this, &ProfilePresenter::copyProfileClicked);
    connect(pDeleteProfileButton, &QPushButton::clicked, this, &ProfilePresenter::deleteProfileClicked);
    connect(pRenameProfileButton, &QPushButton::clicked, this, &ProfilePresenter::renameProfileClicked);

    connect(pDescription,         &QLineEdit::	editingFinished, this, &ProfilePresenter::profileDescriptionChanged);
    connect(pNewProfileName,      &QLineEdit::textChanged,       this, &ProfilePresenter::updateButtonStatus);
}

void ProfilePresenter::profileChanged()
{
    QString profile = pUserManager->getCurrentUserSettings()->profileName();
    if (profile.isEmpty() || profile==UserSettingsManager::DEFAULT_PROFILE_NAME)
    {
        profile = DEFAULT_PROFILE_STR;
    }

    for (QLabel *it : theCurrentNames)
    {
        it->setText(profile);
    }

    pDescription->setText(pUserManager->getCurrentUserSettings()->getProfileDescription());
}

void ProfilePresenter::profileListChanged(const QStringList &profiles)
{
    m_AvailableProfileNames = profiles;

    const int ind = m_AvailableProfileNames.indexOf(UserSettingsManager::DEFAULT_PROFILE_NAME);
    if (ind >= 0)
    {
        m_AvailableProfileNames.replace(ind, DEFAULT_PROFILE_STR);
    }

    pProfileList->clear();
    pProfileList->addItems(m_AvailableProfileNames);

    updateButtonStatus();
}


void ProfilePresenter::changeProfileClicked()
{
    Q_ASSERT(!theCurrentNames.isEmpty());

    ChooseProfileDialog dlg(pChangeCurrentButton, m_AvailableProfileNames, theCurrentNames.first()->text());
    if (dlg.exec() == QDialog::Accepted)
    {
        pUserManager->setCurrentUserProfile(dlg.getChosenProfile());
    }
}

void ProfilePresenter::profileDescriptionChanged()
{
    pUserManager->getCurrentUserSettings()->setProfileDescription(pDescription->text());
}

void ProfilePresenter::updateButtonStatus()
{
    int pos = 0;
    QString name = pNewProfileName->text();

    const bool change = !m_AvailableProfileNames.isEmpty() &&
                        !pNewProfileName->text().isEmpty() &&
                         pNewProfileName->validator()->validate(name, pos) == QValidator::Acceptable;

    pCopyProfileButton->setEnabled(change);
    pRenameProfileButton->setEnabled(change);

    const bool del = !m_AvailableProfileNames.isEmpty();
    pDeleteProfileButton->setEnabled(del);

    pChangeCurrentButton->setEnabled(m_AvailableProfileNames.size() > 1);
}

void ProfilePresenter::copyProfileClicked()
{
    QString name = pNewProfileName->text();
    if (name == DEFAULT_PROFILE_STR)
    {
        name = UserSettingsManager::DEFAULT_PROFILE_NAME;
    }

    if (!name.isEmpty())
    {
        pUserManager->copyProfile(name);
    }
}

void ProfilePresenter::renameProfileClicked()
{
    QString name = pNewProfileName->text();
    if (name == DEFAULT_PROFILE_STR)
    {
        name = UserSettingsManager::DEFAULT_PROFILE_NAME;
    }

    if (!name.isEmpty())
    {
        pUserManager->renameProfile(name);
    }
}

void ProfilePresenter::deleteProfileClicked()
{
    QString profile = pProfileList->currentText();
    if (profile == DEFAULT_PROFILE_STR)
    {
        profile = UserSettingsManager::DEFAULT_PROFILE_NAME;
    }

    if (!profile.isEmpty())
    {
        pUserManager->deleteProfile(profile);
    }
}




SupressDoubleNamesValidator::SupressDoubleNamesValidator(QObject *parent, QLineEdit *line, const QStringList &suppress_names) :
    QValidator(parent),
    pEdit(line),
    m_SuppressNames(suppress_names),
    m_OrigFont(pEdit->font())
{
}

QValidator::State SupressDoubleNamesValidator::validate(QString& input, int& /*pos*/) const
{
    if (!m_SuppressNames.contains(input))
    {
        pEdit->setFont(m_OrigFont);
        return QValidator::Acceptable;
    }

    QFont f = pEdit->font();
    f.setStyle(QFont::StyleItalic);
    pEdit->setFont(f);

    return QValidator::Intermediate;
}
