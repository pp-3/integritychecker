// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PROFILEPRESENTER_H
#define PROFILEPRESENTER_H

#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QObject>
#include <QPushButton>
#include <QSharedPointer>
#include <QValidator>

class UserSettingsManager;
class HelpPresenter;
class HelpLink;
class LanguageSettings;

/**
 * The connection between the profile tab in the options GUI with the underlying model.
 */
class ProfilePresenter : public QObject
{
    Q_OBJECT

public:
    explicit ProfilePresenter(UserSettingsManager *user_manager,
                              HelpPresenter       *help_presenter,
                              LanguageSettings    *lang,
                              QLabel              *current_name1,
                              QLabel              *current_name2,
                              QLabel              *current_name3,
                              QPushButton         *change_current,
                              QPushButton         *current_help,
                              QLineEdit           *description,
                              QComboBox           *profile_list,
                              QPushButton         *copy_profile,
                              QPushButton         *rename_profile,
                              QPushButton         *delete_profile,
                              QLineEdit           *new_profile_name,
                              QPushButton         *admin_help);
    virtual ~ProfilePresenter();

private:
    Q_DISABLE_COPY(ProfilePresenter)

    void initialize(LanguageSettings *lang, QPushButton *current_help, QPushButton *admin_help);
    void initializeConnections();

signals:

private slots:
    void profileChanged();
    void profileListChanged(const QStringList &profiles);

    void changeProfileClicked();
    void profileDescriptionChanged();

    void updateButtonStatus();
    void copyProfileClicked();
    void renameProfileClicked();
    void deleteProfileClicked();

private:
    UserSettingsManager *pUserManager;
    HelpPresenter       *pHelpPresenter;
    QList<QLabel *>      theCurrentNames;
    QPushButton         *pChangeCurrentButton;
    QLineEdit           *pDescription;
    QComboBox           *pProfileList;
    QPushButton         *pCopyProfileButton;
    QPushButton         *pRenameProfileButton;
    QPushButton         *pDeleteProfileButton;
    QLineEdit           *pNewProfileName;
    QStringList          m_AvailableProfileNames;

    static constexpr const char *DEFAULT_PROFILE_STR = "---";
};


class SupressDoubleNamesValidator : public QValidator
{
    Q_OBJECT

public:
    explicit SupressDoubleNamesValidator(QObject *parent, QLineEdit *line, const QStringList &suppress_names);

protected:
    QValidator::State validate(QString& input, int& pos) const;

private:
    QLineEdit         *pEdit;
    const QStringList &m_SuppressNames;
    QFont              m_OrigFont;

};

#endif // PROFILEPRESENTER_H
