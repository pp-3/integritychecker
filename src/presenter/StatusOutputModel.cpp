// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "StatusOutputModel.h"

#include <QStandardItem>

#include "tasks/TaskLifeCycle.h"

#include "TaskLifeCycleWrapper.h"

static const QColor TASK_ACTIVE(227, 255, 227);
static const QColor TASK_WAIT(TASK_WAIT.darker());

StatusOutputModel::StatusOutputModel(QObject *parent) :
    QAbstractTableModel(parent),
    m_TaskData()
{
}

StatusOutputModel::~StatusOutputModel()
{
    removeAll();
}

void StatusOutputModel::addTask(TaskLifeCycleWrapper *w)
{
    const int start_ind = m_TaskData.size();
    beginInsertRows(QModelIndex(), start_ind, start_ind);
    m_TaskData.add(w);
    endInsertRows();
}

void StatusOutputModel::taskProgress(TaskLifeCycleWrapper *w)
{
    w->progressUpdate();
    stateChanged(w);
}

void StatusOutputModel::stateChanged(TaskLifeCycleWrapper *w)
{
    int pos = m_TaskData.getListPos(w);

    QModelIndex left  = QAbstractTableModel::createIndex(pos, 0);
    QModelIndex right = QAbstractTableModel::createIndex(pos, columnCount()-1);
    emit dataChanged(left, right, QVector<int>() << Qt::DisplayRole << Qt::DecorationRole);
}

void StatusOutputModel::removeTasks(QList<TaskLifeCycleWrapper *> to_rem)
{
    while (!to_rem.isEmpty())
    {
        TaskLifeCycleWrapper *w = to_rem.takeFirst();

        const bool b = m_TaskData.contains(w);
        if (b)
        {
            int pos = m_TaskData.getListPos(w);
            beginRemoveRows(QModelIndex(), pos, pos);
        }

        m_TaskData.remove(w);

        if (b)
        {
            endRemoveRows();
        }
    }
}

void StatusOutputModel::removeAll()
{
    beginResetModel();
    m_TaskData.clear();
    endResetModel();
}

void StatusOutputModel::removeFinished()
{
    removeTasks(m_TaskData.getFinished());
}

void StatusOutputModel::remove(int row)
{
    TaskLifeCycleWrapper *w = m_TaskData.topPos(row);
    if (w)
    {
        removeTasks(QList<TaskLifeCycleWrapper *>() << w);
    }
}

void StatusOutputModel::cancel(int row)
{
    TaskLifeCycleWrapper *w = m_TaskData.topPos(row);
    if (w)
    {
        w->cancel();
    }
}



QVariant StatusOutputModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        if (section == 0) return tr("Task");
        if (section == 1) return tr("Total Tasks");
        if (section == 2) return tr("Open Tasks");
        if (section == 3) return tr("Started");
        if (section == 4) return tr("State");
    }

    return QVariant();
}

int StatusOutputModel::rowCount(const QModelIndex &/*parent*/) const
{
    return m_TaskData.size();
}

int StatusOutputModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 5;
}

QVariant StatusOutputModel::data(const QModelIndex &ind, int role) const
{
    const int col = ind.column();
    const int row = ind.row();

    const TaskLifeCycleWrapper *w = m_TaskData.topPos(row);

    if (role == Qt::DisplayRole)
    {
        if (col == 0) return w->name();
        if (col == 1) return QString::number(w->getTotalTasks());
        if (col == 2) return QString::number(w->getOpenTasks());
        if (col == 3) return w->getStartTime().time().toString(Qt::TextDate);
        if (col == 4) return w->getState();
    }
    else
    if (role == Qt::ToolTipRole)
    {
        if (col == 4) return w->getStateStr();
    }
    else
    if (role == Qt::TextAlignmentRole)
    {
        if (col >= 3)
        {
            return QVariant(Qt::AlignHCenter | Qt::AlignVCenter);
        }
        else if (col==1 || col==2)
        {
            return QVariant(Qt::AlignRight | Qt::AlignVCenter);
        }
        else
        {
            return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
        }
    }
    else
    if (role == Qt::BackgroundRole)
    {
        if (col == 2)
        {
            if (w->getOpenTasks() > 0)
            {
              if (w->getState() == TaskLifeCycle::STARTED)
                  return QBrush(TASK_ACTIVE);
              if (w->getState() == TaskLifeCycle::CREATED)
                  return QBrush(TASK_WAIT);
            }
        }
    }

    return QVariant();
}




StatusSortModel::StatusSortModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

StatusSortModel::~StatusSortModel()
{
}

bool StatusSortModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    const int col = left.column();
    Q_ASSERT(col == right.column());

    Q_ASSERT(dynamic_cast<StatusOutputModel *>(sourceModel()));
    StatusOutputModel *m = static_cast<StatusOutputModel *>(sourceModel());

    const TaskLifeCycleWrapper *l = m->m_TaskData.topPos(left.row());
    const TaskLifeCycleWrapper *r = m->m_TaskData.topPos(right.row());

    const bool start = l->getStartTime() < r->getStartTime();
    const bool eq    = l->getStartTime() == r->getStartTime();
    if (col == 0  || (col==3 && eq))
    {
        const int c = l->name().localeAwareCompare(r->name());
        return  c==0 ? start : c<0;
    }
    if (col == 1)
    {
        const int c = l->getProgress().second - r->getProgress().second;
        return  c==0 ? start : c<0;
    }
    if (col == 2)
    {
        const int c = l->getOpenTasks() - r->getOpenTasks();
        return  c==0 ? start : c<0;
    }
    if (col == 3)
    {
        return start;
    }
    if (col == 4)
    {
        const int c = l->getState() - r->getState();
        return  c==0 ? start : c<0;
    }

    return start;
}
