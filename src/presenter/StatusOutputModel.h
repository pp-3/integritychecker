// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef STATUSOUTPUTMODEL_H
#define STATUSOUTPUTMODEL_H

#include <QAbstractTableModel>
#include <QHash>
#include <QList>
#include <QModelIndex>
#include <QSortFilterProxyModel>

class TaskLifeCycle;
class TaskLifeCycleWrapper;

#include "TaskLifeCycleWrapper.h"

class StatusOutputModel : public QAbstractTableModel
{
    Q_OBJECT

    friend class StatusSortModel;

public:
    explicit StatusOutputModel(QObject *parent);
    virtual ~StatusOutputModel();

    void addTask(TaskLifeCycleWrapper *w);
    void taskProgress(TaskLifeCycleWrapper *w);
    void stateChanged(TaskLifeCycleWrapper *w);
    void removeTasks(QList<TaskLifeCycleWrapper *> to_rem);
    void removeAll();
    void removeFinished();
    void remove(int row);
    void cancel(int row);
    const TaskLifeCycleWrapperList& taskList() const { return m_TaskData; }

private:

protected:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &ind, int role = Qt::DisplayRole) const;
    virtual Qt::ItemFlags flags(const QModelIndex & index) const
    {
        return QAbstractItemModel::flags(index) & ~Qt::ItemIsEditable;
    }

signals:

private:
    TaskLifeCycleWrapperList m_TaskData;
};



class StatusSortModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    explicit StatusSortModel(QObject *parent);
    virtual ~StatusSortModel();

protected:
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;
};

#endif // STATUSOUTPUTMODEL_H
