// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "StatusOutputPresenter.h"

#include <QHeaderView>
#include <QPair>
#include <QScrollBar>

#include "model/IntegrityModelIfc.h"
#include "model/RunningTasks.h"
#include "model/TaskDispatcher.h"
#include "shared/ErrorCollector.h"
#include "shared/UserSettings.h"
#include "thread_util/threadcheck.h"
#include "worker/WorkerSettings.h"

#include "StatusOutputModel.h"
#include "StatusOutputStateDelegate.h"
#include "TaskLifeCycleWrapper.h"

StatusOutputPresenter::StatusOutputPresenter(QTableView        *const status_view,
                                             QPushButton       *const cancel_button,
                                             QPushButton       *const cancel_all_button,
                                             QPushButton       *const clear_finished_button,
                                             QPushButton       *const clear_all_button,
                                             QPushButton       *const remove_selected_button,
                                             IntegrityModelIfc *const model) :
    QObject(),
    pStatusView(status_view),
    pCancelButton(cancel_button),
    pCancelAllButton(cancel_all_button),
    pClearFinishedButton(clear_finished_button),
    pClearAllButton(clear_all_button),
    pRemoveSelectedButton(remove_selected_button),
    pModel(model),
    pStatusModel(nullptr),
    m_DelayedUpdate()
{
    initialize();
    initializeController();
}

StatusOutputPresenter::~StatusOutputPresenter()
{
    m_DelayedUpdate.stopSignalSending();

    removeAll();
}

void StatusOutputPresenter::initialize()
{
    delete pStatusView->model();
    pStatusModel = new StatusOutputModel(pStatusView);
    QSortFilterProxyModel *proxyModel = new StatusSortModel(pStatusView);
    proxyModel->setSourceModel(pStatusModel);
    pStatusView->setModel(proxyModel);
    StatusOutputStateDelegate *pStateDelegate = new StatusOutputStateDelegate(pStatusView);
    pStatusView->setItemDelegateForColumn(4, pStateDelegate);

    pStatusView->setSelectionMode(QAbstractItemView::SingleSelection);
    pStatusView->setSortingEnabled(true);
    pStatusView->horizontalHeader()->setSortIndicator(3, Qt::AscendingOrder);

    const QByteArray b = pModel->getUserSettingsManager()->getGlobalSettings()->getStatusColumWidths();
    if (!b.isEmpty())
    {
        pStatusView->horizontalHeader()->restoreState(b);
    }
    else
    {
        QTimer::singleShot(50, this, &StatusOutputPresenter::setDefaultHeaderSize);
    }

    pStatusView->setFocus();
}

void StatusOutputPresenter::initializeController()
{
    connect(pModel->getTaskDispatcher(), &TaskDispatcher::taskAdded, this, &StatusOutputPresenter::taskAdded, Qt::DirectConnection);
    connect(pModel->getErrorInstance(),  &ErrorCollector::newError,  this, &StatusOutputPresenter::newError, Qt::QueuedConnection);

    connect(pCancelButton,         &QPushButton::clicked, this, &StatusOutputPresenter::cancelSelected);
    connect(pCancelAllButton,      &QPushButton::clicked, this, &StatusOutputPresenter::cancelAll);
    connect(pClearFinishedButton,  &QPushButton::clicked, this, &StatusOutputPresenter::removeFinished);
    connect(pClearAllButton,       &QPushButton::clicked, this, &StatusOutputPresenter::removeAll);
    connect(pRemoveSelectedButton, &QPushButton::clicked, this, &StatusOutputPresenter::removeSelected);

    connect(pStatusView->selectionModel(),   &QItemSelectionModel::selectionChanged, this, &StatusOutputPresenter::selectionChanged);
    connect(pStatusView->selectionModel(),   &QItemSelectionModel::currentChanged,   this, &StatusOutputPresenter::selectionChanged);
    connect(pStatusView->horizontalHeader(), &QHeaderView::sectionClicked,           this, &StatusOutputPresenter::selectionChanged);
    connect(pStatusView->horizontalHeader(), &QHeaderView::geometriesChanged,        this, &StatusOutputPresenter::storeHeaderWidths);

    connect(&m_DelayedUpdate, &DelayedUpdate::signalOut,
            this,             &StatusOutputPresenter::delayedUpdateButtonStatus);
}

void StatusOutputPresenter::taskAdded(const TaskLifeCycle::ptr &task, const QString &task_name)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only
    Q_ASSERT(task);

    TaskLifeCycleWrapper *w = new TaskLifeCycleWrapper(task, task_name);

    pStatusModel->addTask(w);

    w->addStartedFunction    ([this, w]()                                            { taskStarted(w); });
    w->addProgressFunction   ([this, w](int /*cur*/, int /*max*/)                    { taskProgress(w); });
    w->addFinishedFunction   ([this, w](bool /*canceled*/)                           { taskFinished(w); });

    updateButtonStatus();
}

void StatusOutputPresenter::taskStarted(TaskLifeCycleWrapper *w)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only
    Q_ASSERT(w);

    pStatusModel->stateChanged(w);

    updateButtonStatus();
}

void StatusOutputPresenter::taskProgress(TaskLifeCycleWrapper *w)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only
    Q_ASSERT(w);

    pStatusModel->taskProgress(w);
}

void StatusOutputPresenter::taskFinished(TaskLifeCycleWrapper *w)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only
    Q_ASSERT(w);

    pStatusModel->stateChanged(w);

    updateButtonStatus();
}

void StatusOutputPresenter::newError(const QString &err, const QString &module_name, const QDateTime &/*timestamp*/)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    const QString e = module_name.isEmpty() ?
                          err :
                          module_name +" => "+ err;

    TaskLifeCycleWrapper *w = new TaskLifeCycleWrapper(TaskLifeCycle::ptr(), e);
    pStatusModel->addTask(w);
}

void StatusOutputPresenter::updateButtonStatus()
{
    m_DelayedUpdate.signalIn();
}

void StatusOutputPresenter::delayedUpdateButtonStatus()
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    // view index -> model index
    QModelIndex cur = pStatusView->selectionModel()->hasSelection() ? pStatusView->currentIndex() : QModelIndex();
    if (cur.isValid())
    {
        Q_ASSERT(dynamic_cast<StatusSortModel *>(pStatusView->model()));
        StatusSortModel *proxyModel = static_cast<StatusSortModel *>(pStatusView->model());
        cur = proxyModel->mapToSource(cur);
    }
    const TaskLifeCycleWrapper *w = cur.isValid() ? pStatusModel->taskList().topPos(cur.row()) : nullptr;

    // cancel
    pCancelButton->setEnabled(cur.isValid() && w && !w->isFinished());

    // remove
    pRemoveSelectedButton->setEnabled(cur.isValid());
    pClearAllButton->setEnabled(!pStatusModel->taskList().isEmpty());

    // lists
    pCancelAllButton->setEnabled(pStatusModel->taskList().anyRunning());
    pClearFinishedButton->setEnabled(pStatusModel->taskList().anyFinished());
}

void StatusOutputPresenter::cancelSelected()
{
    QModelIndex cur = pStatusView->currentIndex();
    if (cur.isValid())
    {
        Q_ASSERT(dynamic_cast<StatusSortModel *>(pStatusView->model()));
        StatusSortModel *proxyModel = static_cast<StatusSortModel *>(pStatusView->model());
        cur = proxyModel->mapToSource(cur);
        pStatusModel->cancel(cur.row());
    }
}

void StatusOutputPresenter::cancelAll()
{
    pModel->getRunningTasks()->cancelAll();
}

void StatusOutputPresenter::removeSelected()
{
    QModelIndex cur = pStatusView->currentIndex();
    if (cur.isValid())
    {
        Q_ASSERT(dynamic_cast<StatusSortModel *>(pStatusView->model()));
        StatusSortModel *proxyModel = static_cast<StatusSortModel *>(pStatusView->model());
        cur = proxyModel->mapToSource(cur);
        pStatusModel->remove(cur.row());
    }
}

void StatusOutputPresenter::removeFinished()
{
    pStatusModel->removeFinished();
}

void StatusOutputPresenter::removeAll()
{
    pStatusModel->removeAll();
}

void StatusOutputPresenter::selectionChanged()
{
    updateButtonStatus();
}

void StatusOutputPresenter::setDefaultHeaderSize()
{
    const int cols = pStatusView->model()->columnCount();
    const int w2 = (pStatusView->width() - pStatusView->horizontalScrollBar()->width()) / 2;

    if (pStatusView->isVisible() && w2>0)
    {
        QHeaderView *v = pStatusView->horizontalHeader();
        v->resizeSection(0, w2);
        for (int i=1 ; i<cols ; i++)
        {
            v->resizeSection(i, (w2/cols-1));
        }
    }
}

void StatusOutputPresenter::storeHeaderWidths()
{
    pModel->getUserSettingsManager()->getGlobalSettings()->setStatusColumWidths(pStatusView->horizontalHeader()->saveState());
}
