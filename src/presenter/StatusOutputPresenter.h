// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef STATUSOUTPUTPRESENTER_H
#define STATUSOUTPUTPRESENTER_H

#include <QObject>
#include <QPushButton>
#include <QTableView>

#include "shared/DelayedUpdate.h"
#include "tasks/TaskLifeCycle.h"

class IntegrityModelIfc;
class StatusOutputModel;
class TaskIfc;
class TaskLifeCycleWrapper;

class StatusOutputPresenter : public QObject
{
    Q_OBJECT

public:
    explicit StatusOutputPresenter(QTableView        *const status_view,
                                   QPushButton       *const cancel_button,
                                   QPushButton       *const cancel_all_button,
                                   QPushButton       *const clear_finished_button,
                                   QPushButton       *const clear_all_button,
                                   QPushButton       *const remove_selected_button,
                                   IntegrityModelIfc *const model);
    virtual ~StatusOutputPresenter();

private:
    void initialize();
    void initializeController();
    void updateButtonStatus();

signals:

private slots:
    void taskAdded(const TaskLifeCycle::ptr &task, const QString &task_name);
    void taskStarted(TaskLifeCycleWrapper *w);
    void taskProgress(TaskLifeCycleWrapper *w);
    void taskFinished(TaskLifeCycleWrapper *w);
    void newError(const QString& err, const QString &module_name, const QDateTime &timestamp);

    void delayedUpdateButtonStatus();
    void cancelSelected();
    void cancelAll();
    void removeSelected();
    void removeFinished();
    void removeAll();

    void selectionChanged();
    void setDefaultHeaderSize();
    void storeHeaderWidths();

private:
    QTableView        *pStatusView;
    QPushButton       *pCancelButton;
    QPushButton       *pCancelAllButton;
    QPushButton       *pClearFinishedButton;
    QPushButton       *pClearAllButton;
    QPushButton       *pRemoveSelectedButton;
    IntegrityModelIfc *pModel;
    StatusOutputModel *pStatusModel;
    DelayedUpdate      m_DelayedUpdate;
};

#endif // STATUSOUTPUTPRESENTER_H
