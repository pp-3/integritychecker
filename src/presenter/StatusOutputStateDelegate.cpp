// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "StatusOutputStateDelegate.h"

#include <QBrush>
#include <QPainter>

#include "view/TaskStateIcons.h"


StatusOutputStateDelegate::StatusOutputStateDelegate(QWidget *parent) :
    QStyledItemDelegate(parent)
{
}

void StatusOutputStateDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_ASSERT(index.model());

    const QVariant v = index.model()->data(index);
    if (!v.canConvert<TaskLifeCycle::RunState>())
    {
        QStyledItemDelegate::paint(painter, option, index);
        return;
    }

    const TaskLifeCycle::RunState state = v.value<TaskLifeCycle::RunState>();
    const QPixmap p = TaskStateIcons::global().getPixmap(state);

    painter->save();
    if (option.state & QStyle::State_Selected)
    {
        painter->fillRect(option.rect, option.palette.highlight());
    }

    // center icon
    const int h = option.rect.height();
    const int w = option.rect.width();
    const int hi = p.height();
    const int wi = p.width();
    const int x = option.rect.x() + std::max(0, (w-wi) / 2);
    const int y = option.rect.y() + std::max(0, (h-hi) / 2);

    painter->drawPixmap(x, y, p);

    painter->restore();
}
