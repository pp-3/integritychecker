// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SummaryCountPresenter.h"

#include "model/IntegrityModelIfc.h"
#include "sum_tree/InfoBase.h"
#include "sum_shared/CheckSumState.h"
#include "view/SumStateIcons.h"
#include "worker/WorkerSettings.h"

SummaryCountPresenter::SummaryCountPresenter(QLabel            *const checked_sum_label,
                                             QLabel            *const error_sum_label,
                                             QLabel            *const checked_sum_cnt,
                                             QLabel            *const error_sum_cnt,
                                             IntegrityModelIfc *const model) :
    QObject(),
    pCheckedSumLabel(checked_sum_label),
    pErrorSumLabel(error_sum_label),
    pCheckedSumCnt(checked_sum_cnt),
    pErrorSumCnt(error_sum_cnt),
    pModel(model),
    theRoot(),
    m_DelayedUpdate()
{
    initialize();
    initializeController();
}

void SummaryCountPresenter::initialize()
{
    const SumStateIcons &ssi = SumStateIcons::global();

    pErrorSumLabel->setPixmap(ssi.getPixmap(CheckSumState::CHECK_ERROR));
    pCheckedSumLabel->setPixmap(ssi.getPixmap(CheckSumState::VERIFIED)),
    pErrorSumCnt->setText("0");
    pCheckedSumCnt->setText("0");
}

void SummaryCountPresenter::initializeController()
{
    connect(pModel, &IntegrityModelIfc::dirRootChanged,      this, &SummaryCountPresenter::dirRootChanged);
    connect(pModel, &IntegrityModelIfc::comparisonFinished,  this, &SummaryCountPresenter::taskFinished);
    connect(pModel, &IntegrityModelIfc::calculationFinished, this, &SummaryCountPresenter::taskFinished);
    connect(pModel, &IntegrityModelIfc::comparisonStarted,   this, &SummaryCountPresenter::taskStarted);
    connect(pModel, &IntegrityModelIfc::calculationStarted,  this, &SummaryCountPresenter::taskStarted);
    connect(&m_DelayedUpdate, &DelayedUpdate::signalOut,     this, &SummaryCountPresenter::updateSummaries);
}

void SummaryCountPresenter::dirRootChanged(const DirInfoRoot::ptr &root)
{
    theRoot = root;
}

void SummaryCountPresenter::updateSummaries()
{
    unsigned long check = 0;
    unsigned long err = 0;

    if (theRoot)
        for (const SumTypeHash &it : pModel->getWorkerSettings()->getEnabledCalcHashes())
        {
            check += theRoot->getRoot()->getSumCount(it, CheckSumState::VERIFIED);
            err   += theRoot->getRoot()->getSumCount(it, CheckSumState::CHECK_ERROR);
        }

    pCheckedSumCnt->setText(QString::number(check));
    pErrorSumCnt->setText(QString::number(err));
}

void SummaryCountPresenter::taskStarted(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr & /*node*/, const TaskLifeCycle::ptr &lifecycle)
{
    if (theRoot.data() != root.data())
    {
        return;
    }

    lifecycle->addTaskProgressCallback(
        [this](int, int)
        {
            m_DelayedUpdate.signalIn();
        },
        this);
}

void SummaryCountPresenter::taskFinished(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr & /*node*/)
{
    if (theRoot != root || theRoot.isNull())
    {
        pCheckedSumCnt->setText("0");
        pErrorSumCnt->setText("0");
        return;
    }

    m_DelayedUpdate.signalIn();
}
