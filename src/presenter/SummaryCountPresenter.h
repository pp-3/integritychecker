// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMMARYCOUNTPRESENTER_H
#define SUMMARYCOUNTPRESENTER_H

#include <QLabel>
#include <QObject>

#include "shared/DelayedUpdate.h"
#include "sum_tree/DirInfoRoot.h"
#include "tasks/TaskLifeCycle.h"

class IntegrityModelIfc;

class SummaryCountPresenter : public QObject
{
    Q_OBJECT

public:
    explicit SummaryCountPresenter(QLabel            *const checked_sum_label,
                                   QLabel            *const error_sum_label,
                                   QLabel            *const checked_sum_cnt,
                                   QLabel            *const error_sum_cnt,
                                   IntegrityModelIfc *const model);

private:
    void initialize();
    void initializeController();

private:
    QLabel            *pCheckedSumLabel;
    QLabel            *pErrorSumLabel;
    QLabel            *pCheckedSumCnt;
    QLabel            *pErrorSumCnt;
    IntegrityModelIfc *pModel;
    DirInfoRoot::ptr   theRoot;
    DelayedUpdate      m_DelayedUpdate;

signals:

private slots:
    void dirRootChanged(const DirInfoRoot::ptr &root);
    void updateSummaries();
    void taskStarted(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node, const TaskLifeCycle::ptr &lifecycle);
    void taskFinished(const DirInfoRoot::ptr &root, const InfoBase::weak_ptr &node);
};

#endif // SUMMARYCOUNTPRESENTER_H
