// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TaskLifeCycleWrapper.h"

#include <QObject>

#include "thread_util/threadcheck.h"

TaskLifeCycleWrapper::TaskLifeCycleWrapper(const TaskLifeCycle::ptr &t, const QString &name) :
    theTlc(t),
    theName(name),
    theStartTime(),
    pSignalContext(new QObject),
    m_Progress(0, 0),
    m_Canceled(false)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (!theTlc.isNull())
    {
        theStartTime = theTlc->getStartTime();
    }
    else
    {
        theStartTime = QDateTime::currentDateTime();
    }

    addFinishedFunction(
        [this](bool canceled)
        {
            Q_ASSERT(isInMainThread()); // call from GUI thread only
            m_Canceled = canceled;
            clearTlc();
        });
}

TaskLifeCycleWrapper::~TaskLifeCycleWrapper()
{
    pSignalContext.reset(nullptr);
}

bool TaskLifeCycleWrapper::isFinished() const
{
    bool b = theTlc.isNull();

    if (!b)
    {
        bool canc;
        b = theTlc->isFinishedOrCanceled(&canc);
        if (b)
        {
            const_cast<TaskLifeCycleWrapper *>(this)->m_Canceled = canc;
            const_cast<TaskLifeCycleWrapper *>(this)->clearTlc();
        }
    }

    return b;
}

bool TaskLifeCycleWrapper::isRunning() const
{
    const bool b = !theTlc.isNull() && theTlc->isRunning();

    return b;
}

void TaskLifeCycleWrapper::cancel()
{
    if (!theTlc.isNull())
    {
        theTlc->cancelTask();
    }
}

void TaskLifeCycleWrapper::addFinishedFunction(TaskFinishedListener f)
{
    if (!theTlc.isNull())
    {
        theTlc->addFinishedCallback(f, pSignalContext.data());
    }
    else
    {
        f(false);
    }
}

void TaskLifeCycleWrapper::addProgressFunction(TaskProgressListener f)
{
    if (!theTlc.isNull())
    {
        theTlc->addTaskProgressCallback(f, pSignalContext.data());
    }
}

void TaskLifeCycleWrapper::addStartedFunction(TaskStartListener f)
{
    if (!theTlc.isNull())
    {
        theTlc->addStartedCallback(f, pSignalContext.data());
    }
}

TaskLifeCycle::RunState TaskLifeCycleWrapper::getState() const
{
    if (!theTlc.isNull())
    {
        return theTlc->getState();
    }
    return m_Canceled ? TaskLifeCycle::CANCELED : TaskLifeCycle::FINISHED;
}

QString TaskLifeCycleWrapper::getStateStr() const
{
    if (!theTlc.isNull())
    {
        return theTlc->getStateStr();
    }
    return QString();
}

void TaskLifeCycleWrapper::progressUpdate()
{
    if (!theTlc.isNull())
    {
        m_Progress = theTlc->getProgress();
    }
}

int TaskLifeCycleWrapper::getTotalTasks() const
{
    return m_Progress.second;
}

int TaskLifeCycleWrapper::getOpenTasks() const
{
    return m_Progress.second - m_Progress.first;
}

void TaskLifeCycleWrapper::clearTlc()
{
    progressUpdate();
    theTlc.clear();
}






TaskLifeCycleWrapperList::TaskLifeCycleWrapperList() :
    m_List()
{
}

TaskLifeCycleWrapperList::~TaskLifeCycleWrapperList()
{
    for (TaskLifeCycleWrapper *ch : m_List)
    {
        delete ch;
    }
    m_List.clear();
}

void TaskLifeCycleWrapperList::add(TaskLifeCycleWrapper *w)
{
    Q_ASSERT(w);
    Q_ASSERT(!contains(w));

    if (w)
    {
        m_List << w;
    }
}

bool TaskLifeCycleWrapperList::remove(TaskLifeCycleWrapper *w)
{
    if (contains(w))
    {
        if (m_List.removeOne(w))
        {
            delete w;
            return true;
        }
        else
        {
            Q_ASSERT(false);
        }
    }

    return false;
}

void TaskLifeCycleWrapperList::clear()
{
    for (TaskLifeCycleWrapper *w : m_List)
    {
        delete w;
    }
    m_List.clear();
}

bool TaskLifeCycleWrapperList::contains(TaskLifeCycleWrapper *w) const
{
    return m_List.contains(w);
}

bool TaskLifeCycleWrapperList::anyRunning() const
{
    return std::any_of(m_List.constBegin(),
                       m_List.constEnd(),
                       [](TaskLifeCycleWrapper *w) -> bool { return w->isRunning(); }
                      );
}

bool TaskLifeCycleWrapperList::anyFinished() const
{
    return std::any_of(m_List.constBegin(),
                       m_List.constEnd(),
                       [](TaskLifeCycleWrapper *w) -> bool { return w->isFinished(); }
                      );
}

QList<TaskLifeCycleWrapper *> TaskLifeCycleWrapperList::getFinished() const
{
    QList<TaskLifeCycleWrapper *> ret;

    for (TaskLifeCycleWrapper *w : m_List)
    {
        if (w->isFinished())
        {
            ret << w;
        }
    }

    return ret;
}
