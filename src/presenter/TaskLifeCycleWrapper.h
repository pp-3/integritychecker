// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKLIFECYCLEWRAPPER_H
#define TASKLIFECYCLEWRAPPER_H

#include <functional>

#include <QHash>
#include <QList>
#include <QPair>
#include <QScopedPointer>
#include <QSharedPointer>

#include "tasks/TaskLifeCycle.h"

class TaskLifeCycleWrapper
{
    friend class TaskLifeCycleWrapperList;

public:
    TaskLifeCycleWrapper(const TaskLifeCycle::ptr &t, const QString &name);
    ~TaskLifeCycleWrapper();

    bool isFinished() const;
    bool isRunning() const;
    void cancel();

    void addFinishedFunction(TaskFinishedListener f);
    void addProgressFunction(TaskProgressListener f);
    void addStartedFunction(TaskStartListener f);

    TaskLifeCycle::RunState getState() const;
    QString getStateStr() const;
    const QString &              name() const { return theName; }
    const QDateTime& getStartTime() const { return theStartTime; }
    void progressUpdate();
    const QPair<int, int> &getProgress() const { return m_Progress; }
    int                    getTotalTasks() const;
    int                    getOpenTasks() const;

private:
    TaskLifeCycleWrapper(const TaskLifeCycleWrapper&) = delete;
    void operator=(const TaskLifeCycleWrapper&) = delete;

    void clearTlc();

private:
    TaskLifeCycle::ptr      theTlc;
    QString                 theName;
    QDateTime               theStartTime;
    QScopedPointer<QObject> pSignalContext;
    QPair<int, int>         m_Progress;
    bool                    m_Canceled;
};


class TaskLifeCycleWrapperList
{
public:
    TaskLifeCycleWrapperList();
    ~TaskLifeCycleWrapperList();

    void add(TaskLifeCycleWrapper *w);
    bool remove(TaskLifeCycleWrapper *w);
    void clear();
          TaskLifeCycleWrapper *topPos(int row)       { return m_List.at(row); }
    const TaskLifeCycleWrapper *topPos(int row) const { return m_List.at(row); }
    bool contains(TaskLifeCycleWrapper *w) const;
    bool isEmpty() const { return m_List.isEmpty(); }
    int  size()    const { return m_List.size(); }
    int getListPos(TaskLifeCycleWrapper *w) const { return m_List.indexOf(w); }

    bool anyRunning() const;
    bool anyFinished() const;
    QList<TaskLifeCycleWrapper *> getFinished() const;

private:
    QList<TaskLifeCycleWrapper *>        m_List;
};

#endif // TASKLIFECYCLEWRAPPER_H
