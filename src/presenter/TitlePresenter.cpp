// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TitlePresenter.h"

#include "Version.h"
#include "shared/UserSettings.h"

TitlePresenter::TitlePresenter(UserSettingsManager *user_manager,
                               QMainWindow         *main_wnd) :
    QObject(nullptr),
    pUserManager(user_manager),
    pMainWnd(main_wnd)
{
    initialize();
    initializeConnections();
}

void TitlePresenter::initialize()
{
    QString profile;
    QSharedPointer<UserProfileSettingsIfc> sett = pUserManager->getCurrentUserSettings();
    if (!sett.isNull() && sett->profileName()!=UserSettingsManager::DEFAULT_PROFILE_NAME)
    {
        profile = sett->profileName();
    }

    QString title("Integrity Checker ");
    title += Version::getStrVersion();
    title += "  -  ";
    title += profile;

    pMainWnd->setWindowTitle(title);
}

void TitlePresenter::initializeConnections()
{
    connect(pUserManager, &UserSettingsManager::profileChanged, this, &TitlePresenter::initialize);
}
