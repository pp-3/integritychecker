// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ViewSettings.h"

#include <QCheckBox>
#include <QSpacerItem>

#include "checksums/SumTypeIfc.h"
#include "checksums/SumTypeManager.h"
#include "model/IntegrityModelIfc.h"
#include "worker/WorkerSettings.h"

ViewSettings::ViewSettings(QWidget *viewOptions, QHBoxLayout *viewTypesGroupLayout, IntegrityModelIfc *model) :
    QObject(),
    pViewOptionsGroup(viewOptions),
    pViewTypesGroupLayout(viewTypesGroupLayout),
    pModel(model)
{
    initialize();

    // connections
    connect(pModel->getWorkerSettings(), &WorkerSettings::s_EnabledTypeChanged,
            this,                        &ViewSettings::enabledModelChanged);
}

void ViewSettings::initialize()
{
    initializeViewOptions();
}

void ViewSettings::initializeViewOptions()
{
    SumTypeManager& stm = SumTypeManager::getInstance();
    const QList<SumTypeIfc *> &types = stm.getRegisteredTypes();
    const SumTypeHashList en = pModel->getWorkerSettings()->getEnabledCalcHashes();

    for (SumTypeIfc *const it : types)
    {
        QCheckBox* cb = new QCheckBox(it->getName(), pViewOptionsGroup);
        cb->setChecked(en.contains(it->getTypeHash()));
        pViewTypesGroupLayout->addWidget(cb);
        connect(cb, &QCheckBox::toggled, this, &ViewSettings::enabledTypeToggled);
    }

    QSpacerItem *filler = new QSpacerItem(10, 40, QSizePolicy::Expanding, QSizePolicy::Minimum);
    pViewTypesGroupLayout->addItem(filler);
}

void ViewSettings::enabledTypeToggled(bool enabled)
{
    QCheckBox *cb = qobject_cast<QCheckBox *>(sender());
    Q_ASSERT(cb);
    SumTypeManager& stm = SumTypeManager::getInstance();
    SumTypeIfc *sti = stm.fromName(cb->text());
    if (sti)
    {
        pModel->getWorkerSettings()->setEnabledType(sti->getTypeHash(), enabled);
    }
}

void ViewSettings::enabledModelChanged()
{
    const SumTypeHashList enabled = pModel->getWorkerSettings()->getEnabledCalcHashes();
    SumTypeManager& stm = SumTypeManager::getInstance();

    for (int i=0 ; i<pViewTypesGroupLayout->count() ; ++i)
    {
        QWidget *w = pViewTypesGroupLayout->itemAt(i)->widget();
        if (w)
        {
            QCheckBox *cb = qobject_cast<QCheckBox *>(w);
            if (cb)
            {
                SumTypeIfc *sti = stm.fromName(cb->text());
                cb->setChecked(sti != nullptr &&
                               enabled.contains(sti->getTypeHash()));
            }
        }
    }
}
