// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DataCache.h"

#include <QReadLocker>
#include <QWriteLocker>

#include "ifc/UserSettingsManagerIfc.h"
#include "model/AsyncCleanUp.h"
#include "model/TaskDispatcher.h"

DataCache::DataCache(UserSettingsManagerIfc *user_settings, TaskDispatcher *task_dispatcher) :
    pUserSettings(user_settings),
    pTaskDispatcher(task_dispatcher),
    m_Cache(),
    m_Lock(),
    m_UsedBytes(0),
    m_SizeCheckRequested(false),
    m_DelayedSizeCheck(1000)
{
    Q_ASSERT(pUserSettings);
    Q_ASSERT(pTaskDispatcher);

    m_SizeCheckConnection = QObject::connect(&m_DelayedSizeCheck, &DelayedUpdate::signalOut, pTaskDispatcher, [this]()
    {
        // request trigger
        if (m_SizeCheckRequested.exchange(true) != false)
            return;

        // check size in task pool
        const qulonglong MAX = pUserSettings->getCurrentUserSettings()->getCacheSize() * 1024*1024;
        QSharedPointer<AsyncAdministrationTask> task(new AsyncAdministrationTask(
            [this, MAX](const AsyncAdministrationTask::ShouldCancelQuery &cancelQuery) -> bool
            {
                asyncCheckSize(MAX, cancelQuery);
                return true;
            },
            "CheckCacheSize"));
        m_SizeCheckTask = pTaskDispatcher->addTask(task.dynamicCast<TaskIfc>());

        m_SizeCheckTask->addFinishedCallback(
            [this](bool /*canceled*/)
            {
                AsyncCleanUp::deleteLater(m_SizeCheckTask);
                m_SizeCheckTask.reset();
                m_SizeCheckRequested = false;
            },
            pTaskDispatcher);
    });
}

DataCache::~DataCache()
{
    close();
}

void DataCache::close()
{
    QObject::disconnect(m_SizeCheckConnection);
    m_DelayedSizeCheck.stopSignalSending();

    TaskLifeCycle::ptr tmp(std::move(m_SizeCheckTask));
    if (tmp)
    {
        tmp->cancelTask();
        tmp->waitForTaskFinished();
    }

    QWriteLocker wl(&m_Lock);
    m_Cache.clear();
}

void DataCache::addData(const DataCacheKEY &key, const DataCache::value_ptr &data)
{
    Q_ASSERT(data);
    if (!data)
    {
        return;
    }

    QWriteLocker wl(&m_Lock);

    Q_ASSERT(!m_Cache.contains(key));
    m_Cache.insert(key, Entry(data));

    wl.unlock();
    checkSize();
}

DataCache::value_ptr DataCache::replaceData(const DataCacheKEY &key, const value_ptr &data)
{
    Q_ASSERT(data);
    if (!data)
    {
        return DataCache::value_ptr();
    }

    QWriteLocker wl(&m_Lock);

    Entry e = m_Cache.value(key);
    m_Cache.remove(key);
    m_Cache.insert(key, Entry(data));

    wl.unlock();
    checkSize();

    return e.Data;
}

bool DataCache::hasData(const DataCacheKEY &key) const
{
    QReadLocker rl(&m_Lock);
    return m_Cache.contains(key);
}

DataCache::const_value_ptr DataCache::getData(const DataCacheKEY &key) const
{
    QWriteLocker wl(&m_Lock);
    Entry e = m_Cache.value(key);
    if (e.Data)
    {
        e.touch();
        m_Cache.insert(key, e);
        return e.Data;
    }
    return DataCache::value_ptr();
}

DataCache::value_ptr DataCache::removeData(const DataCacheKEY &key)
{
    QWriteLocker wl(&m_Lock);
    const Entry e = m_Cache.value(key);
    m_Cache.remove(key);
    return e.Data;
}

bool DataCache::clearData(const DataCacheKEY &key)
{
    QWriteLocker wl(&m_Lock);
    return m_Cache.remove(key)>0;
}

void DataCache::checkSize()
{
    m_DelayedSizeCheck.signalIn();
}

void DataCache::asyncCheckSize(const qulonglong &max_bytes, const AsyncAdministrationTask::ShouldCancelQuery &cancelQuery)
{
    QReadLocker rl(&m_Lock);
    QList<DataCacheKEY> keys = m_Cache.keys();

    // sort for last access
    std::sort(keys.begin(),
              keys.end(),
              [this](const DataCacheKEY &l, const DataCacheKEY &r) -> bool
              {
                  return m_Cache.value(l).LastAccess < m_Cache.value(r).LastAccess;
              });

    qulonglong used = 0;

    QList<DataCacheKEY>::const_iterator it = keys.constBegin();

    // calculate sizes
    while (!cancelQuery() && used<=max_bytes && it!=keys.constEnd())
    {
        const DataCache::Entry e = m_Cache.value(*it);
        Q_ASSERT(e.Data);
        used += sizeof(decltype(m_Cache)::key_type);
        used += it->size();
        used += e.Data->getSize();
        ++it;
    }

    m_UsedBytes = used;

    if (cancelQuery())
        return;

    rl.unlock();
    // upgrading from read to write locker is not thread safe, but as this is only a cache, the worst thing that could happen
    // is removing an updated value, which then needs to be recalculated by the user
    QWriteLocker wl(&m_Lock);

    // remove oldest
    while (!cancelQuery() && it != keys.constEnd())
    {
        m_Cache.remove(*it);
        ++it;
    }
}
