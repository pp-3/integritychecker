// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DATACACHE_H
#define DATACACHE_H

#include <atomic>

#include <QDateTime>
#include <QHash>
#include <QReadWriteLock>

#include "tasks/AsyncAdministrationTask.h"
#include "tasks/TaskLifeCycle.h"

#include "DelayedUpdate.h"
#include "DefaultDataCacheType.h"

class UserSettingsManagerIfc;
class TaskDispatcher;


/**
 * The DataCache class provides a generic caching mechanism for different types of data. Data has to be
 * provided as a DataCacheItem instance. For performance reasons each data instance needs to have a
 * related key, which is used for storing and retrieving the data.
 * The DataCache stores data up to a given size. If the used size exceeds the limit the oldest accessed
 * item is removed. Thus, this cache is only suitable for data which may be restored otherwise, as it
 * may be removed at any time from the cache.
 * Cache is safe for multi threaded use. But also, between two method calls to the cache, the data could
 * have been altered by another thread.
 */
class DataCache : public DefaultDataCacheType
{
public:
    /**
     * Constructor.
     * @param user_settings The user settings needed for determining the maximum cache size.
     * @param task_dispatcher The task dispatcher to send administrative tasks to.
     */
    DataCache(UserSettingsManagerIfc *user_settings, TaskDispatcher *task_dispatcher);
    ~DataCache() override;

    void addData(const DataCacheKEY &key, const DataCache::value_ptr &data) override;
    DataCache::value_ptr replaceData(const DataCacheKEY &key, const DataCacheIfc::value_ptr &data) override;
    bool hasData(const DataCacheKEY &key) const override;
    DataCache::const_value_ptr getData(const DataCacheKEY &key) const override;
    DataCache::value_ptr removeData(const DataCacheKEY &key) override;
    bool clearData(const DataCacheKEY &key) override;

    size_t getUsedBytes() const override { return m_UsedBytes; }

    /**
     * @brief Blocks until internal tasks are finished and clears cache.
     */
    void close();

private:
    void checkSize();
    void asyncCheckSize(const qulonglong &max_bytes, const AsyncAdministrationTask::ShouldCancelQuery &cancelQuery);

private:
    /**
     * Structure used internally for managing the pointer and the last access time to the data.
     */
    struct Entry
    {
        Entry() :
            Data()
        {
        }
        Entry(const DataCache::value_ptr &d) :
            Data(d)
        {
            touch();
        }
        void touch()
        {
            LastAccess = QDateTime::currentMSecsSinceEpoch();
        }

        quint64              LastAccess;
        DataCache::value_ptr Data;
    };

    UserSettingsManagerIfc            *pUserSettings;
    TaskDispatcher                    *pTaskDispatcher;
    mutable QHash<DataCacheKEY, Entry> m_Cache;
    mutable QReadWriteLock             m_Lock;
    qulonglong                         m_UsedBytes;
    std::atomic_bool                   m_SizeCheckRequested;
    DelayedUpdate                      m_DelayedSizeCheck;
    TaskLifeCycle::ptr                 m_SizeCheckTask;
    QMetaObject::Connection            m_SizeCheckConnection;
};

#endif // DATACACHE_H
