// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DATACACHEITEM_H
#define DATACACHEITEM_H

#include <functional>

#include <QSharedPointer>

/**
 * Interface for all data items stored in a DataCache.
 */
struct DataCacheItem
{
    DataCacheItem() = default;
    virtual ~DataCacheItem() = default;

    /**
     * Queries the data size (in bytes). Doesn't have to be exact, but close enough.
     * @return The data size.
     */
    virtual size_t getSize() const = 0;
};


/**
 * An adapter for the DataCacheItem interface. Takes the data (as type T) and a function pointer calculating the data size.
 */
template <typename T>
struct DataCacheItemAdapter : public DataCacheItem
{
    using SizeCalcFunction = std::function<size_t(const T &)>;

    /**
     * Constructor.
     * @param data      The data to store. A copy is stored.
     * @param size_func A function pointer returning the size of above data.
     */
    explicit DataCacheItemAdapter(const T &data, SizeCalcFunction size_func) :
        theData(data), theSizeFunc(size_func)
    {}
    virtual ~DataCacheItemAdapter() = default;

    /**
     * @see DataCacheItem::getSize();
     */
    virtual size_t getSize() const
    {
        return theSizeFunc(theData);
    }
    /**
     * The stored data.
     * @return Reference to the stored data.
     */
    const T &getData() const { return theData; }

private:
    T theData;
    SizeCalcFunction theSizeFunc;
};


/**
 * Convenience adapter for storing a shared pointer in a DataCacheItemAdapter.
 */
template <typename T>
struct DataCachePtrAdapter : public DataCacheItemAdapter<QSharedPointer<T>>
{
    explicit DataCachePtrAdapter(const QSharedPointer<T> &data, std::function<size_t(const QSharedPointer<T> &data)> size_func) :
        DataCacheItemAdapter<QSharedPointer<T>>(data, size_func)
    {}
    virtual ~DataCachePtrAdapter() = default;
};

#endif // DATACACHEITEM_H
