// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DelayedUpdate.h"

#include <QDateTime>
#include <QMutexLocker>
#include <QThread>

DelayedUpdate::DelayedUpdate(int min_delay, QObject *parent) :
    QObject(parent),
    m_MinDelay(min_delay),
    m_pDelayTimer(new QTimer(this)),
    m_LastUpdate(0),
    m_Lock()
{
    m_pDelayTimer->setSingleShot(true);
    m_pDelayTimer->setInterval(m_MinDelay);
    connect(m_pDelayTimer, &QTimer::timeout, this, &DelayedUpdate::timer_timeout);
}

DelayedUpdate::~DelayedUpdate()
{
    deleteTimer();
}

void DelayedUpdate::stopSignalSending()
{
    // final update
    timer_timeout();
    deleteTimer();
}

void DelayedUpdate::deleteTimer()
{
    {
        QMutexLocker ml(&m_Lock);
        if (!m_pDelayTimer)
            return;
    }

    auto stop_func = [this]() -> void
    {
        QMutexLocker ml(&m_Lock);
        if (m_pDelayTimer)
        {
            m_pDelayTimer->stop();
            delete m_pDelayTimer;
            m_pDelayTimer = nullptr;
        }
    };

    if (QThread::currentThread() == thread())
        stop_func();
    else
        QTimer::singleShot(0, this, stop_func);
}

void DelayedUpdate::signalIn()
{
    if (QThread::currentThread() != thread())
    {
        QTimer::singleShot(0, this, &DelayedUpdate::signalIn);
        return;
    }

    QMutexLocker ml(&m_Lock);
    if (m_pDelayTimer)
    {
        if (QDateTime::currentMSecsSinceEpoch() - m_LastUpdate > m_MinDelay)
        {
            ml.unlock();
            timer_timeout();
        }
        else
        {
            if (!m_pDelayTimer->isActive())
            {
                m_pDelayTimer->start(m_MinDelay);
            }
        }
    }
}

void DelayedUpdate::timer_timeout()
{
    {
        QMutexLocker ml(&m_Lock);
        m_LastUpdate = QDateTime::currentMSecsSinceEpoch();
    }

    emit signalOut();
}
