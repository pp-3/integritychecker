// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DELAYEDUPDATE_H
#define DELAYEDUPDATE_H

#include <QMutex>
#include <QObject>
#include <QTimer>

/**
 * The DelayedUpdate class provides a way to slow down fast update cycles. This class collects contiguous updates
 * in a given time span and sends out a single update after the maximum tolerable delaying time is elapsed. This
 * class is useful for a specific setup:
 * - the signal handler is quite "expensive",
 * - updates are expected to be fast, and
 * - it's sufficient for the signal handler to get the last update, but probably losing some midway.
 *
 * The internal timer runs in the thread of the constructor's caller, if no parent is set. Or, if a parent is given, it runs
 * in the parent's context.
 * The public interface is thread safe.
 */
class DelayedUpdate : public QObject
{
    Q_OBJECT

public:
    /**
     * Constructor.
     * @param min_delay The time (in ms) between two out signals.
     * @param parent    The Qt parent.
     */
    explicit DelayedUpdate(int min_delay = 500, QObject *parent = 0);
    virtual ~DelayedUpdate() override;

    void stopSignalSending();

private:
    void deleteTimer();

signals:
    /**
     * The signal sent out, after the given time span elapsed.
     */
    void signalOut();

public slots:
    /**
     * An update is requested. The signalOut() is sent out after the given time span.
     */
    void signalIn();

private slots:
    void timer_timeout();

private:
    int     m_MinDelay;
    QTimer *m_pDelayTimer;
    qint64  m_LastUpdate;
    QMutex  m_Lock;
};

#endif // DELAYEDUPDATE_H
