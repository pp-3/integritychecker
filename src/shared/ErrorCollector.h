// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ERRORCOLLECTOR_H
#define ERRORCOLLECTOR_H

#include <QObject>
#include <QDateTime>

#include "ifc/ErrorPostIfc.h"

/**
 * Instance for collecting run time errors and propagate them to registered listeners.
 */
class ErrorCollector : public QObject,
                       public ErrorPostIfc
{
    Q_OBJECT

public:
    ErrorCollector();

signals:
    /**
     * A new error was posted. Signal is emitted from the thread sending it.
     * @param err         The error string.
     * @param module_name An optional module name.
     * @param timestamp   Time when the error was posted.
     */
    void newError(const QString &err, const QString &module_name, const QDateTime &timestamp);

    /**
     * A new warning was posted. Signal is emitted from the thread sending it.
     * @param warning     The warning.
     * @param module_name An optional module name.
     * @param timestamp   Time when the warning was posted.
     */
    void newWarning(const QString &warning, const QString &module_name, const QDateTime &timestamp);

public slots:
    void addError(const QString &err, const QString &module_name = QString()) override;
    void addWarning(const QString &warning, const QString &module_name = QString()) override;
};

#endif // ERRORCOLLECTOR_H
