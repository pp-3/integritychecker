// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include "ExitManager.h"

std::unique_ptr<ExitManager> ExitManager::m_pInstance;
std::atomic_bool ExitManager::m_ShuttingDown(false);

ExitManager::ExitManager() :
    m_Listeners()
{
}

ExitManager &ExitManager::getInstance()
{
    if (!m_pInstance)
    {
#ifndef ENABLE_TEST_ADDONS
        Q_ASSERT(!isShutDown());  // access after shutdown
                                  // disable for tests, as individual test cases start with a clean environment
#endif

        m_pInstance.reset(new ExitManager);
    }

    return *m_pInstance;
}

void ExitManager::tearDown()
{
    if (!ExitManager::m_pInstance)
        return;

    ExitManager::m_pInstance->m_ShuttingDown = true;

    while (!ExitManager::m_pInstance->m_Listeners.isEmpty())
    {
        ExitListnerIfc el = ExitManager::m_pInstance->m_Listeners.pop();
        el();
    }

    m_pInstance.reset();
}

void ExitManager::addExitListener(const ExitListnerIfc &listener)
{
    m_Listeners.push(listener);
}
