// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef EXITMANAGER_H
#define EXITMANAGER_H

#include <atomic>
#include <memory>

#include <QStack>

/**
 * The ExitManager class is intended for services which need to be informed about a closing application.
 */
class ExitManager
{
public:
    using ExitListnerIfc = std::function<void()>;

public:
    /**
     * Returns, whether the shut down process has started.
     * @return True, if shutting down. False, otherwise.
     */
    static bool isShutDown() { return m_ShuttingDown.load(); }

    /**
     * Returns the single instance.
     * @return The ExitManager instance.
     */
    static ExitManager &getInstance();

    /**
     * Adds a new exit listener. Will be called before process ends in reverse order.
     * @param listener The exit listener to call.
     */
    void addExitListener(const ExitListnerIfc &listener);

    /**
     * Notifies all exit listener. Call only from main before process ends.
     */
    static void tearDown();

private:
    ExitManager();

    static std::unique_ptr<ExitManager> m_pInstance;
    static std::atomic_bool             m_ShuttingDown;

    QStack<ExitListnerIfc> m_Listeners;
};

#endif // EXITMANAGER_H
