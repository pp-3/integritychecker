// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "LanguageSettings.h"

#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>
#include <QLibraryInfo>

#include "UserSettings.h"

LanguageSettings::LanguageSettings(UserSettingsManager *sett_manager) :
    QObject(nullptr),
    pUserSettingsManager(sett_manager),
    m_CurrentLang(QLocale::English)
{
    settingsProfileChanged();

    connect(pUserSettingsManager, &UserSettingsManager::profileChanged, this, &LanguageSettings::settingsProfileChanged);
}

LanguageSettings::~LanguageSettings()
{
    QCoreApplication::removeTranslator(&m_QtTranslator);
    QCoreApplication::removeTranslator(&m_AppTranslator);
}

void LanguageSettings::settingsProfileChanged()
{
    QSharedPointer<UserProfileSettingsIfc> user = pUserSettingsManager->getCurrentUserSettings();

    if (isUserConfigured())
    {
        setLanguage(QLocale(user->getLanguage()));
    }
    else
    {
        setLanguage(QLocale::system());
        user->setLanguage(QString());
    }
}

bool LanguageSettings::setLanguage(const QString &lang_abbreviation)
{
    QLocale l(lang_abbreviation);
    if (!l.name().isEmpty())
    {
        if (installTranslator(l))
        {
            m_CurrentLang = l;
            return true;
        }
        else if (l.name() == QLocale(QLocale::English).name())
        {
            return true;
        }
    }

    return false;
}

void LanguageSettings::setLanguage(const QLocale &l)
{
    if (m_CurrentLang != l)
    {
        m_CurrentLang = l;

        if (!installTranslator(m_CurrentLang))
        {
            m_CurrentLang = QLocale::English;
        }

        QSharedPointer<UserProfileSettingsIfc> user = pUserSettingsManager->getCurrentUserSettings();
        user->setLanguage(m_CurrentLang.name());
        QLocale::setDefault(m_CurrentLang);

        emit languageChanged(m_CurrentLang);
    }
    else
    {
        QSharedPointer<UserProfileSettingsIfc> user = pUserSettingsManager->getCurrentUserSettings();
        if (user->getLanguage() != m_CurrentLang.name())
        {
            user->setLanguage(m_CurrentLang.name());
        }
    }
}

bool LanguageSettings::installTranslator(const QLocale &lang)
{
    QCoreApplication::removeTranslator(&m_QtTranslator);
    QCoreApplication::removeTranslator(&m_AppTranslator);

    if (m_QtTranslator.load("qt_" + lang.name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
    {
        QCoreApplication::installTranslator(&m_QtTranslator);
    }

    if (m_AppTranslator.load(lang, I18N_PREFIX, QString(), I18N_SUB_DIR))
    {
        QCoreApplication::installTranslator(&m_AppTranslator);
        return true;
    }
    else
    {
        return false;
    }
}

const QLocale &LanguageSettings::getCurrentLanguage() const
{
    return m_CurrentLang;
}

QList<QLocale> LanguageSettings::getSupportedLanguages() const
{
    QList<QLocale> ret;
    ret << QLocale::English;

    QDir d(QCoreApplication::applicationDirPath() + "/" + I18N_SUB_DIR);
    QStringList filter;
    filter << QString(I18N_PREFIX)+I18N_FILTER;
    const QFileInfoList entries = d.entryInfoList(filter, QDir::AllEntries | QDir::NoDotAndDotDot);

    for (const QFileInfo &it : entries)
    {
        QString file = it.baseName();
        const int ind = file.lastIndexOf('_');
        if (ind > 0)
        {
            file = file.mid(ind+1);
            QLocale l(file);
            if (!l.name().isEmpty())
            {
                ret << l;
            }
        }
    }

    return ret;
}

bool LanguageSettings::isUserConfigured() const
{
    QSharedPointer<UserProfileSettingsIfc> user = pUserSettingsManager->getCurrentUserSettings();
    return !user->getLanguage().isEmpty();
}

QString LanguageSettings::getHelpPath() const
{
    QString name = QLocale().name();
    const int ind = name.indexOf("_");
    if (ind > 0)
    {
        name = name.left(ind);
    }

    QString file = QString(HELP_NAME) + name + ".html";

    return QCoreApplication::applicationDirPath() + "/" + I18N_SUB_DIR + "/" + file;
}
