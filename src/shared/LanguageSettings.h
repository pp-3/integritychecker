// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef LANGUAGESETTINGS_H
#define LANGUAGESETTINGS_H

#include <QList>
#include <QLocale>
#include <QObject>
#include <QString>
#include <QTranslator>

class UserSettingsManager;

/**
 * Stores the current used language. Language is initialized from user settings. If not set, the system setting is used.
 * If requested language is not supported, language falls back to build in English.
 */
class LanguageSettings : public QObject
{
    Q_OBJECT

public:
    /**
     * Constructor.
     * @param sett_manager The user settings.
     */
    explicit LanguageSettings(UserSettingsManager *sett_manager);
    virtual ~LanguageSettings() override;

    /**
     * Tries to change temporarily the language to one provided via the language abbreviation (e.g. en). Setting is not written to user settings.
     * @param lang_abbreviation The language.
     * @return True, if successful. False, otherwise.
     */
    bool setLanguage(const QString &lang_abbreviation);
    /**
     * Changes the current language. If language is not supported (_??.qm file not found i18n sub folder), falls back to English.
     * @param l The new language to use for string translations.
     */
    void setLanguage(const QLocale &l);
    /**
     * Returns the currently used language.
     * @return The locale used for the set language.
     */
    const QLocale &getCurrentLanguage() const;
    /**
     * Returns a list of supported languages. A language is supported, if a _??.qm file is found in the i18n sub folder.
     * @return List of supported languages. List contains at least English.
     */
    QList<QLocale> getSupportedLanguages() const;
    /**
     * Checks, if a language is configured in the current user settings.
     * @return True, if configured. False, otherwise.
     */
    bool isUserConfigured() const;

    /**
     * Builds a full path to the language related help html file (integritychecker_help_??.html) in the i18n sub folder. Doesn't check, if file exists.
     * @return The help file path for the current language.
     */
    QString getHelpPath() const;

private:
    bool installTranslator(const QLocale &lang);

private slots:
    void settingsProfileChanged();

signals:
    /**
     * Signals a language change.
     * @param lang The new language.
     */
    void languageChanged(const QLocale &lang);

private:
    UserSettingsManager *pUserSettingsManager;
    QLocale              m_CurrentLang;
    QTranslator          m_QtTranslator;
    QTranslator          m_AppTranslator;

    constexpr static const char *I18N_SUB_DIR = "i18n";
    constexpr static const char *I18N_PREFIX  = "integritychecker_";
    constexpr static const char *I18N_FILTER  = "*.qm";
    constexpr static const char *HELP_NAME    = "integritychecker_help_";
};

#endif // LANGUAGESETTINGS_H
