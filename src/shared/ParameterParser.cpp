// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ParameterParser.h"

#include <iostream>

#include <QList>

#include "LanguageSettings.h"
#include "Tr.h"
#include "Version.h"

ParameterParser::ParameterParser() :
    m_ShowGui(true),
    m_ProfileName(),
    m_LangSet(false),
    m_Interactive(false)
{
}

bool ParameterParser::parse(int argc, char *argv[], LanguageSettings *lang)
{
    QList<QString> params;
    for (int i=1 ; i<argc ; i++)
    {
        params << argv[i];
    }

    bool b;
    while (!params.isEmpty())
    {
        auto p = nextParam(params, b);
        if (p == "-h")
        {
            showHelp();
            return false;
        }
        else if (p == "-p")
        {
            m_ProfileName = nextParam(params, b);
            if (!b)
            {
                std::cout << Tr::tr("Error: profile name missing").toStdString() << std::endl;
                return false;
            }
        }
        else if (p=="-nogui" || p=="-console")
        {
            m_ShowGui = false;
        }
        else if (p == "-lang")
        {
            const QString lan_chars = nextParam(params, b);
            if (!b)
            {
                std::cout << Tr::tr("Error: language missing. Add 2 character abbreviation. E.g. en").toStdString() << std::endl;
                return false;
            }
            else
            {
                m_LangSet = lang->setLanguage(lan_chars);
            }
        }
        else if (p=="-i" || p=="-console")
        {
            m_Interactive = true;
        }
        else
        {
            std::cout << Tr::tr("Unknown parameter:").toStdString() << " " << p.toStdString() << std::endl;
            std::cout << std::endl;
            showHelp();
            return false;
        }
    }

    return true;
}

QString ParameterParser::nextParam(QList<QString> &l, bool &found)
{
    found = !l.isEmpty();
    if (found)
    {
        return l.takeFirst();
    }

    return QString();
}

void ParameterParser::showHelp()
{
    std::cout << "IntegrityChecker " << Version::getStrVersion().toStdString() << std::endl;
    std::cout << std::endl;
    std::cout << Tr::tr("Usage:").toStdString() << std::endl;
    QString params("integritychecker");
    params += " [-lang " + Tr::tr("language") +"]";
    params += " [-h]";
    params += " [-p " + Tr::tr("profile") + "]";
    params += " [-nogui]";
    params += " [-i]";
    params += " [-console]";
    std::cout << params.toStdString() << std::endl;
    std::cout << std::endl;
    std::cout << "-lang lang   " << Tr::tr("Set 2 character language for GUI").toStdString() << std::endl;
    std::cout << "-h           " << Tr::tr("Show help"                       ).toStdString() << std::endl;
    std::cout << "-p profile   " << Tr::tr("Profile name for user settings"  ).toStdString() << std::endl;
    std::cout << "-nogui       " << Tr::tr("Start without gui"               ).toStdString() << std::endl;
    std::cout << "-i           " << Tr::tr("Start an interactive shell"      ).toStdString() << std::endl;
    std::cout << "-console     " << Tr::tr("Shortcut for -nogui plus -i"     ).toStdString() << std::endl;
    std::cout << std::endl;
}
