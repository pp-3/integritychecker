// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PARAMETERPARSER_H
#define PARAMETERPARSER_H

#include "ifc/ApplicationParameterParserIfc.h"

class LanguageSettings;

/**
 * Command line parser. Returns user provided information.
 */
class ParameterParser : public ApplicationParameterParserIfc
{
public:
    explicit ParameterParser();
    ~ParameterParser() override = default;

    /**
     * Parses the command line parameters.
     * @param argc main's argc
     * @param argv main's argv
     * @param lang The language selection interface.
     * @return True, if parsing was successful. False, if not. Usage is printed to console.
     */
    bool parse(int argc, char *argv[], LanguageSettings *lang);

    bool showGui() const override { return m_ShowGui; }
    QString getProfile() const override { return m_ProfileName; }
    bool isLangSet() const override { return m_LangSet; }
    bool isInteractive() const override { return m_Interactive; }

private:
    /**
     * Returns the next parameter from list if available. Parameter is removed from list.
     * @param l The list to look into.
     * @param found Returns true, if parameter was found. False, otherwise.
     * @return The found parameter or empty, if not found.
     */
    static QString nextParam(QList<QString> &l, bool &found);

    /**
     * Prints the available parameters to the console.
     */
    void showHelp();

private:
    bool    m_ShowGui;
    QString m_ProfileName;
    bool    m_LangSet;
    bool    m_Interactive;
};

#endif // PARAMETERPARSER_H
