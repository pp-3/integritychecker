// Copyright 2020 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "PathUtils.h"


static const QList<QChar> sParantheses { '"', '\'' };

QStringList PathUtils::combinePaths(const QStringList &param)
{
    QStringList ret;

    QStringList left = param;
    QChar combiningChar = QChar();

    while (!left.isEmpty())
    {
        QString current = left.takeFirst();

        if (combiningChar.isNull())
        {
            const bool start = std::any_of(sParantheses.cbegin(),
                                           sParantheses.cend(),
                                           [current](const QChar &c) -> bool
                                           {
                                               return current.startsWith(c);
                                           });

            if (start)
            {
                combiningChar = current.at(0);
                current = current.mid(1);

                const bool end = current.endsWith(combiningChar);
                if (end)
                {
                    combiningChar = QChar();
                    current = current.left(current.size() - 1);
                }
            }

            ret << current;
        }
        else if (!combiningChar.isNull())
        {
            const bool end = current.endsWith(combiningChar);
            if (end)
            {
                combiningChar = QChar();
                current = current.left(current.size() - 1);
            }

            if (!ret.isEmpty())
                ret.last().append(QStringLiteral(" ") + current);
            else
                ret << current;
        }
    }

    return ret;
}
