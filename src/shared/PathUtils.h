// Copyright 2020 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PATHUTILS_H
#define PATHUTILS_H

#include <QString>
#include <QStringList>


/**
 * @brief Utility functions file system pathes.
 */
class PathUtils
{
public:
    /**
     * @brief A parameter list which uses blanks as parameter separator needs to enclose
     * file system pathes with blanks in parantheses. combinePaths() combines entries in
     * parantheses into a single string.
     * @param param The input parameters
     * @return The parameter list with all pathes combined.
     */
    static QStringList combinePaths(const QStringList &param);
};

#endif // PATHUTILS_H
