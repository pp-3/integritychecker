// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QString>

/**
 * The ResourceReader class provides an interface for reading resources.
 */
class ResourceReader
{
public:
    /**
     * Reads a string resource.
     * @param resource_name The name / alias of the resource to read.
     * @param ok An (optional) return value. If provided, the read success will be stored there.
     * @return The read in resource, if successful. An empty string, if reading failed.
     */
    QString readString(const QString &resource_name, bool *ok = nullptr);
};
