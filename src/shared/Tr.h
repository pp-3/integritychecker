// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TR_H
#define TR_H

#include <QString>

/**
 * Helper interface for translating strings in classes not sub classed from QObject. Thus, instead
 * of tr("string") use Tr::tr("string"). The lupdate tool recognizes the tr and offers the strings
 * in its GUI for translation.
 */
struct Tr
{
    /**
     * Translates the given string at run time.
     * @param c The string to translate.
     * @return The translated string, if translation was found, or the untranslated string, if not.
     */
    static QString tr(const char *c);

private:
    Tr();
};

#endif // TR_H
