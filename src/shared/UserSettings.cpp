// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "UserSettings.h"

#include <QThread>
#include <QVariant>

#include <thread_util/threadcheck.h>

const char *UserGlobalSettings::MAINWNDSIZE       = "MainWndSize";
const char *UserGlobalSettings::MAINWNDPOS        = "MainWndPos";
const char *UserGlobalSettings::SPLITPOS          = "SplitPos";
const char *UserGlobalSettings::FILETABLEWIDTHS   = "FileViewWidths";
const char *UserGlobalSettings::STATUSTABLEWIDTHS = "StatusTableWidths";
const char *UserGlobalSettings::LAST_PROFILE      = "LastProfile";

const char *UserGlobalSettings::DEFAULT_PROFILE_GROUP = "_default";

const char *UserProfileSettings::OUTPUTDIR_SEL     = "OutputDirSelection";
const char *UserProfileSettings::INPUT_DIR         = "InputDir";
const char *UserProfileSettings::OUTPUT_DIR        = "OutputDir";
const char *UserProfileSettings::WORKER_THREADS    = "WorkerThreads";
const char *UserProfileSettings::CACHE_SIZE        = "CacheSize";
const char *UserProfileSettings::ENABLED_TYPES     = "EnabledTypes";
const char *UserProfileSettings::LANGUAGE          = "Lang";
const char *UserProfileSettings::PROFILE_DESC      = "ProfileDesc";

const char *UserSettingsManager::DEFAULT_PROFILE_NAME = UserGlobalSettings::DEFAULT_PROFILE_GROUP;


UserSettingsManager::UserSettingsManager(std::unique_ptr<UserGlobalSettings> &&global_settings,
                                         QSharedPointer<UserProfileSettings> &&current_profile_settings) :
    QObject(),
    m_pGlobalSettings(std::move(global_settings)),
    m_pCurrentProfileSettings(std::move(current_profile_settings))
{
}

UserSettingsManager::UserSettingsManager() :
    UserSettingsManager(std::unique_ptr<UserGlobalSettings>(new UserGlobalSettings(createUserSettings(QString()))), {})
{
}

UserSettingsManager::~UserSettingsManager()
{
    m_pCurrentProfileSettings.clear();
    if (m_pGlobalSettings)
        if (m_pGlobalSettings->m_pSettings)
            m_pGlobalSettings->m_pSettings->sync();
    m_pGlobalSettings.reset();
}

QSharedPointer<QSettings> UserSettingsManager::createUserSettings(const QString &profile_group) const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QSharedPointer<QSettings> ret = QSharedPointer<QSettings>(new QSettings(QSettings::IniFormat, QSettings::UserScope, QString("integritychecker")));
    if (!profile_group.isEmpty())
    {
        ret->beginGroup(profile_group);
    }
    return ret;
}

QStringList UserSettingsManager::getProfiles() const
{
    QSharedPointer<QSettings> q = createUserSettings(QString());
    return q->childGroups();
}

bool UserSettingsManager::existsProfile(const QString &profile) const
{
    return getProfiles().contains(profile);
}

QSharedPointer<UserProfileSettingsIfc> UserSettingsManager::getCurrentUserSettings()
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pCurrentProfileSettings.isNull())
    {
        QString profile = m_pGlobalSettings->getLastProfileName();
        if (!existsProfile(profile))
        {
            profile.clear();
        }
        m_pCurrentProfileSettings = setCurrentUserProfile(profile).staticCast<UserProfileSettings>();
    }

    return m_pCurrentProfileSettings;
}

QSharedPointer<UserProfileSettingsIfc> UserSettingsManager::setCurrentUserProfile(QString profile)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (!profile.isEmpty() && !existsProfile(profile))
    {
        profile.clear();
    }

    const QString group = profile.isEmpty() ? UserGlobalSettings::DEFAULT_PROFILE_GROUP : profile;

    if (m_pGlobalSettings->getLastProfileName() == profile && !m_pCurrentProfileSettings.isNull())
    {
        return m_pCurrentProfileSettings;
    }

    const QSharedPointer<UserProfileSettings> old = m_pCurrentProfileSettings;
    m_pGlobalSettings->setLastProfileName(profile);

    QSharedPointer<QSettings> q = createUserSettings(group);
    m_pCurrentProfileSettings = QSharedPointer<UserProfileSettings>(new UserProfileSettings(q, group));

    emit profileChanged(old, m_pCurrentProfileSettings);

    return m_pCurrentProfileSettings;
}

QSharedPointer<UserProfileSettingsIfc> UserSettingsManager::copyProfile(const QString &to)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    const QString group = m_pCurrentProfileSettings->m_pSettings->group();
    if (m_pCurrentProfileSettings.isNull() ||
       to.isEmpty() ||
       to.compare(UserGlobalSettings::DEFAULT_PROFILE_GROUP, Qt::CaseInsensitive)==0 ||
       to.compare(group, Qt::CaseInsensitive)==0)
    {
        return QSharedPointer<UserProfileSettings>();
    }

    if (m_pCurrentProfileSettings->profileName().compare(to, Qt::CaseInsensitive) == 0)
    {
        return m_pCurrentProfileSettings;
    }

    const QSharedPointer<UserProfileSettings> old = m_pCurrentProfileSettings;
    old->m_pSettings->sync();
    QSharedPointer<QSettings> q = createUserSettings(to);
    m_pGlobalSettings->setLastProfileName(to);

    for (QString key : old->m_pSettings->childKeys())
    {
        q->setValue(key, old->m_pSettings->value(key));
    }

    q->sync();
    m_pCurrentProfileSettings = QSharedPointer<UserProfileSettings>(new UserProfileSettings(q, to));

    emit profileChanged(old, m_pCurrentProfileSettings);
    emit profileListChanged(getProfiles());

    return m_pCurrentProfileSettings;
}

QSharedPointer<UserProfileSettingsIfc> UserSettingsManager::renameProfile(const QString &to)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    const QString group = m_pCurrentProfileSettings->m_pSettings->group();
    if (m_pCurrentProfileSettings.isNull() ||
       to.isEmpty() ||
       to.compare(UserGlobalSettings::DEFAULT_PROFILE_GROUP, Qt::CaseInsensitive)==0 ||
       to.compare(group, Qt::CaseInsensitive)==0)
    {
        return QSharedPointer<UserProfileSettings>();
    }

    if (m_pCurrentProfileSettings->profileName().compare(to, Qt::CaseInsensitive) == 0)
    {
        return m_pCurrentProfileSettings;
    }

    m_pCurrentProfileSettings = copyProfile(to).staticCast<UserProfileSettings>();

    deleteProfile(group);

    return m_pCurrentProfileSettings;
}

QSharedPointer<UserProfileSettingsIfc> UserSettingsManager::deleteProfile(const QString &profile)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QSharedPointer<QSettings> q = createUserSettings(QString());
    q->remove(profile);
    q->sync();
    q.clear();

    if (m_pCurrentProfileSettings->profileName().compare(profile, Qt::CaseInsensitive) == 0)
    {
        const QSharedPointer<UserProfileSettings> old = m_pCurrentProfileSettings;
        old->stopWriting();
        m_pGlobalSettings->setLastProfileName(QString());
        m_pCurrentProfileSettings.clear();

        getCurrentUserSettings();
        emit profileChanged(old, m_pCurrentProfileSettings);
    }

    emit profileListChanged(getProfiles());

    return getCurrentUserSettings();
}





UserGlobalSettings::UserGlobalSettings(const QSharedPointer<QSettings> &global_settings) :
    m_pSettings(global_settings)
{
}

UserGlobalSettings::~UserGlobalSettings()
{
    m_pSettings->sync();
    m_pSettings.clear();
}

QSize UserGlobalSettings::getMainWndSize() const
{
    return m_pSettings->value(MAINWNDSIZE, QSize(0, 0)).value<QSize>();
}
void UserGlobalSettings::setMainWndSize(const QSize &size)
{
    m_pSettings->setValue(MAINWNDSIZE, size);
}

QPoint UserGlobalSettings::getMainWndPos() const
{
    return m_pSettings->value(MAINWNDPOS, QPoint(0, 0)).value<QPoint>();
}
void UserGlobalSettings::setMainWndPos(const QPoint &pos)
{
    m_pSettings->setValue(MAINWNDPOS, pos);
}

QByteArray UserGlobalSettings::getSplitPos() const
{
    return m_pSettings->value(SPLITPOS, QByteArray()).toByteArray();
}
void UserGlobalSettings::setSplitPos(const QByteArray &pos)
{
    m_pSettings->setValue(SPLITPOS, pos);
}

QByteArray UserGlobalSettings::getFileTableColumWidths() const
{
    return m_pSettings->value(FILETABLEWIDTHS, QByteArray()).toByteArray();
}

void UserGlobalSettings::setFileTableColumWidths(const QByteArray &widths)
{
    m_pSettings->setValue(FILETABLEWIDTHS, widths);
}

QByteArray UserGlobalSettings::getStatusColumWidths() const
{
    return m_pSettings->value(STATUSTABLEWIDTHS, QByteArray()).toByteArray();
}

void UserGlobalSettings::setStatusColumWidths(const QByteArray &widths)
{
    m_pSettings->setValue(STATUSTABLEWIDTHS, widths);
}

QString UserGlobalSettings::getLastProfileName() const
{
    return m_pSettings->value(LAST_PROFILE, QString(DEFAULT_PROFILE_GROUP)).toString();
}
void UserGlobalSettings::setLastProfileName(const QString &profile)
{
    m_pSettings->setValue(LAST_PROFILE, profile);
}






UserProfileSettings::UserProfileSettings(const QSharedPointer<QSettings> &profile_settings, const QString &profile_name) :
    m_ProfileName(profile_name),
    m_pSettings(profile_settings)
{
}

UserProfileSettings::~UserProfileSettings()
{
    stopWriting();
}

void UserProfileSettings::stopWriting()
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (!m_pSettings.isNull())
    {
        m_pSettings->sync();
        m_pSettings.clear();
    }
}

QString UserProfileSettings::getInputDir() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
    {
        Q_ASSERT(false);
        return QString();
    }

    return m_pSettings->value(INPUT_DIR, QString("")).toString();
}
void UserProfileSettings::setInputDir(const QString &dir)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
        return;

    if (dir.isEmpty())
    {
        m_pSettings->remove(INPUT_DIR);
    }
    else
    {
        m_pSettings->setValue(INPUT_DIR, dir);
    }
}

QString UserProfileSettings::getOutputDirSelection() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
    {
        Q_ASSERT(false);
        return {};
    }

    return m_pSettings->value(OUTPUTDIR_SEL, QVariant(0)).toString();
}
void UserProfileSettings::setOutputDirSelection(const QString &sel)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
        return;

    if (sel.isEmpty())
    {
        m_pSettings->remove(OUTPUTDIR_SEL);
    }
    else
    {
        m_pSettings->setValue(OUTPUTDIR_SEL, QVariant(sel));
    }
}

QString UserProfileSettings::getOutputDir() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
    {
        Q_ASSERT(false);
        return QString();
    }

    return m_pSettings->value(OUTPUT_DIR, QString("")).toString();
}
void UserProfileSettings::setOutputDir(const QString &dir)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
        return;

    if (dir.isEmpty())
    {
        m_pSettings->remove(OUTPUT_DIR);
    }
    else
    {
        m_pSettings->setValue(OUTPUT_DIR, dir);
    }
}

unsigned int UserProfileSettings::getWorkerThreads() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
    {
        Q_ASSERT(false);
        return 0;
    }

    const int cores = std::max(2, QThread::idealThreadCount());
    return m_pSettings->value(WORKER_THREADS, QVariant(cores)).toUInt();
}
void UserProfileSettings::setWorkerThreads(unsigned int w)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
        return;

    m_pSettings->setValue(WORKER_THREADS, QVariant(w));
}

uint UserProfileSettings::getCacheSize() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
    {
        Q_ASSERT(false);
        return 0;
    }

    return m_pSettings->value(CACHE_SIZE, QVariant(100)).toUInt();
}
void UserProfileSettings::setCacheSize(uint c)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
        return;

    m_pSettings->setValue(CACHE_SIZE, QVariant(c));
}

QStringList UserProfileSettings::getEnabledTypes() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
    {
        Q_ASSERT(false);
        return QStringList();
    }

    QVariant types = m_pSettings->value(ENABLED_TYPES, QVariant(QString()));
    if (!types.isNull() && types.isValid())
    {
        QString str = types.toString();
        if (!str.isEmpty())
        {
            return str.split(",");
        }
    }
    return QStringList();
}

void UserProfileSettings::setEnabledTypes(const QStringList &types)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
        return;

    m_pSettings->setValue(ENABLED_TYPES, types.join(","));
}

QString UserProfileSettings::getLanguage() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
    {
        Q_ASSERT(false);
        return QString();
    }

    return m_pSettings->value(LANGUAGE, QVariant(QString())).toString();
}
void UserProfileSettings::setLanguage(const QString &lang)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
        return;

    m_pSettings->setValue(LANGUAGE, lang);
}

QString UserProfileSettings::getProfileDescription() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
    {
        Q_ASSERT(false);
        return QString();
    }

    return m_pSettings->value(PROFILE_DESC, QVariant(QString())).toString();
}
void UserProfileSettings::setProfileDescription(const QString &desc)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (m_pSettings.isNull())
        return;

    m_pSettings->setValue(PROFILE_DESC, desc);
}
