// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef USERSETTINGS_H
#define USERSETTINGS_H

#include <QObject>
#include <QScopedPointer>
#include <QSettings>

#include "ifc/UserSettingsManagerIfc.h"

/**
 * The settings instance containing all values which are independent of a profile.
 */
class UserGlobalSettings : public UserGlobalSettingsIfc
{
    friend class UserSettingsManager;

private:
    UserGlobalSettings(const QSharedPointer<QSettings> &global_settings);

public:
    virtual ~UserGlobalSettings() override;

    QSize getMainWndSize() const override;
    void setMainWndSize(const QSize &size) override;

    QPoint getMainWndPos() const override;
    void setMainWndPos(const QPoint &pos) override;

    QByteArray getSplitPos() const override;
    void setSplitPos(const QByteArray &pos) override;

    QByteArray getFileTableColumWidths() const override;
    void setFileTableColumWidths(const QByteArray &widths) override;

    QByteArray getStatusColumWidths() const override;
    void setStatusColumWidths(const QByteArray &widths) override;

private:
    QString getLastProfileName() const;
    void setLastProfileName(const QString &profile);

private:
    QSharedPointer<QSettings> m_pSettings;

    static const char* MAINWNDSIZE;
    static const char* MAINWNDPOS;
    static const char* SPLITPOS;
    static const char* FILETABLEWIDTHS;
    static const char* STATUSTABLEWIDTHS;
    static const char *LAST_PROFILE;
    static const char *DEFAULT_PROFILE_GROUP;
};


/**
 * The user settings which belong to a profile. User may switch between different settings at run time. The
 * settings are managed via profiles. For each profile one instance of this user settings exists.
 */
class UserProfileSettings : public UserProfileSettingsIfc
{
    friend class UserSettingsManager;

private:
    UserProfileSettings(const QSharedPointer<QSettings> &profile_settings, const QString &profile_name);
    void stopWriting();

public:
    virtual ~UserProfileSettings() override;

    const QString &profileName() const override { return m_ProfileName; }

    QString getInputDir() const override;
    void setInputDir(const QString &dir) override;

    QString getOutputDirSelection() const override;
    void setOutputDirSelection(const QString &sel) override;

    QString getOutputDir() const override;
    void setOutputDir(const QString &dir) override;

    unsigned int getWorkerThreads() const override;
    void setWorkerThreads(unsigned int w) override;

    uint getCacheSize() const override;
    void setCacheSize(uint c) override;

    QStringList getEnabledTypes() const override;
    void setEnabledTypes(const QStringList &types) override;

    QString getLanguage() const override;
    void setLanguage(const QString &lang) override;

    QString getProfileDescription() const override;
    void setProfileDescription(const QString &desc) override;

private:
    QString                   m_ProfileName;
    QSharedPointer<QSettings> m_pSettings;

    static const char* OUTPUTDIR_SEL;
    static const char* INPUT_DIR;
    static const char* OUTPUT_DIR;
    static const char* WORKER_THREADS;
    static const char* CACHE_SIZE;
    static const char* ENABLED_TYPES;
    static const char* LANGUAGE;
    static const char* PROFILE_DESC;
};


/**
 * The UserSettingsManager provides the global settings and manages different profiles for the user settings.
 * Users of the user settings should listen to the profileChanged() signal, as the current profile may change at run time.
 */
class UserSettingsManager : public QObject,
                            public UserSettingsManagerIfc
{
    Q_OBJECT

private:
    friend class UserSettingsManagerMock;  // constructor for tests
    UserSettingsManager(std::unique_ptr<UserGlobalSettings> &&global_settings,
                        QSharedPointer<UserProfileSettings> &&current_profile_settings);

public:
    explicit UserSettingsManager();
    virtual ~UserSettingsManager() override;

    const UserGlobalSettingsIfc *getGlobalSettings() const override { return m_pGlobalSettings.get(); }
          UserGlobalSettingsIfc *getGlobalSettings()       override { return m_pGlobalSettings.get(); }
    QStringList getProfiles() const override;
    bool existsProfile(const QString &profile) const override;
    QSharedPointer<UserProfileSettingsIfc> getCurrentUserSettings() override;
    QSharedPointer<UserProfileSettingsIfc> setCurrentUserProfile(QString profile) override;
    QSharedPointer<UserProfileSettingsIfc> copyProfile(const QString &to) override;
    QSharedPointer<UserProfileSettingsIfc> renameProfile(const QString &to) override;
    QSharedPointer<UserProfileSettingsIfc> deleteProfile(const QString &profile) override;

    QSharedPointer<QSettings> createUserSettings(const QString &profile_group) const;

signals:
    /**
     * Signals a change of the current user settings profile. Direct connected listeners may rely on both profiles are valid as they receive the signal.
     * @param old_profile The current profile getting switched off.
     * @param new_profile The profile the user settings are switching to.
     */
    void profileChanged(const QSharedPointer<UserProfileSettings> &old_profile,
                        const QSharedPointer<UserProfileSettings> &new_profile);
    /**
     * Signals a change in the list of available profiles.
     * @param profiles The new profile list.
     */
    void profileListChanged(const QStringList &profiles);

private:
    std::unique_ptr<UserGlobalSettings> m_pGlobalSettings;
    QSharedPointer<UserProfileSettings> m_pCurrentProfileSettings;

public:
    static const char *DEFAULT_PROFILE_NAME;
};

#endif // USERSETTINGS_H
