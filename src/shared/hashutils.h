// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef HASHUTILS_H
#define HASHUTILS_H

#include <QString>

#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
namespace std
{

/**
 * @brief Adapter for QT versions not providing a std::hash<QString>
 */
template<>
struct hash<QString> {
    /**
     * Provides a std::hash<QString> by reusing qHash.
     */
    std::size_t operator()(const QString &v) const noexcept
    {
        return qHash(v);
    }
};

}
#endif

#endif // HASHUTILS_H


