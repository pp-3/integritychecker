// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMTYPES_H
#define SUMTYPES_H

#include <QList>
#include <QSet>
#include <QString>

#include <shared/Tr.h>

/**
 * Wrapper for the integrity enum states a file may be assigned to.
 */
struct CheckSumState
{
    /**
     * The States enum represents the current state of a file.
     */
    enum States
    {
        UNKNOWN,      ///< The initial state. If no integrity data exists for a file, the file stays in this state.
        NOT_CHECKING, ///< Files in this state are neither verified, nor the integrity data is calculated from them.
        EXISTS,       ///< Stored integrity data has been found for the file in this state, but no verification has been done yet.
        VERIFIED,     ///< Stored integrity data has been found and it matched with a fresh calculation.
        CHECK_ERROR   ///< Stored integrity data has been found. A comparison with a fresh calculation failed.
    };

    /**
     * Returns the list of the states with the implied meaning, that the integrity data exists.
     * @return List of states with integrity data.
     */
    static QList<CheckSumState::States> getStateList()
    {
        return QList<CheckSumState::States>() << EXISTS << VERIFIED << CHECK_ERROR;
    }
    /**
     * Returns a list with all possible states.
     * @return All enums as list.
     */
    static QList<CheckSumState::States> getAllStatesList()
    {
        return getStateList() << UNKNOWN << NOT_CHECKING;
    }

    /**
     * Returns a string representation of the states with integrity data involved.
     * @param s The state.
     * @return The state string.
     */
    static QString toString(const CheckSumState::States s)
    {
        switch (s)
        {
            case EXISTS      : return Tr::tr("Integrity data exists");
            case VERIFIED    : return Tr::tr("Integrity data is verified");
            case CHECK_ERROR : return Tr::tr("Integrity data error");
            default          : return QString();
        }
    }
};

/**
 * Definition of an hash value for an integrity type.
 */
using SumTypeHash = unsigned long;
using SumTypeHashList = QSet<SumTypeHash>;

#endif // SUMTYPES_H
