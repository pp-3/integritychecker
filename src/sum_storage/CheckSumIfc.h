// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CHECKSUMIFC_H
#define CHECKSUMIFC_H

/**
 * Interface for all integrity data formats (check sums, ...)
 */
class CheckSumIfc
{
public:
    virtual ~CheckSumIfc() = default;

    /**
     * Compares two check sums. Comparing different types (sub classes) will always fail.
     * @param other The integrity data to compare to.
     * @return True, if the integrity data matches. False, otherwise.
     */
    virtual bool compare(const CheckSumIfc &other) const = 0;

protected:
    CheckSumIfc() = default;
};

#endif // CHECKSUMIFC_H
