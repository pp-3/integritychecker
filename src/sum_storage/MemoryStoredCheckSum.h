// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef MEMORYSTOREDCHECKSUM_H
#define MEMORYSTOREDCHECKSUM_H

#include <QByteArray>

#include "CheckSumIfc.h"

/**
 * The MemoryStoredCheckSum class stores short check sums in memory for fast access.
 */
class MemoryStoredCheckSum : public CheckSumIfc
{
public:
    /**
     * Constructor.
     * @param checksum The checksum to store.
     */
    explicit MemoryStoredCheckSum(const QByteArray &checksum);
    virtual ~MemoryStoredCheckSum() override = default;

    /**
     * Returns the stored checksum.
     * @return The checksum.
     */
    const QByteArray &toByteArray() const { return theChecksum; }
    /**
     * Compares the stored checksum if another MemoryStoredCheckSum.
     * @param other The checksum to compare to.
     * @return True, if equal. False, otherwise.
     */
    bool compare(const CheckSumIfc &other) const override;

private:
    QByteArray theChecksum;
};

#endif // MEMORYSTOREDCHECKSUM_H
