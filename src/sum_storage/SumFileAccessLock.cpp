// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumFileAccessLock.h"

#include <QFileInfo>
#include <QMutexLocker>

SumFileAccessLock &SumFileAccessLock::getInstance()
{
    static SumFileAccessLock instance;
    return instance;
}

SumFileAccessLocker SumFileAccessLock::getReadLocker(const QFile &file)
{
    QString path = getPath(file);
    if (path.isEmpty())
    {
        Q_ASSERT(false);
        return SumFileAccessLocker();
    }

    QReadWriteLock *l = getLock(path);
    return SumFileAccessLocker::createReadLocker(std::move(path), l);
}

SumFileAccessLocker SumFileAccessLock::getWriteLocker(const QFile &file)
{
    QString path = getPath(file);
    if (path.isEmpty())
    {
        Q_ASSERT(false);
        return SumFileAccessLocker();
    }

    QReadWriteLock *l = getLock(path);
    return SumFileAccessLocker::createWriteLocker(std::move(path), l);
}

QString SumFileAccessLock::getPath(const QFile &file)
{
    QFileInfo fi(file);
    return fi.absoluteFilePath();
}

QReadWriteLock *SumFileAccessLock::getLock(const QString &file)
{
    QMutexLocker ml(&m_InternalLock);

    AccessCount &l = m_FileLocks[file];
    l.inc();

    return &l.m_Lock;
}

void SumFileAccessLock::returnLock(const QString &file)
{
    QMutexLocker ml(&m_InternalLock);

    AccessCount &l = m_FileLocks[file];
    Q_ASSERT(l.m_Users > 0);

    if (l.dec())
    {
        // last user
        m_FileLocks.erase(file);
    }
}




SumFileAccessLocker::SumFileAccessLocker(QString &&p, std::unique_ptr<QReadLocker> &&r) :
    path(std::move(p)),
    m_ReadLocker(std::move(r))
{
}

SumFileAccessLocker::SumFileAccessLocker(QString &&p, std::unique_ptr<QWriteLocker> &&w) :
    path(std::move(p)),
    m_WriteLocker(std::move(w))
{
}

SumFileAccessLocker::~SumFileAccessLocker()
{
    m_ReadLocker.reset();
    m_WriteLocker.reset();

    if (isLocked())
        SumFileAccessLock::getInstance().returnLock(path);
}

SumFileAccessLocker SumFileAccessLocker::createReadLocker(QString &&p, QReadWriteLock *l)
{
    return SumFileAccessLocker(std::move(p), std::make_unique<QReadLocker>(l));
}

SumFileAccessLocker SumFileAccessLocker::createWriteLocker(QString &&p, QReadWriteLock *l)
{
    return SumFileAccessLocker(std::move(p), std::make_unique<QWriteLocker>(l));
}

const QString &SumFileAccessLocker::getLockedPath() const
{
    return path;
}

bool SumFileAccessLocker::isLocked() const
{
    return !path.isEmpty();
}
