// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMFILEACCESSLOCK_H
#define SUMFILEACCESSLOCK_H

#include <memory>
#include <unordered_map>

#include <QFile>
#include <QMutex>
#include <QReadLocker>
#include <QReadWriteLock>
#include <QString>
#include <QWriteLocker>

#include <shared/hashutils.h>

class SumFileAccessLocker;

/**
 * As all integrity data files may be accessed from different threads, the parallel access needs to be
 * managed. All access to an integrity file should fetch a read or write locker from the single
 * instance of this class and keep the instance during reading / writing the file.
 */
class SumFileAccessLock
{
    friend class SumFileAccessLocker;

    /**
     * Internal structure for managing multiple accesses to one file.
     */
    struct AccessCount
    {
        AccessCount() :
            m_Lock(),
            m_Users(0)
        {}

        void inc() { m_Users++; }
        bool dec() { m_Users--; return m_Users==0; }

        QReadWriteLock m_Lock;
        size_t         m_Users;
    };

public:
    /**
     * Returns a read locker for the given file. The read lock is held as long the instance is also held. Deleting the locker releases the lock.
     * @param file The file the returned locker is related to.
     * @return The read locker.
     */
    SumFileAccessLocker getReadLocker(const QFile &file);
    /**
     * Returns a write locker for the given file. The write lock is held as long the instance is also held. Deleting the locker releases the lock.
     * @param file The file the returned locker is related to.
     * @return The write locker.
     */
    SumFileAccessLocker getWriteLocker(const QFile &file);

    static SumFileAccessLock &getInstance();

private:
    Q_DISABLE_COPY(SumFileAccessLock)

    SumFileAccessLock() = default;

    static QString getPath(const QFile &file);
    QReadWriteLock *getLock(const QString &file);
    void returnLock(const QString &file);

private:
    QMutex                                   m_InternalLock;
    std::unordered_map<QString, AccessCount> m_FileLocks;
};


/**
 * The SumFileAccessLocker class manages the life time of a file lock. The SumFileAccessLock class manages read / write locks for individual files
 * and provides SumFileAccessLocker instances for those files. The requested lock for a given file is held as long the according SumFileAccessLocker
 * instance is kept alive. Releasing the SumFileAccessLocker instance automatically releases the file lock.
 */
class SumFileAccessLocker
{
    friend class SumFileAccessLock;

public:
    SumFileAccessLocker() = default;
    SumFileAccessLocker(SumFileAccessLocker &&) = default;
    SumFileAccessLocker &operator=(SumFileAccessLocker &&) = default;
    ~SumFileAccessLocker();

    /**
     * Returns the full path of file being locked with this locker instance.
     * @return The locked file path.
     */
    const QString &getLockedPath() const;

    /**
     * Returns whether a file lock is actually held. Can be only false, if the provided file path was invalid.
     * @return True, if the file exists and is locked. False, if the file is not available.
     */
    bool isLocked() const;

private:
    SumFileAccessLocker(QString &&p, std::unique_ptr<QReadLocker> &&r);
    SumFileAccessLocker(QString &&p, std::unique_ptr<QWriteLocker> &&w);
    SumFileAccessLocker(const SumFileAccessLocker &) = delete;

    static SumFileAccessLocker createReadLocker(QString &&p, QReadWriteLock *l);
    static SumFileAccessLocker createWriteLocker(QString &&p, QReadWriteLock *l);

private:
    QString path;
    std::unique_ptr<QWriteLocker> m_WriteLocker;
    std::unique_ptr<QReadLocker> m_ReadLocker;
};

#endif // SUMFILEACCESSLOCK_H
