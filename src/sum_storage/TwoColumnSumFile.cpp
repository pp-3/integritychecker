// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TwoColumnSumFile.h"

#include <QDir>
#include <QFileInfo>
#include <QMutexLocker>
#include <QRegExp>

#include "ifc/ErrorPostIfc.h"
#include "shared/Tr.h"

#include "SumFileAccessLock.h"

static const char *MODULE_NAME = "TwoColumnSumFile";

TwoColumnSumFile::TwoColumnSumFile(const QDir                 &checksum_dir,
                                   const QString              &checksum_file,
                                         SumFileAccessLock    &lock,
                                         DefaultDataCacheType *cache,
                                         ErrorPostIfc         *errors,
                                   const int                   hash_char_len,
                                   const QRegExp              &separator) :
    m_File(checksum_dir.filePath(checksum_file)),
    theFileLock(lock),
    theCache(cache),
    theErrorCollector(errors),
    m_HashCharLen(hash_char_len),
    m_Separator(separator)
{
}

bool TwoColumnSumFile::exists() const
{
    return m_File.exists();
}

bool TwoColumnSumFile::isValid()
{
    return isValid(false);
}

bool TwoColumnSumFile::isValid(bool already_locked)
{
    if (!exists())
        return false;

    SumFileAccessLocker lock;
    if (!already_locked)
        lock = theFileLock.getReadLocker(m_File);

    return isCacheUpToDate(true) || parse(true);
}

QByteArray TwoColumnSumFile::readCheckSum(const QString &file_name)
{
    SumFileAccessLocker lock = theFileLock.getReadLocker(m_File);

    if (!isValid(true))
        return QByteArray();

    // isValid parsed the value and stored it in the cache
    QSharedPointer<const CachedData> data = theCache->getTypedData<TwoColumnSumFile::CachedData>(m_File.fileName());
    if (data.isNull())
    {
        theErrorCollector->addWarning(Tr::tr("Expected data vanished"), MODULE_NAME);
        return QByteArray();
    }

    return data->m_LocalSums.value(file_name);
}

bool TwoColumnSumFile::writeCheckSum(const QString &file_name, const QByteArray &checksum, const QString &write_separator)
{
    SumFileAccessLocker lock = theFileLock.getWriteLocker(m_File);

    if (!isValid(true))
    {
        if (m_File.exists())
        {
            theErrorCollector->addError(Tr::tr("Check sum file exists, but reading failed before writing: ")+m_File.fileName(), MODULE_NAME);
            return false;
        }
    }

    QSharedPointer<const CachedData> data = theCache->getTypedData<TwoColumnSumFile::CachedData>(m_File.fileName());

    CachedData new_data;
    if (!data.isNull())
    {
        new_data = *data;
        data.clear();
    }
    theCache->removeData(m_File.fileName()); // fill cache during next read

    new_data.m_LocalSums.insert(file_name, checksum);

    return writeLocalDataToFile(write_separator, new_data);
}

bool TwoColumnSumFile::writeLocalDataToFile(const QString &write_separator, const CachedData &data)
{
    if (!ensurePathExist(m_File))
    {
        theErrorCollector->addError(Tr::tr("Creating path failed for: ")+m_File.fileName(), MODULE_NAME);
        theErrorCollector->addError(m_File.errorString(), MODULE_NAME);
        return false;
    }

    // copy existing file to bak for recovering in error case
    QString bak_file;
    if (m_File.exists())
    {
        for (int i=0 ; i<10 && bak_file.isEmpty() ; i++)
        {
            bak_file = m_File.fileName()+".bak."+QString::number(i+1);
            if (QFile(bak_file).exists())
            {
                bak_file.clear();
            }
            else
            {
                QFile bak(m_File.fileName());
                if (!bak.rename(bak_file))
                {
                    bak_file.clear();
                }
            }
        }
    }

    bool ok = m_File.open(QIODevice::WriteOnly | QIODevice::Truncate | QFile::Text);
    if (!ok)
    {
        theErrorCollector->addError(Tr::tr("Check sum file open failed: ")+m_File.fileName(), MODULE_NAME);
        theErrorCollector->addError(m_File.errorString(), MODULE_NAME);
    }
    else
    {
        for (QMap<QString, QByteArray>::const_iterator it=data.m_LocalSums.constBegin() ; it!=data.m_LocalSums.constEnd() ; ++it)
        {
            const QString str = write_separator + it.key() + "\n";
            if (m_File.write(it.value().toHex() + str.toUtf8()) < 0)
            {
                ok = false;
                theErrorCollector->addError(Tr::tr("Writing check sum file failed: ")+m_File.fileName(), MODULE_NAME);
                theErrorCollector->addError(m_File.errorString(), MODULE_NAME);
            }
        }
    }

    m_File.close();

    if (!bak_file.isEmpty())
    {
        if (!ok)
        {
            // recover
            QFile rec(bak_file);
            if (rec.exists())
            {
                m_File.remove();
                if (!rec.rename(m_File.fileName()))
                {
                    theErrorCollector->addError(Tr::tr("Check sum back up file renaming failed: ")+rec.fileName()+" -> "+m_File.fileName(), MODULE_NAME);
                    theErrorCollector->addError(m_File.errorString(), MODULE_NAME);
                }
            }
        }
        else
        {
            // remove bak file
            QFile rec(bak_file);
            if (rec.exists())
            {
                if (!rec.remove())
                {
                    theErrorCollector->addError(Tr::tr("Removing check sum back file failed: ")+rec.fileName(), MODULE_NAME);
                    theErrorCollector->addError(m_File.errorString(), MODULE_NAME);
                }
            }
        }
    }

    return ok;
}

bool TwoColumnSumFile::isCacheUpToDate(bool already_locked)
{
    SumFileAccessLocker lock;
    if (!already_locked)
        lock = theFileLock.getReadLocker(m_File);

    const QSharedPointer<const CachedData> data = theCache->getTypedData<TwoColumnSumFile::CachedData>(m_File.fileName());
    if (data.isNull())
    {
        return false;
    }

    QFileInfo fi(m_File);
    const bool b = data->m_LastModified.isValid() && fi.lastModified() == data->m_LastModified;
    if (!b)
    {
        theCache->clearData(m_File.fileName());
    }
    return b;
}

bool TwoColumnSumFile::parse(bool already_locked)
{
    SumFileAccessLocker lock;
    if (!already_locked)
        lock = theFileLock.getReadLocker(m_File);

    QList<QPair<QString, QByteArray>> lines;

    bool ok = m_File.open(QIODevice::ReadOnly | QIODevice::Text);
    if (ok)
    {
        while (!m_File.atEnd())
        {
            const QByteArray b = m_File.readLine();

            QPair<QString, QByteArray> name_sum;
            if (parseLine(b, name_sum))
            {
                lines << name_sum;
            }
            else
            {
                ok = false;
                theErrorCollector->addError(Tr::tr("Error during parsing line in checksum file: ")+m_File.fileName()+" : "+QString::fromUtf8(b), MODULE_NAME);
            }
        }
        m_File.close();
    }

    if (!lines.isEmpty())
    {
        QSharedPointer<CachedData> data = QSharedPointer<CachedData>::create();

        QFileInfo fi(m_File);
        data->m_LastModified = fi.lastModified();

        for (QPair<QString, QByteArray> it : lines)
        {
            data->m_LocalSums.insert(it.first, it.second);
        }

        theCache->replaceData(m_File.fileName(), data);
    }
    else
    {
        theCache->clearData(m_File.fileName());
    }

    return ok;
}

bool TwoColumnSumFile::parseLine(const QByteArray &line, QPair<QString, QByteArray> &name_sum)
{
    QByteArray b = line;

    // take hash
    if (b.size() <= m_HashCharLen)
    {
        theErrorCollector->addError(Tr::tr("Check sum line size shorter than required: ")+QString("%1 <-> %2").arg(b.size()).arg(m_HashCharLen)+" -> "+QString::fromUtf8(line), MODULE_NAME);
        return false;
    }

    name_sum.second = QByteArray::fromHex(b.left(m_HashCharLen));
    b = b.mid(m_HashCharLen);

    // string version
    QString l = QString::fromUtf8(b);

    // search for white space separator
    if (m_Separator.indexIn(l) != 0)
    {
        theErrorCollector->addError(Tr::tr("Check sum separator not found in line: ")+l, MODULE_NAME);
        return false;
    }

    // file name
    l = l.mid(m_Separator.matchedLength());
    name_sum.first = l.trimmed();

    return true;
}

bool TwoColumnSumFile::ensurePathExist(const QFile &f)
{
    QFileInfo fi(f);
    const QDir d = fi.absoluteDir();

    return d.mkpath(d.path());
}



size_t TwoColumnSumFile::CachedData::getSize() const
{
    if (m_LocalSums.isEmpty())
    {
        return 0;
    }

    // simplified calculation
    QMap<QString, QByteArray>::const_iterator it = m_LocalSums.constBegin();
    return m_LocalSums.size() * (it.key().size() + it.value().size());
}
