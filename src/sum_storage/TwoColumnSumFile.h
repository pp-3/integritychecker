// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TWOCOLUMNSUMFILE_H
#define TWOCOLUMNSUMFILE_H

#include <QByteArray>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QMap>
#include <QPair>
#include <QRegExp>
#include <QSharedPointer>
#include <QString>

#include "shared/DefaultDataCacheType.h"

class SumFileAccessLock;
class ErrorPostIfc;

/**
 * The TwoColumnSumFile class reads and writes integrity data from a file containing one integrity data and the related file name per line.
 * The integrity data and the file names are set up in two columns. The left columns contains the integrity data with a fixed character length.
 * The right column contains the file names.
 */
class TwoColumnSumFile
{
    /**
     * Internal structure used for the global cache.
     */
    struct CachedData : public DataCacheItem
    {
        QDateTime                 m_LastModified;
        QMap<QString, QByteArray> m_LocalSums;

        virtual size_t getSize() const;
    };

public:
    /**
     * Constructor.
     * @param checksum_dir  The directory with the integrity data file name
     * @param checksum_file The integrity data file name (without path).
     * @param lock          The file access lock instance.
     * @param cache         The global cache instance.
     * @param errors        The error interface.
     * @param hash_char_len The count of characters for the integrity data in the left column.
     * @param separator     The RE separating both columns.
     */
    TwoColumnSumFile(const QDir                 &checksum_dir,
                     const QString              &checksum_file,
                           SumFileAccessLock    &lock,
                           DefaultDataCacheType *cache,
                           ErrorPostIfc         *errors,
                     const int                   hash_char_len,
                     const QRegExp              &separator);

    /**
     * Checks, whether the integrity data file exists.
     * @return True, for exists. False, otherwise.
     */
    bool exists() const;
    /**
     * Checks, whether the integrity data file contains a valid file structure (two columns).
     * @return True, if valid. False, otherwise.
     */
    bool isValid();
    /**
     * Returns the integrity data for the given file name.
     * @param file_name The file name to look for in the integrity data file.
     * @return The read integrity data, if file name found. Empty, otherwise.
     */
    QByteArray readCheckSum(const QString &file_name);
    /**
     * Writes the given integrity data to for the given file name to the integrity data file. If the file name already exists, it's overwritten.
     * @param file_name The file name the integrity data belongs to.
     * @param checksum The integrity data to write.
     * @param write_separator The separator to write in between the integrity data and the file name.
     * @return True, if successful. False, otherwise. In the later case, the error log contains the reason.
     */
    bool writeCheckSum(const QString &file_name, const QByteArray &checksum, const QString &write_separator);

protected:
    bool writeLocalDataToFile(const QString &write_separator, const CachedData &data);
    bool isValid(bool already_locked);
    bool isCacheUpToDate(bool already_locked);
    bool parse(bool already_locked);
    bool parseLine(const QByteArray &line, QPair<QString, QByteArray> &name_sum);

    static bool ensurePathExist(const QFile &f);

private:
    QFile                 m_File;
    SumFileAccessLock    &theFileLock;
    DefaultDataCacheType *theCache;
    ErrorPostIfc         *theErrorCollector;
    int                   m_HashCharLen;
    QRegExp               m_Separator;
};

#endif // TWOCOLUMNSUMFILE_H
