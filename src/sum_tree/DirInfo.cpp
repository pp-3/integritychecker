// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DirInfo.h"

#include <QMutexLocker>

#include "TreeDataIfc.h"

DirInfo::DirInfo(const QString &name) :
    InfoBase(name),
    InfoParent(),
    m_Children()
{
}

DirInfo::~DirInfo()
{
    DirInfo::shutdown();
}

void DirInfo::shutdown()
{
    if (isShutDown())
        return;

    DirInfo::removeAllChildren();

    InfoBase::shutdown();
}

void DirInfo::registerChild(const InfoBase::ptr &child)
{
    QMutexLocker ml(getTreeData().getLock());

    child->init(this, theTreeData);
    Q_ASSERT(!m_Children.contains(child));
    m_Children.append(child);

    childSumCountChanged();
}

void DirInfo::removeChild(InfoBase *child)
{
    QMutexLocker ml(getTreeData().getLock());

    QList<InfoBase::ptr>::iterator B = m_Children.begin();

    for (auto it = B ; it != m_Children.end() ; ++it) {
        if (it->data() == child)
        {
            InfoBase::ptr tmp = std::move(*it);

            m_Children.erase(it);
            childSumCountChanged();
            ml.unlock();

            tmp->shutdown();
            return;
        }
    }
}

void DirInfo::removeAllChildren()
{
    QMutexLocker ml(getTreeData().getLock());
    QList<InfoBase::ptr> tmp;
    tmp.swap(m_Children);
    m_Children.clear();
    ml.unlock();

    for (InfoBase::ptr &it : tmp)
        it->shutdown();

    tmp.clear();

    childSumCountChanged();
}

InfoBase::ptr DirInfo::searchChild(const InfoBase *child) const
{
    QMutexLocker ml(getTreeData().getLock());

    for (const InfoBase::ptr &it : std::as_const(m_Children))
    {
        if (child == it.data())
            return it;

        if (it->isDir())
        {
            InfoBase::ptr ret = DirInfo::cast(it)->searchChild(child);
            if (ret)
                return ret;
        }
    }

    return {};
}

QList<InfoBase::ptr> DirInfo::getChildren() const
{
    QMutexLocker ml(getTreeData().getLock());
    return m_Children;
}

unsigned long DirInfo::getSumCount(SumTypeHash type_hash, CheckSumState::States state) const
{
    QMutexLocker ml(getTreeData().getLock());

    if (m_CachedStates.contains(type_hash))
    {
        const SumStateCount &cnt = m_CachedStates[type_hash];
        if (cnt.contains(state))
        {
            return cnt[state];
        }
    }

    unsigned long count = 0;
    for (const InfoBase::ptr &it : m_Children)
    {
        count = count + it->getSumCount(type_hash, state);
    }

    // store in cache
    m_CachedStates[type_hash][state] = count;

    return count;
}

InfoBase::SumStateCount DirInfo::getSumStateCount(SumTypeHash type_hash) const
{
    QMutexLocker ml(getTreeData().getLock());
    InfoBase::SumStateCount ret;

    // look into cache
    if (m_CachedStates.contains(type_hash))
    {
        ret = m_CachedStates[type_hash];
    }

    // calculate the missing data
    const QList<CheckSumState::States> states = CheckSumState::getAllStatesList();

    for (const CheckSumState::States it : states)
    {
        if (!ret.contains(it))
            ret.insert(it, getSumCount(type_hash, it));
    }

    return ret;
}

void DirInfo::childSumCountChanged(SumTypeHash type_hash)
{
    if (isShutDown())
        return;

    QMutexLocker ml(getTreeData().getLock());
    if (!m_CachedStates.contains(type_hash))
        return;

    // clear cache
    m_CachedStates.remove(type_hash);

    // preload cache
    getSumStateCount(type_hash);

    getParent()->childSumCountChanged(type_hash);
}

void DirInfo::childSumCountChanged()
{
    if (isShutDown())
        return;

    {
        QMutexLocker ml(getTreeData().getLock());
        if (m_CachedStates.isEmpty())
            return;

        m_CachedStates.clear();
    }

    InfoBase *ib = getParent()->getInfoBase();
    if (ib)
    {
        DirInfo::cast(ib)->childSumCountChanged();
    }
}
