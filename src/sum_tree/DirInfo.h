// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DIRINFO_H
#define DIRINFO_H

#include <QHash>
#include <QList>

#include "InfoBase.h"
#include "InfoParent.h"

/**
 * The DirInfo class represents a directory in the file system. The integrity summary count is
 * a recursive sum of all children.
 */
class DirInfo : public InfoBase,
                public InfoParent
{
public:
    using ptr = QSharedPointer<DirInfo>;
    using const_ptr = QSharedPointer<const DirInfo>;
    using weak_ptr = QWeakPointer<DirInfo>;

    /**
     * Constructor.
     * @param name The directory name.
     */
    explicit DirInfo(const QString &name);
    virtual ~DirInfo() override;

    bool isDir() const override final
    {
        return InfoParent::isDir();
    }
    const QString &getName() const override
    {
        return InfoBase::getName();
    }
    QString getSubPath() const override
    {
        return InfoBase::getSubPath();
    }
    InfoParent *getParent() const override
    {
        return InfoBase::getParent();
    }
    /**
     * Returns current instance as InfoBase type. Needed, because defined in InfoParent.
     * @return This instance as InfoBase pointer.
     */
    InfoBase *getInfoBase() override final
    {
        return this;
    }
    const InfoBase *getInfoBase() const override final
    {
        return this;
    }

    /**
     * Returns the count of files (including all sub directories) which are in the given state for the given integrity data type.
     * @param type_hash The integrity data type (e.g. MD5) as related hash value.
     * @param state     The file state (e.g. VERIFIED) to check for.
     * @return Count of files.
     */
    unsigned long getSumCount(SumTypeHash type_hash, CheckSumState::States state) const override;
    InfoBase::SumStateCount getSumStateCount(SumTypeHash type_hash) const override;

    void registerChild(const InfoBase::ptr &child) override;
    void removeChild(InfoBase *child) override;
    void removeAllChildren() override;
    InfoBase::ptr searchChild(const InfoBase *child) const override;
    QList<InfoBase::ptr> getChildren() const override;

    /**
     * Casts an InfoBase instance to a DirInfo, if it's a directory.
     * @param base The base class instance.
     * @return The DirInfo pointer, if it's a directory. Otherwise, nullptr.
     */
    static DirInfo *cast(InfoBase *const base)
    {
        if (base->isDir())
        {
            Q_ASSERT(dynamic_cast<DirInfo *>(base));
            return static_cast<DirInfo *>(base);
        }

        return nullptr;
    }
    static DirInfo::ptr cast(const InfoBase::ptr &base)
    {
        if (base->isDir())
        {
            Q_ASSERT(base.dynamicCast<DirInfo>());
            return base.staticCast<DirInfo>();
        }

        return nullptr;
    }

protected:
    /**
     * Signals this instance it is going to be discarded. Shuts down all children.
     */
    void shutdown() override;

    void childSumCountChanged(SumTypeHash type_hash) override;

private:
    void childSumCountChanged();

private:
    QList<InfoBase::ptr> m_Children;
    mutable QHash<SumTypeHash, SumStateCount> m_CachedStates;
};

#endif // DIRINFO_H
