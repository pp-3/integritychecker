// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DirInfoRoot.h"

#include <checksums/SumTypeManager.h>
#include <sum_shared/CheckSumState.h>


DirInfoRoot::DirInfoRoot(const QDir &input_dir) :
    InfoParent(),
    m_pRoot(),
    m_pGlobalTreeLock(QSharedPointer<GlobalLock>::create())
{
    setRoot(input_dir);
}

DirInfoRoot::~DirInfoRoot()
{
    deleteTree();
}

void DirInfoRoot::setRoot(const QDir &root)
{
    deleteTree();
    // top dir contains the name of the input dir, but does not add to the sub path
    m_pRoot.reset(new TopDir(root.exists() ?
                               root.dirName() :
                               QString(),
                             this,
                             m_pGlobalTreeLock));
}

void DirInfoRoot::deleteTree()
{
    DirInfo::ptr tmp = std::move(m_pRoot);
    m_pRoot.reset();

    if (tmp)
    {
        tmp.dynamicCast<TopDir>()->shutdown();
        tmp.reset();
    }
}

void DirInfoRoot::cleanFromUnusedIntegrityTypes(const SumTypeHashList &enabled_types)
{
    QMutexLocker ml(m_pGlobalTreeLock->getLock());

    if (!m_pRoot)
        return;

    const QList<SumTypeIfc *> &all_sums = SumTypeManager::getInstance().getRegisteredTypes();

    SumTypeHashList typesToRemove;
    for (SumTypeIfc *sum : all_sums)
    {
        const SumTypeHash hash = sum->getTypeHash();
        if (!enabled_types.contains(hash))
            typesToRemove << hash;
    }

    cleanFromUnusedIntegrityTypes(typesToRemove, m_pRoot);
}

void DirInfoRoot::cleanFromUnusedIntegrityTypes(const SumTypeHashList &typesToRemove, DirInfo::ptr &di)
{
    for (const InfoBase::ptr &ib : di->getChildren())
    {
        if (ib->isDir())
        {
            DirInfo::ptr d = DirInfo::cast(ib);
            cleanFromUnusedIntegrityTypes(typesToRemove, d);
        }
        else
        {
            FileInfo::ptr fi = FileInfo::cast(ib);
            cleanFromUnusedIntegrityTypes(typesToRemove, fi);
        }
    }
}

void DirInfoRoot::cleanFromUnusedIntegrityTypes(const SumTypeHashList &typesToRemove, FileInfo::ptr &fi)
{
    for (const SumTypeHash hash : typesToRemove)
    {
        fi->clearSumState(hash);
    }
}

InfoBase::ptr DirInfoRoot::searchChild(const InfoBase *child) const
{
    QMutexLocker ml(m_pGlobalTreeLock->getLock());

    if (!m_pRoot)
        return {};

    if (child == m_pRoot.data())
        return m_pRoot;

    return m_pRoot->searchChild(child);
}
