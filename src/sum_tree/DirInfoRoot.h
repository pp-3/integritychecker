// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DIRINFOROOT_H
#define DIRINFOROOT_H

#include <QDir>
#include <QString>

#include "DirInfo.h"
#include "FileInfo.h"
#include "InfoParent.h"
#include "TreeDataIfc.h"

/**
 * The DirInfoRoot class is the root instance for the integrity data tree.
 */
class DirInfoRoot : public InfoParent
{
public:
    using ptr = QSharedPointer<DirInfoRoot>;
    using weak_ptr = QWeakPointer<DirInfoRoot>;

    /**
     * Constructor.
     * @param input_dir The root dir in the file system, as set by the user.
     */
    explicit DirInfoRoot(const QDir &input_dir);
    virtual ~DirInfoRoot() override;

    /**
     * Let the TreeData clean its tree from unused integrity types.
     * @param enabled_types The currently enabled and used types.
     */
    void cleanFromUnusedIntegrityTypes(const SumTypeHashList &enabled_types);

    InfoBase::ptr searchChild(const InfoBase *child) const override;

    /**
      * Returns the DirInfo instance representing the root directory of the integrity data tree.
      * @return
      */
    DirInfo::ptr       getRoot()       { return m_pRoot; }
    DirInfo::const_ptr getRoot() const { return m_pRoot; }

protected:
    void childSumCountChanged(SumTypeHash /*type_hash*/) override final {}

private:
    void setRoot(const QDir &root);
    void cleanFromUnusedIntegrityTypes(const SumTypeHashList &typesToRemove, DirInfo::ptr &di);
    void cleanFromUnusedIntegrityTypes(const SumTypeHashList &typesToRemove, FileInfo::ptr &fi);

    void deleteTree();
    void registerChild(const InfoBase::ptr &/*child*/) override final { Q_ASSERT(false); }
    void removeChild(InfoBase */*child*/) override final {}
    void removeAllChildren() override final { Q_ASSERT(false); }
    const QString &getName() const override final { Q_ASSERT(false); static QString null; return null; }
    QString getSubPath() const override final { Q_ASSERT(false); return QString(); }
    InfoParent *getParent() const override final { return nullptr; }
    QList<InfoBase::ptr> getChildren() const override final { Q_ASSERT(false); return QList<InfoBase::ptr>(); }
    InfoBase *getInfoBase() override final { return nullptr; }
    const InfoBase *getInfoBase() const override final { return nullptr; }

private:
    DirInfo::ptr                m_pRoot;
    QSharedPointer<TreeDataIfc> m_pGlobalTreeLock;

    /**
     * @brief The TopDir class stores the root entry (/) of the checksum tree. Main task is to terminate
     * upwards searches (e.g. getParent()).
     */
    class TopDir : public DirInfo
    {
    public:
        TopDir(const QString &name, InfoParent *parent, const QSharedPointer<TreeDataIfc> &treedata) :
            DirInfo(name)
        {
            init(parent, treedata);
        }
        QString getSubPath() const override final { return QString(); }
        //InfoParent *getParent() const override final { return nullptr; }
        void shutdown() override
        {
            DirInfo::shutdown();
        }
    };

    class GlobalLock : public TreeDataIfc
    {
    public:
        GlobalLock() :
            m_GlobalTreeLock(QMutex::Recursive)
        {}
        QMutex *getLock() override final { return &m_GlobalTreeLock; }

    private:
        QMutex m_GlobalTreeLock;
    };
};

#endif // DIRINFOROOT_H
