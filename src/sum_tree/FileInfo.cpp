// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "FileInfo.h"

#include <QMutexLocker>

#include "InfoParent.h"
#include "TreeDataIfc.h"

FileInfo::FileInfo(const QString &name) :
    InfoBase(name)
{
}

FileInfo::~FileInfo()
{
    FileInfo::shutdown();
}

void FileInfo::shutdown()
{
    if (isShutDown())
        return;

    QMutexLocker ml(getTreeData().getLock());
    m_SumInfo.clear();

    InfoBase::shutdown();
}

unsigned long FileInfo::getSumCount(SumTypeHash type_hash, CheckSumState::States state) const
{
    QMutexLocker ml(getTreeData().getLock());
    if (m_SumInfo.value(type_hash, CheckSumState::UNKNOWN) == state)
    {
        return 1;
    }

    return 0;
}

InfoBase::SumStateCount FileInfo::getSumStateCount(SumTypeHash type_hash) const
{
    InfoBase::SumStateCount ret;

    QMutexLocker ml(getTreeData().getLock());
    ret.insert(m_SumInfo.value(type_hash, CheckSumState::UNKNOWN), 1);

    return ret;
}

void FileInfo::setSumState(SumTypeHash type_hash, CheckSumState::States new_state)
{
    if (isShutDown())
        return;

    QMutexLocker ml(getTreeData().getLock());

    if (m_SumInfo.value(type_hash, CheckSumState::UNKNOWN) != new_state)
    {
        m_SumInfo.insert(type_hash, new_state);

        getParent()->childSumCountChanged(type_hash);
    }
}

void FileInfo::clearSumState(SumTypeHash type_hash)
{
    if (isShutDown())
        return;

    QMutexLocker ml(getTreeData().getLock());

    if (m_SumInfo.remove(type_hash) > 0)
    {
        getParent()->childSumCountChanged(type_hash);
    }
}

bool FileInfo::existsChecksum(SumTypeHash type_hash) const
{
    QMutexLocker ml(getTreeData().getLock());

    const CheckSumState::States s = m_SumInfo.value(type_hash, CheckSumState::UNKNOWN);
    ml.unlock();

    return (s!=CheckSumState::UNKNOWN && s!=CheckSumState::NOT_CHECKING);
}
