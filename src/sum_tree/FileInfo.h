// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FILEINFO_H
#define FILEINFO_H

#include <QHash>

#include "InfoBase.h"

class DirInfo;

/**
 * The FileInfo class represents a file in the file system and stores the integrity data state the file is currently in.
 */
class FileInfo : public InfoBase
{
public:
    using ptr = QSharedPointer<FileInfo>;
    using weak_ptr = QWeakPointer<FileInfo>;

    /**
     * Constructor.
     * @param name The file name.
     */
    explicit FileInfo(const QString &name);
    virtual ~FileInfo() override;

    /**
     * @see TreeInfoIfc::isDir()
     * @return Always false.
     */
    bool isDir() const override final
    {
        return false;
    }

    /**
     * Returns the information, if the file is in the given state for a given integrity data type.
     * @param type_hash The integrity data type (e.g. MD5) as related hash value.
     * @param state     The file state (e.g. VERIFIED) to check for.
     * @return 1, if the file is in this state for the given data type, or 0 if not.
     */
    unsigned long getSumCount(SumTypeHash type_hash, CheckSumState::States state) const override;
    InfoBase::SumStateCount getSumStateCount(SumTypeHash type_hash) const override;
    /**
     * Sets a new file state for the given integrity data type.
     * @param type_hash The integrity data type (e.g. MD5) as related hash value.
     * @param new_state The new state.
     */
    void setSumState(SumTypeHash type_hash, CheckSumState::States new_state);
    /**
     * Removes the integrity data state information for the given data type.
     * @param type_hash The integrity data type (e.g. MD5) as related hash value.
     */
    void clearSumState(SumTypeHash type_hash);
    /**
     * Checks, if integrity data was found (EXISTS, VERIFIED, ERROR) for the given data type.
     * @param type_hash The integrity data type (e.g. MD5) as related hash value.
     * @return True, if integrity data exists. False, otherwise.
     */
    bool existsChecksum(SumTypeHash type_hash) const;

    /**
     * Casts an InfoBase to a FileInfo, if appropriate.
     * @param base The base class instance.
     * @return The FileInfo instance, if it's a file. nullptr, otherwise.
     */
    static FileInfo *cast(InfoBase *const base)
    {
        if (!base->isDir())
        {
            Q_ASSERT(dynamic_cast<FileInfo *>(base));
            return static_cast<FileInfo *>(base);
        }

        return nullptr;
    }
    static FileInfo::ptr cast(const InfoBase::ptr &base)
    {
        if (!base->isDir())
        {
            Q_ASSERT(base.dynamicCast<FileInfo>());
            return base.staticCast<FileInfo>();
        }

        return nullptr;
    }

protected:
    /**
     * Signals this instance it is going to be discarded. Shuts down all children.
     */
    void shutdown() override;

private:
    // file info -> type hash to state for files
    QHash<SumTypeHash, CheckSumState::States> m_SumInfo;
};

#endif // FILEINFO_H
