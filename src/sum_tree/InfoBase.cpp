// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "InfoBase.h"
#include "InfoParent.h"
#include "TreeDataIfc.h"

InfoBase::InfoBase(const QString &name) :
    m_Name(name),
    pParent(nullptr),
    m_ShuttingDown(false),
    theTreeData(nullptr)
{
}

void InfoBase::shutdown()
{
    if (isShutDown())
    {
        return;
    }

    QMutexLocker ml(getTreeData().getLock());

    m_ShuttingDown = true;
    m_Name.clear();

    if (pParent != nullptr)
    {
        InfoParent *p = pParent;
        pParent = nullptr;

        p->removeChild(this);
    }
    theTreeData = nullptr;
}

QString InfoBase::getSubPath() const
{
    if (m_ShuttingDown)
    {
        return QString();
    }

    InfoParent *p = getParent();
    if (p)
    {
        InfoBase *b = p->getInfoBase();
        if (b)
        {
            QString sub = b->getSubPath();
            if (!sub.isEmpty())
            {
                sub += DirSeparator;
            }
            return sub + getName();
        }
    }

    return getName();
}
