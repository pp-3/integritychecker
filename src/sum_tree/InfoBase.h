// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INFOBASE_H
#define INFOBASE_H

#include <atomic>

#include <QHash>
#include <QList>
#include <QReadWriteLock>
#include <QSharedPointer>

#include "sum_shared/CheckSumState.h"

#include "TreeInfoIfc.h"

class InfoParent;
class TreeDataIfc;

/**
 * Base class for integrity tree items providing access to the current integrity state(s).
 */
class InfoBase : public TreeInfoIfc
{
    friend class TreeDataIfc;
    friend class DirInfo;

public:
    using ptr = QSharedPointer<InfoBase>;
    using weak_ptr = QWeakPointer<InfoBase>;
    using SumStateCount = QHash<CheckSumState::States, unsigned long>;

    static constexpr char DirSeparator = '/';

    /**
     * Constructor.
     * @param name   The name of the tree entry (= file / directory name in the file system).
     */
    explicit InfoBase(const QString &name);
    ~InfoBase() override = default;

    const QString &getName() const override
    {
        return m_Name;
    }

    QString getSubPath() const override;

    InfoParent *getParent() const override
    {
        return pParent;
    }

    /**
     * Returns the count items in this instance which are in the given state for the given integrity data type.
     * @param type_hash The integrity data type (e.g. MD5) as related hash value.
     * @param state     The file / directory state (e.g. VERIFIED) to check for.
     * @return The count of files in the given state for the given integrity data type.
     */
    virtual unsigned long getSumCount(SumTypeHash type_hash, CheckSumState::States state) const = 0;

    /**
     * Returns all counts for a given integrity data type. Returns the same counts as getSumCount(), but reduces the calls, if all counts are needed.
     * @param type_hash The integrity data type (e.g. MD5) as related hash value.
     * @return A mapping of a file / directory state (e.g. VERIFIED) and the related count.
     */
    virtual SumStateCount getSumStateCount(SumTypeHash type_hash) const = 0;

    /**
     * Returns, whether this instance is intended for being discarded.
     * @return True, if not in use anymore. False, if in active use.
     */
    bool isShutDown() const { return m_ShuttingDown.load(); }

protected:
    void init(InfoParent *parent, const QSharedPointer<TreeDataIfc> &treedata)
    {
        Q_ASSERT(pParent == nullptr);
        Q_ASSERT(parent != nullptr);
        Q_ASSERT(treedata != nullptr);
        pParent = parent;
        theTreeData = treedata;
    }

    TreeDataIfc &getTreeData() const
    {
        Q_ASSERT(theTreeData != nullptr);
        return *theTreeData;
    }
    /**
     * Signals this instance it is going to be discarded. Disconnects from parent.
     */
    virtual void shutdown();

private:
    QString                     m_Name;
    InfoParent                 *pParent;
    std::atomic_bool            m_ShuttingDown;
    QSharedPointer<TreeDataIfc> theTreeData;
};

#endif // INFOBASE_H
