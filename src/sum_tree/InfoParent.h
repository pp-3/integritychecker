// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INFOPARENT_H
#define INFOPARENT_H

#include <QSharedPointer>

#include "InfoBase.h"
#include "TreeInfoIfc.h"

/**
 * Base class for integrity tree instances which are parents to other instances.
 * This is either a directory or the tree root instance.
 */
class InfoParent : public TreeInfoIfc
{
public:
    using ptr = QSharedPointer<InfoParent>;

    ~InfoParent() override = default;

    /**
     * @see TreeInfoIfc::isDir()
     * @return Always true.
     */
    bool isDir() const override
    {
        return true;
    }
    /**
     * Registers a new child.
     * @param child The child instance.
     */
    virtual void registerChild(const InfoBase::ptr &child) = 0;
    /**
     * Removes a child from this parent instance.
     * @param child The child to remove.
     */
    virtual void removeChild(InfoBase *child) = 0;
    /**
     * Removes all children from this parent instance.
     */
    virtual void removeAllChildren() = 0;
    /**
     * Searches recursively for a child.
     * @return The shared pointer when found.
     */
    virtual InfoBase::ptr searchChild(const InfoBase *child) const = 0;
    /**
     * Returns a list of all children.
     * @return The children instances.
     */
    virtual QList<InfoBase::ptr> getChildren() const = 0;
    /**
     * If this parent instance provides also integrity data information, return the (casted) instance for this.
     * @return The InfoBase cast, or nullptr, if not available.
     */
    virtual InfoBase *getInfoBase() = 0;
    virtual const InfoBase *getInfoBase() const = 0;

    /**
     * Child signal, that a summary count has changed.
     */
    virtual void childSumCountChanged(SumTypeHash type_hash) = 0;
};

#endif // INFOPARENT_H
