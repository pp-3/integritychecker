// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TREEDATAIFC_H
#define TREEDATAIFC_H

#include <QMutex>

/**
 * The TreeDataIfc interface provides access to functions relating the full data tree representing the check sums in the filesystem.
 */
class TreeDataIfc
{
    friend class DirInfoRoot;
    friend class FileInfo;

public:
    virtual ~TreeDataIfc() = default;

    /**
     * The integrity data tree uses one global lock for accessing the tree.
     * Returns the single lock used in the integrity data tree to protect the data access.
     * @return The lock to use before accessing the internal data.
     */
    virtual QMutex *getLock() = 0;
};

#endif // TREEDATAIFC_H
