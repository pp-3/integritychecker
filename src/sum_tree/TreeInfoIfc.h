// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TREEINFOIFC_H
#define TREEINFOIFC_H

#include <QString>

class InfoParent;

/**
 * @brief The TreeInfoIfc class contains the interface for all entries in the data tree.
 */
class TreeInfoIfc
{
public:
    virtual ~TreeInfoIfc() = default;

    /**
     * Returns, whether current instance is a directory and may contain child items.
     * @return True, if directory. False, if file.
     */
    virtual bool isDir() const = 0;

    /**
     * Returns the file / directory name.
     * @return The instance name.
     */
    virtual const QString &getName() const = 0;

    /**
     * Returns the relative sub path from the root directory.
     * @return The sub path in the root directory.
     */
    virtual QString getSubPath() const = 0;

    /**
     * Returns the parent for this integrity tree instance.
     * @return The parent.
     */
    virtual InfoParent *getParent() const = 0;
};

#endif // TREEINFOIFC_H
