// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TreeUtil.h"

#include "DirInfo.h"
#include "InfoBase.h"

QList<DirInfo *> TreeUtil::getDirHierarchy(InfoBase *ib)
{
    if (ib->isDir())
    {
        return getDirHierarchy(DirInfo::cast(ib));
    }

    return QList<DirInfo *>();
}

QList<DirInfo *> TreeUtil::getDirHierarchy(DirInfo *dir)
{
    QList<DirInfo *> hier;

    while (dir)
    {
        hier << dir;
        InfoParent *ip = dir->getParent();
        dir = nullptr;
        if (ip)
        {
            InfoBase *ib = ip->getInfoBase();
            if (ib)
            {
                Q_ASSERT(ib->isDir());
                dir = DirInfo::cast(ib);
            }
        }
    }

    return hier;
}

std::optional<QString> TreeUtil::getParentSubPath(const FileInfo::weak_ptr &file)
{
    FileInfo::ptr fi = file.toStrongRef();
    if (!fi)
    {
        return {};
    }

    InfoParent *parent = fi->getParent();
    Q_ASSERT(parent);
    if (!parent)
    {
        return {};
    }

    InfoBase *pb = parent->getInfoBase();
    Q_ASSERT(pb);
    if (!pb)
    {
        return {};
    }

    return pb->getSubPath();
}

DirInfo *TreeUtil::findCommonParent(DirInfo *d1, DirInfo *d2)
{
    QList<DirInfo *> hier1 = TreeUtil::getDirHierarchy(d1);
    QList<DirInfo *> hier2 = TreeUtil::getDirHierarchy(d2);

    if (hier1.isEmpty() && hier2.isEmpty())
        return nullptr;
    if (hier1.isEmpty())
        return hier2.front();
    if (hier2.isEmpty())
        return hier1.front();
    if (hier1.last() != hier2.last())
        return nullptr;

    DirInfo *dir = nullptr;

    while (!hier1.isEmpty() && !hier2.isEmpty())
    {
        if (hier1.last() == hier2.last())
        {
            dir = hier1.takeLast();
            hier2.removeLast();
        }
        else
        {
            break;
        }
    }

    return dir;
}
