// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TREEUTIL_H
#define TREEUTIL_H

#include <optional>

#include <QList>

#include "FileInfo.h"

class InfoBase;
class DirInfo;

struct TreeUtil
{
    /**
     * Returns the directory hierarchy if the given instance is a directory.
     * @param ib The start instance.
     * @return The directory hierarchy or empty, if start instance is not a directory.
     */
    static QList<DirInfo *> getDirHierarchy(InfoBase *ib);
    /**
     * Returns the directory hierarchy from the given start dir up to the top most root dir.
     * @param dir The directory to start with.
     * @return The directory hierarchy. Start dir is first. Root dir is last.
     */
    static QList<DirInfo *> getDirHierarchy(DirInfo *dir);

    /**
     * Returns the absolute path of the parent directory. Thus, the directory the file is located in.
     * @param file The file to find the directory for.
     * @return The parent's directory absolute path, if the pointer is valid and the file has a parent.
     *         An empty optional otherwise.
     */
    static std::optional<QString> getParentSubPath(const FileInfo::weak_ptr &file);

    /**
     * @brief Returns the first common parent for both given directories.
     * @param d1 The first directory.
     * @param d2 The second directory.
     * @return Pointer to the directory where both given directories depart.
     *         nullptr, if there is no common parent directory.
     */
    static DirInfo *findCommonParent(DirInfo *d1, DirInfo *d2);
};

#endif // TREEUTIL_H
