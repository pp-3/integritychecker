// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ASYNCADMINISTRATIONTASK_H
#define ASYNCADMINISTRATIONTASK_H

#include <functional>

#include <QString>

#include "TaskWithResult.h"

/**
 * A task for executing the provided function in a separate thread.
 */
class AsyncAdministrationTask : public TaskWithResult<bool>
{
public:
    /**
     * Function for retrieving whether an ongoing administration task should be canceled.
     * @return True, if a cancel request exists.
     */
    using ShouldCancelQuery = std::function<bool()>;

    /**
     * The function interface for the AsyncAdministrationTask.
     * @return Success state.
     */
    using AdministrationFunc = std::function<bool(const ShouldCancelQuery&)>;

    /**
     * Constructor.
     * @param f The function to execute.
     * @param name The function name (short description).
     */
    explicit AsyncAdministrationTask(AdministrationFunc f, const QString &name);
    virtual ~AsyncAdministrationTask() override = default;

    QString getName() const override final { return "Admin: "+m_Name; }
    bool isInternal() const override final { return true; }

protected:
    void doTask() override;

private:
    AdministrationFunc theFunc;
    QString            m_Name;
};

#endif // ASYNCADMINISTRATIONTASK_H
