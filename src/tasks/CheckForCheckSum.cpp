// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "CheckForCheckSum.h"

#include <algorithm>

#include <qglobal.h>

#include "checksums/SumTypeIfc.h"
#include "checksums/SumTypeManager.h"
#include "shared/Tr.h"
#include "sum_tree/DirInfo.h"
#include "sum_tree/InfoBase.h"

CheckForCheckSum::CheckForCheckSum(const QString            &input_path,
                                   const InfoBase::weak_ptr &path,
                                   const SumTypeHashList    &check_sums,
                                   const bool                recursive,
                                   const CheckType          &check_type,
                                         ErrorPostIfc       *errors) :
    ParentTaskIfc(errors),
    theInputPath(input_path),
    pPath(path),
    theCheckSums(check_sums),
    Recursive(recursive),
    theCheckType(check_type)
{
}

QString CheckForCheckSum::getName() const
{
    QString ret;
    InfoBase::ptr file = pPath.toStrongRef();
    if (!file)
        return ret;

    switch (theCheckType)
    {
    case EXISTS:
        ret = Tr::tr("Search integrity data");
        break;
    case COMPARE   : ret = Tr::tr("Compare integrity data");
                     break;
    case CALCULATE : ret = Tr::tr("Calculate integrity data");
                     break;
    }

    QStringList types;
    for (SumTypeHash it : theCheckSums)
    {
        SumTypeIfc *st = SumTypeManager::getInstance().fromHash(it);
        Q_ASSERT(st);
        if (st)
        {
            types << st->getName();
        }
    }

    if (!types.isEmpty())
    {
        ret += " ("+types.join(",")+")";
    }

    ret += " for " + theInputPath +"/"+ file->getSubPath();

    return ret;
}

void CheckForCheckSum::doParentTask()
{
    if (shouldCancel())
    {
        return;
    }

    InfoBase::ptr start_node = pPath.toStrongRef();
    if (start_node)
        scanNode(start_node, true);
}

void CheckForCheckSum::scanNode(const InfoBase::ptr &path, bool top_level)
{
    if (theCheckType != EXISTS)
    {
        // skip node, if there are no files in this node
        const bool file_found =
            std::any_of(theCheckSums.cbegin(),
                        theCheckSums.cend(),
                        [&path](const SumTypeHash &hash) -> bool
                        {
                            const InfoBase::SumStateCount cnt = path->getSumStateCount(hash);
                            return std::any_of(cnt.cbegin(),
                                               cnt.cend(),
                                               [](const auto &cnt) -> bool
                                               {
                                                   return cnt > 0;
                                               });
                        });

        if (!file_found)
            return;
    }

    if (path->isDir())
    {
        if (!Recursive && !top_level)
            return;

        const DirInfo::ptr d = DirInfo::cast(path);
        Q_ASSERT(d);

        const QList<InfoBase::ptr> todo = sortFilesFirst(d->getChildren());
        for (const InfoBase::ptr &it : todo)
        {
            if (shouldCancel())
                return;

            scanNode(it, false);
        }
    }
    else
    {
        checkFile(path);
    }
}

QList<InfoBase::ptr> CheckForCheckSum::sortFilesFirst(QList<InfoBase::ptr> l)
{
    std::sort(l.begin(),
              l.end(),
              [](const InfoBase::ptr &le, const InfoBase::ptr &ri) -> bool
              {
                  return (!le->isDir() && ri->isDir());
              });

    return l;
}

void CheckForCheckSum::checkFile(const InfoBase::ptr &path)
{
    Q_ASSERT(!path->isDir());
    FileInfo::weak_ptr file = FileInfo::cast(path);

    for (const SumTypeHash &it : std::as_const(theCheckSums))
    {
        SumTypeIfc *st = SumTypeManager::getInstance().fromHash(it);
        Q_ASSERT(st);

        QSharedPointer<CheckFileForCheckSum> subtask(new CheckFileForCheckSum(theInputPath, file, st, theCheckType));
        startSubTask(subtask);
    }
}











CheckFileForCheckSum::CheckFileForCheckSum(const QString                      &input_path,
                                           const FileInfo::weak_ptr           &file,
                                           const SumTypeIfc                   *sum_type,
                                           const CheckForCheckSum::CheckType &check_type) :
    TaskWithResult(),
    theInputPath(input_path),
    pFile(file),
    theCheckSumType(sum_type->getTypeHash()),
    pSumType(sum_type),
    theCheckType(check_type)
{
}

QString CheckFileForCheckSum::getName() const
{
    FileInfo::ptr file = pFile.toStrongRef();
    if (!file)
        return {};

    return file->getSubPath() +" : "+ pSumType->getName();
}

void CheckFileForCheckSum::doTask()
{
    if (shouldCancel())
    {
        return;
    }

    if (pSumType->getSumFileManager()->isCheckSumFile(pFile))
    {
        FileInfo::ptr file = pFile.toStrongRef();
        if (!file)
            return;

        file->setSumState(theCheckSumType, CheckSumState::NOT_CHECKING);
        return;
    }

    switch (theCheckType)
    {
    case CheckForCheckSum::EXISTS:
    {
        if (checkSumExists())
            setResult(pFile);
    }
    break;
    case CheckForCheckSum::COMPARE:
    {
        if (compareCheckSum())
            setResult(pFile);
    }
    break;
    case CheckForCheckSum::CALCULATE:
    {
        if (calculateCheckSum())
            setResult(pFile);
    }
    break;
    }
}

bool CheckFileForCheckSum::checkSumExists()
{
    QSharedPointer<SumTypeCalculatorIfc> calc = pSumType->createCalculator(theInputPath, pFile);
    const bool hasCheckSum = calc->existsCheckSum();

    FileInfo::ptr file = pFile.toStrongRef();
    if (!file)
        return false;

    if (!hasCheckSum)
    {
        file->clearSumState(theCheckSumType);
        return false;
    }
    else
    {
        if (file->getSumCount(theCheckSumType, CheckSumState::UNKNOWN) > 0)
        {
            // currently unknown -> existing
            file->setSumState(theCheckSumType, CheckSumState::EXISTS);
        }
        // else: keep old state

        return true;
    }
}

bool CheckFileForCheckSum::compareCheckSum()
{
    {
        FileInfo::ptr file = pFile.toStrongRef();
        if (!file)
            return false;

        if (!file->existsChecksum(theCheckSumType))
            return false;
    }

    if (checkSumExists())
    {
        QSharedPointer<SumTypeCalculatorIfc> calc = pSumType->createCalculator(theInputPath, pFile);

        FileInfo::ptr file = pFile.toStrongRef();
        if (!file)
            return false;

        if (calc->compareCheckSum())
        {
            file->setSumState(theCheckSumType, CheckSumState::VERIFIED);
        }
        else
        {
            file->setSumState(theCheckSumType, CheckSumState::CHECK_ERROR);
        }
        return true;
    }

    return false;
}

bool CheckFileForCheckSum::calculateCheckSum()
{
    QSharedPointer<SumTypeCalculatorIfc> calc = pSumType->createCalculator(theInputPath, pFile);

    FileInfo::ptr file = pFile.toStrongRef();
    if (!file)
        return false;

    if (calc->createCheckSum(true))
    {
        file->setSumState(theCheckSumType, CheckSumState::VERIFIED);
        return true;
    }
    else
    {
        file->setSumState(theCheckSumType, CheckSumState::UNKNOWN);
        file.reset();
        checkSumExists();
        return false;
    }
}
