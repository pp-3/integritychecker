// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CHECKFORCHECKSUM_H
#define CHECKFORCHECKSUM_H

#include <QSet>
#include <QString>
#include <QWeakPointer>

#include "sum_shared/CheckSumState.h"
#include "sum_tree/FileInfo.h"
#include "sum_tree/InfoBase.h"

#include "ParentTaskIfc.h"

class SumTypeIfc;

/**
 * Task for evaluating integrity data operations on files in the file system. For each file in the path and each enabled
 * integrity data type a CheckFileForCheckSum task is launched.
 */
class CheckForCheckSum : public ParentTaskIfc<InfoBase::weak_ptr>
{
public:
    /**
     * The integrity data operation enum (CheckType) which type of check sum operation shall be executed.
     */
    enum CheckType
    {
        EXISTS,   ///< Checks, if integrity data exists.
        COMPARE,  ///< Compares the stored integrity data with a fresh calculated one.
        CALCULATE ///< Calculates the integrity data and stores it in the according file.
    };

    /**
     * Constructor.
     * @param input_path The related path for the root instance in the file system.
     * @param path       The node in the root instance to start at.
     * @param check_sums The list of integrity data types to execute the given operation on.
     * @param recursive  If path is a directory and recursive is true, the full sub tree is evaluated. If not recursive, only all files in the directory are evaluated. For a file, only the file is evaluated.
     * @param check_type The operation to execute on the given files.
     * @param errors     The interface to report errors to.
     */
    explicit CheckForCheckSum(const QString            &input_path,
                              const InfoBase::weak_ptr &path,
                              const SumTypeHashList    &check_sums,
                              const bool                recursive,
                              const CheckType          &check_type,
                                    ErrorPostIfc       *errors);
    virtual ~CheckForCheckSum() override = default;

    QString getName() const override;


protected:
    /**
     * Iterates through the tree and executes the requested check sum type.
     */
    void doParentTask() override;
    /**
     * Iterates through the given node and executes the requested check sum type.
     * @param path The node to start the iteration at.
     * @param top_level True, if the given node is the start path for this CheckForCheckSum instance. If it's set even for a non-recursive scan a directory will be scanned.
     *                  False, if scanNode already scanning on a deeper level.
     */
    void scanNode(const InfoBase::ptr &path, bool top_level);
    /**
     * Sorts the files first in a list of tree nodes.
     * @param l The list to sort.
     * @return The sorted list.
     */
    static QList<InfoBase::ptr> sortFilesFirst(QList<InfoBase::ptr> l);
    /**
     * Starts for the given file and each enabled integrity data type a CheckFileForCheckSum task.
     * @param path The node instance to file.
     */
    void checkFile(const InfoBase::ptr &path);

private:
    QString            theInputPath;
    InfoBase::weak_ptr pPath;
    SumTypeHashList    theCheckSums;
    bool               Recursive;
    CheckType          theCheckType;
};


/**
 * The CheckFileForCheckSum tasks performs a given integrity data operation on one given file for one integrity data type.
 * Return value is bool and means for:
 * - EXISTS    : If true, the integrity data exists. If false, an error occurred or it doesn't exist.
 * - COMPARE   : If true, the integrity data matches. If false, an error occurred or it doesn't match.
 * - CALCULATE : If true, calculation and storing finished successfully. If false, an error occurred.
 */
class CheckFileForCheckSum : public TaskWithResult<InfoBase::weak_ptr>
{
public:
    /**
     * Constructor.
     * @param input_path The related path for the root instance in the file system.
     * @param file       The file to execute the given operation on.
     * @param sum_type   The related integrity data type instance. Hash value should be the same as above.
     * @param check_type The operation to execute for the given file. @see CheckForCheckSum::CHECK_TYPE
     */
    explicit CheckFileForCheckSum(const QString                      &input_path,
                                  const FileInfo::weak_ptr           &file,
                                  const SumTypeIfc                   *sum_type,
                                  const CheckForCheckSum::CheckType &check_type);
    virtual ~CheckFileForCheckSum() override = default;

    QString getName() const override;

protected:
    void doTask() override;
    bool checkSumExists();
    bool compareCheckSum();
    bool calculateCheckSum();

private:
    QString                      theInputPath;
    FileInfo::weak_ptr           pFile;
    SumTypeHash                  theCheckSumType;
    const SumTypeIfc            *pSumType;
    CheckForCheckSum::CheckType  theCheckType;
};

#endif // CHECKFORCHECKSUM_H
