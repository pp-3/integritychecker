// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PARENTTASKIFC_H
#define PARENTTASKIFC_H

#include <functional>

#include <QList>
#include <QSharedPointer>

#include "TaskResult.h"
#include "TaskWithResult.h"
#include "ifc/ErrorPostIfc.h"

/**
 * The interface for tasks receiving a SubTaskRunner function pointer.
 */
class ParentTaskSubTaskRunnerIfc
{
public:
    /**
     * The function interface to call for starting a sub task.
     */
    using SubTaskRunner = std::function<void(const QSharedPointer<TaskIfc> &subtask)>;


    virtual ~ParentTaskSubTaskRunnerIfc() = default;
    /**
     * Sets the SubTaskRunner function to call for starting a sub task.
     * @param runner The SubTaskRunner function pointer.
     */
    virtual void setSubTaskRunner(SubTaskRunner runner) = 0;
};

/**
 * Base class for all tasks that may start sub tasks for parallel execution.
 * @tparam The stored results type
 */
template <typename T>
class ParentTaskIfc : public TaskWithResult<QList<typename TaskResult<T>::ptr>>,
                      public ParentTaskSubTaskRunnerIfc
{
    friend class TaskDispatcher;

public:
    using SingleResult = typename TaskResult<T>::ptr;
    using SubResultList = QList<SingleResult>;
    using ResultType = typename TaskResult<SubResultList>::ptr;

public:
    virtual ~ParentTaskIfc() = default;
    /**
     * Returns always true. The actually started sub task count is returned by getTaskCount().
     * @return True.
     */
    bool hasSubTasks() const override { return true; }
    /**
     * Returns the total started task count. Includes this parent task and all sub tasks.
     * @return The task count. At least 1.
     */
    size_t getTaskCount() const override;

    /**
     * @brief Returns only results of sub tasks which provided a result. TaskWithResult::getTaskResult() will return
     * exactly one entry for each sub task. Each entry may get a result set once the sub task finished.
     * getAvailableTaskResults() returns a filtered list with results which already contain a result. As long as the
     * parent task is not finished subsequent calls may return a different list.
     * @return The list of already available task results.
     */
    SubResultList getAvailableTaskResults() const;

protected:
    ParentTaskIfc(ErrorPostIfc *errors);

    /**
     * Implements the TaskIfc doTask() and calls the doParentTask() for the real sub task.
     * After this method returns (parent task finishes) the returned list of sub task results is filled.
     * Sub tasks may still run or have already finished at this time.
     * The TaskLifeCycle of this parent task emits the task finished signal after all sub tasks have also finished.
     */
    void doTask() override final;
    /**
     * Interface to implement for parent tasks. When this method is called, the SubTaskRunner is set accordingly
     * and sub tasks may be started. After this method returns from the sub class the TaskResults of all
     * started sub tasks are collected into the return list of this parent's task.
     */
    virtual void doParentTask() = 0;

    /**
     * Called by the TaskDispatcher after creation and before task is started.
     * @param runner The interface to use for starting sub tasks.
     */
    void setSubTaskRunner(SubTaskRunner runner) override { theSubTaskRunner = std::move(runner); }
    /**
     * Starting a sub task for parallel execution.
     * @param s The sub task to start.
     */
    void startSubTask(const QSharedPointer<TaskWithResult<T>> &s);
    /**
     * Remembers the task result of a started sub task.
     * @param result The TaskResult instance.
     */
    void addResult(const SingleResult &result);

protected:
    ErrorPostIfc *theErrorCollector;

private:
    SubResultList  m_SubTaskResults;
    SubTaskRunner  theSubTaskRunner;
    size_t         m_TaskCount;
};

#include "ParentTaskIfc.tpp"

#endif // PARENTTASKIFC_H
