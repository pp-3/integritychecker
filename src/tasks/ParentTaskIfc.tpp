// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


template <typename T>
ParentTaskIfc<T>::ParentTaskIfc(ErrorPostIfc *errors) :
    TaskWithResult<SubResultList>(),
    theErrorCollector(errors),
    m_SubTaskResults(),
    theSubTaskRunner(nullptr),
    m_TaskCount(1)  // count parent task as separate task
{}

template <typename T>
void ParentTaskIfc<T>::doTask()
{
    doParentTask();

    SubResultList tmp;
    {
        QMutexLocker ml(this->getMutexLocker());
        tmp.swap(m_SubTaskResults);
    }

    this->setResult(tmp);
}

template <typename T>
size_t ParentTaskIfc<T>::getTaskCount() const
{
    QMutexLocker l(this->getMutexLocker());
    return m_TaskCount;
}

template <typename T>
typename ParentTaskIfc<T>::SubResultList ParentTaskIfc<T>::getAvailableTaskResults() const
{
    SubResultList ret;

    const ResultType all_task_results = this->getTaskResult();
    if (!all_task_results->hasResult())
        return ret;

    const SubResultList all_results = all_task_results->getResult();

    // filter returned list
    for (const SingleResult &it : all_results)
    {
        if (it->hasResult())
            ret << it;
    }

    return ret;
}


template <typename T>
void ParentTaskIfc<T>::startSubTask(const QSharedPointer<TaskWithResult<T>> &s)
{
    Q_ASSERT(s);
    QMutexLocker l(this->getMutexLocker());

    reinterpret_cast<TaskIfc *>(s.data())->m_pShouldCancel = reinterpret_cast<TaskIfc *>(this)->m_pShouldCancel;
    m_TaskCount++;
    l.unlock();

    addResult(s->getTaskResult());

    if (theSubTaskRunner)
    {
        theSubTaskRunner(s);
    }
    else
    {
        theErrorCollector->addError("SubTaskRunner missing for: " + s->getName());
        Q_ASSERT(false);
        // fallback
        s->doTask();
    }
}

template <typename T>
void ParentTaskIfc<T>::addResult(const SingleResult &result)
{
    QMutexLocker l(this->getMutexLocker());
    m_SubTaskResults << result;
}
