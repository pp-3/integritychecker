// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ReScanFsTask.h"

#include "checksums/SumTypeManager.h"
#include "shared/Tr.h"
#include "sum_tree/TreeUtil.h"

#include "CheckForCheckSum.h"

ReScanFsTask::ReScanFsTask(const DirInfoRoot::weak_ptr &root,
                           const QString               &input_path,
                           const DirInfo::weak_ptr     &dir,
                                 bool                   recursive,
                           const SumTypeHashList       &check_sums,
                                 ErrorPostIfc          *errors) :
    ParentTaskIfc(errors),
    m_InputPath(input_path),
    m_Root(root),
    pStartDir(dir),
    m_Recursive(recursive),
    theCheckSums(check_sums),
    pTopChangedDir(nullptr)
{
}

QString ReScanFsTask::getName() const
{
    DirInfo::ptr dir = pStartDir.toStrongRef();
    if (!dir)
        return {};

    return Tr::tr("Re-scan files: ")+m_InputPath +"/"+ dir->getSubPath();
}

void ReScanFsTask::doParentTask()
{
    DirInfoRoot::ptr root = m_Root.toStrongRef();
    if (!root)
        return;

    DirInfo::ptr dir = pStartDir.toStrongRef();

    if (!m_InputPath.isEmpty() && dir)
    {
        pTopChangedDir = nullptr;
        QDir d(m_InputPath+"/"+dir->getSubPath());
        QList<QString> already_scanned;

        updateDir(d, dir, already_scanned);
        already_scanned.clear();

        if (pTopChangedDir)
        {
            InfoBase::weak_ptr top = root->searchChild(pTopChangedDir);
            TaskResult<InfoBase::weak_ptr>::ptr result = TaskResult<InfoBase::weak_ptr>::ptr::create();
            result->setResult(top);
            addResult(result);
        }
    }
}

void ReScanFsTask::updateDir(const QDir &fsDir, const DirInfo::ptr &treeDir, QList<QString> &already_scanned)
{
    Q_ASSERT(treeDir);

    // simple way to detect loops
    QString can = fsDir.canonicalPath();
    if (already_scanned.contains(can))
    {
        return;
    }
    else
    {
        already_scanned << can;
        can.clear();
    }

    if (!shouldCancel())
    {
        if (!fsDir.exists())
        {
            // not existing dir -> try to work up the parent hierarchy
            updateTopDir(treeDir);

            // clear old state
            treeDir->removeAllChildren();

            if (treeDir->getParent() && treeDir->getParent()->getInfoBase())
            {
                InfoBase *ib = treeDir->getParent()->getInfoBase();
                DirInfoRoot::ptr root = m_Root.toStrongRef();
                Q_ASSERT(root); // never NULL, as checked in doParentTask
                InfoBase::ptr ibl = root->searchChild(ib);

                QDir p1 = fsDir;
                if (ibl && p1.cdUp())
                {
                    DirInfo::ptr di = DirInfo::cast(ibl);
                    updateDir(p1, di, already_scanned);  // parent directory found
                }
                else if (ibl)
                {
                    // parent node found, but cd failed
                    theErrorCollector->addError(Tr::tr("Directory scan failed for: ") + fsDir.absolutePath());
                }
            }

            return;
        }

        QMap<QString, DirInfo::ptr> sub_dirs;
        QMap<QString, InfoBase::ptr> sub_files;
        getChildren(treeDir, sub_dirs, sub_files);

        const QStringList entries = fsDir.entryList(QDir::Dirs | QDir::Files | QDir::Readable | QDir::NoDotAndDotDot | QDir::NoSymLinks);
        for (QStringList::const_iterator it=entries.constBegin() ; !shouldCancel() && it!=entries.constEnd() ; ++it)
        {
            QList<FileInfo::ptr> files_to_check;

            QFileInfo fi(fsDir, *it);

            if (fi.isFile())
            {
                if (sub_files.contains(fi.fileName()))
                {
                    // existing file -> update state
                    files_to_check << FileInfo::cast(sub_files[fi.fileName()]);
                    sub_files.remove(fi.fileName());
                }
                else
                {
                    // new file found -> add to parent
                    updateTopDir(treeDir);
                    FileInfo::ptr f(FileInfo::ptr::create(fi.fileName()));
                    files_to_check << f;
                    treeDir->registerChild(f);
                }
            }
            else
            {
                DirInfo::ptr pDir = sub_dirs.take(fi.fileName());
                if (!pDir/*sub_dirs.remove(*it) == 0*/)
                {
                    updateTopDir(treeDir);
                    pDir = DirInfo::ptr::create(fi.fileName());
                    treeDir->registerChild(pDir);
                    // scan new directory even if not recursive is requested
                    updateDir(QDir(fi.absoluteFilePath()), pDir, already_scanned);
                }
                else if (m_Recursive)
                {
                    updateDir(QDir(fi.absoluteFilePath()), pDir, already_scanned);
                }
            }

            // check new files for available check sums
            for (const FileInfo::ptr &it : std::as_const(files_to_check))
            {
                checkFileForCheckSums(it);
            }
        }

        if (!sub_dirs.isEmpty() || !sub_files.isEmpty())
        {
            updateTopDir(treeDir);

            // remove not existing files / dirs
            for (const DirInfo::ptr &it : sub_dirs.values())
            {
                treeDir->removeChild(it.data());
            }
            for (const InfoBase::ptr &it : sub_files.values())
            {
                treeDir->removeChild(it.data());
            }
        }
    }
}

void ReScanFsTask::getChildren(const DirInfo::ptr &parent, QMap<QString, DirInfo::ptr> &sub_dirs, QMap<QString, InfoBase::ptr> &sub_files)
{
    sub_dirs.clear();
    sub_files.clear();

    for (const InfoBase::ptr &it : parent->getChildren())
    {
        if (it->isDir())
        {
            sub_dirs.insert(it->getName(), DirInfo::cast(it));
        }
        else
        {
            sub_files.insert(it->getName(), it);
        }
    }
}

void ReScanFsTask::updateTopDir(const DirInfo::ptr &parent)
{
    if (pTopChangedDir==nullptr)
    {
        pTopChangedDir = parent.data();
        return;
    }

    // find top most parent
    DirInfo *const top = TreeUtil::findCommonParent(parent.data(), pTopChangedDir);
    if (top)
        pTopChangedDir = top;
}

void ReScanFsTask::checkFileForCheckSums(const FileInfo::ptr &f)
{
    for (SumTypeHash it : std::as_const(theCheckSums))
    {
        SumTypeIfc *st = SumTypeManager::getInstance().fromHash(it);
        Q_ASSERT(st);

        QSharedPointer<CheckFileForCheckSum> subtask(new CheckFileForCheckSum(m_InputPath, f, st, CheckForCheckSum::EXISTS));
        startSubTask(subtask);
    }
}
