// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef RESCANFSTASK_H
#define RESCANFSTASK_H

#include <QString>
#include <QDir>
#include <QList>
#include <QMap>

#include "sum_shared/CheckSumState.h"
#include "sum_tree/DirInfoRoot.h"
#include "sum_tree/FileInfo.h"

#include "ParentTaskIfc.h"

/**
 * Updates a file system tree. This task compares the directories and files in the file system with the given DirInfoRoot.
 * The DirInfoRoot sub tree gets updated, if necessary. Unchanged entries are not changed. Returns the highest directory
 * where a change was detected.
 */
class ReScanFsTask : public ParentTaskIfc<InfoBase::weak_ptr>
{
public:
    /**
     * Contstructor.
     * @param root       The current file system representation. Updates are applied directly into the sub tree.
     * @param input_path The root path in the file system to start search in. Should relate to the DirInfoRoot instance.
     * @param dir        The directory in the sub tree to start the search in.
     * @param recursive  If true, the full sub tree under dir is scanned. Otherwise, only the given directory is scanned for changes.
     * @param check_sums The type of check sums to look for.
     * @param errors     The (optional) interface to report (string) errors to.
     */
    explicit ReScanFsTask(const DirInfoRoot::weak_ptr &root,
                          const QString               &input_path,
                          const DirInfo::weak_ptr     &dir,
                                bool                   recursive,
                          const SumTypeHashList       &check_sums,
                                ErrorPostIfc          *errors);
    virtual ~ReScanFsTask() override = default;

    QString getName() const override;

protected:
    void doParentTask() override;

private:
    void updateDir(const QDir &fsDir, const DirInfo::ptr &treeDir, QList<QString> &already_scanned);
    static void getChildren(const DirInfo::ptr &parent, QMap<QString, DirInfo::ptr> &sub_dirs, QMap<QString, InfoBase::ptr> &sub_files);
    void updateTopDir(const DirInfo::ptr &parent);
    void checkFileForCheckSums(const FileInfo::ptr &f);

private:
    QString               m_InputPath;
    DirInfoRoot::weak_ptr m_Root;
    DirInfo::weak_ptr     pStartDir;
    bool                  m_Recursive;
    SumTypeHashList       theCheckSums;
    DirInfo              *pTopChangedDir;
};

#endif // RESCANFSTASK_H
