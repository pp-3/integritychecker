// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ScanFsTask.h"

#include <QFileInfo>
#include <QReadWriteLock>
#include <QStringList>

#include "ifc/ErrorPostIfc.h"
#include "shared/Tr.h"
#include "sum_tree/InfoBase.h"
#include "sum_tree/FileInfo.h"

ScanFsTask::ScanFsTask(const QString      &path,
                             ErrorPostIfc *errors) :
    TaskWithResult(),
    m_Path(path),
    theErrorCollector(errors)
{
}

QString ScanFsTask::getName() const
{
    return Tr::tr("Find files: ")+m_Path;
}

void ScanFsTask::doTask()
{
    QDir d(m_Path);

    if (d.exists())
    {
        DirInfoRoot::ptr res(DirInfoRoot::ptr::create(d.absolutePath()));

        iterateDir(d, res->getRoot().data());
        setResult(res);
    }
}

void ScanFsTask::iterateDir(const QDir &dir, DirInfo *parent)
{
    if (!shouldCancel() && dir.exists())
    {
        const QStringList entries = dir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::NoSymLinks);
        for (QStringList::const_iterator it=entries.constBegin() ; !shouldCancel() && it!=entries.constEnd() ; ++it)
        {
            QFileInfo fi(dir, *it);

            if (fi.isFile())
            {
                parent->registerChild(FileInfo::ptr::create(fi.fileName()));
            }
            else
            {
                DirInfo::ptr subdir = DirInfo::ptr::create(fi.fileName());
                parent->registerChild(subdir);

                QDir sd(dir.absolutePath());
                if (sd.cd(fi.fileName()))
                    iterateDir(sd, subdir.data());
                else
                    theErrorCollector->addError(Tr::tr("Directory scan failed for: ") + sd.absolutePath());
            }
        }
    }
}




