// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SCANFSTASK_H
#define SCANFSTASK_H

#include <QDir>
#include <QString>

#include "sum_tree/DirInfo.h"
#include "sum_tree/DirInfoRoot.h"

#include "TaskWithResult.h"

class ErrorPostIfc;

/**
 * Scans the file system starting at a given path in the file system and builds the representing tree.
 * Returns a DirInfoRoot instance containing the full sub tree.
 */
class ScanFsTask : public TaskWithResult<DirInfoRoot::ptr>
{
public:
    /**
     * Constructor.
     * @param path The root file path to start the scan in.
     * @param errors The (optional) interface to report (string) errors to.
     */
    explicit ScanFsTask(const QString       &path,
                              ErrorPostIfc  *errors);
    virtual ~ScanFsTask() override = default;

    QString getName() const override;

protected:
    void doTask() override;

private:
    void iterateDir(const QDir &dir, DirInfo *parent);

private:
    QString       m_Path;
    ErrorPostIfc *theErrorCollector;
};



#endif // SCANFSTASK_H
