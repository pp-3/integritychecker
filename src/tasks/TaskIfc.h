// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKIFC_H
#define TASKIFC_H

#include <atomic>

#include <QDateTime>
#include <QSharedPointer>
#include <QString>

/**
 * Base class for all tasks. Tasks are intended to run in the background and avoid blocking
 * the main thread. Long running tasks should watch out for the cancel flag regularly, as each task
 * should cancel in a reasonable time.
 */
class TaskIfc
{
    friend class TaskRunnable;
    template <typename T>
    friend class ParentTaskIfc;

public:
    virtual ~TaskIfc() = default;

    /**
     * Returns the task name.
     * @return The task name.
     */
    virtual QString getName() const = 0;
    /**
     * Requests this task to cancel. The actual time the task cancels is not foreseeable.
     */
    virtual void cancel() { m_pShouldCancel->store(true); }
    /**
     * Returns, whether this task splits up its work in (parallel) sub tasks.
     * @return True, if this task may start itself new tasks. False, otherwise.
     */
    virtual bool hasSubTasks() const { return false; }
    /**
     * Returns the total count of tasks (self + sub tasks) this task is responsible for.
     * @return The total task count.
     */
    virtual size_t getTaskCount() const { return 1; }
    /**
     * Return, whether the task has an internal root and not started from the user.
     * @return True, for internal. False, otherwise.
     */
    virtual bool isInternal() const { return false; }

    /**
     * Returns the time stamp this task was started at.
     * @return The start time.
     */
    const QDateTime &getStartTime() const { return m_Started; }


protected:
    explicit TaskIfc();
    /**
     * The method to be implemented by the real task. This method will usually be started in a different thread than
     * the task was created in. After the method returns, the task is considered finished and will be deleted soon.
     * Returning computed values has to be done in the specialized sub class.
     */
    virtual void doTask() = 0;
    /**
     * Convenient method for checking, if this task was requested to cancel.
     * @return If true, task should stop as soon as possible.
     */
    bool shouldCancel() const { return m_pShouldCancel->load(); }

private:
    QSharedPointer<std::atomic_bool> m_pShouldCancel;
    QDateTime                        m_Started;
};


#endif // TASKIFC_H
