// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TaskLifeCycle.h"

#include "TaskIfc.h"

#include <thread_util/ResultWait.h>

TaskLifeCycle::TaskLifeCycle(const QSharedPointer<TaskIfc> &task) :
    QObject(),
    QEnableSharedFromThis(),
    theTask(task),
    m_State(INIT),
    m_Lock(),
    m_LifeCycleState(),
    m_pDelayedUpdate(new DelayedUpdate(500, this)),
    m_LastCurMaxProgress(0, 1)
{
    qRegisterMetaType<TaskLifeCycle::ptr>("TaskLifeCycle::ptr");

    connect(m_pDelayedUpdate, &DelayedUpdate::signalOut,
            this, &TaskLifeCycle::delayedProgressUpdate, Qt::DirectConnection);
}

TaskLifeCycle::~TaskLifeCycle()
{
    cancelTask();
    waitForTaskFinished(nullptr);
}

TaskLifeCycle::BoolWithConnection TaskLifeCycle::addStartedCallback(TaskStartListener started_func, QObject *context)
{
    Q_ASSERT(context);
    if (!context)
        return BoolWithConnection(false, {});

    QMutexLocker l(&m_Lock);
    const bool started = m_State==STARTED;
    const bool finished = m_State==FINISHED || m_State==CANCELED;
    l.unlock();

    if (started && !finished)
    {
        started_func();
        return BoolWithConnection(true, {});
    }
    else
    {
        BoolWithConnection ret(false, {});

        if (!finished)
            ret.second = connect(this, &TaskLifeCycle::t_TaskStarted, context, started_func, Qt::QueuedConnection);

        return ret;
    }
}

TaskLifeCycle::BoolWithConnection TaskLifeCycle::addFinishedCallback(TaskFinishedListener finished_func, QObject *context)
{
    Q_ASSERT(context);
    if (!context)
        return BoolWithConnection(false, {});

    QMutexLocker l(&m_Lock);
    const bool finished = m_State==FINISHED || m_State==CANCELED;
    const bool cancel   = m_State==CANCELED;

    if (finished)
    {
        l.unlock();
        finished_func(cancel);
        return BoolWithConnection(true, {});
    }
    else
    {
        const auto connection = connect(this, &TaskLifeCycle::t_TaskFinished,
                                        context, finished_func, Qt::QueuedConnection);
        return BoolWithConnection(false, connection);
    }
}

TaskLifeCycle::BoolWithConnection TaskLifeCycle::addTaskProgressCallback(TaskProgressListener progress_func, QObject *context)
{
    Q_ASSERT(context);
    if (!context)
        return BoolWithConnection(false, {});

    QMutexLocker l(&m_Lock);
    const bool finished = m_State==FINISHED || m_State==CANCELED;
    l.unlock();

    if (finished)
    {
        return BoolWithConnection(false, {});
    }
    else
    {
        const auto connection = connect(this, &TaskLifeCycle::t_TaskProgress,
                                        context, progress_func, Qt::QueuedConnection);
        return BoolWithConnection(true, connection);
    }
}

void TaskLifeCycle::cancelTask()
{
    const QSharedPointer<TaskIfc> &t = getTask();
    if (!t.isNull())
    {
        t->cancel();
    }
}

QDateTime TaskLifeCycle::getStartTime() const
{
    return theTask->getStartTime();
}

LifeCycleStateIfc::ptr TaskLifeCycle::getLifeCycleStateIfc()
{
    Q_ASSERT(sharedFromThis());

    if (m_LifeCycleState.isNull())
    {
        if (theTask->hasSubTasks())
        {
            m_LifeCycleState.reset(new SubTasksCollectingLifeCycleState(sharedFromThis()));
        }
        else
        {
            m_LifeCycleState.reset(new SingleTaskLifeCycleState(sharedFromThis()));
        }
    }

    return m_LifeCycleState;
}

QString TaskLifeCycle::getName() const
{
    return theTask->getName();
}

TaskLifeCycle::RunState TaskLifeCycle::getState() const
{
    QMutexLocker l(&m_Lock);
    return m_State;
}

QString TaskLifeCycle::getStateStr() const
{
    TaskLifeCycle::RunState r = getState();
    switch (r)
    {
        case INIT     : return "Init";
        case CREATED  : return tr("Waiting");
        case STARTED  : return tr("Running");
        case FINISHED : return tr("Finished");
        case CANCELED : return tr("Canceled");
    }

    Q_ASSERT(false);
    std::abort();
}

bool TaskLifeCycle::isRunning() const
{
    QMutexLocker l(&m_Lock);
    return (m_State==STARTED);
}

bool TaskLifeCycle::isFinishedOrCanceled(bool *canceled) const
{
    QMutexLocker l(&m_Lock);
    if (canceled)
        *canceled = (m_State==CANCELED);

    return (m_State==FINISHED || m_State==CANCELED);
}

bool TaskLifeCycle::waitForTaskFinished(bool *canceled, int timeout)
{
    unsigned long time;

    if (timeout < 0)
    {
        // <0 -> no timeout
        time = ResultWait<bool>::WAIT_FOR_EVER;
    }
    else
    {
        time = timeout;
    }

    // thread is going to be blocked -> ensure to connect to finish signal directly
    QMutexLocker l(&m_Lock);
    const bool finished = m_State==FINISHED || m_State==CANCELED;

    if (finished)
    {
        const bool cancel = m_State==CANCELED;
        if (canceled)
            *canceled = cancel;
        return true;
    }
    else
    {
        ResultWait<bool> condition(false);
        auto finished_callback = [&condition, canceled](bool cancel) -> void
        {
            if (canceled)
                *canceled = cancel;
            condition.setResult(true);
        };
        const auto connection = connect(this, &TaskLifeCycle::t_TaskFinished,
                                        this, finished_callback, Qt::DirectConnection);

        l.unlock();
        const bool ret = condition.wait(time);

        QObject::disconnect(connection);
        return ret;
    }
}

void TaskLifeCycle::gotoCreated()
{
    {
        QMutexLocker l(&m_Lock);
        m_State = CREATED;
    }
}

void TaskLifeCycle::gotoStarted()
{
    {
        QMutexLocker l(&m_Lock);
        m_State = STARTED;
    }

    emit t_TaskStarted();
}

void TaskLifeCycle::progress(int current, int max)
{
    QMutexLocker l(&m_Lock);
    max = std::max(max, m_LastCurMaxProgress.second);
    m_LastCurMaxProgress.first = current;
    m_LastCurMaxProgress.second = max;

    l.unlock();

    m_pDelayedUpdate->signalIn();
}

void TaskLifeCycle::delayedProgressUpdate()
{
    QMutexLocker l(&m_Lock);
    const int cur = m_LastCurMaxProgress.first;
    const int max = m_LastCurMaxProgress.second;
    l.unlock();

    emit t_TaskProgress(cur, max);
}

void TaskLifeCycle::gotoFinished(bool canceled)
{
    {
        QMutexLocker l(&m_Lock);
        Q_ASSERT(m_State != CANCELED && m_State != FINISHED); // single finish trigger is expected
        m_State = canceled ? CANCELED : FINISHED;
    }

    m_pDelayedUpdate->stopSignalSending();  // sends out final update

    emit t_TaskFinished(canceled);

    // As task is finished, all listeners for start, progress, and finished may be removed
    disconnect(this);
}





SingleTaskLifeCycleState::SingleTaskLifeCycleState(TaskLifeCycle::weak_ptr upstream_tlc) :
    upstreamTlc(upstream_tlc)
{}
void SingleTaskLifeCycleState::gotoCreated()
{
    TaskLifeCycle::ptr tlc = upstreamTlc.lock();
    if (tlc)
    {
        tlc->gotoCreated();
        tlc->progress(0, 1);
    }
}

void SingleTaskLifeCycleState::gotoStarted()
{
    TaskLifeCycle::ptr tlc = upstreamTlc.lock();
    if (tlc)
        tlc->gotoStarted();
}

void SingleTaskLifeCycleState::progress(int /*current*/, int /*max*/)
{
}

void SingleTaskLifeCycleState::gotoFinished(bool canceled)
{
    TaskLifeCycle::ptr tlc = upstreamTlc.lock();
    if (tlc)
    {
        tlc->progress(1, 1);
        tlc->gotoFinished(canceled);
    }
}



SubTasksCollectingLifeCycleState::SubTasksCollectingLifeCycleState(TaskLifeCycle::weak_ptr upstream_tlc) :
    upstreamTlc(upstream_tlc),
    m_Lock(),
    m_StartSent(false),
    m_FinishedSubTasks(0),
    m_CanceledSubTasks(0)
{
    TaskLifeCycle::ptr tlc = upstreamTlc.lock();
    if (tlc)
        tlc->gotoCreated();

    sendProgress(QMutexLocker(&m_Lock));
}

void SubTasksCollectingLifeCycleState::gotoCreated()
{
}

void SubTasksCollectingLifeCycleState::gotoStarted()
{
    const bool sent = m_StartSent.exchange(true);
    if (!sent)
    {
        // first sub task starts this life cycle
        TaskLifeCycle::ptr tlc = upstreamTlc.lock();
        if (tlc)
            tlc->gotoStarted();
    }
}

void SubTasksCollectingLifeCycleState::progress(int /*current*/, int /*max*/)
{
}

void SubTasksCollectingLifeCycleState::gotoFinished(bool canceled)
{
    QMutexLocker l(&m_Lock);

    if (!canceled)
    {
        m_FinishedSubTasks++;
    }
    else
    {
        m_CanceledSubTasks++;
    }

    sendProgress(std::move(l));
}

void SubTasksCollectingLifeCycleState::sendProgress(QMutexLocker &&lock)
{
    TaskLifeCycle::ptr tlc = upstreamTlc.lock();
    if (!tlc)
        return;

    const size_t max = tlc->getTask()->getTaskCount();  // parent task returns self + sub tasks
    const size_t cur = m_FinishedSubTasks + m_CanceledSubTasks;  // parent also needs to finish
    Q_ASSERT(cur <= max);
    const bool finished = (cur == max); // all tasks finished

    lock.unlock();

    tlc->progress(cur, max);

    if (finished)
        tlc->gotoFinished(m_CanceledSubTasks>0);
}
