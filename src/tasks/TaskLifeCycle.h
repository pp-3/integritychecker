// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKLIFECYCLE_H
#define TASKLIFECYCLE_H

#include <atomic>
#include <functional>
#include <optional>

#include <QDateTime>
#include <QEnableSharedFromThis>
#include <QMutex>
#include <QMutexLocker>
#include <QObject>
#include <QPair>
#include <QSharedPointer>
#include <QWeakPointer>

#include <shared/DelayedUpdate.h>

class TaskIfc;

/**
 * The LifeCycleStateIfc interface signals changes in the state of a task.
 */
class LifeCycleStateIfc
{
public:
    using ptr = QSharedPointer<LifeCycleStateIfc>;

    virtual ~LifeCycleStateIfc() = default;

    /**
     * The task was created.
     */
    virtual void gotoCreated() = 0;
    /**
     * The task started its operation.
     */
    virtual void gotoStarted() = 0;
    /**
     * An optional progress message.
     * @param current The current progress value.
     * @param max     The maximum progress value. Maximum may change over time.
     */
    virtual void progress(int current, int max) = 0;
    /**
     * Task finished its operation.
     * @param canceled True, if task was canceled. False, if task finished regurarly.
     */
    virtual void gotoFinished(bool canceled) = 0;
};

/**
 * Call back function definition for the task started listeners.
 */
using TaskStartListener = std::function<void()>;
/**
 * Call back function definition for the task finished listeners.
 */
using TaskFinishedListener = std::function<void(bool canceled)>;
/**
 * Call back function definition for the task progress listeners.
 * Parameters are: the current progress and the final progress count. The current one runs from 0 to the final one. The final one may grow during the run.
 */
using TaskProgressListener = std::function<void(int, int)>;

/**
 * The TaskLifeCycle class manages the life cycle of one task. If a task is started via the TaskDispatcher each task
 * gets a TaskLifeCycle instance assigned to which manages the task instance and offers various ways to monitor
 * the task.
 */
class TaskLifeCycle : public QObject,
                      public QEnableSharedFromThis<TaskLifeCycle>
{
    Q_OBJECT

    friend class TaskDispatcher;
    friend class SingleTaskLifeCycleState;
    friend class SubTasksCollectingLifeCycleState;

public:
    using ptr = QSharedPointer<TaskLifeCycle>;
    using weak_ptr = QWeakPointer<TaskLifeCycle>;
    /**
     * A boolean state with an optional setup connection.
     */
    using BoolWithConnection = QPair<bool, std::optional<QMetaObject::Connection>>;

    /**
     * The possible states for each task:
     */
    enum RunState
    {
        INIT,     ///< The initial value. Task wasn't added to the TaskDispatcher yet.
        CREATED,  ///< Task runnable was created and queued.
        STARTED,  ///< The task started its operation.
        FINISHED, ///< The task finished regularly.
        CANCELED  ///< The task was canceled.
    };

    /**
     * Constructor.
     * @param task The task to manage. The TaskLifeCycle takes over the ownership. If the TaskLifeCycle is deleted, the task is canceled.
     */
    explicit TaskLifeCycle(const QSharedPointer<TaskIfc> &task);
    ~TaskLifeCycle() override;

    /**
     * Adds a call back function which is called after the task has started. If the task has already started, but is not finished the callback is called immediately.
     * @param started_func The callback to call after task has started.
     * @param context The callback is called from the thread's event loop this object is belonging to.
     * @return True, if task is already running (started, but not yet finished).
     *         False, if task isn't yet started or already is finished.
     *         Connection is set, if task wasn't started yet and the callback will called later on.
     */
    BoolWithConnection addStartedCallback(TaskStartListener started_func, QObject *context);
    /**
     * Adds a call back function which is called after the task has finished. If task is already finished, the call back is called immediately.
     * @param finished_func The callback to call once the task has finished.
     * @param context The callback is called from the thread's event loop this object is belonging to.
     * @return True, if task is already finished.
     *         False, if the call back function is called later. Connection part returns the set up callback connection.
     */
    BoolWithConnection addFinishedCallback(TaskFinishedListener finished_func, QObject *context);
    /**
     * Adds a callback function which is called with the current task progress.
     * The updates are throttled to two per second.
     * @param progress_func The callback to call as long as the task is running. It's not guaranteed to be called or provide specific values.
     * @param context The callback is called from the thread's event loop this object is belonging to.
     * @return True, if task is still running and further updates can be expected. Connection part returns the set up callback connection.
     *         False, if task is already finished and no further updates will be sent.
     */
    BoolWithConnection addTaskProgressCallback(TaskProgressListener progress_func, QObject *context);
    /**
     * Sends a cancel message to the task.
     */
    void cancelTask();
    /**
     * Returns the time stamp the task was started at.
     * @return The task start time.
     */
    QDateTime getStartTime() const;

    /**
     * @brief Returns the task name.
     * @return The task name.
     */
    QString getName() const;
    /**
     * Returns the current task state as enum.
     * @return The task state.
     */
    TaskLifeCycle::RunState getState() const;
    /**
     * Returns the current task state as string.
     * @return The task state.
     */
    QString getStateStr() const;
    /**
     * Returns the current task progress.
     * @return A current/max pair.
     */
    const QPair<int, int> &getProgress() const { return m_LastCurMaxProgress; }

    /**
     * Returns, whether the task is currently running (-> started, but not finished).
     * @return True, if running. False, otherwise.
     */
    bool isRunning() const;
    /**
     * Returns, whether the task has finished.
     * @param canceled Optional return value. If true, task was canceled. If false, task finished regurarly.
     * @return True, if task has finished. False, otherwise.
     */
    bool isFinishedOrCanceled(bool *canceled) const;
    /**
     * Blocks calling thread until result is computed
     * @param canceled Optional return value. True, if task was canceled. False, if finished.
     * @param timeout Time out (in ms) to wait for result. <0 means unlimited.
     * @return True, if task finished (canceled / computed). False, if time out occurs.
     */
    bool waitForTaskFinished(bool *canceled = nullptr, int timeout = -1);

private:
    QSharedPointer<TaskIfc> theTask;
    RunState                m_State;
    mutable QMutex          m_Lock;
    LifeCycleStateIfc::ptr  m_LifeCycleState;
    DelayedUpdate          *m_pDelayedUpdate;
    QPair<int, int>         m_LastCurMaxProgress;

private:
    /**
     * Returns the interface to inform about task state changes. Is called from friend TaskRunnable.
     * @return The life cycle state change instance.
     */
    LifeCycleStateIfc::ptr getLifeCycleStateIfc();
    /**
     * The task, this life cycle instance relates to. Only available for friend result query instances.
     * @return The task. May be null, if already cleared.
     */
    const QSharedPointer<TaskIfc> &getTask() const
    {
        return theTask;
    }
    /**
     * Deletes the task instance. May be only called from friend result query instances, after they read the result.
     */
    void clearTask() { theTask.clear(); }

    void gotoCreated();
    void gotoStarted();
    void progress(int current, int max);
    void gotoFinished(bool canceled);

signals:
    /**
     * Signals the execution start of this task. Is called from the worker thread.
     */
    void t_TaskStarted();
    /**
     * Signals a progress of this task. Is called from the worker thread.
     * @param current The current progress value. This value increases from 0 to max.
     * @param max The value when reached means the task is finished. This value may increase during the run.
     */
    void t_TaskProgress(int current, int max);
    /**
     * Signals the task end. If not canceled the task result is now available. Is called from the worker thread.
     * @param canceled If true, the task was canceled / interrupted. The task result is not available.
     *                 If false, the task finished. The task result is available.
     */
    void t_TaskFinished(bool canceled);

private slots:
    void delayedProgressUpdate();
};

Q_DECLARE_METATYPE(TaskLifeCycle::RunState)



/**
 * A task state change implementation for tasks without children. It decouples the state provider (the worker task) from
 * the TaskLifeCycle holder.
 */
class SingleTaskLifeCycleState : public LifeCycleStateIfc
{
public:
    SingleTaskLifeCycleState(TaskLifeCycle::weak_ptr upstream_tlc);
    ~SingleTaskLifeCycleState() override = default;

    void gotoCreated() override;
    void gotoStarted() override;
    void gotoFinished(bool canceled) override;
private:
    void progress(int current, int max) override;

private:
    TaskLifeCycle::weak_ptr upstreamTlc;
};

/**
 * A task state change implementation for parent tasks. Collects all state changes and passes a summary to the parent TaskLifeCycle instance.
 *
 *  Calls gotoCreated() to the parent TaskLifeCycle when the parent task life cycle get created.
 *  Calls gotoStarted() to the parent TaskLifeCycle when the parent task gets started.
 *  Calls progress() to the parent TaskLifeCycle when a sub task finishes.
 *  Calls gotoFinished() to the parent TaskLifeCycle when the parent task is finished started. That's after the last sub task has finished.
 */
class SubTasksCollectingLifeCycleState : public LifeCycleStateIfc
{
public:
    SubTasksCollectingLifeCycleState(TaskLifeCycle::weak_ptr upstream_tlc);
    ~SubTasksCollectingLifeCycleState() override = default;

    void gotoCreated() override;
    void gotoStarted() override;
    void gotoFinished(bool canceled) override;
private:
    void progress(int current, int max) override;

private:
    /**
     * @brief Sends the current progress to the upstream TaskLifeCycle.
     * Important: mutex has to be held before calling
     * @param lock The held mutex. Will be unlocked during operation.
     */
    void sendProgress(QMutexLocker &&lock);

private:
    TaskLifeCycle::weak_ptr upstreamTlc;
    mutable QMutex          m_Lock;
    std::atomic_bool        m_StartSent;
    uint                    m_FinishedSubTasks;
    uint                    m_CanceledSubTasks;
};

#endif // TASKLIFECYCLE_H
