// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKRESULT_H
#define TASKRESULT_H

#include <optional>

#include <QMutex>
#include <QMutexLocker>
#include <QObject>
#include <QScopedPointer>
#include <QSharedPointer>

/**
 * A Qt signal instance which sends the resultAvailable() signal once after the result has been set.
 */
class TaskResultNotifier : public QObject
{
    Q_OBJECT

    template <typename T>
    friend class TaskResult;

    void sendSignal()
    {
        emit resultAvailable();
    }

signals:
    /**
     * When a task sets its result, this signal is emitted.
     */
    void resultAvailable() const;
};

/**
 * Like a QFuture each task instance returns one TaskResult instance which will contain the task's
 * result after the task has finished.
 * @tparam The stored result type
 */
template <typename T>
class TaskResult
{
public:
    using ptr = QSharedPointer<TaskResult<T>>;

    explicit TaskResult() :
        m_Result(),
        m_Lock(),
        m_pSignalSender()
    {}

    /**
     * Returns, whether a task result has been already set.
     * @return True, if this instance contains the result. False, otherwise.
     */
    bool hasResult() const
    {
        QMutexLocker ml(&m_Lock);
        return m_Result.has_value();
    }

    /**
     * Sets the result.
     * @param result The result.
     */
    void setResult(const T &result)
    {
        QMutexLocker ml(&m_Lock);
        m_Result = result;

        if (m_pSignalSender)
        {
            ml.unlock();
            m_pSignalSender->sendSignal();
        }
    }
    /**
     * Returns the task's result, if it was already set. Otherwise, the execution will be terminated.
     * @return The task's result.
     */
    const T &getResult() const
    {
        Q_ASSERT(hasResult());
        if (!hasResult())
            std::abort();

        QMutexLocker ml(&m_Lock);
        return *m_Result;
    }

    /**
     * Returns a signaling instance which emits a Qt signal after the task result is set.
     * @return The Qt signal emitting instance related to this TaskResult instance.
     */
    TaskResultNotifier *getNotifier()
    {
        QMutexLocker ml(&m_Lock);
        if (m_pSignalSender.isNull())
        {
            m_pSignalSender.reset(new TaskResultNotifier);
        }
        return m_pSignalSender.data();
    }

private:
    std::optional<T> m_Result;
    mutable QMutex   m_Lock;
    QScopedPointer<TaskResultNotifier> m_pSignalSender;
};

#endif // TASKRESULT_H
