// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKRESULTQUERY_H
#define TASKRESULTQUERY_H

#include <QSharedPointer>

#include "TaskLifeCycle.h"
#include "TaskResult.h"

/**
 * The TaskResultQuery struct is a wrapper interface for the TaskResult interface and the related life cycle instance.
 * @tparam RESULT The managed result type.
 */
template <typename RESULT>
class TaskResultQuery
{
public:
    explicit TaskResultQuery(const TaskLifeCycle::ptr &lf,
                             const typename TaskResult<RESULT>::ptr &result) :
        Lf(lf), Result(result)
    {}

    bool isValid() const { return Lf && Result; }

    const TaskLifeCycle::ptr &getTaskLifeCycle() const { return Lf; }
    RESULT fetchResult() const { return Result->getResult(); }

    static TaskResultQuery<RESULT> createInvalid()
    {
        return TaskResultQuery<RESULT>(TaskLifeCycle::ptr(),
                                       typename TaskResult<RESULT>::ptr());
    }

protected:
    TaskLifeCycle::ptr Lf;
    typename TaskResult<RESULT>::ptr Result;
};


#endif // TASKRESULTQUERY_H
