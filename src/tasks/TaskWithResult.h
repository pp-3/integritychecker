// Copyright 2020 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKWITHRESULT_H
#define TASKWITHRESULT_H

#include <QMutex>
#include <QMutexLocker>

#include "TaskIfc.h"
#include "TaskResult.h"

/**
 * The TaskWithResult class enhances the TaskIfc with the ability to store a task result. getResult() returns a
 * shared pointer to the TaskResult instance which contains (or will contain) the result. Thus, getResult() return
 * immediately, but the result may be still not available. TaskResult provides suitable methods to get the actual state.
 * @tparam T is the task result type.
 */
template <typename T>
class TaskWithResult : public TaskIfc
{
public:
    using ResultType = typename TaskResult<T>::ptr;

    ResultType getTaskResult() const
    {
        QMutexLocker l(&m_Lock);
        return m_Result;
    }

protected:
    /**
     * @param def Default value for the result.
     */
    explicit TaskWithResult() :
        m_Result(ResultType::create())
    {}

    virtual ~TaskWithResult() override = default;

    void setResult(const T &res)
    {
        QMutexLocker l(&m_Lock);
        m_Result->setResult(res);
    }

    QMutexLocker getMutexLocker() const
    {
        return QMutexLocker(&m_Lock);
    }

private:
    ResultType m_Result;
    mutable QMutex m_Lock;
};

#endif // TASKWITHRESULT_H
