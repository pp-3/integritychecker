// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef THREADPOOLEXECUTION_HPP
#define THREADPOOLEXECUTION_HPP

#include <QThreadPool>

#include "ifc/TaskExecutionIfc.h"

/**
 * @brief The ThreadPoolExecution class executes QRunnable instances in a thread pool on the lowest priority.
 */
class ThreadPoolExecution : public TaskExecutionIfc
{
public:
    ~ThreadPoolExecution() override;

    void executeTask(QRunnable *r) override;
    void setMaxThreadPoolSize(size_t s) override;
    void shutdown() override;

private:
    QThreadPool m_Pool;
};

#endif // THREADPOOLEXECUTION_HPP
