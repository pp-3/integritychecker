// Copyright 2020 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ASYNCEXEC_H
#define ASYNCEXEC_H

#include <QFuture>
#include <QObject>
#include <QSharedPointer>
#include <QThreadPool>
#include <QTimer>
#include <QtConcurrent>

#include "ResultWait.h"
#include "threadcheck.h"

/**
 * @brief The AsyncExec class contains utility functions for executing functions in a different thread.
 */
class AsyncExec
{
public:

    /**
     * @brief Calls the function func in the thread context of the context instance and returns a QFuture
     * for the return value.
     * @param context The thread to execute the function in is taken from the thread affinity of this instance.
     *                The instance is not used directly.
     * @param func    The function to call in the event loop of the given thread.
     * @return A QFuture for the result. Allows to wait for the result being set in the other thread.
     *         If context is the own thread, waiting for the result will dead lock.
     */
    template <typename FUNC>
    static QFuture<std::invoke_result_t<FUNC>> execIn(QObject *context, FUNC &&func)
    {
        using RESULT = std::invoke_result_t<FUNC>;
        QSharedPointer<ResultWait<RESULT>> result = QSharedPointer<ResultWait<RESULT>>::create();

        auto f = [func, result]
        {
            result->setResult(func());
        };

        QTimer::singleShot(0, context, f);

        auto waiter = [result]() -> RESULT
        {
            return result->wait();
        };

        return QtConcurrent::run(waiter);
    }

    /**
     * Calls the function func in the main (UI) thread.
     */
    template <typename FUNC>
    static QFuture<std::invoke_result_t<FUNC>> execInMain(FUNC &&func)
    {
        return execIn<FUNC>(getMainThreadAffinity(), std::forward<FUNC>(func));
    }
};

#endif // ASYNCEXEC_H
