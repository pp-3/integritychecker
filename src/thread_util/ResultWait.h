// Copyright 2020 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef RESULTWAIT_H
#define RESULTWAIT_H

#include <limits>

#include <QMutex>
#include <QMutexLocker>
#include <QWaitCondition>


/**
 * @brief The ResultWait class provides the ability to wait in one thread for a result
 * set in another thread.
 * @tparam RESULT The exchanged result type.
 */
template <typename RESULT>
class ResultWait
{
public:
    /**
     * @param _default The default return value, until a new one is set.
     */
    ResultWait(RESULT &&_default = RESULT()) :
        m_Lock(),
        m_Condition(),
        m_Result(std::move(_default)),
        m_ResultSet(false)
    {
    }
    ResultWait(const ResultWait &) = delete;

    /**
     * @brief Sets the result. All waiting threads are released.
     * @param result The new result.
     */
    void setResult(const RESULT &result)
    {
        QMutexLocker ml(&m_Lock);
        m_Result = result;
        m_ResultSet.store(true);

        m_Condition.wakeAll();
    }

    /**
     * @brief Returns, whether the result has been set.
     * @return True, result is available. wait() will return immediately.
     *         False, result is not available. wait() will block.
     */
    bool hasResult() const
    {
        return m_ResultSet.load();
    }

    /**
     * Value for an unlimited result waiting time.
     */
    constexpr static unsigned long WAIT_FOR_EVER = std::numeric_limits<unsigned long>::max();

    /**
     * @brief Waits for the result to be set.
     * @param time An (optional) time out (in ms). For an unlimited wait use: WAIT_FOR_EVER.
     * @param result_set If provided it will be set to true, if the returned result was set or false, if the default result is returned.
     * @return The set result, if it was set or
     *         the default result, if the wait timed out.
     */
    RESULT wait(unsigned long time = WAIT_FOR_EVER, bool *result_set = nullptr) const
    {
        QMutexLocker ml(&m_Lock);

        if (m_ResultSet.load())
        {
            // result already set
            if (result_set)
                *result_set = m_ResultSet.load();

            return m_Result;
        }

        // wait for result
        m_Condition.wait(&m_Lock, time);

        if (result_set)
            *result_set = m_ResultSet.load();

        return m_Result;
    }

private:
    mutable QMutex         m_Lock;
    mutable QWaitCondition m_Condition;
    RESULT                 m_Result;
    std::atomic_bool       m_ResultSet;
};

#endif // RESULTWAIT_H
