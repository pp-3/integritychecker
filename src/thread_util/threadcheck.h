// Copyright 2020 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef THREADCHECK_H
#define THREADCHECK_H

#include <QObject>

/**
 * @brief Checks, whether the current thread is the main (UI) one.
 * @return True, if called from the main thread. False, otherwise.
 */
bool isInMainThread();

/**
 * @brief Checks, whether the current thread is the one the given OObject has
 * an affinity to.
 * @param affinityInstance The instance to check against.
 * @return True, if the instance affinity equals the current thread. False, otherwise.
 * @see QObject::thread()
 */
bool isInThread(QObject *affinityInstance);

/**
 * @brief In the tests the QApplication main thread is occupied by the test execution. Therefore, another "main" thread
 * with a Qt event loop is created. getMainThreadAffinity() returns an object which has its thread affinity set to the
 * current "main" thread.
 * @return In stand-alone execution an object with its thread affinity set to the Qt main thread is returned.
 *         In testing the temporarily set up "main" thread is returned.
 */
QObject *getMainThreadAffinity();

#ifdef ENABLE_TEST_ADDONS

struct MainThreadResetter
{
    MainThreadResetter() = default;
    MainThreadResetter(const MainThreadResetter &) = delete;
    MainThreadResetter(MainThreadResetter &&move);
    ~MainThreadResetter();

    void reset();

    MainThreadResetter &operator =(const MainThreadResetter &) = delete;
    MainThreadResetter &operator=(MainThreadResetter &&move);

protected:
    std::atomic_bool NeedsReset = false;
};

[[nodiscard]] MainThreadResetter setMainThread(QObject *affinityInstance);

#endif

#endif // THREADCHECK_H
