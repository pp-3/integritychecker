// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ChooseProfileDialog.h"
#include "ui_ChooseProfileDialog.h"

ChooseProfileDialog::ChooseProfileDialog(QWidget *parent, const QStringList &profiles, const QString &current_profile) :
    QDialog(parent),
    ui(new Ui::ChooseProfileDialog)
{
    ui->setupUi(this);

    setWindowTitle(tr("Choose Profile to use"));

    initialize(profiles, current_profile);
    initializeController();
}

ChooseProfileDialog::~ChooseProfileDialog()
{
    delete ui;
}

void ChooseProfileDialog::initialize(const QStringList &profiles, const QString &current_profile)
{
    int i = -1;
    int sel = -1;
    for (const QString &it : profiles)
    {
        i++;
        ui->profileComboBox->addItem(it);
        if (it == current_profile)
        {
            sel = i;
        }
    }

    if (sel >= 0)
    {
        ui->profileComboBox->setCurrentIndex(sel);
    }
}

void ChooseProfileDialog::initializeController()
{
}

QString ChooseProfileDialog::getChosenProfile() const
{
    return ui->profileComboBox->currentText();
}
