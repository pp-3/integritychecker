// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CHOOSEPROFILEDIALOG_H
#define CHOOSEPROFILEDIALOG_H

#include <QDialog>
#include <QString>
#include <QStringList>

namespace Ui {
class ChooseProfileDialog;
}

/**
 * A dialog which shows the available profile names and lets the user choose one.
 */
class ChooseProfileDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * Constructor.
     * @param parent The parent widget for this dialog.
     * @param profiles The available profile names.
     * @param current_profile The current profile name.
     */
    explicit ChooseProfileDialog(QWidget *parent, const QStringList &profiles, const QString &current_profile);
    ~ChooseProfileDialog() override;

    /**
     * If the user accepts, this method returns the chosen profile. If the user rejects, it returns the last selected profile.
     * @return The chosen profile name.
     */
    QString getChosenProfile() const;

private:
    void initialize(const QStringList &profiles, const QString &current_profile);
    void initializeController();

private:
    Ui::ChooseProfileDialog *ui;
};

#endif // CHOOSEPROFILEDIALOG_H
