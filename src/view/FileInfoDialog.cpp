// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "FileInfoDialog.h"
#include "ui_FileInfoDialog.h"

#include <QHash>

#include "checksums/SumTypeManager.h"
#include "sum_tree/InfoBase.h"
#include "sum_tree/InfoParent.h"

#include "FileInfoForm.h"
#include "FileSorter.h"
#include "SumStateIcons.h"

FileInfoDialog::FileInfoDialog(const QList<QPair<InfoBase::weak_ptr, SumTypeHash>> &infos, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileInfoDialog)
{
    ui->setupUi(this);

    setWindowTitle(tr("File Details"));

    initialize(infos);
    initializeController();
}

FileInfoDialog::~FileInfoDialog()
{
    delete ui;
}

void FileInfoDialog::initialize(const QList<QPair<InfoBase::weak_ptr, SumTypeHash>> &infos)
{
    QHash<InfoBase::ptr, QList<SumTypeHash>> h;

    for (QPair<InfoBase::weak_ptr, SumTypeHash> it : infos)
    {
        InfoBase::ptr ib = it.first.toStrongRef();

        if (ib)
            h[ib] << it.second;
    }

    QList<InfoBase::ptr> l = h.keys();

    FileSorter fs;
    fs.sort(l);

    const SumStateIcons &ssi = SumStateIcons::global();
    const QList<CheckSumState::States> STATES = CheckSumState::getStateList();
    SumTypeManager& stm = SumTypeManager::getInstance();

    for (const InfoBase::ptr &it : l)
    {
        QList<FileInfoFormStruct> fifs;
        for (SumTypeHash it2 : h.value(it))
        {
            Q_ASSERT(stm.fromHash(it2));
            const QString type = stm.fromHash(it2)->getName();

            for (CheckSumState::States it3 : STATES)
            {
                const unsigned long cnt = it->getSumCount(it2, it3);
                if (cnt > 0)
                {
                    QString info;
                    if (it->isDir())
                    {
                        info = QString::number(cnt) + tr(" file(s): ") + CheckSumState::toString(it3);
                    }
                    else
                    {
                        info = CheckSumState::toString(it3);
                    }

                    fifs << FileInfoFormStruct(ssi.getPixmap(it3),
                                               type,
                                               info);
                }
            }

            if (it->isDir())
            {
                const unsigned long cnt = it->getSumCount(it2, CheckSumState::UNKNOWN);
                if (cnt > 0)
                {
                    const QString info2 = QString::number(cnt) + tr(" file(s) without check sum");
                    fifs << FileInfoFormStruct(ssi.getPixmap(CheckSumState::UNKNOWN),
                                               type,
                                               info2);
                }
            }
        }

        if (fifs.isEmpty())
        {
            fifs << FileInfoFormStruct(QPixmap(),
                                       QString(),
                                       tr("No check sums exist."));
        }

        QString path = it->getParent() && it->getParent()->getInfoBase() ?
            it->getParent()->getInfoBase()->getSubPath() :
            QString();
        if (path.isEmpty())
        {
            path = "/";
        }
        ui->infoList->layout()->addWidget(new FileInfoForm(it->getName(), path, fifs, ui->infoList));
    }
}

void FileInfoDialog::initializeController()
{
}
