// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FILEINFODIALOG_H
#define FILEINFODIALOG_H

#include <QDialog>

#include "sum_shared/CheckSumState.h"
#include "sum_tree/InfoBase.h"

namespace Ui {
class FileInfoDialog;
}

class FileInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FileInfoDialog(const QList<QPair<InfoBase::weak_ptr, SumTypeHash> > &infos, QWidget *parent = 0);
    ~FileInfoDialog() override;

private:
    void initialize(const QList<QPair<InfoBase::weak_ptr, SumTypeHash> > &infos);
    void initializeController();

private:
    Ui::FileInfoDialog *ui;
};

#endif // FILEINFODIALOG_H
