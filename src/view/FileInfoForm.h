// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FILEINFOFORM_H
#define FILEINFOFORM_H

#include <QFrame>
#include <QPixmap>

namespace Ui {
class FileInfoForm;
}

struct FileInfoFormStruct
{
    FileInfoFormStruct(const QPixmap &i, const QString &t, const QString &inf) :
        Icon(i), Type(t), Info(inf)
    {}

    QPixmap Icon;
    QString Type;
    QString Info;
};

class FileInfoForm : public QFrame
{
    Q_OBJECT

public:
    explicit FileInfoForm(const QString& header, const QString& path, const QList<FileInfoFormStruct> &infos, QWidget *parent = 0);
    ~FileInfoForm() override;

private:
    Ui::FileInfoForm *ui;
};

#endif // FILEINFOFORM_H
