// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FILESORTER_H
#define FILESORTER_H

#include "sum_tree/InfoBase.h"

/**
 * @brief The FileSorter class provides utitlity functions for sorting files.
 */
class FileSorter
{
public:
    /**
     * @brief Sorts the names of the tree instances alphabetically. Directories are sorted to the front.
     * @param fs_instances The names to sort.
     */
    void sort(QList<InfoBase::ptr> &fs_instances) const;
};

#endif // FILESORTER_H
