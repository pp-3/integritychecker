// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "LanguageSelectionDialog.h"
#include "ui_LanguageSelectionDialog.h"

#include "shared/LanguageSettings.h"

LanguageSelectionDialog::LanguageSelectionDialog(QWidget *parent, LanguageSettings *lang_settings) :
    QDialog(parent),
    ui(new Ui::LanguageSelectionDialog),
    pLangSettings(lang_settings)
{
    ui->setupUi(this);

    setWindowTitle(tr("Language Selection"));
    setWindowIcon(QIcon(":/icon"));

    initialize();
}

LanguageSelectionDialog::~LanguageSelectionDialog()
{
    delete ui;
}

void LanguageSelectionDialog::initialize()
{
    const QList<QLocale> l = pLangSettings->getSupportedLanguages();
    QLocale sel = pLangSettings->getCurrentLanguage();

    int active = -1;
    int i=0;
    for (const QLocale &it : l)
    {
        ui->languageComboBox->addItem(it.nativeLanguageName(), it);
        if (active<0 && sel.name() == it.name())
        {
            active = i;
        }
        i++;
    }

    if (active >= 0)
    {
        ui->languageComboBox->setCurrentIndex(active);
    }

    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &LanguageSelectionDialog::changeLanguage);
}

void LanguageSelectionDialog::changeLanguage()
{
    pLangSettings->setLanguage(ui->languageComboBox->currentData(Qt::UserRole).toLocale());
}
