// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QByteArray>
#include <QPoint>
#include <QSize>
#include <QString>

#include "model/IntegrityModelIfc.h"
#include "presenter/PresenterManager.h"
#include "shared/LanguageSettings.h"
#include "shared/UserSettings.h"

#include "TaskStateIcons.h"
#include "SumStateIcons.h"

MainWindow::MainWindow(IntegrityModelIfc *model) :
    QMainWindow(nullptr),
    ui(new Ui::mainWnd),
    m_Presenters(),
    pModel(model)
{
    ui->setupUi(this);

    // adapt icons to current font size
    adjustToFont();

    readUserSettings(pModel->getUserSettingsManager()->getGlobalSettings());
    m_Presenters.reset(new PresenterManager(this, pModel));

    initialize();
    initializeConnections();
}

MainWindow::~MainWindow()
{
    saveUserSettings(pModel->getUserSettingsManager()->getGlobalSettings());

    m_Presenters.reset(nullptr);

    delete ui;
}

void MainWindow::readUserSettings(UserGlobalSettingsIfc * const sett)
{
    const QPoint p = sett->getMainWndPos();
    if (p.x()>0 && p.y()>0)
    {
        move(p);
    }

    const QSize s = sett->getMainWndSize();
    if (s.width()>0 && s.height()>0)
    {
        resize(s);
    }

    const QByteArray b = sett->getSplitPos();
    if (!b.isEmpty())
    {
        ui->inputViews->restoreState(b);
    }
    else
    {
        QTimer::singleShot(10, this, &MainWindow::initializeSplitterPos);
    }
}

void MainWindow::saveUserSettings(UserGlobalSettingsIfc * const sett)
{
    sett->setMainWndPos(pos());
    sett->setMainWndSize(size());
    sett->setSplitPos(ui->inputViews->saveState());
}

void MainWindow::initialize()
{
    setWindowIcon(QIcon(":/icon"));
}

void MainWindow::initializeConnections()
{
    connect(pModel->getLanguageSettings(), &LanguageSettings::languageChanged,
            this, [this]() { ui->retranslateUi(this); });
}

void MainWindow::initializeSplitterPos()
{
    const QSize s = size();
    const int w1 = s.width() / 3;
    const int w2 = s.width() - w1;

    ui->inputViews->setSizes(QList<int>() << w1 << w2);
}

void MainWindow::adjustToFont()
{
    QFontMetrics fm(font());
    TaskStateIcons::global().setHeight(fm.ascent());
    SumStateIcons::global().setHeight(fm.ascent());
}
