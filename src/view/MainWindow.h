// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class mainWnd;
}

class UserGlobalSettingsIfc;
class PresenterManager;
class IntegrityModelIfc;

class MainWindow : public QMainWindow
{
    Q_OBJECT

    friend class PresenterManager;

public:
    explicit MainWindow(IntegrityModelIfc *model);
    ~MainWindow() override;

protected:
    void readUserSettings(UserGlobalSettingsIfc * const sett);
    void saveUserSettings(UserGlobalSettingsIfc * const sett);
    void initialize();
    void initializeConnections();
    void adjustToFont();

private slots:
    void initializeSplitterPos();

private:
    Ui::mainWnd *ui;
    QScopedPointer<PresenterManager> m_Presenters;
    IntegrityModelIfc *pModel;
};

#endif // MAINWINDOW_H
