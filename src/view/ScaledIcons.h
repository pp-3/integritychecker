// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SCALEDICONS_H
#define SCALEDICONS_H

#include <QHash>
#include <QIcon>
#include <QPixmap>

/**
 * The class ScaledIcons is a base class for look up classes, which provide an icon for a given state / value.
 * @tparam T The type to store the related icons for.
 */
template <typename T>
class ScaledIcons
{
public:
    ScaledIcons(int iconHeight) :
        m_IconHeight(iconHeight)
    {}
    virtual ~ScaledIcons() = default;

    /**
     * Returns the related icon for the given value.
     * @param val The value to look for.
     * @return The icon as QPixmap.
     */
    QPixmap getPixmap(const T &val) const
    {
        if (m_CachedIcons.isEmpty())
            updateCache();

        Q_ASSERT(m_CachedIcons.contains(val));

        return m_CachedIcons.value(val);
    }

    /**
     * Returns the related icon for the given value.
     * @param val The value to look for.
     * @return The icon as QIcon.
     */
    QIcon   getIcon(const T &val) const
    {
        return QIcon(getPixmap(val));
    }

    /**
     * Sets the height the icons shall scaled to. If not set (0  (== default)) the the icons are not scaled.
     * @param h The pixel height to scale the icons to.
     */
    void setHeight(int h)
    {
        if (h != m_IconHeight)
        {
            m_IconHeight = h;
            clearCache();
        }
    }

    /**
     * Returns the current height all icons are scaled to.
     * @return The pixel height. 0 means icons stay unscaled.
     */
    int height() const
    {
        return m_IconHeight;
    }

protected:
    /**
     * Sub class have to fill the provided QHash with the original (unscaled) icons.
     * @param originals The instance to fill all available icons to.
     */
    virtual void fillCache(QHash<T, QPixmap> &originals) const = 0;

private:
    /**
     * Scales all provided icons to the requested height.
     */
    void updateCache() const
    {
        clearCache();

        QHash<T, QPixmap> originals;
        fillCache(originals);

        for (const T &it : originals.keys())
        {
            QPixmap tmp = originals.take(it);

            if (m_IconHeight != 0 && m_IconHeight != tmp.height())
            {
                tmp = tmp.scaledToHeight(m_IconHeight, Qt::SmoothTransformation);
            }

            m_CachedIcons.insert(it, std::move(tmp));
        }
    }

    void clearCache() const
    {
        m_CachedIcons.clear();
    }

private:
    int                       m_IconHeight = 0;
    mutable QHash<T, QPixmap> m_CachedIcons;
};

#endif // SCALEDICONS_H
