// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SetupWizardCalc.h"
#include "ui_SetupWizardCalc.h"

#include "checksums/SumTypeManager.h"
#include "worker/WorkerSettings.h"

SetupWizardCalc::SetupWizardCalc(QWidget *parent, WorkerSettings *settings) :
    QWizardPage(parent),
    ui(new Ui::SetupWizardCalc),
    pSettings(settings),
    m_OldEnabledTypes(),
    m_Checksums()
{
    ui->setupUi(this);

    initialize();
}

SetupWizardCalc::~SetupWizardCalc()
{
    delete ui;
}

void SetupWizardCalc::initialize()
{
    setTitle(QString("IntegrityChecker"));
    setSubTitle(tr("Integrity data types"));

    m_OldEnabledTypes = pSettings->getEnabledCalcHashes();

    SumTypeManager& stm = SumTypeManager::getInstance();
    const QList<SumTypeIfc *> &types = stm.getRegisteredTypes();

    for (SumTypeIfc *it : types)
    {
        QCheckBox* cb = new QCheckBox(it->getName(), ui->calcContainer);
        ui->calcContainer->layout()->addWidget(cb);
        m_Checksums.insert(it, cb);
        connect(cb, &QCheckBox::toggled, this, &SetupWizardCalc::completeChanged);
    }
}

bool SetupWizardCalc::isComplete() const
{
    const QList<QCheckBox *> cbs = m_Checksums.values();
    return std::any_of(cbs.constBegin(),
                       cbs.constEnd(),
                       [](const QCheckBox *cb) -> bool
                       {
                           return cb->isChecked();
                       });
}

void SetupWizardCalc::initializePage()
{
    const SumTypeHashList en = pSettings->getEnabledCalcHashes();

    for (QHash<SumTypeIfc *, QCheckBox *>::iterator it=m_Checksums.begin() ; it!=m_Checksums.end() ; ++it)
    {
        it.value()->setChecked(en.contains(it.key()->getTypeHash()));
    }
}

bool SetupWizardCalc::validatePage()
{
    for (QHash<SumTypeIfc *, QCheckBox *>::iterator it=m_Checksums.begin() ; it!=m_Checksums.end() ; ++it)
    {
        pSettings->setEnabledType(it.key()->getTypeHash(), it.value()->isChecked());
    }
    return true;
}

void SetupWizardCalc::cleanupPage()
{
    for (QHash<SumTypeIfc *, QCheckBox *>::iterator it=m_Checksums.begin() ; it!=m_Checksums.end() ; ++it)
    {
        pSettings->setEnabledType(it.key()->getTypeHash(), m_OldEnabledTypes.contains(it.key()->getTypeHash()));
    }
}
