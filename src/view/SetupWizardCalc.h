// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SETUPWIZARDCALC_H
#define SETUPWIZARDCALC_H

#include <QCheckBox>
#include <QHash>
#include <QSet>
#include <QWizardPage>

#include "checksums/SumTypeIfc.h"
#include "sum_shared/CheckSumState.h"

namespace Ui {
class SetupWizardCalc;
}

class WorkerSettings;

class SetupWizardCalc : public QWizardPage
{
    Q_OBJECT

public:
    explicit SetupWizardCalc(QWidget *parent, WorkerSettings *settings);
    virtual ~SetupWizardCalc();

protected:
    void initialize();
    virtual bool isComplete() const;
    virtual void initializePage();
    virtual bool validatePage();
    virtual void cleanupPage();

private:
    Ui::SetupWizardCalc             *ui;
    WorkerSettings                  *pSettings;
    SumTypeHashList                  m_OldEnabledTypes;
    QHash<SumTypeIfc *, QCheckBox *> m_Checksums;
};

#endif // SETUPWIZARDCALC_H
