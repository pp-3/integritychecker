// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SetupWizardIntro.h"
#include "ui_SetupWizardIntro.h"

SetupWizardIntro::SetupWizardIntro(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::SetupWizardIntro)
{
    ui->setupUi(this);

    setTitle(QString("IntegrityChecker"));
    setSubTitle(tr("Overview"));

    ui->intro->setText(tr("It seems IntegrityChecker is not configured properly.\nPlease select a base path for looking up existing integrity data files and the type of integrity data to show in the main window.\n\nAll settings may be changed later on in the 'Options' tab."));
}

SetupWizardIntro::~SetupWizardIntro()
{
    delete ui;
}
