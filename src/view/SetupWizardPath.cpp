// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SetupWizardPath.h"
#include "ui_SetupWizardPath.h"

#include <QFileDialog>
#include <QFileInfo>

#include "presenter/IsDirValidator.h"
#include "worker/WorkerSettings.h"

SetupWizardPath::SetupWizardPath(QWidget *parent, WorkerSettings *settings) :
    QWizardPage(parent),
    ui(new Ui::SetupWizardPath),
    pSettings(settings)
{
    ui->setupUi(this);

    initialize();
}

SetupWizardPath::~SetupWizardPath()
{
    delete ui;
}

void SetupWizardPath::initialize()
{
    setTitle(QString("IntegrityChecker"));
    setSubTitle(tr("Input path"));

    m_OldPath = pSettings->getInputDir();

    // register view listeners
    QObject::connect(ui->pathEdit, &QLineEdit::textEdited, this, &SetupWizardPath::completeChanged);

    // button listeners
    connect(ui->browseButton, &QPushButton::clicked, this, &SetupWizardPath::browseInputDir);

    // validator
    ui->pathEdit->setValidator(new IsDirValidator(ui->pathEdit));
}

bool SetupWizardPath::isComplete() const
{
    QFileInfo fi(ui->pathEdit->text());

    return (fi.isDir() && fi.isReadable());
}

void SetupWizardPath::initializePage()
{
    ui->pathEdit->setText(pSettings->getInputDir());
}

bool SetupWizardPath::validatePage()
{
    pSettings->setInputDir(ui->pathEdit->text());
    pSettings->setOutputDirSelection(WorkerSettings::AS_INPUT);
    pSettings->setOutputDir(QString());
    return true;
}

void SetupWizardPath::cleanupPage()
{
    pSettings->setInputDir(m_OldPath);
}

void SetupWizardPath::browseInputDir()
{
    const QString ret = QFileDialog::getExistingDirectory(this, tr("Choose directory"), ui->pathEdit->text(), QFileDialog::ShowDirsOnly);
    if (!ret.isEmpty())
    {
        ui->pathEdit->setText(ret);
    }
}
