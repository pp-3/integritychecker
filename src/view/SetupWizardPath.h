// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SETUPWIZARDPATH_H
#define SETUPWIZARDPATH_H

#include <QString>
#include <QWizardPage>

namespace Ui {
class SetupWizardPath;
}

class WorkerSettings;

class SetupWizardPath : public QWizardPage
{
    Q_OBJECT

public:
    explicit SetupWizardPath(QWidget *parent, WorkerSettings *settings);
    virtual ~SetupWizardPath();

protected:
    void initialize();
    virtual bool isComplete() const;
    virtual void initializePage();
    virtual bool validatePage();
    virtual void cleanupPage();

private slots:
    void browseInputDir();

private:
    Ui::SetupWizardPath *ui;
    WorkerSettings      *pSettings;
    QString              m_OldPath;
};

#endif // SETUPWIZARDPATH_H
