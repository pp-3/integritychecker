// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumStateIcons.h"

SumStateIcons SumStateIcons::s_Global(0);

SumStateIcons &SumStateIcons::global()
{
    return s_Global;
}

void SumStateIcons::fillCache(QHash<CheckSumState::States, QPixmap> &originals) const
{
    originals.insert(CheckSumState::EXISTS,      QPixmap(":/exists"));
    originals.insert(CheckSumState::VERIFIED,    QPixmap(":/checked"));
    originals.insert(CheckSumState::CHECK_ERROR, QPixmap(":/checkederror"));
}
