// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SUMSTATEICONS_H
#define SUMSTATEICONS_H

#include "ScaledIcons.h"

#include "sum_shared/CheckSumState.h"

/**
 * @brief The SumStateIcons class provides the icons for the CheckSumState::States enum.
 */
class SumStateIcons : public ScaledIcons<CheckSumState::States>
{
public:
    SumStateIcons(int iconHeight) :
        ScaledIcons(iconHeight)
    {}

    /**
     * Returns an always available (default) cache for the summar state icons.
     * @return The global instance.
     */
    static SumStateIcons &global();

protected:
    void fillCache(QHash<CheckSumState::States, QPixmap> &originals) const override;

private:
    static SumStateIcons s_Global;
};

#endif // SUMSTATEICONS_H
