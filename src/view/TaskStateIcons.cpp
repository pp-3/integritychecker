// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TaskStateIcons.h"

TaskStateIcons TaskStateIcons::s_Global(0);

TaskStateIcons &TaskStateIcons::global()
{
    return s_Global;
}

void TaskStateIcons::fillCache(QHash<TaskLifeCycle::RunState, QPixmap> &originals) const
{
    originals.insert(TaskLifeCycle::CREATED,  QPixmap(":/exists"));
    originals.insert(TaskLifeCycle::STARTED,  QPixmap(":/running"));
    originals.insert(TaskLifeCycle::FINISHED, QPixmap(":/finished"));
    originals.insert(TaskLifeCycle::CANCELED, QPixmap(":/canceled"));
}
