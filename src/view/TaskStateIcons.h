// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TASKSTATEICONS_H
#define TASKSTATEICONS_H

#include "ScaledIcons.h"

#include "tasks/TaskLifeCycle.h"

/**
 * @brief The TaskStateIcons class provides the icons for the TaskLifeCycle::RunState enum.
 */
class TaskStateIcons : public ScaledIcons<TaskLifeCycle::RunState>
{
public:
    TaskStateIcons(int iconHeight) :
        ScaledIcons(iconHeight)
    {}
    /**
     * Returns an always available (default) cache for the tast state icons.
     * @return The global instance.
     */
    static TaskStateIcons &global();

protected:
    void fillCache(QHash<TaskLifeCycle::RunState, QPixmap> &originals) const override;

private:
    static TaskStateIcons s_Global;
};

#endif // TASKSTATEICONS_H
