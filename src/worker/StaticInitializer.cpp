// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "StaticInitializer.h"

#include "ifc/ErrorPostIfc.h"
#include "ifc/WorkerSettingsIfc.h"
#include "shared/ExitManager.h"

std::unique_ptr<StaticInitializer> StaticInitializer::pInstance;

StaticInitializer::StaticInitializer() :
    m_WaitingWorkerUpdates(),
    m_WaitingDataCacheUpdates(),
    m_WaitingErrorCollectorUpdates(),
    pW(nullptr),
    pD(nullptr),
    pE(nullptr)
{
    ExitManager::getInstance().addExitListener([]()
        {
            pInstance.reset();
        });
}

StaticInitializer &StaticInitializer::getInstance()
{
    if (!pInstance)
    {
        pInstance.reset(new StaticInitializer);
    }

    return *pInstance;
}

void StaticInitializer::populateSettings(WorkerSettingsIfc *w, DefaultDataCacheType *d, ErrorPostIfc *e)
{
    pW = w;
    pD = d;
    pE = e;

    postUpdates();
}

void StaticInitializer::postUpdates()
{
    for (WorkerSettingsUpdate *w : std::as_const(m_WaitingWorkerUpdates))
        w->workerSettingsUpdate(pW);
    for (DataCacheUpdate *d : std::as_const(m_WaitingDataCacheUpdates))
        d->dataCacheUpdate(pD);
    for (ErrorCollectorUpdate *e : std::as_const(m_WaitingErrorCollectorUpdates))
        e->errorCollectorUpdate(pE);
}

void StaticInitializer::updateMe(WorkerSettingsUpdate *w)
{
    getInstance().m_WaitingWorkerUpdates << w;
    w->workerSettingsUpdate(getInstance().pW);
}

void StaticInitializer::udpateMe(DataCacheUpdate *d)
{
    getInstance().m_WaitingDataCacheUpdates << d;
    d->dataCacheUpdate(getInstance().pD);
}

void StaticInitializer::updateMe(ErrorCollectorUpdate *e)
{
    getInstance().m_WaitingErrorCollectorUpdates << e;
    e->errorCollectorUpdate(getInstance().pE);
}
