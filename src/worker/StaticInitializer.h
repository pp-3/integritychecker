// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef STATICINITIALIZER_H
#define STATICINITIALIZER_H

#include <memory>

#include <QSet>

#include "shared/DefaultDataCacheType.h"

class WorkerSettingsIfc;
class ErrorPostIfc;

/**
 * Interface for an update of the WorkerSettings instance.
 */
struct WorkerSettingsUpdate
{
    virtual ~WorkerSettingsUpdate() = default;
    virtual void workerSettingsUpdate(WorkerSettingsIfc *w) = 0;
};

/**
 * Interface for an update of the DataCache instance.
 */
struct DataCacheUpdate
{
    virtual ~DataCacheUpdate() = default;
    virtual void dataCacheUpdate(DefaultDataCacheType *d) = 0;
};

/**
 * Interface for an update of the ErrorCollector instance.
 */
struct ErrorCollectorUpdate
{
    virtual ~ErrorCollectorUpdate() = default;
    virtual void errorCollectorUpdate(ErrorPostIfc *e) = 0;
};


/**
 * The StaticInitializer class allows classes which are instantiated statically to register for updates
 * delivering instances of services which are created later during the application start up.
 */
class StaticInitializer
{
public:
    /**
     * Returns the single instance of this class.
     * @return The StaticInitializer instance.
     */
    static StaticInitializer &getInstance();

    /**
     * Requests an update of the WorkerSettings instance, as soon as it's available.
     * @param w The callback.
     */
    static void updateMe(WorkerSettingsUpdate *w);
    /**
     * Requests an update of the DataCache instance, as soon as it's available.
     * @param d The callback.
     */
    static void udpateMe(DataCacheUpdate *d);
    /**
     * Requests an update of the ErrorCollector instance, as soon as it's available.
     * @param e The callback.
     */
    static void updateMe(ErrorCollectorUpdate *e);

    /**
     * Sets the instances during start up, after they are valid.
     * @param w The WorkerSettings instance.
     * @param d The DataCache instance.
     * @param e The ErrorCollector instance.
     */
    void populateSettings(WorkerSettingsIfc *w, DefaultDataCacheType *d, ErrorPostIfc *e);

protected:
    StaticInitializer();
    void postUpdates();

private:
    static std::unique_ptr<StaticInitializer> pInstance;

    QSet<WorkerSettingsUpdate *> m_WaitingWorkerUpdates;
    QSet<DataCacheUpdate *>      m_WaitingDataCacheUpdates;
    QSet<ErrorCollectorUpdate *> m_WaitingErrorCollectorUpdates;

    WorkerSettingsIfc    *pW;
    DefaultDataCacheType *pD;
    ErrorPostIfc         *pE;
};

#endif // STATICINITIALIZER_H
