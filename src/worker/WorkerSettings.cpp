// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "WorkerSettings.h"

#include <QDir>
#include <QFileInfo>
#include <QMutexLocker>
#include <QThread>

#include "checksums/SumTypeIfc.h"
#include "checksums/SumTypeManager.h"
#include "shared/ErrorCollector.h"
#include "shared/UserSettings.h"
#include "thread_util/threadcheck.h"

WorkerSettings::WorkerSettings(UserSettingsManager *user_settings, ErrorCollector *error_ifc) :
    pUserSettings(user_settings),
    theErrorIfc(error_ifc),
    m_InputDir(),
    m_OutputDirSelection(static_cast<WorkerSettings::OUTPUT_SELECTION>(WorkerSettings::AS_INPUT)),
    m_OutputDir(),
    m_WorkerThreads(0),
    m_CacheSize(0),
    m_Lock()
{
    readUserSettings(pUserSettings->getCurrentUserSettings().data());

    connect(pUserSettings, &UserSettingsManager::profileChanged, this, &WorkerSettings::settingsProfileChanged);
}

void WorkerSettings::settingsProfileChanged(const QSharedPointer<UserProfileSettings> & /*old_profile*/, const QSharedPointer<UserProfileSettings> &new_profile)
{
    readUserSettings(new_profile.data());
}

void WorkerSettings::readUserSettings(UserProfileSettingsIfc *sett)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (sett == nullptr)
        return;

    setOutputDirSelection(fromString(sett->getOutputDirSelection(), nullptr));
    setInputDir(sett->getInputDir());
    setOutputDir(sett->getOutputDir());
    setWorkerThreads(sett->getWorkerThreads());
    setCacheSize(sett->getCacheSize());

    SumTypeManager &stm = SumTypeManager::getInstance();
    const QStringList calc_types = sett->getEnabledTypes();
    for (SumTypeIfc *it : stm.getRegisteredTypes())
    {
        const bool en = calc_types.isEmpty() ||
                        calc_types.contains(it->getName(), Qt::CaseInsensitive);

        setEnabledType(it->getTypeHash(), en);
    }
}

void WorkerSettings::setOutputDirSelection(const WorkerSettings::OUTPUT_SELECTION sel)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QMutexLocker ml(&m_Lock);

    if (m_OutputDirSelection != sel)
    {
        m_OutputDirSelection = sel;

        QSharedPointer<UserProfileSettingsIfc> sett = pUserSettings->getCurrentUserSettings();
        Q_ASSERT(sett);
        sett->setOutputDirSelection(toString(m_OutputDirSelection));

        ml.unlock();
        emit s_OutputDirSelectionChanged(m_OutputDirSelection);
    }
}

WorkerSettingsIfc::OUTPUT_SELECTION WorkerSettings::getOutputDirSelection() const
{
    QMutexLocker ml(&m_Lock);

    return m_OutputDirSelection;
}

void WorkerSettings::setInputDir(const QString &dir)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QMutexLocker ml(&m_Lock);

    if (m_InputDir != dir)
    {
        m_InputDir = dir;

        QSharedPointer<UserProfileSettingsIfc> sett = pUserSettings->getCurrentUserSettings();
        Q_ASSERT(sett);
        sett->setInputDir(m_InputDir);

        ml.unlock();
        emit s_InputDirChanged(m_InputDir);
    }
}

QString WorkerSettings::getInputDir() const
{
    QMutexLocker ml(&m_Lock);

    return m_InputDir;
}

void WorkerSettings::setOutputDir(const QString &dir)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QMutexLocker ml(&m_Lock);

    if (m_OutputDir != dir)
    {
        m_OutputDir = dir;

        QSharedPointer<UserProfileSettingsIfc> sett = pUserSettings->getCurrentUserSettings();
        Q_ASSERT(sett);
        sett->setOutputDir(m_OutputDir);

        ml.unlock();
        emit s_OutputDirChanged(m_OutputDir);
    }
}

QString WorkerSettings::getOutputDir() const
{
    QMutexLocker ml(&m_Lock);

    return m_OutputDir;
}

bool WorkerSettings::isInputDirValid() const
{
    QMutexLocker ml(&m_Lock);

    QFileInfo fi(m_InputDir);
    const bool ret = (fi.isDir() && fi.isReadable());
    if (!ret)
        theErrorIfc->addWarning(tr("Input directory invalid: ")+m_InputDir);
    return ret;
}

bool WorkerSettings::isOutputDirValid() const
{
    QMutexLocker ml(&m_Lock);

    QFileInfo fo(m_OutputDir);
    const bool ret = (fo.isDir() && fo.isReadable());
    if (!ret)
        theErrorIfc->addWarning(tr("Output directory invalid: ")+m_OutputDir);
    return ret;
}

bool WorkerSettings::areDirsValid() const
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    if (!isInputDirValid())
    {
        return false;
    }
    if (getOutputDirSelection() == OTHER_DIR)
    {
        return isOutputDirValid();
    }

    return true;
}

bool WorkerSettings::createOutputDir()
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QMutexLocker ml(&m_Lock);

    if (m_OutputDir.isEmpty())
    {
        theErrorIfc->addError(tr("Output directory not set"));
        return false;
    }

    QDir d;
    return d.mkpath(m_OutputDir);
}

void WorkerSettings::setWorkerThreads(unsigned int w)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QMutexLocker ml(&m_Lock);

    if (m_WorkerThreads != w)
    {
        const QPair<unsigned int, unsigned int> minmax = WorkerSettings::getMinMaxWorkerThreads();
        w = qMax(w, minmax.first);
        w = qMin(w, minmax.second);

        m_WorkerThreads = w;
        ml.unlock();

        QSharedPointer<UserProfileSettingsIfc> sett = pUserSettings->getCurrentUserSettings();
        Q_ASSERT(sett);
        sett->setWorkerThreads(m_WorkerThreads);

        ml.unlock();
        emit s_WorkerThreadsChanged(m_WorkerThreads);
    }
}

unsigned int WorkerSettings::getWorkerThreads() const
{
    QMutexLocker ml(&m_Lock);

    return m_WorkerThreads;
}

QPair<unsigned int, unsigned int> WorkerSettings::getMinMaxWorkerThreads() const
{
    return QPair<unsigned int, unsigned int>((unsigned int)1, (unsigned int)1 + QThread::idealThreadCount());
}

void WorkerSettings::setCacheSize(const uint &s)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QMutexLocker ml(&m_Lock);

    if (m_CacheSize != s)
    {
        const uint s2 = qMax(s, 10u);
        m_CacheSize = s2;
        ml.unlock();

        QSharedPointer<UserProfileSettingsIfc> sett = pUserSettings->getCurrentUserSettings();
        Q_ASSERT(sett);
        sett->setCacheSize(m_CacheSize);

        ml.unlock();
        emit s_CacheSizeChanged(m_CacheSize);
    }
}

size_t WorkerSettings::getCacheSize() const
{
    QMutexLocker ml(&m_Lock);

    return m_CacheSize;
}

void WorkerSettings::setEnabledType(const SumTypeHash &hash, bool enabled)
{
    Q_ASSERT(isInMainThread());   // call from GUI thread only

    QMutexLocker ml(&m_Lock);

    const bool changed = m_EnabledCalcTypes.contains(hash) != enabled;

    if (enabled)
    {
        m_EnabledCalcTypes.insert(hash);
    }
    else
    {
        m_EnabledCalcTypes.remove(hash);
    }

    ml.unlock();
    if (changed)
    {
        QSharedPointer<UserProfileSettingsIfc> sett = pUserSettings->getCurrentUserSettings();
        Q_ASSERT(sett);

        SumTypeManager &stm = SumTypeManager::getInstance();
        QStringList calc_types;
        for (const unsigned long &it : std::as_const(m_EnabledCalcTypes))
        {
            SumTypeIfc *t = stm.fromHash(it);
            if (t)
            {
                calc_types << t->getName();
            }
        }
        sett->setEnabledTypes(calc_types);

        ml.unlock();
        emit s_EnabledTypeChanged();
    }
}

SumTypeHashList WorkerSettings::getEnabledCalcHashes() const
{
    QMutexLocker ml(&m_Lock);

    return m_EnabledCalcTypes;
}
