// Copyright 2016 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef WORKERSETTINGS_H
#define WORKERSETTINGS_H

#include <QMutex>
#include <QPair>
#include <QSet>
#include <QString>

#include "ifc/WorkerSettingsIfc.h"
#include "sum_shared/CheckSumState.h"

class UserSettingsManager;
class UserProfileSettings;
class UserProfileSettingsIfc;
class ErrorCollector;

/**
 * @brief The WorkerSettings class stores all values related to the background workers calculating the check sums.
 * The settings reflect the ones in the chosen profile and thus may change when the user changes a value or selects
 * a different profile.
 */
class WorkerSettings : public WorkerSettingsIfc
{
    Q_OBJECT

public:
    explicit WorkerSettings(UserSettingsManager *user_settings, ErrorCollector *error_ifc);
    virtual ~WorkerSettings() override = default;

    WorkerSettingsIfc::OUTPUT_SELECTION getOutputDirSelection() const override;
    QString getInputDir() const override;
    QString getOutputDir() const override;
    bool isInputDirValid() const override;
    bool isOutputDirValid() const override;
    bool areDirsValid() const override;
    bool createOutputDir() override;
    unsigned int getWorkerThreads() const override;
    QPair<unsigned int, unsigned int> getMinMaxWorkerThreads() const override;
    size_t getCacheSize() const override;
    SumTypeHashList getEnabledCalcHashes() const override;

public slots:
    void setOutputDirSelection(WorkerSettings::OUTPUT_SELECTION sel);
    void setInputDir(const QString &dir);
    void setOutputDir(const QString &dir);
    void setWorkerThreads(unsigned int w);
    void setCacheSize(const uint &s);
    void setEnabledType(const SumTypeHash &hash, bool enabled);

private slots:
    void settingsProfileChanged(const QSharedPointer<UserProfileSettings> &old_profile, const QSharedPointer<UserProfileSettings> &new_profile);

private:
    void readUserSettings(UserProfileSettingsIfc *sett);

private:
    UserSettingsManager *pUserSettings;
    ErrorCollector      *theErrorIfc;
    QString              m_InputDir;
    OUTPUT_SELECTION     m_OutputDirSelection;
    QString              m_OutputDir;
    unsigned int         m_WorkerThreads;
    uint                 m_CacheSize;
    SumTypeHashList      m_EnabledCalcTypes;
    mutable QMutex       m_Lock;
};

#endif // WORKERSETTINGS_H
