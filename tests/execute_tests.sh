#!/bin/bash

# Copyright 2023 Peter Peters
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

rm -rf coverage
mkdir coverage

# remove previous gcov data
find src -type f -name '*.gcda' -delete
# execute tests
tests/integritychecker_tests --gtest_output=xml:test_result.xml
# capture coverage data
lcov --capture --directory src/ --output-file coverage/coverage.info.all
# remove unwanted path
lcov --remove coverage/coverage.info.all '/usr/*' '*autogen*' '*Qt_*' -o coverage/coverage.info
# create html overview
genhtml --pref $(cd $(pwd)/../; pwd) coverage/coverage.info --output-directory=coverage/html

