// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <QCoreApplication>
#include <QTimer>

#include "checksums/InitializeInstances.h"
#include "checksums/SumTypeManager.h"
#include "shared/ExitManager.h"

int main(int argc, char **argv) {
    // create singletons of compiled in check sum calculators
    initializeDefaultInstances();

    for (const auto *type : SumTypeManager::getInstance().getRegisteredTypes())
    {
        std::cout << "Registered check sum type: " << type->getName().toStdString() << std::endl;
    }

    QCoreApplication app{argc, argv};

    QTimer::singleShot(0,
                       [&]()
                       {
                           ::testing::InitGoogleTest(&argc, argv);
                           const int testResult = RUN_ALL_TESTS();
                           app.exit(testResult);

                           // call all exit listeners
                           ExitManager::tearDown();
                       });

    return app.exec();
}
