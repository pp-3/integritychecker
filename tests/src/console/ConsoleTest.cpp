// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "checksums/SumTypeManager.h"
#include "console/CommandHandler.h"
#include "console/ConsoleUtils.h"
#include "console/ifc/CommandHandlerConstants.h"
#include "console/ifc/ConsoleConstants.h"
#include "model/RunningTasks.h"

#include "data/TestFsUtils.h"
#include "model/IntegrityModelTest.h"
#include "utils/test_utils.h"

class ConsoleTest : public IntegrityModelTest
{
protected:
    std::unique_ptr<ConsoleModel> console_model;
    std::unique_ptr<CommandHandlerIfc> command_handler;
    ::testing::MockFunction<void(const QString &)> response_function;
    ::testing::MockFunction<void(const QString &)> query_function;

    void SetUp() override
    {
        IntegrityModelTest::SetUp();

        // create console model
        console_model = std::make_unique<ConsoleModel>(model.get());
        CommandFactory command_factory(console_model.get());
        command_handler = std::make_unique<CommandHandler>(command_factory, console_model.get());

        // initial console header
        {
            testing::InSequence seq;
            EXPECT_CALL(response_function, Call(::testing::_))
                .Times(2)
                .WillRepeatedly(
                    [](const QString &header)
                    {
                        EXPECT_GT(header.size(), 0);
                    });
            EXPECT_CALL(response_function, Call(::testing::_))
                .WillOnce(
                    [](const QString &header)
                    {
                        EXPECT_EQ(header.size(), 0);
                    });
        }
        command_handler->setResponseIfc(response_function.AsStdFunction());

        // prompt
        EXPECT_CALL(query_function, Call(::testing::_)).WillRepeatedly(testing::Return());
        command_handler->setQueryIfc(query_function.AsStdFunction());
    }

    void TearDown() override
    {
        // final words
        EXPECT_CALL(response_function, Call(::testing::_)).WillRepeatedly(::testing::Return());

        command_handler->close();
        command_handler.reset();
        console_model->close();
        console_model.reset();

        IntegrityModelTest::TearDown();
    }

    /**
     * Flag set, if it's a directory. Name of the directory / file.
     */
    using NameDirFlag = QPair<bool, QString>;

    /**
     * @brief Parses the output of the `ls` command.
     * @param ls_line A single (one file / folder) response line returned from the `ls` command.
     * @return True / false for being a directory and file / folder name.
     */
    NameDirFlag parseLsOutput(const QString &ls_line) const
    {
        const int space = ls_line.indexOf(" ");
        if (space < 0)
        {
            EXPECT_TRUE(false) << ls_line.toStdString() << " is not a valid ls output";
            return NameDirFlag(false, QString());
        }

        const QString type = ls_line.mid(0, space);
        const QString name = ls_line.mid(space + 1);

        bool btype = false;
        if (type == ConsoleConstants::FILE_TAG)
            btype = false;
        else if (type == ConsoleConstants::DIR_TAG)
            btype = true;
        else
            EXPECT_TRUE(false) << type.toStdString() << " is not a valid file / directory type";

        return NameDirFlag(btype, name);
    }

    /**
     * @brief Checks, if the check sum tree directory instance has a specific child (file / subfolder).
     * @param parent_dir The directory to look for the child in.
     * @param is_sub_dir True, if a sub folder is looked for. False, if it's a file.
     * @param name The name to look for.
     * @return True, if the entry has been found. False, if there is no match for the name / isSubDir pair in the directory.
     */
    bool hasChild(DirInfo *parent_dir, bool is_sub_dir, const QString &name) const
    {
        EXPECT_NE(parent_dir, nullptr);
        for (const InfoBase::ptr &child : parent_dir->getChildren())
        {
            if (child->isDir() == is_sub_dir && child->getName() == name)
                return true;
        }

        return false;
    }

    static QStringList sortedNonEmptyUppercase(QStringList in)
    {
        QStringList out;
        for (const QString &it : in)
            if (!it.isEmpty())
                out << it.toUpper();

        out.sort(Qt::CaseSensitive);
        return out;
    }

    /**
     * Index range ( [) ) in a string.
     */
    using IndexRange = QPair<int, int>;

    bool parseStatusHeaderOutput(const QString &status_line,
                                 const QStringList &expected_check_sums,
                                 QList<SumTypeHash> &found_check_sums,
                                 QList<IndexRange> &check_sum_positions,
                                 unsigned &first_sum_index) const
    {
        const int first_check_sum = status_line.indexOf(QRegExp("\\w.*"));
        if (first_check_sum <= 0)
            return false;

        first_sum_index = first_check_sum;

        int index_it = first_check_sum;
        const QStringList check_sums = status_line.mid(first_check_sum).split(" ");
        EXPECT_EQ(sortedNonEmptyUppercase(expected_check_sums), sortedNonEmptyUppercase(check_sums));

        for (QString sum_str : check_sums)
        {
            sum_str = sum_str.trimmed();
            if (sum_str.isEmpty())
            {
                ++index_it;
                continue;
            }

            EXPECT_TRUE(expected_check_sums.contains(sum_str, Qt::CaseInsensitive));

            SumTypeIfc *sum = SumTypeManager::getInstance().fromName(sum_str);
            EXPECT_NE(sum, nullptr) << sum_str.toStdString() << " is not a valid check sum";
            if (!sum)
                return false;

            found_check_sums << sum->getTypeHash();
            check_sum_positions << IndexRange(index_it, index_it + sum_str.size());
            index_it += sum_str.size();
        }

        return true;
    }

    bool parseStatusHeaderUnderlineOutput(const QString &status2_line,
                                          const QList<SumTypeHash> &found_check_sums,
                                          unsigned first_sum_index) const
    {
        const QStringList under_lines = status2_line.mid(first_sum_index).split(" ");
        int count = 0;
        for (QString line : under_lines)
        {
            line = line.trimmed();
            if (!line.isEmpty())
                ++count;
        }

        EXPECT_EQ(count, found_check_sums.size()) << "Underline mismatch";
        return count == found_check_sums.size();
    }

    static CheckSumState::States convertState(const QString &state_str)
    {
        if (state_str.compare(ConsoleConstants::STATE_UNKNOWN) == 0)
            return CheckSumState::UNKNOWN;
        if (state_str.compare(ConsoleConstants::STATE_EXISTS) == 0)
            return CheckSumState::EXISTS;
        if (state_str.compare(ConsoleConstants::STATE_VERIFIED) == 0)
            return CheckSumState::VERIFIED;
        if (state_str.compare(ConsoleConstants::STATE_CHECK_ERROR) == 0)
            return CheckSumState::CHECK_ERROR;

        return CheckSumState::NOT_CHECKING;
    }

    bool checkStatusStateOutput(const QString &status_state_line,
                                const QList<SumTypeHash> &found_check_sums,
                                const QList<IndexRange> &check_sum_positions,
                                unsigned first_sum_index,
                                DirInfo *parent_dir) const
    {
        const QString name_part = status_state_line.left(first_sum_index - 1).trimmed();
        const QString status_part = status_state_line.mid(first_sum_index);

        const NameDirFlag name = parseLsOutput(name_part);
        EXPECT_TRUE(hasChild(parent_dir, name.first, name.second)) << "file / dir has to be child of given parent dir";
        InfoBase *ib = TestFsUtils::findChild(parent_dir, name.second, false);
        EXPECT_NE(ib, nullptr);
        if (!ib)
            return false;

        const auto B1 = found_check_sums.constBegin();
        const auto E1 = found_check_sums.constEnd();
        const auto B2 = check_sum_positions.constBegin();
        const auto E2 = check_sum_positions.constEnd();

        bool ret = true;
        auto it1 = B1;
        auto it2 = B2;
        for ( ; it1 != E1 && it2 != E2 ; ++it1, ++it2)
        {
            const SumTypeHash &hash = *it1;
            const IndexRange &index_range = *it2;

            const QString state_str = status_part.mid(index_range.first - first_sum_index,
                                                      index_range.second - index_range.first).trimmed();
            const CheckSumState::States state = convertState(state_str);
            const InfoBase::SumStateCount counts = ib->getSumStateCount(hash);

            if (state != CheckSumState::States::NOT_CHECKING)
            {
                EXPECT_TRUE(counts.contains(state));
                EXPECT_GT(counts.value(state), 0) << "Missing state for: "
                                                  << name.second.toStdString()
                                                  << ", type: "
                                                  << CheckSumState::toString(state).toStdString();
            }
        }

        return ret;
    }

    static bool checkTaskHeader(const QString &header)
    {
        QStringList parts = header.split(" ");
        auto it = parts.begin();
        while (it != parts.end())
        {
            if (it->isEmpty())
                it = parts.erase(it);
            else
                ++it;
        }
        return parts.size() == 5;
    }

    static bool checkTaskUnderlines(const QString     &lines,
                                    QList<IndexRange> &task_column_positions)
    {
        const QStringList parts = lines.split(" ");
        unsigned start_pos = 0;
        for (const QString &column : parts)
        {
            task_column_positions << IndexRange(start_pos, start_pos + column.size());
            start_pos += column.size() + 1;
        }

        return parts.size() == 5;
    }

    // extracts a sub string from a string accoring to the given column information
    static QString substring(const QString &str, const IndexRange &column)
    {
        return str.mid(column.first, column.second - column.first).trimmed();
    }

    static bool parseTaskLine(const QString            &task_line,
                              const QList<IndexRange>  &task_column_positions,
                              QSet<RunningTasksId::Id> &running_ids)
    {
        std::cout << "task_line: " << task_line.toStdString() << std::endl;
        bool ok;
        // id column
        const RunningTasksId::Id id = substring(task_line, task_column_positions.at(0)).toUInt(&ok);
        EXPECT_TRUE(ok);
        EXPECT_FALSE(running_ids.contains(id));
        running_ids << id;

        // state column
        const QString state = substring(task_line, task_column_positions.at(1));
        EXPECT_TRUE(state == "Running" || state == "Waiting") << "Unexpected state: '" << state.toStdString() << "'";

        return true;
    }
};

TEST_F(ConsoleTest, help)
{
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [](const QString &help)
            {
                ASSERT_GT(help.size(), 10);
            });
    command_handler->processCommand("help");
}

TEST_F(ConsoleTest, pwd)
{
    EXPECT_CALL(response_function, Call(QString("/"))).Times(1);
    command_handler->processCommand("pwd");
}

TEST_F(ConsoleTest, pwd_wrong_parameter)
{
    {
        testing::InSequence seq;
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(1);
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [](const QString &err)
                {
                    ASSERT_TRUE(err.startsWith(CommandHandlerConstants::GENERAL_ERROR));
                });
    }
    command_handler->processCommand("pwd dir1");
}

TEST_F(ConsoleTest, cdpwd)
{
    testing::InSequence seq;

    EXPECT_CALL(response_function, Call(QString("/"))).Times(1);
    command_handler->processCommand("pwd");

    EXPECT_CALL(response_function, Call(QString("dir1/"))).Times(1);
    command_handler->processCommand("cd dir1");
    command_handler->processCommand("pwd");

    EXPECT_CALL(response_function, Call(QString("/"))).Times(1);
    command_handler->processCommand("cd ..");
    command_handler->processCommand("pwd");

    EXPECT_CALL(response_function, Call(QString("dir2/"))).Times(1);
    command_handler->processCommand("cd dir2");
    command_handler->processCommand("pwd");


    EXPECT_CALL(response_function, Call(QString("/"))).Times(1);
    command_handler->processCommand("cd /");
    command_handler->processCommand("pwd");

    EXPECT_CALL(response_function, Call(QString("dir2/dir3/dir5/"))).Times(1);
    command_handler->processCommand("cd dir2/dir3/dir5");
    command_handler->processCommand("pwd");

    EXPECT_CALL(response_function, Call(QString("dir2/"))).Times(1);
    command_handler->processCommand("cd ../..");
    command_handler->processCommand("pwd");

    EXPECT_CALL(response_function, Call(QString("dir2/dir3/"))).Times(1);
    command_handler->processCommand("cd /dir2/dir3/");
    command_handler->processCommand("pwd");

    EXPECT_CALL(response_function, Call(QString("/"))).Times(1);
    command_handler->processCommand("cd");
    command_handler->processCommand("pwd");

    // wrong parameters
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(2)
        .WillRepeatedly(::testing::Return());  // error strings
    command_handler->processCommand("cd dir1 dir2");
    EXPECT_CALL(response_function, Call(QString("/"))).Times(1);
    command_handler->processCommand("pwd");
}

TEST_F(ConsoleTest, cd_not_existing_dir)
{
    {
        testing::InSequence seq;
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(1);
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [](const QString &err)
                {
                    ASSERT_TRUE(err.startsWith(CommandHandlerConstants::GENERAL_ERROR));
                });
    }
    command_handler->processCommand("cd not_existing");
}

TEST_F(ConsoleTest, ls)
{
    testing::InSequence seq;

    const DirInfo::ptr r = root->getRoot();
    const QList<InfoBase::ptr> root_children = r->getChildren();
    ASSERT_GT(root_children.size(), 0);

    // dir 1 content
    InfoBase *ib1 = TestFsUtils::findChild(r.get(), "dir1", false);
    DirInfo *di1 = DirInfo::cast(ib1);
    ASSERT_NE(di1, nullptr);
    const QList<InfoBase::ptr> dir1_children = di1->getChildren();
    ASSERT_GT(dir1_children.size(), 0);

    // dir 3 content
    InfoBase *ib3 = TestFsUtils::findChild(r.get(), "dir3", true);
    DirInfo *di3 = DirInfo::cast(ib3);
    ASSERT_NE(di3, nullptr);
    ASSERT_GT(di3->getChildren().size(), 0);

    // dir 5 content
    InfoBase *ib5 = TestFsUtils::findChild(r.get(), "dir5", true);
    DirInfo *di5 = DirInfo::cast(ib5);
    ASSERT_NE(di5, nullptr);
    const QList<InfoBase::ptr> dir5_children = di5->getChildren();
    ASSERT_GT(dir5_children.size(), 0);

    // root folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(root_children.size())
        .WillRepeatedly(
            [this, &r](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(entry.first);
                EXPECT_TRUE(hasChild(r.get(), entry.first, entry.second));
            });
    command_handler->processCommand("ls");

    // dir 1 from root folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [this, &r](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(entry.first);
                EXPECT_TRUE(hasChild(r.get(), entry.first, entry.second));
            });
    command_handler->processCommand("ls dir1");

    // dir 1 entries from root folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(dir1_children.size())
        .WillRepeatedly(
            [this, di1](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(hasChild(di1, entry.first, entry.second));
            });
    command_handler->processCommand("ls dir1/");

    // dir 5 from root folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [this, di3](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(entry.first);
                EXPECT_TRUE(hasChild(di3, entry.first, entry.second));
            });
    command_handler->processCommand("ls dir2/dir3/dir5");

    // dir 5 entries from root folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(dir5_children.size())
        .WillRepeatedly(
            [this, di5](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(hasChild(di5, entry.first, entry.second));
            });
    command_handler->processCommand("ls dir2/dir3/dir5/");

    // file 1 from root folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [this, di1](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_FALSE(entry.first);
                EXPECT_TRUE(hasChild(di1, entry.first, entry.second));
            });
    command_handler->processCommand("ls dir1/file1");

    // parent dir from root folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [](const QString &error_text)
            {
                EXPECT_GT(error_text.size(), 0);
            });
    command_handler->processCommand("ls ..");

    // dir 1 from dir 1 folder
    command_handler->processCommand("cd dir1");

    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(dir1_children.size())
        .WillRepeatedly(
            [this, di1](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(hasChild(di1, entry.first, entry.second));
            });
    command_handler->processCommand("ls");

    // file 1 from dir 1 folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [this, di1](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_FALSE(entry.first);
                EXPECT_TRUE(hasChild(di1, entry.first, entry.second));
            });
    command_handler->processCommand("ls file1");

    // dir 1 entries as current directory
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(dir1_children.size())
        .WillRepeatedly(
            [this, di1](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(hasChild(di1, entry.first, entry.second));
            });
    command_handler->processCommand("ls .");

    // root folder as parent folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(root_children.size())
        .WillRepeatedly(
            [this, &r](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(entry.first);
                EXPECT_TRUE(hasChild(r.get(), entry.first, entry.second));
            });
    command_handler->processCommand("ls ..");

    // dir 1 entries via parent
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(dir1_children.size())
        .WillRepeatedly(
            [this, di1](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(hasChild(di1, entry.first, entry.second));
            });
    command_handler->processCommand("ls ../dir1/");

    // root folder as root folder
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(root_children.size())
        .WillRepeatedly(
            [this, &r](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(entry.first);
                EXPECT_TRUE(hasChild(r.get(), entry.first, entry.second));
            });
    command_handler->processCommand("ls /");

    // dir 5 entries as full path
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(dir5_children.size())
        .WillRepeatedly(
            [this, di5](const QString &entry_name)
            {
                const NameDirFlag entry = parseLsOutput(entry_name);
                EXPECT_TRUE(hasChild(di5, entry.first, entry.second));
            });
    command_handler->processCommand("ls /dir2/dir3/dir5/");

    // wrong parameters
    EXPECT_CALL(response_function, Call(::testing::_))
        .Times(2)
        .WillRepeatedly(::testing::Return());  // error strings
    command_handler->processCommand("ls dir1 dir2");
}

TEST_F(ConsoleTest, status_root_folder)
{
    const DirInfo::ptr r = root->getRoot();
    const int root_children = r->getChildren().size();

    // root folder status
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(root_children)
            .WillRepeatedly(
                [this, r, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            r.get());
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status");
    }
}

TEST_F(ConsoleTest, status_sub_dir)
{
    const DirInfo::ptr r = root->getRoot();

    // dir 1 content
    InfoBase *ib1 = TestFsUtils::findChild(r.get(), "dir1", false);
    DirInfo *di1 = DirInfo::cast(ib1);
    ASSERT_NE(di1, nullptr);
    const QList<InfoBase::ptr> dir1_children = di1->getChildren();
    ASSERT_GT(dir1_children.size(), 0);

    // dir1 folder status
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, r, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            r.get());
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status dir1");
    }
}

TEST_F(ConsoleTest, status_sub_dir_contents)
{
    const DirInfo::ptr r = root->getRoot();

    // dir 1 content
    InfoBase *ib1 = TestFsUtils::findChild(r.get(), "dir1", false);
    DirInfo *di1 = DirInfo::cast(ib1);
    ASSERT_NE(di1, nullptr);
    const QList<InfoBase::ptr> dir1_children = di1->getChildren();
    ASSERT_GT(dir1_children.size(), 0);

    // dir1 folder status
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(dir1_children.size())
            .WillRepeatedly(
                [this, di1, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            di1);
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status dir1/");
    }
}

TEST_F(ConsoleTest, status_sub_file)
{
    const DirInfo::ptr r = root->getRoot();

    // dir 1 content
    InfoBase *ib1 = TestFsUtils::findChild(r.get(), "dir1", false);
    DirInfo *di1 = DirInfo::cast(ib1);
    ASSERT_NE(di1, nullptr);
    const QList<InfoBase::ptr> dir1_children = di1->getChildren();
    ASSERT_GT(dir1_children.size(), 0);

    // dir1/file1 file status
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, di1, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            di1);
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status dir1/file1");
    }
}

TEST_F(ConsoleTest, status_deep_sub_dir)
{
    const DirInfo::ptr r = root->getRoot();

    // dir 4 content
    InfoBase *ib4 = TestFsUtils::findChild(r.get(), "dir4", true);
    DirInfo *di4 = DirInfo::cast(ib4);
    ASSERT_NE(di4, nullptr);
    const QList<InfoBase::ptr> dir4_children = di4->getChildren();
    ASSERT_GT(dir4_children.size(), 0);

    // dir2/dir4/dir8 folder status
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillRepeatedly(
                [this, di4, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            di4);
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status dir2/dir4/dir8");
    }
}

TEST_F(ConsoleTest, status_deep_sub_dir_contents)
{
    const DirInfo::ptr r = root->getRoot();

    // dir 8 content
    InfoBase *ib8 = TestFsUtils::findChild(r.get(), "dir8", true);
    DirInfo *di8 = DirInfo::cast(ib8);
    ASSERT_NE(di8, nullptr);
    const QList<InfoBase::ptr> dir8_children = di8->getChildren();
    ASSERT_GT(dir8_children.size(), 0);

    // dir2/dir4/dir8/ folder status
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(dir8_children.size())
            .WillRepeatedly(
                [this, di8, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            di8);
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status dir2/dir4/dir8/");
    }
}

TEST_F(ConsoleTest, status_empty_dir)
{
    const DirInfo::ptr r = root->getRoot();

    // dir 6 content
    InfoBase *ib6 = TestFsUtils::findChild(r.get(), "dir6", true);
    DirInfo *di6 = DirInfo::cast(ib6);
    ASSERT_NE(di6, nullptr);
    const QList<InfoBase::ptr> dir6_children = di6->getChildren();
    ASSERT_EQ(dir6_children.size(), 0);

    // dir2/dir3/dir6/ folder status -> no output
    EXPECT_CALL(response_function, Call(::testing::_)).Times(0);
    command_handler->processCommand("status dir2/dir3/dir6/");
}

TEST_F(ConsoleTest, status_not_existing_dir)
{
    const DirInfo::ptr r = root->getRoot();

    // not existing folder status -> error output
    {
        testing::InSequence seq;
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(1);
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [](const QString &err)
                {
                    ASSERT_TRUE(err.startsWith(CommandHandlerConstants::GENERAL_ERROR));
                });
    }
    command_handler->processCommand("status non_existing/");
}

TEST_F(ConsoleTest, status_cd_dir)
{
    const DirInfo::ptr r = root->getRoot();

    command_handler->processCommand("cd dir2");

    // dir 2 content
    InfoBase *ib2 = TestFsUtils::findChild(r.get(), "dir2", false);
    DirInfo *di2 = DirInfo::cast(ib2);
    ASSERT_NE(di2, nullptr);
    const QList<InfoBase::ptr> dir2_children = di2->getChildren();
    ASSERT_GT(dir2_children.size(), 0);

    // dir2 folder status
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(dir2_children.size())
            .WillRepeatedly(
                [this, di2, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            di2);
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status");
    }
}

TEST_F(ConsoleTest, scan_root_dir)
{
    const DirInfo::ptr r = root->getRoot();

    const QStringList dir = {"dir2", "dir3", "dir6"};
    const QString file_name = "new_file1";
    const QByteArray content = {"abc"};
    const DirInfo *subdir = TestFsUtils::findDir(dir, r.get());
    ASSERT_NE(subdir, nullptr);
    const DirInfo::ptr di = DirInfo::cast(root->searchChild(subdir));
    ASSERT_NE(di, nullptr);

    // create new file
    ASSERT_TRUE(test_data.createFile(dir, file_name, content));

    // rescan
    waitForFinishedCheckSumScan(
        [this]()
        {
            command_handler->processCommand("scan");
        });

    ASSERT_NE(nullptr, ConsoleUtils::getChild(di, file_name));
}

TEST_F(ConsoleTest, scan_parent_dir)
{
    const DirInfo::ptr r = root->getRoot();

    const QStringList dir = {"dir2", "dir3", "dir6"};
    const QString file_name = "new_file1";
    const QByteArray content = {"abc"};
    const DirInfo *subdir = TestFsUtils::findDir(dir, r.get());
    ASSERT_NE(subdir, nullptr);
    const DirInfo::ptr di = DirInfo::cast(root->searchChild(subdir));
    ASSERT_NE(di, nullptr);

    // create new file
    ASSERT_TRUE(test_data.createFile(dir, file_name, content));

    // rescan
    waitForFinishedCheckSumScan(
        [this]()
        {
            command_handler->processCommand("scan dir2/dir3/dir6");
        });

    ASSERT_NE(nullptr, ConsoleUtils::getChild(di, file_name));
}

TEST_F(ConsoleTest, scan_nested_dir)
{
    const DirInfo::ptr r = root->getRoot();

    const QStringList file_dir = {"dir2", "dir3"};
    const QStringList scan_dir = {"dir2", "dir3", "dir6"};
    const QString file_name = "new_file1";
    const QByteArray content = {"abc"};
    const DirInfo *subdir = TestFsUtils::findDir(scan_dir, r.get());
    ASSERT_NE(subdir, nullptr);
    const DirInfo::ptr di = DirInfo::cast(root->searchChild(subdir));
    ASSERT_NE(di, nullptr);

    // create new file
    ASSERT_TRUE(test_data.createFile(file_dir, file_name, content));

    // rescan
    waitForFinishedFsScan(
        [this]()
        {
            command_handler->processCommand("scan dir2/dir3/dir6");
        });

    ASSERT_EQ(nullptr, ConsoleUtils::getChild(di, file_name));
}

TEST_F(ConsoleTest, check_dir)
{
    const DirInfo::ptr r = root->getRoot();

    waitForFinishedCheckSumVerification([this]() {
                                            command_handler->processCommand("check dir1");
                                        });

    // dir 1 content
    InfoBase *ib1 = TestFsUtils::findChild(r.get(), "dir1", false);

    // dir1 check files
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, r, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            r.get());
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status dir1");
    }

    // check back states
    const QList<CheckSumState::States> all_states = { CheckSumState::VERIFIED, CheckSumState::CHECK_ERROR };
    for (const SumTypeHash hash : std::as_const(all_sum_hashes))
    {
        const InfoBase::SumStateCount counts = ib1->getSumStateCount(hash);

        const bool has_checks = std::any_of(all_states.cbegin(),
                                            all_states.cend(),
                                            [&counts](const CheckSumState::States state) -> bool
                                            {
                                                return counts[state] > 0;
                                            });
        ASSERT_TRUE(has_checks);
    }
}

TEST_F(ConsoleTest, check_dir_contents)
{
    const DirInfo::ptr r = root->getRoot();

    waitForFinishedCheckSumVerification([this]() {
                                            command_handler->processCommand("check dir2/dir4/dir8");
                                        });

    // dir 7 content
    InfoBase *ib7 = TestFsUtils::findChild(r.get(), "dir7", true);

    // dir 8 content
    InfoBase *ib8 = TestFsUtils::findChild(r.get(), "dir8", true);
    DirInfo *di8 = DirInfo::cast(ib8);
    ASSERT_NE(di8, nullptr);
    const int dir8_children = di8->getChildren().size();

    // dir8 check files
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             all_sum_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(dir8_children)
            .WillRepeatedly(
                [this, di8, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            di8);
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status dir2/dir4/dir8/");
    }

    // check back expected states
    const QList<CheckSumState::States> all_states = { CheckSumState::VERIFIED, CheckSumState::CHECK_ERROR };
    for (const SumTypeHash hash : std::as_const(all_sum_hashes))
    {
        const InfoBase::SumStateCount counts = ib8->getSumStateCount(hash);

        const bool has_checks = std::any_of(all_states.cbegin(),
                                            all_states.cend(),
                                            [&counts](const CheckSumState::States state) -> bool
                                            {
                                                return counts[state] > 0;
                                            });
        ASSERT_TRUE(has_checks);
    }

    // check back not expected states
    for (const SumTypeHash hash : std::as_const(all_sum_hashes))
    {
        const InfoBase::SumStateCount counts = ib7->getSumStateCount(hash);

        const bool has_no_checks = std::none_of(all_states.cbegin(),
                                                all_states.cend(),
                                                [&counts](const CheckSumState::States state) -> bool
                                                {
                                                    return counts[state] > 0;
                                                });
        ASSERT_TRUE(has_no_checks);
    }
}

TEST_F(ConsoleTest, create_file)
{
    const DirInfo::ptr r = root->getRoot();

    static const QString test_file = "error1";
    waitForFinishedCheckSumCreation([this]() {
                                        command_handler->processCommand(QString("create dir1/") + test_file);
                                    });

    // dir 1 content
    InfoBase *ib1 = TestFsUtils::findChild(r.get(), "dir1", false);
    DirInfo *di1 = DirInfo::cast(ib1);
    ASSERT_NE(di1, nullptr);
    const QList<InfoBase::ptr> dir1_children = di1->getChildren();

    for (const SumTypeHash hash : std::as_const(all_sum_hashes))
    {
        for (const InfoBase::ptr &file : dir1_children)
        {
            const InfoBase::SumStateCount counts = file->getSumStateCount(hash);
            if (file->getName() == test_file)
                ASSERT_EQ(counts[CheckSumState::VERIFIED], 1);
            else
                ASSERT_EQ(counts[CheckSumState::VERIFIED], 0);
            ASSERT_EQ(counts[CheckSumState::CHECK_ERROR], 0);
        }
    }
}

TEST_F(ConsoleTest, create_dir)
{
    const DirInfo::ptr r = root->getRoot();

    waitForFinishedCheckSumCreation([this]() {
                                        command_handler->processCommand("create dir1");
                                    });

    // dir 1 content
    InfoBase *ib1 = TestFsUtils::findChild(r.get(), "dir1", false);
    DirInfo *di1 = DirInfo::cast(ib1);
    ASSERT_NE(di1, nullptr);
    const QList<InfoBase::ptr> dir1_children = di1->getChildren();

    const QList<const char *> checked_files_prefixes = {TestData::FilesWithErrorsPrefix,
                                                        TestData::FilesWithCheckSumsPrefix,
                                                        TestData::FilesWithMissingCheckSumsPrefix,
                                                        TestData::FilesWithMixedPrefix};
    QList<InfoBase::ptr> checked_files;
    std::copy_if(dir1_children.begin(),
                 dir1_children.end(),
                 std::back_inserter(checked_files),
                 [&checked_files_prefixes](const InfoBase::ptr &ib) -> bool
                 {
                     return std::any_of(checked_files_prefixes.cbegin(),
                                        checked_files_prefixes.cend(),
                                        [&ib](const char *prefix) -> bool
                                        {
                                            return ib->getName().startsWith(prefix);
                                        });
                 });
    EXPECT_GT(checked_files.size(), 0);


    for (const SumTypeHash hash : std::as_const(all_sum_hashes))
    {
        for (const InfoBase::ptr &file : std::as_const(checked_files))
        {
            const InfoBase::SumStateCount counts = file->getSumStateCount(hash);
            ASSERT_EQ(counts[CheckSumState::VERIFIED], 1);
            ASSERT_EQ(counts[CheckSumState::CHECK_ERROR], 0);
        }
    }
}

TEST_F(ConsoleTest, create_checksum_part)
{
    // create only part of the checksums
    ASSERT_GT(all_sum_hashes.size(), 3);
    SumTypeHashList enabled_hashes;
    QStringList enabled_names;
    for (SumTypeIfc *sum : SumTypeManager::getInstance().getRegisteredTypes().mid(0, 3))
    {
        enabled_hashes << sum->getTypeHash();
        enabled_names << sum->getName();
    }

    const DirInfo::ptr r = root->getRoot();

    command_handler->processCommand(QString("setup checks ") + enabled_names.join(ConsoleUtils::PARAM_SEP));
    waitForFinishedCheckSumCreation([this]() {
                                        command_handler->processCommand("create dir2/dir4/dir7");
                                    });

    // dir 7 content
    InfoBase *ib7 = TestFsUtils::findChild(r.get(), "dir7", true);
    DirInfo *di7 = DirInfo::cast(ib7);
    ASSERT_NE(di7, nullptr);
    const QList<InfoBase::ptr> dir7_children = di7->getChildren();

    // check creation
    const QList<const char *> checked_files_prefixes = {TestData::FilesWithErrorsPrefix,
                                                        TestData::FilesWithCheckSumsPrefix,
                                                        TestData::FilesWithMissingCheckSumsPrefix,
                                                        TestData::FilesWithMixedPrefix};
    QList<InfoBase::ptr> checked_files;
    std::copy_if(dir7_children.begin(),
                 dir7_children.end(),
                 std::back_inserter(checked_files),
                 [&checked_files_prefixes](const InfoBase::ptr &ib) -> bool
                 {
                     return std::any_of(checked_files_prefixes.cbegin(),
                                        checked_files_prefixes.cend(),
                                        [&ib](const char *prefix) -> bool
                                        {
                                            return ib->getName().startsWith(prefix);
                                        });
                 });
    EXPECT_GT(checked_files.size(), 0);

    for (const SumTypeHash hash : std::as_const(all_sum_hashes))
    {
        const bool is_enabled = enabled_hashes.contains(hash);
        for (const InfoBase::ptr &file : std::as_const(checked_files))
        {
            const InfoBase::SumStateCount counts = file->getSumStateCount(hash);
            ASSERT_EQ(counts[CheckSumState::VERIFIED], is_enabled ? 1 : 0);
            ASSERT_EQ(counts[CheckSumState::CHECK_ERROR], 0);
        }
    }

    // check partial dir2/dir4/dir7/ folder status
    QList<SumTypeHash> found_check_sums;
    QList<IndexRange> check_sum_positions;
    unsigned first_sum_index;
    {
        testing::InSequence seq;
        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, enabled_names, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &header)
                {
                    const bool ret = parseStatusHeaderOutput(header,
                                                             enabled_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // header underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, &found_check_sums, &first_sum_index](const QString &under_lines)
                {
                    const bool ret = parseStatusHeaderUnderlineOutput(under_lines,
                                                                      found_check_sums,
                                                                      first_sum_index);
                    EXPECT_TRUE(ret);
                });
        // states
        EXPECT_CALL(response_function, Call(::testing::_))
            .Times(dir7_children.size())
            .WillRepeatedly(
                [this, di7, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &state_line)
                {
                    const bool ret = checkStatusStateOutput(state_line,
                                                            found_check_sums,
                                                            check_sum_positions,
                                                            first_sum_index,
                                                            di7);
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand("status dir2/dir4/dir7/");
    }

    ASSERT_EQ(found_check_sums.size(), enabled_hashes.size());
}

TEST_F(ConsoleTest, enabled_types)
{
    QStringList all_names;
    for (SumTypeIfc *sum : SumTypeManager::getInstance().getRegisteredTypes())
    {
        all_names << sum->getName();
    }
    ASSERT_GT(all_names.size(), 1);

    while (all_names.size() > 1)
    {
        // remove a check type
        all_names.removeFirst();

        testing::InSequence seq;

        // setup fewer checks
        EXPECT_CALL(*user_settings_manager.profile_settings, setEnabledTypes(::testing::_))
            .WillRepeatedly(
                [all_names](const QStringList &types)
                {
                    for (const QString &type : types)
                    {
                        EXPECT_TRUE(all_names.contains(type, Qt::CaseInsensitive));
                    }
                });
        command_handler->processCommand(QString("setup checks ") + all_names.join(ConsoleUtils::PARAM_SEP));

        // read it back
        QList<SumTypeHash> found_check_sums;
        QList<IndexRange> check_sum_positions;
        unsigned first_sum_index;
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [this, all_names, &found_check_sums, &check_sum_positions, &first_sum_index](const QString &checksums)
                {
                    const bool ret = parseStatusHeaderOutput(QString(" ") + checksums, // parseStatusHeaderOutput assumes it a head with prefix spaces
                                                             all_names,
                                                             found_check_sums,
                                                             check_sum_positions,
                                                             first_sum_index);
                    EXPECT_TRUE(ret);
                });
        command_handler->processCommand(QString("setup checks"));
    }
}

TEST_F(ConsoleTest, setup_default)
{
    // t.b.d
    EXPECT_CALL(response_function, Call(::testing::_)).Times(1);
    command_handler->processCommand("setup");
}

TEST_F(ConsoleTest, read_input_dir)
{
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [this](const QString &path)
            {
                ASSERT_EQ(path, test_data_root_path) <<
                    "path mismatch: " << path.toStdString() <<
                    " / " << test_data_root_path.toStdString();
            });
    command_handler->processCommand("setup input");
}

TEST_F(ConsoleTest, change_input_dir)
{
    const QString new_dir = test_data_root_path + "/dir2";
    DirInfoRoot::ptr new_root;

    // callback for new root
    const auto root_connection = QObject::connect(
        model.get(),
        &IntegrityModel::dirRootChanged,
        getMainThreadAffinity(),
        [&new_root](const DirInfoRoot::ptr &root) {
            new_root = root;
        },
        Qt::DirectConnection);
    ASSERT_TRUE(root_connection);

    // change input dir
    EXPECT_CALL(*user_settings_manager.profile_settings, setInputDir(new_dir)).WillOnce(testing::Return());
    EXPECT_CALL(response_function, Call(::testing::_)).Times(0);
    waitForFinishedCheckSumScan(
        [this, new_dir]()
        {
            command_handler->processCommand("setup input " + new_dir);
        });

    QObject::disconnect(root_connection);
    ASSERT_NE(new_root, nullptr);

    // check for sub paths
    // dir 7 still available
    InfoBase *ib7 = TestFsUtils::findChild(new_root->getRoot().get(), "dir7", true);
    ASSERT_NE(ib7, nullptr);
    // dir 1 out of scope
    InfoBase *ib1 = TestFsUtils::findChild(new_root->getRoot().get(), "dir1", true);
    ASSERT_EQ(ib1, nullptr);
}

TEST_F(ConsoleTest, empty_tasks)
{
    EXPECT_CALL(response_function, Call(::testing::_)).Times(0);
    command_handler->processCommand("tasks");
}

TEST_F(ConsoleTest, check_task_list)
{
    const QStringList dir;
    const QString file_name = "LARGE1";

    // create large file, in order to get a "slow" enough computation
    ASSERT_TRUE(test_data.createLargeFile(dir, file_name));

    // rescan
    waitForFinishedCheckSumScan(
        [this]()
        {
            command_handler->processCommand("scan");
        });

    // parse task output
    QList<IndexRange> task_column_positions;
    QSet<RunningTasksId::Id> running_ids;

    {
        testing::InSequence seq;

        // header
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [](const QString &header)
                {
                    const bool ret = checkTaskHeader(header);
                    EXPECT_TRUE(ret);
                });

        // underlines
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillOnce(
                [&task_column_positions](const QString &lines)
                {
                    const bool ret = checkTaskUnderlines(lines, task_column_positions);
                    EXPECT_TRUE(ret);
                });

        // tasks
        EXPECT_CALL(response_function, Call(::testing::_))
            .WillRepeatedly(
                [&task_column_positions, &running_ids](const QString &task)
                {
                    const bool ret = parseTaskLine(task, task_column_positions, running_ids);
                    EXPECT_TRUE(ret);
                });
    }

    command_handler->processCommand("create");
    QThread::msleep(20);
    command_handler->processCommand("tasks");

    // cancel all tasks
    EXPECT_CALL(response_function, Call(::testing::_)).Times(0);
    waitForFinishedCheckSumCreation(
        [this, &running_ids]()
        {
            for (const RunningTasksId::Id &id : std::as_const(running_ids))
            {
                command_handler->processCommand("cancel " + QString::number(id));
            }
        });

    for (const TaskLifeCycle::ptr &task : model->getRunningTasks()->currentTasks())
    {
        bool canceled = false;
        task->waitForTaskFinished(&canceled);

        EXPECT_TRUE(canceled);
        EXPECT_EQ(task->getState(), TaskLifeCycle::RunState::CANCELED) <<
            "Task not canceled: " << task->getName().toStdString() <<
            " -> " << task->getStateStr().toStdString();
    }

    // check at least for some files the check sums are not calculated
    for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
    {
        const InfoBase::SumStateCount stats = root->getRoot()->getSumStateCount(hash);
        std::cout << "UNKNOWN : " << stats[CheckSumState::States::UNKNOWN] << std::endl;
        std::cout << "EXISTS  : " << stats[CheckSumState::States::EXISTS] << std::endl;
        std::cout << "VERIFIED: " << stats[CheckSumState::States::VERIFIED] << std::endl;
        EXPECT_GT(stats[CheckSumState::States::UNKNOWN] + stats[CheckSumState::States::EXISTS], 10);
    }
}

TEST_F(ConsoleTest, cancel_tasks_at_model_close)
{
    const QStringList dir;
    const QString file_name = "LARGE1";

    // create large file, in order to get a "slow" enough computation
    ASSERT_TRUE(test_data.createLargeFile(dir, file_name));

    // rescan
    waitForFinishedCheckSumScan(
        [this]()
        {
            command_handler->processCommand("scan");
        });

    command_handler->processCommand("create");
    QThread::msleep(20);
    EXPECT_CALL(response_function, Call(::testing::_)).Times(testing::AtLeast(1));
    command_handler->processCommand("tasks");

    // close model
    console_model->close();
    // if currentTasks() is not yet empty, at least it should be finished
    for (const TaskLifeCycle::ptr &task : model->getRunningTasks()->currentTasks())
    {
        ASSERT_TRUE(task->isFinishedOrCanceled(nullptr));
    }
}

TEST_F(ConsoleTest, profile_default)
{
    // t.b.d
    EXPECT_CALL(response_function, Call(::testing::_)).Times(1);
    command_handler->processCommand("profile");
}

TEST_F(ConsoleTest, list_profiles)
{
    const QStringList profiles = {"abc", "def", "ghi"};
    const QSet<QString> profile_set = toSet(profiles);

    EXPECT_CALL(user_settings_manager, getProfiles).WillOnce(testing::Return(profiles));
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [&profile_set](const QString &profiles)
            {
                const QSet<QString> set = toSet(profiles.split(" "));

                EXPECT_EQ(set, profile_set) <<
                    "profile mismatch: " << profiles.toStdString() <<
                    "set: \n" << toList(set);
            });

    command_handler->processCommand("profile list");
}

TEST_F(ConsoleTest, current_profile)
{
    const QString profile_name("test_name");
    user_settings_manager.profile_settings->m_ProfileName = profile_name;

    EXPECT_CALL(user_settings_manager, getCurrentUserSettings)
        .WillOnce(testing::Return(user_settings_manager.profile_settings));
    EXPECT_CALL(response_function, Call(::testing::_))
        .WillOnce(
            [&profile_name](const QString &name)
            {
                EXPECT_EQ(name, profile_name) <<
                    "name mismatch: " << name.toStdString();
            });

    command_handler->processCommand("profile current");
}

TEST_F(ConsoleTest, switch_profile)
{
    const QString profile_name("test_name");
    EXPECT_CALL(user_settings_manager, existsProfile(profile_name))
        .WillOnce(testing::Return(true));
    EXPECT_CALL(user_settings_manager, setCurrentUserProfile(profile_name))
        .WillOnce(testing::Return(user_settings_manager.profile_settings));

    command_handler->processCommand("profile switch " + profile_name);
}

TEST_F(ConsoleTest, cp_profile)
{
    const QString profile_name("test_name");
    EXPECT_CALL(user_settings_manager, copyProfile(profile_name))
        .WillOnce(testing::Return(user_settings_manager.profile_settings));

    command_handler->processCommand("profile cp " + profile_name);
}

TEST_F(ConsoleTest, rename_profile)
{
    const QString profile_name("test_name");
    EXPECT_CALL(user_settings_manager, renameProfile(profile_name))
        .WillOnce(testing::Return(user_settings_manager.profile_settings));

    command_handler->processCommand("profile rename " + profile_name);
}

TEST_F(ConsoleTest, delete_profile)
{
    const QString profile_name("test_name");
    EXPECT_CALL(user_settings_manager, existsProfile(profile_name))
        .WillOnce(testing::Return(true));
    EXPECT_CALL(user_settings_manager, deleteProfile(profile_name))
        .WillOnce(testing::Return(user_settings_manager.profile_settings));

    command_handler->processCommand("profile delete " + profile_name);
}
