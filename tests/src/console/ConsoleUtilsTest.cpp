// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "console/ConsoleUtils.h"

#include "sum_tree/SumTreeHierarchyTest.h"
#include "utils/test_utils.h"

class ConsoleUtilsTest : public SumTreeHierarchyTest
{
protected:
    DirInfo::ptr r;
    FileInfo::ptr f1;
    DirInfo::ptr d1;
    FileInfo::ptr f2;
    DirInfo::ptr d2;

    void SetUp() override
    {
        SumTreeHierarchyTest::SetUp();

        /* Set up tree
         *          root
         *         /    \
         *       d1      f1
         *      /  \
         *     d2   f2
         */

        r = root->getRoot();

        f1 = addFileChild(r.data(), f1name);
        d1 = addDirChild(r.data(), d1name);

        f2 = addFileChild(d1.data(), f2name);
        d2 = addDirChild(d1.data(), d2name);
    }

    void TearDown() override
    {
        r.reset();
        f1.reset();
        d1.reset();
        f2.reset();
        d2.reset();

        SumTreeHierarchyTest::TearDown();
    }
};

TEST_F(ConsoleUtilsTest, getChild)
{
    ASSERT_EQ(ConsoleUtils::getChild(r, f1name), f1);
    ASSERT_EQ(ConsoleUtils::getChild(r, d1name), d1);
    ASSERT_EQ(ConsoleUtils::getChild(d1, d2name), d2);
    ASSERT_EQ(ConsoleUtils::getChild(d1, f1name), nullptr);
    ASSERT_EQ(ConsoleUtils::getChild(r, f2name), nullptr);
}

TEST_F(ConsoleUtilsTest, getChildren)
{
    QString error;
    QList<InfoBase::ptr> l;

    l = ConsoleUtils::getChildren(r, QStringList() << f1name, &error);
    ASSERT_TRUE(error.isEmpty());
    ASSERT_EQ(toSet(l), QSet<InfoBase::ptr>() << f1);

    l = ConsoleUtils::getChildren(r, QStringList() << f1name << d1name, &error);
    ASSERT_TRUE(error.isEmpty());
    ASSERT_EQ(toSet(l), QSet<InfoBase::ptr>() << f1 << d1);

    l = ConsoleUtils::getChildren(d1, QStringList() << f1name << f2name, &error);
    ASSERT_FALSE(error.isEmpty());
    ASSERT_EQ(toSet(l), QSet<InfoBase::ptr>() << f2);
}

TEST_F(ConsoleUtilsTest, relPath)
{
    ConsoleUtils::PathWithContent pc = ConsoleUtils::relPath(r, r, "/");
    ASSERT_EQ(pc.first, r);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, d1, "/");
    ASSERT_EQ(pc.first, r);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, r, "d1");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, r, "d1/");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, r, "/d1");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, r, "/d1/");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, d1, "/d1");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, d1, "/d1/");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, d2, "/d1");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, d2, "/d1/");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, r, "d1/d2");
    ASSERT_EQ(pc.first, d2);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, r, "d1/d2/");
    ASSERT_EQ(pc.first, d2);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, r, "/d1/d2");
    ASSERT_EQ(pc.first, d2);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, r, "/d1/d2/");
    ASSERT_EQ(pc.first, d2);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, d1, "d2");
    ASSERT_EQ(pc.first, d2);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, d1, "d2/");
    ASSERT_EQ(pc.first, d2);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, r, ".");
    ASSERT_EQ(pc.first, r);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, r, "./");
    ASSERT_EQ(pc.first, r);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, r, "..");
    ASSERT_EQ(pc.first, nullptr);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, r, "../");
    ASSERT_EQ(pc.first, nullptr);
    ASSERT_EQ(pc.second, false);

    pc = ConsoleUtils::relPath(r, d1, "..");
    ASSERT_EQ(pc.first, r);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, d1, "../");
    ASSERT_EQ(pc.first, r);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, d2, "..");
    ASSERT_EQ(pc.first, d1);
    ASSERT_EQ(pc.second, true);

    pc = ConsoleUtils::relPath(r, d2, "../..");
    ASSERT_EQ(pc.first, r);
    ASSERT_EQ(pc.second, true);
}
