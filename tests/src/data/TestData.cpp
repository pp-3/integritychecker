// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TestData.h"

#include <cmath>

#include <QCoreApplication>
#include <QProcess>

bool TestData::create()
{
    // create temporary directory
    pDir.reset(new QTemporaryDir(QDir::tempPath() + "/integritychecker_test"));
    if (!pDir->isValid())
        return false;

    // unpack test data
    QStringList arguments;
    arguments << "xf";
    arguments << QCoreApplication::applicationDirPath() + "/testsums.tar";
    arguments << "-C";
    arguments << pDir->path();

    const int ret = QProcess::execute("tar", arguments);

    if (ret == 0)
    {
        return true;
    }
    else
    {
        destroy();
        return false;
    }
}

bool TestData::available() const
{
    return pDir && pDir->isValid();
}

void TestData::destroy()
{
    pDir.reset();
}

QString TestData::rootPath() const
{
    QString ret;

    if (pDir)
        ret = pDir->path();

    return ret;
}

bool TestData::existsFile(const QStringList &dirs, const QString &name) const
{
    if (!available())
        return false;

    QDir d(rootPath());

    for (const QString &it : dirs)
    {
        if (!d.cd(it))
            return false;
    }

    return d.exists(name);
}

bool TestData::existsDir(const QStringList &dirs) const
{
    if (!available())
        return false;

    QDir d(rootPath());

    for (const QString &it : dirs)
    {
        if (!d.cd(it))
            return false;
    }

    return d.exists();
}

bool TestData::removeFile(const QStringList &dirs, const QString &file_name)
{
    if (!available())
        return false;

    QDir d(rootPath());

    for (const QString &it : dirs)
    {
        if (!d.cd(it))
            return false;
    }

    if (!d.exists(file_name))
        return false;

    return d.remove(file_name);
}

bool TestData::createFile(const QStringList &dirs, const QString &file_name, const QByteArray &content)
{
    QDir d(rootPath());

    for (const QString &it : dirs)
    {
        if (!d.cd(it))
            return false;
    }

    QFile f(d.filePath(file_name));
    if (!f.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        return false;
    }

    const bool ret = f.write(content) == content.size();
    f.close();

    return ret;
}

bool TestData::createLargeFile(const QStringList &dirs, const QString &file_name)
{
    QDir d(rootPath());

    for (const QString &it : dirs)
    {
        if (!d.cd(it))
            return false;
    }

    QFile f(d.filePath(file_name));
    if (!f.open(QIODevice::WriteOnly | QIODevice::Truncate))
        return false;

    if (!f.seek(pow(10, 9)))
        return false;

    QByteArray content(1, ' ');
    const bool ret = f.write(content) == content.size();
    f.close();

    return ret;
}

bool TestData::setFilePermissions(const QStringList &dirs, const QString &file_name, QFileDevice::Permissions permissions)
{
    if (!available())
        return false;

    QDir d(rootPath());

    for (const QString &it : dirs)
    {
        if (!d.cd(it))
            return false;
    }

    if (!d.exists(file_name))
        return false;

    QFile f(d.absoluteFilePath(file_name));
    return f.setPermissions(permissions);
}

bool TestData::setLastDirPermissions(const QStringList &dirs, QFileDevice::Permissions permissions)
{
    if (dirs.isEmpty())
        return false;

    if (!available())
        return false;

    QDir d(rootPath());

    for (const QString &it : dirs)
    {
        if (!d.cd(it))
            return false;
    }

    const QString path = d.absolutePath();
    QFile f(path);
    return f.setPermissions(permissions);
}

bool TestData::hasAtLeastOneValidChecksum(const QString &name) {
    return name.startsWith("file") || name.startsWith("mixed");
}

bool TestData::hasAtLeastOneErrorChecksum(const QString &name) {
    return name.startsWith("error") || name.startsWith("mixed");
}

bool TestData::hasNoChecksums(const QString &name) {
    return name.startsWith("missing");
}
