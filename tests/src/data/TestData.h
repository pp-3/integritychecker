// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QList>
#include <QScopedPointer>
#include <QString>
#include <QTemporaryDir>

/**
 * The TestData class unpacks the test data into a temporary directory and provides the path to it.
 * Upon destruction all files in that directory are removed.
 *
 * The provided test data contains these directories and files:
   | dir1
   |     /error1
   |     /error2
   |     /file1
   |     /file2
   |     /MD5SUMS
   |     /missing1
   |     /missing2
   |     /SHA1SUMS
   |     /SHA256SUMS
   |     /SHA3_256SUMS
   |     /SHA3_512SUMS
   |     /SHA512SUMS
   | dir2
   |     /dir3
   |          /dir5
   |               /error3
   |               /error4
   |               /file3
   |               /file4
   |               /file5
   |               /MD5SUMS
   |               /missing3
   |               /SHA1SUMS
   |               /SHA256SUMS
   |               /SHA3_256SUMS
   |               /SHA3_512SUMS
   |               /SHA512SUMS
   |          /dir6
   |     /dir4
   |          /dir7
   |               /error5
   |               /file7
   |               /MD5SUMS
   |               /mixed1
   |               /SHA1SUMS
   |               /SHA256SUMS
   |               /SHA3_256SUMS
   |               /SHA3_512SUMS
   |               /SHA512SUMS
   |          /dir8
   |               /file10
   |               /file11
   |               /file12
   |               /file8
   |               /file9
   |               /MD5SUMS
   |               /missing4
   |               /SHA1SUMS
   |               /SHA256SUMS
   |               /SHA3_256SUMS
   |               /SHA3_512SUMS
   |               /SHA512SUMS
   |          /file6
   |          /MD5SUMS
   |          /SHA1SUMS
   |          /SHA256SUMS
   |          /SHA3_256SUMS
   |          /SHA3_512SUMS
   |          /SHA512SUMS
 */
class TestData
{
public:
    /**
     * Creates the temporary directory and unpacks the test data into it.
     * @return True, if successful. False, if providing of the test data failed.
     */
    bool create();
    /**
     * Returns whether the test data is available.
     * @return True, if the test data is available. False, if not.
     */
    bool available() const;
    /**
     * Removes the temporary directory with all files in there.
     */
    void destroy();

    /**
     * Returns the root path to the temporary directory, if available.
     * @return The root path or an empty string if the test data is not available.
     */
    QString rootPath() const;

    /**
     * Returns whether a file exists in a sub directory of the test root path.
     * @param dirs A list of sub directories starting at the root path. Thus an empty list represents the root path.
     * @param name The file name to look for in the given sub directory.
     * @return True, if the file exists. False, if not.
     */
    bool existsFile(const QStringList &dirs, const QString &name) const;

    /**
     * Returns whether a directory hierarchy exists in the test root path.
     * @param dirs A list of sub directories starting at the root path. Thus an empty list represents the root path.
     * @return True, if all sub directories exist. False, if not.
     */
    bool existsDir(const QStringList &dirs) const;

    /**
     * @brief Removes a file from the test data.
     * @param dirs A list of sub directories starting at the root path. Thus an empty list represents the root path.
     * @param file_name The file name to remove in the given sub directory.
     * @return True, if deletion succeeded. False, if not.
     */
    bool removeFile(const QStringList &dirs, const QString &file_name);

    /**
     * @brief Creates a new file in the test data directory.
     * @param dirs A list of sub directories starting at the root path. Thus an empty list represents the root path.
     * @param file_name The file name to create in the given sub directory.
     * @param content The content to write to the file.
     * @return True, if the new file was successfully created and written. False, if not.
     */
    bool createFile(const QStringList &dirs, const QString &file_name, const QByteArray &content);

    /**
     * @brief Creates a new large file which mostly consists out of zeros.
     * @param dirs A list of sub directories starting at the root path. Thus an empty list represents the root path.
     * @param file_name The file name to create in the given sub directory.
     * @return True, if the new file was successfully created and written. False, if not.
     */
    bool createLargeFile(const QStringList &dirs, const QString &file_name);

    /**
     * @brief Sets the permission of the given file in the test data.
     * @param dirs A list of sub directories starting at the root path. Thus an empty list represents the root path.
     * @param file_name The file name to set the permissions in the given sub directory.
     * @param permissions The permissions to set.
     * @return True, if permissions setting succeeded. False, if not.
     */
    bool setFilePermissions(const QStringList &dirs, const QString &file_name, QFileDevice::Permissions permissions);

    /**
     * @brief Sets the permission of the last directory in the test data.
     * @param dirs A list of sub directories starting at the root path. The permissions of the last directory are changed.
     * @param permissions The permissions to set.
     * @return True, if permissions setting succeeded. False, if not.
     */
    bool setLastDirPermissions(const QStringList &dirs, QFileDevice::Permissions permissions);

    /**
     * Returns whether for the file with the given name at least one valid checksum exists in the test data.
     * @param name The file name (without path) to check.
     * @return True, if at least one checksum exists for the file. False, if no checksum data exists.
     */
    static bool hasAtLeastOneValidChecksum(const QString &name);

    /**
     * Returns whether for the file with the given name at least one invalid checksum exists in the test data.
     * @param name The file name (without path) to check.
     * @return True, if at least one invalid checksum exists for the file. False, if no checksum or only valid checksums exist.
     */
    static bool hasAtLeastOneErrorChecksum(const QString &name);

    /**
     * Returns whether for the file with the given name no checksums exist in the test data.
     * @param name The file name (without path) to check.
     * @return True, if no checksum data exists for the file. False, if any checksum exists.
     */
    static bool hasNoChecksums(const QString &name);

    static constexpr const char *FilesWithErrorsPrefix = "error";
    static constexpr const char *FilesWithCheckSumsPrefix = "file";
    static constexpr const char *FilesWithMissingCheckSumsPrefix = "missing";
    static constexpr const char *FilesWithMixedPrefix = "mixed";

    static QStringList getFileNamesWithOnlyErrors()
    {
        QStringList ret;

        for (int i=1 ; i<=5 ; ++i)
            ret << QString(FilesWithErrorsPrefix) + QString::number(i);

        return ret;
    }

    static QStringList getFileNamesWithOnlyCheckSums()
    {
        QStringList ret;

        for (int i=1 ; i<=7 ; ++i)
            ret << QString(FilesWithCheckSumsPrefix) + QString::number(i);

        return ret;
    }

    static QStringList getFileNamesWithSingleCheckSum()
    {
        QStringList ret;

        for (int i=8 ; i<=13 ; ++i)
            ret << QString(FilesWithCheckSumsPrefix) + QString::number(i);

        return ret;
    }

    static QStringList getFileNamesWithMixedCheckSumsAndErrors()
    {
        return { QString(FilesWithMixedPrefix) + QString::number(1) };
    }

    static QStringList getFileNamesWithMissingCheckSums()
    {
      QStringList ret;

      for (int i=1 ; i<=4 ; ++i)
          ret << QString(FilesWithMissingCheckSumsPrefix) + QString::number(i);

      return ret;
    }

    static QStringList getDirNamesWithMixedCheckSumsAndErrors()
    {
        return {"dir1", "dir5", "dir7"};
    }

    static QStringList getDirNamesWithoutErrors()
    {
        return {"dir8"};
    }

    static QStringList getEmptyDirs()
    {
        return {"dir6"};
    }

private:
    QScopedPointer<QTemporaryDir> pDir;
};
