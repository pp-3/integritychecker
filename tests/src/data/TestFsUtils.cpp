// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TestFsUtils.h"

#include <cassert>

InfoBase *TestFsUtils::findChild(DirInfo *dir, const QString &name, bool recursive)
{
    const QList<InfoBase::ptr> kidz = dir->getChildren();

    for (const InfoBase::ptr &it : kidz)
    {
        if (it->getName() == name)
            return it.get();

        if (recursive && it->isDir())
        {
            DirInfo::ptr di = DirInfo::cast(it);
            InfoBase *ib = findChild(di.get(), name, recursive);

            if (ib)
                return ib;
        }
    }

    return nullptr;
}

QSet<FileInfo *> TestFsUtils::getAllFiles(DirInfo *dir, bool recursive)
{
    QSet<FileInfo *> ret;

    const QList<InfoBase::ptr> kidz = dir->getChildren();

    for (const InfoBase::ptr &it : kidz)
    {
        if (!it->isDir())
        {
            FileInfo::ptr fi = FileInfo::cast(it);
            assert(fi);

            ret << fi.data();
        }
        else
        {
            DirInfo::ptr di = DirInfo::cast(it);
            assert(di);

            if (recursive)
                ret += getAllFiles(di.data(), recursive);
        }
    }

    return ret;
}

DirInfo *TestFsUtils::findDir(QStringList dir_paths, DirInfo *start_search_dir)
{
    assert(!dir_paths.isEmpty());
    assert(start_search_dir != nullptr);

    const QString path = dir_paths.takeFirst();
    InfoBase *ib = findChild(start_search_dir, path, false);
    if (ib == nullptr)
        return nullptr;

    if (!ib->isDir())
        return nullptr;

    DirInfo *di =  DirInfo::cast(ib);

    if (dir_paths.isEmpty())
        return di;

    return findDir(dir_paths, di);
}
