// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QSet>
#include <QString>
#include <QStringList>

#include "sum_tree/DirInfo.h"
#include "sum_tree/FileInfo.h"

/**
 * Utility functions for traversing the data tree.
 */
class TestFsUtils
{
public:
    /**
     * Searches for a file / directory in a / starting from given directory.
     * @param dir The directory to search in.
     * @param name The file / directory name to search for.
     * @param recursive If true, also all sub folder will be searched for the given name.
     * @return The instance pointing to the searched instance or nullptr, if not found.
     */
    static InfoBase *findChild(DirInfo *dir, const QString &name, bool recursive);

    /**
     * Returns all files (optional recursivly) starting at the given directory.
     * @dir The directory to start the search at.
     * @return A set containing all found files in the data tree.
     */
    static QSet<FileInfo *> getAllFiles(DirInfo *dir, bool recursive);

    /**
     * Searches for a directory starting at a given directory traversing a list of names.
     * @param dir_paths The list of directory names to traverse to.
     * @param start_search_dir The directory to start the search at.
     * @return The instance pointing to the last name in dir_paths. Or nullptr, if a path from dir_paths wasn't found.
     */
    static DirInfo *findDir(QStringList dir_paths, DirInfo *start_search_dir);
};
