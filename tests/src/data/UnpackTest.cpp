// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "TestData.h"

class UnpackTest : public ::testing::Test
{
protected:
    TestData test_data;

    void SetUp() override
    {
        const bool b = test_data.create();
        ASSERT_TRUE(b) << "Test data unpacking failed";
    }

    void TearDown() override
    {
        test_data.destroy();
    }
};


TEST_F(UnpackTest, SetUp)
{
    ASSERT_TRUE(test_data.available());
}

TEST_F(UnpackTest, TestFile)
{
    QStringList dirs;
    dirs << "dir1";

    ASSERT_TRUE(test_data.existsFile(dirs, "file1"));
}

TEST_F(UnpackTest, SumFile)
{
    QStringList dirs;
    dirs << "dir2" << "dir4" << "dir8";

    ASSERT_TRUE(test_data.existsFile(dirs, "SHA256SUMS"));
}

TEST_F(UnpackTest, EmptyDir)
{
    QStringList dirs;
    dirs << "dir2" << "dir3" << "dir6";

    ASSERT_TRUE(test_data.existsDir(dirs));
}
