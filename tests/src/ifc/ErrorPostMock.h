// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ERRORPOSTMOCK_H
#define ERRORPOSTMOCK_H

#include <gmock/gmock.h>

#include "ifc/ErrorPostIfc.h"

class ErrorPostMock : public ErrorPostIfc
{
public:
    ErrorPostMock()
    {
        ON_CALL(*this, addError)
            .WillByDefault(
                [](const QString &err, const QString &module_name)
                {
                    std::cout << "Error log: " << err.toStdString() << ", in module: " << module_name.toStdString() << std::endl;
                });
        ON_CALL(*this, addWarning)
            .WillByDefault(
                [](const QString &warning, const QString &module_name)
                {
                    std::cout << "Warning log: " << warning.toStdString() << ", in module: " << module_name.toStdString() << std::endl;
                });
    }

    MOCK_METHOD(void, addError, (const QString &err, const QString &module_name));
    MOCK_METHOD(void, addWarning, (const QString &err, const QString &module_name));
};

#endif // ERRORPOSTMOCK_H
