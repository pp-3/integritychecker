// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef WORKERSETTINGSMOCK_H
#define WORKERSETTINGSMOCK_H

#include <gmock/gmock.h>

#include "ifc/WorkerSettingsIfc.h"

/**
 * @brief The WorkerSettingsMock class mocks the getter methods from WorkerSettingsIfc, but doesn't send out any
 * signal.
 */
class WorkerSettingsMock : public WorkerSettingsIfc
{
public:
    MOCK_METHOD(WorkerSettingsIfc::OUTPUT_SELECTION, getOutputDirSelection, (), (const, override));
    MOCK_METHOD(QString, getInputDir, (), (const, override));
    MOCK_METHOD(QString, getOutputDir, (), (const, override));
    MOCK_METHOD(bool, isInputDirValid, (), (const, override));
    MOCK_METHOD(bool, isOutputDirValid, (), (const, override));
    MOCK_METHOD(bool, areDirsValid, (), (const, override));
    MOCK_METHOD(bool, createOutputDir, (), (override));
    unsigned int getWorkerThreads() const override { return 4; }
    MOCK_METHOD((QPair<unsigned int, unsigned int>), getMinMaxWorkerThreads, (), (const, override));
    MOCK_METHOD(size_t, getCacheSize, (), (const, override));
    MOCK_METHOD(SumTypeHashList, getEnabledCalcHashes, (), (const, override));
};

#endif // WORKERSETTINGSMOCK_H
