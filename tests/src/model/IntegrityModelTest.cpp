// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "IntegrityModelTest.h"

#include "checksums/SumTypeManager.h"
#include "shared/ExitManager.h"
#include "tasks/TaskExecutionMock.h"
#include "thread_util/ResultWait.h"
#include "worker/StaticInitializer.h"

void IntegrityModelTest::SetUp()
{
    const bool b = test_data.create();
    if (!b)
        GTEST_SKIP() << "Test data unpacking failed";

    // test data direct access helpers
    test_data_root_path = test_data.rootPath();
    all_sum_hashes = SumTypeManager::getInstance().getRegisteredHashes();
    for (const SumTypeHash it : std::as_const(all_sum_hashes))
        all_sum_names << SumTypeManager::getInstance().fromHash(it)->getName();
    all_sum_names.sort();

    // setup default configuration
    EXPECT_CALL(parameter_parser, getProfile).WillRepeatedly(testing::Return(QString()));
    user_settings_manager.SetUp();
    EXPECT_CALL(*user_settings_manager.profile_settings, getCacheSize).WillRepeatedly(testing::Return(0));
    EXPECT_CALL(*user_settings_manager.profile_settings, getLanguage).WillRepeatedly(testing::Return(QString()));
    EXPECT_CALL(*user_settings_manager.profile_settings, setLanguage).WillRepeatedly(testing::Return());
    EXPECT_CALL(*user_settings_manager.profile_settings, getInputDir).WillRepeatedly(testing::Return(test_data_root_path));
    EXPECT_CALL(*user_settings_manager.profile_settings, setInputDir(test_data_root_path)).WillOnce(testing::Return());
    EXPECT_CALL(*user_settings_manager.profile_settings, getOutputDir).WillRepeatedly(testing::Return(test_data_root_path));
    EXPECT_CALL(*user_settings_manager.profile_settings, setOutputDir(test_data_root_path)).WillOnce(testing::Return());
    EXPECT_CALL(*user_settings_manager.profile_settings, getOutputDirSelection).WillRepeatedly(testing::Return(QString()));
    EXPECT_CALL(*user_settings_manager.profile_settings, getWorkerThreads).WillRepeatedly(testing::Return(1));
    EXPECT_CALL(*user_settings_manager.profile_settings, setWorkerThreads(1)).WillRepeatedly(testing::Return());
    EXPECT_CALL(*user_settings_manager.profile_settings, getEnabledTypes).WillRepeatedly(testing::Return(all_sum_names));
    EXPECT_CALL(*user_settings_manager.profile_settings, setEnabledTypes(::testing::_))
        .WillRepeatedly(
            [this](const QStringList &types)
            {
                for (const QString &type : types)
                {
                    EXPECT_TRUE(all_sum_names.contains(type, Qt::CaseInsensitive));
                }
            });

    // model setup
    language_settings = std::make_unique<LanguageSettings>(&user_settings_manager);
    model = std::make_unique<IntegrityModel>(&parameter_parser, &user_settings_manager, language_settings.get());

    // error check
    EXPECT_CALL(errors, addError).Times(0); // no errors expected

    // move model to separate event loop and wait for its initial scan
    ResultWait<bool> root_scan_barrier(false);
    QList<QObject *> moveToEventLoopThread;
    moveToEventLoopThread << model.get();

    temporary_event_loop = TemporaryEventLoop::create(moveToEventLoopThread);
    temporary_main_thread = setMainThread(temporary_event_loop.get());

    // callback for set up root + check sum scan
    const auto root_connection = QObject::connect(
        model.get(),
        &IntegrityModel::dirRootChanged,
        getMainThreadAffinity(),
        [this](const DirInfoRoot::ptr &root) {
            this->root = root;
        },
        Qt::DirectConnection);
    ASSERT_TRUE(root_connection);

    // callback for finished check sum search
    const auto scan_connection = QObject::connect(
        model.get(),
        &IntegrityModel::checksumSearchFinished,
        getMainThreadAffinity(),
        [&root_scan_barrier](const DirInfoRoot::ptr &/*root*/, const InfoBase::weak_ptr &/*node*/) {
            root_scan_barrier.setResult(true);
        },
        Qt::DirectConnection);
    ASSERT_TRUE(scan_connection);

    // set up model
    QTimer::singleShot(0,  // initModel needs to run in separate thread, as it relies on a running event loop
                       temporary_event_loop.get(),
                       [this]() {
                           model->initModel();
                       });

    // wait for initial scan
    const bool scanned = root_scan_barrier.wait(20000);
    temporary_event_loop->waitForEvents();

    QObject::disconnect(scan_connection);
    QObject::disconnect(root_connection);
    ASSERT_TRUE(scanned) << "Initial scan timed out. Further testing is not possible";

    // inject mocks
    DataCache &cache = model->getDataCache();
    StaticInitializer::getInstance().populateSettings(model->getWorkerSettings(), reinterpret_cast<DefaultDataCacheType *>(&cache), &errors);
}

void IntegrityModelTest::TearDown()
{
    ResultWait<bool> barrier(false);
    QTimer::singleShot(0,  // closeModel needs to run in separate thread, as it relies on a running event loop
                       temporary_event_loop.get(),
                       [this, &barrier]() {
                           model->closeModel();
                           barrier.setResult(true);
                       });

    EXPECT_TRUE(barrier.wait(2000));
    temporary_main_thread.reset();
    temporary_event_loop.reset();
    model.reset();
    language_settings.reset();
    user_settings_manager.TearDown();
    test_data.destroy();

    // delete all static data, as multiple tests create individual IntegrityModel instances
    const QList<SumTypeIfc *> all_sum_types = SumTypeManager::getInstance().getRegisteredTypes();
    ExitManager::tearDown();
    // Re-register check sum types as they are also cleaned up. Next tests will them.
    for (SumTypeIfc *type : all_sum_types)
    {
        SumTypeManager::getInstance().addSumType(type);
    }
}

void IntegrityModelTest::waitForFinishedFsScan(const TestAction &action)
{
    ResultWait<bool> barrier(false);

    const auto connection = QObject::connect(
        model.get(),
        &IntegrityModel::dirContentScanFinished,
        getMainThreadAffinity(),
        [&barrier](const DirInfoRoot::ptr &/*root*/, const InfoBase::weak_ptr &/*node*/) {
            barrier.setResult(true);
        },
        Qt::DirectConnection);
    ASSERT_TRUE(connection);

    action();

    const bool result_set = barrier.wait(30000);
    QObject::disconnect(connection);
    ASSERT_TRUE(result_set);
}

void IntegrityModelTest::waitForFinishedCheckSumScan(const TestAction &action)
{
    ResultWait<bool> barrier(false);

    const auto connection = QObject::connect(
        model.get(),
        &IntegrityModel::checksumSearchFinished,
        getMainThreadAffinity(),
        [&barrier](const DirInfoRoot::ptr &/*root*/, const InfoBase::weak_ptr &/*node*/) {
            barrier.setResult(true);
        },
        Qt::DirectConnection);
    ASSERT_TRUE(connection);

    action();

    const bool result_set = barrier.wait(20000);
    QObject::disconnect(connection);
    ASSERT_TRUE(result_set);
}

void IntegrityModelTest::waitForFinishedCheckSumVerification(const TestAction &action)
{
    ResultWait<bool> barrier(false);

    const auto connection = QObject::connect(
        model.get(),
        &IntegrityModel::comparisonFinished,
        getMainThreadAffinity(),
        [&barrier](const DirInfoRoot::ptr &/*root*/, const InfoBase::weak_ptr &/*node*/) {
            barrier.setResult(true);
        },
        Qt::DirectConnection);
    ASSERT_TRUE(connection);

    action();

    const bool result_set = barrier.wait(30000);
    QObject::disconnect(connection);
    ASSERT_TRUE(result_set);
}

void IntegrityModelTest::waitForFinishedCheckSumCreation(const TestAction &action)
{
    ResultWait<bool> barrier(false);

    const auto connection = QObject::connect(
        model.get(),
        &IntegrityModel::calculationFinished,
        getMainThreadAffinity(),
        [&barrier](const DirInfoRoot::ptr &/*root*/, const InfoBase::weak_ptr &/*node*/) {
            barrier.setResult(true);
        },
        Qt::DirectConnection);
    ASSERT_TRUE(connection);

    action();

    const bool result_set = barrier.wait(40000);
    QObject::disconnect(connection);
    ASSERT_TRUE(result_set);
}

TEST_F(IntegrityModelTest, InitialModelState)
{
    ASSERT_NE(model->getCurrentRoot().get(), nullptr);
    ASSERT_EQ(model->getCurrentRoot().get(), root);
}
