// Copyright 2024 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INTEGRITYMODELTEST_H
#define INTEGRITYMODELTEST_H

#include <gtest/gtest.h>

#include "thread_util/threadcheck.h"

#include "data/TestData.h"
#include "ifc/ErrorPostMock.h"
#include "model/IntegrityModel.h"
#include "shared/ApplicationParameterParserIfcMock.h"
#include "shared/LanguageSettings.h"
#include "shared/UserSettingsManagerMock.h"
#include "qt/TemporaryEventLoop.h"

class IntegrityModelTest : public ::testing::Test
{
protected:
    ErrorPostMock errors;
    TestData test_data;
    QString test_data_root_path;
    SumTypeHashList all_sum_hashes;
    QStringList all_sum_names;
    UserSettingsManagerMock user_settings_manager;
    ApplicationParameterParserIfcMock parameter_parser;
    std::unique_ptr<IntegrityModel> model;
    std::unique_ptr<LanguageSettings> language_settings;
    std::unique_ptr<TemporaryEventLoop> temporary_event_loop;
    MainThreadResetter temporary_main_thread;
    DirInfoRoot::ptr root;  // initial root

    void SetUp() override;
    void TearDown() override;

    using TestAction = std::function<void()>;

    /**
     * @brief Sets up the listener for a finished file system change scan. Then calls back the test action before
     * waiting for the scan to finish. Fails the test, if the scan doesn't finish.
     * @param action The test action to call before waiting for the FS scan.
     */
    void waitForFinishedFsScan(const TestAction &action);
    /**
     * @brief Sets up the listener for a finished check sum scan. Then calls back the test action before waiting for the
     * check sum to finish. Fails the test, if the scan doesn't finish.
     * @param action The test action to call before waiting for the check sum scan.
     */
    void waitForFinishedCheckSumScan(const TestAction &action);
    /**
     * @brief Sets up the listener for a finished check sum verification. Then calls back the test action before waiting
     * for the verification to finish. Fails the test, if the verification doesn't finish.
     * @param action The test action to call before waiting for the verification scan.
     */
    void waitForFinishedCheckSumVerification(const TestAction &action);
    /**
     * @brief Sets up the listener for a finished check sum creation. Then calls back the test action before waiting
     * for the creation to finish. Fails the test, if the creation doesn't finish.
     * @param action The test action to call before waiting for the creation scan.
     */
    void waitForFinishedCheckSumCreation(const TestAction &action);
};

#endif // INTEGRITYMODELTEST_H
