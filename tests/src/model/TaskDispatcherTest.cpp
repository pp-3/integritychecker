// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <QCoreApplication>

#include "model/TaskDispatcher.h"
#include "tasks/ThreadPoolExecution.h"
#include "thread_util/threadcheck.h"

#include "ifc/WorkerSettingsMock.h"
#include "qt/TemporaryEventLoop.h"
#include "tasks/SleepingTasks.h"

class TaskDispatcherTest : public ::testing::Test
{
protected:
    WorkerSettingsMock worker_settings;
    std::unique_ptr<TaskDispatcher> task_dispatcher;
    MainThreadResetter keep_main_thread;
    ::testing::MockFunction<void(const TaskLifeCycle::ptr &task, const QString &task_name)> task_added_function;

    void SetUp() override
    {
        keep_main_thread = setMainThread(QCoreApplication::instance());  // keep QCoreApplication main thread, as no running event loop is needed for these tests
        task_dispatcher.reset(new TaskDispatcher(&worker_settings, std::make_unique<ThreadPoolExecution>()));
        QObject::connect(task_dispatcher.get(), &TaskDispatcher::taskAdded,
                         getMainThreadAffinity(), task_added_function.AsStdFunction());
    }

    void TearDown() override
    {
        task_dispatcher->shutdown();
        task_dispatcher.reset();
        keep_main_thread.reset();
    }

    bool waitForTaskRunning(const TaskLifeCycle::ptr &tlc)
    {
        for (size_t i = 0 ; i<20 ; ++i)
        {
            QThread::usleep(10000);
            if (tlc->isRunning())
                return true;
        }

        return false;
    }

    void waitForTaskFinished(const TaskLifeCycle::ptr &tlc, bool *canceled)
    {
        QList<QObject *> moveToEventLoopThread;
        moveToEventLoopThread << tlc.get();

        std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(moveToEventLoopThread);
        tlc->waitForTaskFinished(canceled);
    }
};

TEST_F(TaskDispatcherTest, ExecuteTask)
{
    QSharedPointer<TaskIfc> task = QSharedPointer<SleepingSingleTask>::create();
    EXPECT_CALL(task_added_function, Call(::testing::_, ::testing::_)).WillOnce(::testing::Return());
    TaskLifeCycle::ptr tlc = task_dispatcher->addTask(task);

    ASSERT_TRUE(waitForTaskRunning(tlc));
    bool canceled = true;
    waitForTaskFinished(tlc, &canceled);
    EXPECT_FALSE(canceled);

    canceled = true;
    EXPECT_TRUE(tlc->isFinishedOrCanceled(&canceled));
    EXPECT_FALSE(canceled);
}

TEST_F(TaskDispatcherTest, CancelTask)
{
    QSharedPointer<TaskIfc> task = QSharedPointer<SleepingSingleTask>::create();
    EXPECT_CALL(task_added_function, Call(::testing::_, ::testing::_)).WillOnce(::testing::Return());
    TaskLifeCycle::ptr tlc = task_dispatcher->addTask(task);

    ASSERT_TRUE(waitForTaskRunning(tlc));
    task->cancel();
    bool canceled = false;
    waitForTaskFinished(tlc, &canceled);
    EXPECT_TRUE(canceled);

    canceled = false;
    EXPECT_TRUE(tlc->isFinishedOrCanceled(&canceled));
    EXPECT_TRUE(canceled);
}

TEST_F(TaskDispatcherTest, CancelLifeCycle)
{
    QSharedPointer<TaskIfc> task = QSharedPointer<SleepingSingleTask>::create();
    EXPECT_CALL(task_added_function, Call(::testing::_, ::testing::_)).WillOnce(::testing::Return());
    TaskLifeCycle::ptr tlc = task_dispatcher->addTask(task);

    ASSERT_TRUE(waitForTaskRunning(tlc));
    tlc->cancelTask();
    bool canceled = false;
    waitForTaskFinished(tlc, &canceled);
    EXPECT_TRUE(canceled);

    canceled = false;
    EXPECT_TRUE(tlc->isFinishedOrCanceled(&canceled));
    EXPECT_TRUE(canceled);
}

TEST_F(TaskDispatcherTest, ExecuteTaskWithResult)
{
    QSharedPointer<TaskWithResult<bool>> task = QSharedPointer<SleepingSingleTaskWithResult>::create();
    EXPECT_CALL(task_added_function, Call(::testing::_, ::testing::_)).WillOnce(::testing::Return());
    TaskLifeCycle::ptr tlc = task_dispatcher->addTask(task);

    ASSERT_TRUE(waitForTaskRunning(tlc));
    bool canceled = true;
    waitForTaskFinished(tlc, &canceled);
    EXPECT_FALSE(canceled);

    TaskWithResult<bool>::ResultType result = task->getTaskResult();
    ASSERT_TRUE(result);

    EXPECT_TRUE(result->hasResult());
    EXPECT_TRUE(result->getResult());
}

TEST_F(TaskDispatcherTest, ExecuteParentTask)
{
    constexpr size_t CHILDREN = 4;

    QSharedPointer<ParentTaskIfc<bool>> task = QSharedPointer<SleepingParentTask>::create(CHILDREN, false);
    EXPECT_EQ(task->getTaskCount(), 1); // parent only

    EXPECT_CALL(task_added_function, Call(::testing::_, ::testing::_)).WillOnce(::testing::Return());
    TaskLifeCycle::ptr tlc = task_dispatcher->addTask(task);

    ASSERT_TRUE(waitForTaskRunning(tlc));
    EXPECT_TRUE(task->hasSubTasks());
    EXPECT_EQ(task->getTaskCount(), CHILDREN + 1); // parent + children

    bool canceled = true;
    waitForTaskFinished(tlc, &canceled);
    EXPECT_FALSE(canceled);

    ParentTaskIfc<bool>::ResultType results = task->getTaskResult();
    ASSERT_TRUE(results);

    EXPECT_TRUE(results->hasResult());
    QList<TaskResult<bool>::ptr> result_list = results->getResult();
    EXPECT_EQ(result_list.size(), CHILDREN);

    EXPECT_TRUE(std::all_of(result_list.constBegin(),
                            result_list.constEnd(),
                            [](const TaskResult<bool>::ptr &p) -> bool {
                                return p && p->getResult();
                            }));
}

TEST_F(TaskDispatcherTest, ExecuteCancelOnlyTasks)
{
    constexpr size_t CHILDREN = 4;

    QSharedPointer<ParentTaskIfc<bool>> task = QSharedPointer<SleepingParentTask>::create(CHILDREN, true);
    EXPECT_CALL(task_added_function, Call(::testing::_, ::testing::_)).WillOnce(::testing::Return());
    TaskLifeCycle::ptr tlc = task_dispatcher->addTask(task);

    ASSERT_TRUE(waitForTaskRunning(tlc));
    task->cancel();

    bool canceled = false;
    waitForTaskFinished(tlc, &canceled);
    EXPECT_TRUE(canceled);

    ParentTaskIfc<bool>::ResultType results = task->getTaskResult();
    ASSERT_TRUE(results);

    EXPECT_TRUE(results->hasResult());
    QList<TaskResult<bool>::ptr> result_list = results->getResult();
    EXPECT_EQ(result_list.size(), CHILDREN);

    EXPECT_TRUE(std::all_of(result_list.constBegin(),
                            result_list.constEnd(),
                            [](const TaskResult<bool>::ptr &p) -> bool {
                                return p && p->getResult();
                            }));
}
