// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "SignalsMock.h"
#include "TemporaryEventLoop.h"

/*
 * Verify Qt signals are working together with googletest
 */

class QtSignalsTest : public ::testing::Test
{
protected:
    SignalsMock receiver;
    SignalsMockEmitter emitter;
    QMetaObject::Connection connection;

    void SetUp() override
    {
        connection = QObject::connect(&emitter, &SignalsMockEmitter::testSignal, &receiver, &SignalsMock::callback);
    }
};

TEST_F(QtSignalsTest, DirectConnection)
{
    EXPECT_TRUE(connection);

    EXPECT_CALL(receiver, callback()).Times(1);

    emitter.sendSignal();
}

TEST_F(QtSignalsTest, QueueConnection)
{
    // disconnect exiting connection
    EXPECT_TRUE(connection);
    EXPECT_TRUE(QObject::disconnect(connection));
    EXPECT_FALSE(connection);

    // create queued connection
    connection = QObject::connect(&emitter, &SignalsMockEmitter::testSignal, &receiver, &SignalsMock::callback, Qt::QueuedConnection);
    EXPECT_TRUE(connection);

    // don't expect queued connection to work with googletest
    EXPECT_CALL(receiver, callback()).Times(0);

    emitter.sendSignal();
}

TEST_F(QtSignalsTest, TemporaryEventLoop)
{
    EXPECT_TRUE(QObject::disconnect(connection));

    QList<QObject *> moveToEventLoopThread;
    moveToEventLoopThread << &receiver;

    std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(moveToEventLoopThread);

    // create queued connection
    connection = QObject::connect(&emitter, &SignalsMockEmitter::testSignal, &receiver, &SignalsMock::callback, Qt::QueuedConnection);
    EXPECT_TRUE(connection);

    // callback being called from TemporaryEventLoop
    EXPECT_CALL(receiver, callback()).Times(1);
    emitter.sendSignal();
    tel->waitForEvents();
}
