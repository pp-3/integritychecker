// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gmock/gmock.h>

#include <QObject>

class SignalsMockCallbackIfc
{
public:
    virtual ~SignalsMockCallbackIfc() = default;

    virtual void callback() = 0;
};

class SignalsMock : public QObject,
                    public SignalsMockCallbackIfc
{
    Q_OBJECT

public:
    MOCK_METHOD(void, callback, (), (override));
};

class SignalsMockEmitter : public QObject
{
    Q_OBJECT

public:
    void sendSignal()
    {
        emit testSignal();
    }

signals:
    void testSignal();
};
