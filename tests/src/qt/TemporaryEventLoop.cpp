// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TemporaryEventLoop.h"

#include <QTimer>

#include "thread_util/threadcheck.h"

TemporaryEventLoop::TemporaryEventLoop(const QList<QObject *> &temporaryObjects) :
    m_TemporaryObjects(temporaryObjects),
    pCallersThreadContext(nullptr),
    m_pThread(new QThread),
    m_ThreadStartedCondition(false),
    m_ThreadFinishedCondition(false)
{
    pCallersThreadContext = QThread::currentThread();

    m_pThread->setObjectName("TemporaryEventLoop");
    moveToThread(m_pThread);

    for (QObject *o : std::as_const(m_TemporaryObjects))
    {
        o->moveToThread(m_pThread);
    }

    connect(m_pThread, &QThread::started,  this, &TemporaryEventLoop::threadInit);
    connect(m_pThread, &QThread::finished, this, &TemporaryEventLoop::threadClose);
    connect(m_pThread, &QThread::finished, m_pThread, &QThread::deleteLater);
}

TemporaryEventLoop::~TemporaryEventLoop()
{
    shutdown();
}

std::unique_ptr<TemporaryEventLoop> TemporaryEventLoop::create(const QList<QObject *> &temporaryObjects)
{
    std::unique_ptr<TemporaryEventLoop> tel(new TemporaryEventLoop(temporaryObjects));
    tel->m_pThread->start();
    tel->m_ThreadStartedCondition.wait();

    return tel;
}

void TemporaryEventLoop::waitForEvents(size_t additional_delay_in_ms)
{
    Q_ASSERT(!isInThread(this));  // additional thread needs to run for processing events

    ResultWait<bool> waitForEventLoop(false);

    QTimer::singleShot(additional_delay_in_ms, this, [&waitForEventLoop]() {
        waitForEventLoop.setResult(true);
    });

    waitForEventLoop.wait();
}

void TemporaryEventLoop::shutdown()
{
    if (m_ThreadFinishedCondition.hasResult())
        return;  // additional thread already finished

    // process all events in the queue
    waitForEvents();
    m_pThread->quit();

    // wait for additional thread to finish
    m_ThreadFinishedCondition.wait();
    m_pThread = nullptr;
}

void TemporaryEventLoop::threadInit()
{
    m_ThreadStartedCondition.setResult(true);
}

void TemporaryEventLoop::threadClose()
{
    for (QObject *o : std::as_const(m_TemporaryObjects))
    {
        o->moveToThread(pCallersThreadContext);
    }

    moveToThread(pCallersThreadContext);

    m_ThreadFinishedCondition.setResult(true);
}
