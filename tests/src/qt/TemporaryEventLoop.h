// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TEMPORARYEVENTLOOP_H
#define TEMPORARYEVENTLOOP_H

#include <memory>

#include <QList>
#include <QObject>
#include <QThread>

#include "thread_util/ResultWait.h"

/**
 * @brief The TemporaryEventLoop class attempts to mitigate the limitation of the main event loop being blocked in
 * the tests. For test cases depending on a running event loop (e.g. for queued connections) TemporaryEventLoop provides
 * one way to circumvent the blocked event loop.
 * Creating a TemporaryEventLoop starts a new thread and moves a provided list of object's thread context to that new
 * thread. All incoming events (e.g. connected signals) are then processed in that thread's event loop. As the
 * QApplication's main event loop is blocked by google test it shouldn't create new multithreading issues.
 * Calling shutdown() clears the event queue, shuts down the additional thread, and moves all temporarly moved objects
 * back to the original caller's thread context.
 */
class TemporaryEventLoop : public QObject
{
    Q_OBJECT

    TemporaryEventLoop(const QList<QObject *> &temporaryObjects);

public:
    ~TemporaryEventLoop() override;

    /**
     * @brief Creates a new TemporaryEventLoop instance. Before the instance is returned it is ensured that the new
     * thread is running and incoming events are processed.
     * @param temporaryObjects The objects to move temporarily to the background thread's context.
     * @return The instance managing the background thread. Releasing it will stop the background thread and move back
     *         all temporary objects to the caller's thread context.
     */
    static std::unique_ptr<TemporaryEventLoop> create(const QList<QObject *> &temporaryObjects);

    /**
     * @brief Ensures all events in the thread's event loop are processed before this method returns.
     * @param additional_delay_in_ms An additional delay (in ms) to wait before looking the the thread's event queue.
     */
    void waitForEvents(size_t additional_delay_in_ms = 0);

    /**
     * @brief Shuts the additional thread down and moves all temporary objects back to the original caller's thread
     * context.
     */
    void shutdown();

private slots:
    void threadInit();
    void threadClose();

private:
    QList<QObject *> m_TemporaryObjects;
    QThread         *pCallersThreadContext;
    QThread         *m_pThread;
    ResultWait<bool> m_ThreadStartedCondition;
    ResultWait<bool> m_ThreadFinishedCondition;
};

#endif // TEMPORARYEVENTLOOP_H
