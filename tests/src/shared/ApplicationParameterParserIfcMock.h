// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef APPLICATIONPARAMETERPARSERIFCMOCK_H
#define APPLICATIONPARAMETERPARSERIFCMOCK_H

#include <gmock/gmock.h>

#include "ifc/ApplicationParameterParserIfc.h"

class ApplicationParameterParserIfcMock : public ApplicationParameterParserIfc
{
public:
    MOCK_METHOD(bool, showGui, (), (const));
    MOCK_METHOD(QString, getProfile, (), (const));
    MOCK_METHOD(bool, isLangSet, (), (const));
    MOCK_METHOD(bool, isInteractive, (), (const));
};

#endif // APPLICATIONPARAMETERPARSERIFCMOCK_H
