// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DATACACHEMOCK_H
#define DATACACHEMOCK_H

#include <gmock/gmock.h>

#include "shared/DefaultDataCacheType.h"

class DataCacheMock : public DefaultDataCacheType
{
public:
    MOCK_METHOD(void, addData, (const DataCacheMock::key_type &key, const DataCacheMock::value_ptr &data), (override));
    MOCK_METHOD(DataCacheMock::value_ptr, replaceData, (const DataCacheMock::key_type &key, const DataCacheMock::value_ptr &data), (override));
    MOCK_METHOD(bool, hasData, (const DataCacheMock::key_type &key), (const, override));
    MOCK_METHOD(DataCacheMock::const_value_ptr, getData, (const DataCacheMock::key_type &key), (const, override));
    MOCK_METHOD(DataCacheMock::value_ptr, removeData, (const DataCacheMock::key_type &key), (override));
    MOCK_METHOD(bool, clearData, (const DataCacheMock::key_type &key), (override));
    MOCK_METHOD(size_t, getUsedBytes, (), (const, override));
};

#endif // DATACACHEMOCK_H
