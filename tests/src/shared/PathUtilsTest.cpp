// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "shared/PathUtils.h"
#include "utils/test_utils.h"

TEST(PathUtilsTest, PathTestSingle) {
    QStringList in;
    in << "a";

    const auto out = PathUtils::combinePaths(in);

    EXPECT_EQ(out, in) << out;
}

TEST(PathUtilsTest, PathTestNoSpace) {
    QStringList in;
    in << "a";
    in << "b";

    const auto out = PathUtils::combinePaths(in);

    EXPECT_EQ(out, in) << out;
}

TEST(PathUtilsTest, PathTest1Space) {
    QStringList in;
    in << "'a";
    in << "b'";
    in << "c";

    const auto out = PathUtils::combinePaths(in);

    QStringList exp;
    exp << "a b";
    exp << "c";

    EXPECT_EQ(out, exp) << out;
}

TEST(PathUtilsTest, PathTest1Space2) {
    QStringList in;
    in << "\"a";
    in << "b\"";
    in << "c";

    const auto out = PathUtils::combinePaths(in);

    QStringList exp;
    exp << "a b";
    exp << "c";

    EXPECT_EQ(out, exp) << out;
}

TEST(PathUtilsTest, PathTest2Space) {
    QStringList in;
    in << "'a";
    in << "b";
    in << "c'";
    in << "d";

    const auto out = PathUtils::combinePaths(in);

    QStringList exp;
    exp << "a b c";
    exp << "d";

    EXPECT_EQ(out, exp) << out;
}

TEST(PathUtilsTest, PathTest0Space) {
    QStringList in;
    in << "'a b'";
    in << "c";

    const auto out = PathUtils::combinePaths(in);

    QStringList exp;
    exp << "a b";
    exp << "c";

    EXPECT_EQ(out, exp) << out;
}

TEST(PathUtilsTest, PathTest2Param) {
    QStringList in;
    in << "'a";
    in << "b'";
    in << "'c";
    in << "d'";

    const auto out = PathUtils::combinePaths(in);

    QStringList exp;
    exp << "a b";
    exp << "c d";

    EXPECT_EQ(out, exp) << out;
}
