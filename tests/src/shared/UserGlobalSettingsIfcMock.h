// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef USERGLOBALSETTINGIFCSMOCK_HPP
#define USERGLOBALSETTINGIFCSMOCK_HPP

#include <gmock/gmock.h>

#include "ifc/UserGlobalSettingsIfc.h"

class UserGlobalSettingsIfcMock : public UserGlobalSettingsIfc
{
public:
    MOCK_METHOD(QSize, getMainWndSize, (), (const, override));
    MOCK_METHOD(void, setMainWndSize, (const QSize &size), (override));

    MOCK_METHOD(QPoint, getMainWndPos, (), (const, override));
    MOCK_METHOD(void, setMainWndPos, (const QPoint &pos), (override));

    MOCK_METHOD(QByteArray, getSplitPos, (), (const, override));
    MOCK_METHOD(void, setSplitPos, (const QByteArray &pos), (override));

    MOCK_METHOD(QByteArray, getFileTableColumWidths, (), (const, override));
    MOCK_METHOD(void, setFileTableColumWidths, (const QByteArray &widths), (override));

    MOCK_METHOD(QByteArray, getStatusColumWidths, (), (const, override));
    MOCK_METHOD(void, setStatusColumWidths, (const QByteArray &widths), (override));
};

#endif // USERGLOBALSETTINGIFCSMOCK_HPP
