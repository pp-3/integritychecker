// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef USERPROFILESETTINGSIFCMOCK_HPP
#define USERPROFILESETTINGSIFCMOCK_HPP

#include <gmock/gmock.h>

#include "ifc/UserProfileSettingsIfc.h"

class UserProfileSettingsIfcMock : public UserProfileSettingsIfc
{
public:
    QString m_ProfileName = "mock";
    const QString &profileName() const override { return m_ProfileName; }

    MOCK_METHOD(QString, getInputDir, (), (const, override));
    MOCK_METHOD(void, setInputDir, (const QString &dir), (override));

    MOCK_METHOD(QString, getOutputDirSelection, (), (const, override));
    MOCK_METHOD(void, setOutputDirSelection, (const QString &sel), (override));

    MOCK_METHOD(QString, getOutputDir, (), (const, override));
    MOCK_METHOD(void, setOutputDir, (const QString &dir), (override));

    MOCK_METHOD(unsigned int, getWorkerThreads, (), (const, override));
    MOCK_METHOD(void, setWorkerThreads, (unsigned int w), (override));

    MOCK_METHOD(uint, getCacheSize, (), (const, override));
    MOCK_METHOD(void, setCacheSize, (uint c), (override));

    MOCK_METHOD(QStringList, getEnabledTypes, (), (const, override));
    MOCK_METHOD(void, setEnabledTypes, (const QStringList &types), (override));

    MOCK_METHOD(QString, getLanguage, (), (const, override));
    MOCK_METHOD(void, setLanguage, (const QString &lang), (override));

    MOCK_METHOD(QString, getProfileDescription, (), (const, override));
    MOCK_METHOD(void, setProfileDescription, (const QString &desc), (override));
};

#endif // USERPROFILESETTINGSIFCMOCK_HPP
