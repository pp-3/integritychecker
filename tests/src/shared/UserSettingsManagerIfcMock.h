// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef USERSETTINGSMANAGERIFCMOCK_HPP
#define USERSETTINGSMANAGERIFCMOCK_HPP

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "ifc/UserSettingsManagerIfc.h"

#include "UserGlobalSettingsIfcMock.h"
#include "UserProfileSettingsIfcMock.h"

/**
 * @brief The UserSettingsManagerIfcMock class provides a mock for the UserSettingsManagerIfc, where a plain
 * UserSettingsManagerIfc is sufficient. E.g. its not subclassed from QObject
 */
class UserSettingsManagerIfcMock : public UserSettingsManagerIfc
{
public:
    MOCK_METHOD(const UserGlobalSettingsIfc *, getGlobalSettings, (), (const, override));
    MOCK_METHOD(UserGlobalSettingsIfc *, getGlobalSettings, (), (override));
    MOCK_METHOD(QStringList, getProfiles, (), (const, override));
    MOCK_METHOD(bool, existsProfile, (const QString &profile), (const, override));
    MOCK_METHOD(QSharedPointer<UserProfileSettingsIfc>, getCurrentUserSettings, (), (override));
    MOCK_METHOD(QSharedPointer<UserProfileSettingsIfc>, setCurrentUserProfile, (QString profile), (override));
    MOCK_METHOD(QSharedPointer<UserProfileSettingsIfc>, copyProfile, (const QString &to), (override));
    MOCK_METHOD(QSharedPointer<UserProfileSettingsIfc>, renameProfile, (const QString &to), (override));
    MOCK_METHOD(QSharedPointer<UserProfileSettingsIfc>, deleteProfile, (const QString &profile), (override));

    std::unique_ptr<UserGlobalSettingsIfcMock> global_settings;
    QSharedPointer<UserProfileSettingsIfcMock> profile_settings;

    /**
     * @brief SetUp sets up default mocks for the returned current profile and the global settings.
     */
    void SetUp()
    {
        global_settings = std::make_unique<UserGlobalSettingsIfcMock>();
        profile_settings = QSharedPointer<UserProfileSettingsIfcMock>(new UserProfileSettingsIfcMock);

        EXPECT_CALL(::testing::Const(*this), getGlobalSettings()).WillRepeatedly(::testing::Return(global_settings.get()));
        EXPECT_CALL(*this, getCurrentUserSettings).WillRepeatedly(::testing::Return(profile_settings));
    }

    void TearDown()
    {
        profile_settings.clear();
        global_settings.reset();
    }
};

#endif // USERSETTINGSMANAGERIFCMOCK_HPP
