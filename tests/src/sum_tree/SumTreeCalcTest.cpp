// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumTreeHierarchyTest.h"

#include <algorithm>

#include "checksums/SumTypeIfc.h"
#include "checksums/SumTypeManager.h"

class SumTreeCalcTest : public SumTreeHierarchyTest
{
protected:
    DirInfo::ptr r;
    FileInfo::ptr f1;
    DirInfo::ptr d1;
    FileInfo::ptr f2;
    DirInfo::ptr d2;
    QList<SumTypeIfc *> all_sums;
    QList<CheckSumState::States> all_states;
    QList<InfoBase *> all_nodes;


    void SetUp() override
    {
        SumTreeHierarchyTest::SetUp();

        /* Set up tree
         *          root
         *         /    \
         *       d1      f1
         *      /  \
         *     d2   f2
         */

        r = root->getRoot();

        f1 = addFileChild(r.data(), f1name);
        d1 = addDirChild(r.data(), d1name);

        f2 = addFileChild(d1.data(), f2name);
        d2 = addDirChild(d1.data(), d2name);

        all_sums = SumTypeManager::getInstance().getRegisteredTypes();
        all_states = CheckSumState::getStateList();
        all_nodes << f1.data() << d1.data() << f2.data() << d2.data();
    }

    void TearDown() override
    {
        all_sums.clear();
        all_states.clear();
        all_nodes.clear();
        r.reset();
        f1.reset();
        d1.reset();
        f2.reset();
        d2.reset();

        SumTreeHierarchyTest::TearDown();
    }
};

// Ensure all base check sums are available, as further tests will rely onto that
TEST_F(SumTreeCalcTest, CheckSumCount)
{
    ASSERT_GE(all_sums.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512
}

// Data tree doesn't contain any results at the beginning
TEST_F(SumTreeCalcTest, NoData)
{
    for (const SumTypeIfc *sum : std::as_const(all_sums))
    {
        const SumTypeHash hash = sum->getTypeHash();

        for (const InfoBase *node : std::as_const(all_nodes))
        {
            for (const CheckSumState::States &state : std::as_const(all_states))
            {
                ASSERT_EQ(node->getSumCount(hash, state), 0) << "at: " << node->getName().toStdString()
                                                             << ", for: " << sum->getName().toStdString()
                                                             << ", state: " << CheckSumState::toString(state).toStdString();
            }

            const InfoBase::SumStateCount cnt = node->getSumStateCount(hash);
            for (const CheckSumState::States &state : std::as_const(all_states))
            {
                ASSERT_EQ(cnt[state], 0) << "at: " << node->getName().toStdString()
                                         << ", for: " << sum->getName().toStdString()
                                         << ", state: " << CheckSumState::toString(state).toStdString();
            }
        }
    }
}

// Set all check sums in a single file
TEST_F(SumTreeCalcTest, SetFile2)
{
    QList<InfoBase *> affected_nodes;
    affected_nodes << r.data() << d1.data() << f2.data();
    QList<InfoBase *> unaffected_nodes;
    unaffected_nodes << f1.data() << d2.data();

    for (const SumTypeIfc *sum : std::as_const(all_sums))
    {
        const SumTypeHash hash = sum->getTypeHash();

        QList<CheckSumState::States> set;
        QList<CheckSumState::States> not_set = all_states;

        while (!not_set.isEmpty())
        {
            CheckSumState::States state = not_set.takeFirst();
            set << state;

            f2->setSumState(hash, state);

            for (CheckSumState::States check_state : std::as_const(set))
            {
                const size_t expected_count = state == check_state ? 1 : 0;
                ASSERT_TRUE(std::all_of(affected_nodes.cbegin(),
                                        affected_nodes.cend(),
                                        [hash, check_state, expected_count](const InfoBase *node) -> bool {
                                            return node->getSumCount(hash, check_state) == expected_count;
                                        }))
                    << ", for: " << sum->getName().toStdString()
                    << ", state: " << CheckSumState::toString(check_state).toStdString();

                ASSERT_FALSE(std::any_of(unaffected_nodes.cbegin(),
                                         unaffected_nodes.cend(),
                                         [hash, check_state](const InfoBase *node) -> bool {
                                             return node->getSumCount(hash, check_state) != 0;
                                         }))
                    << ", for: " << sum->getName().toStdString()
                    << ", state: " << CheckSumState::toString(check_state).toStdString();
            }
        }
    }
}

// Set all check sums in two file
TEST_F(SumTreeCalcTest, SetBothFiles)
{
    for (const SumTypeIfc *sum : std::as_const(all_sums))
    {
        const SumTypeHash hash = sum->getTypeHash();

        QList<CheckSumState::States> not_set = all_states;

        while (!not_set.isEmpty())
        {
            CheckSumState::States state = not_set.takeFirst();

            f1->setSumState(hash, state);
            f2->setSumState(hash, state);

            ASSERT_EQ(f1->getSumCount(hash, state), 1) << "for: " << sum->getName().toStdString();
            ASSERT_EQ(f2->getSumCount(hash, state), 1) << "for: " << sum->getName().toStdString();
            ASSERT_EQ(d2->getSumCount(hash, state), 0) << "for: " << sum->getName().toStdString();
            ASSERT_EQ(d1->getSumCount(hash, state), 1) << "for: " << sum->getName().toStdString();
            ASSERT_EQ(r->getSumCount(hash, state), 2) << "for: " << sum->getName().toStdString();
        }
    }
}

// set one check sum per file
TEST_F(SumTreeCalcTest, DifferentSumTypes)
{
    static constexpr CheckSumState::States TEST_STATE = CheckSumState::EXISTS;

    if (all_sums.size() < 2)
        GTEST_SKIP() << "At least two types needed";

    QVector<FileInfo::ptr> files;
    QVector<SumTypeIfc *> sum_types;

    // create new files in d2
    for (int i=0 ; i<all_sums.size() ; ++i)
    {
        files << addFileChild(d2.data(), QString("n")+QString::number(i));
        sum_types << all_sums.at(i);

        const SumTypeHash hash = sum_types.last()->getTypeHash();
        files.last()->setSumState(hash, TEST_STATE);
    }

    // check summary
    QList<InfoBase *> affected_nodes;
    affected_nodes << r.data() << d1.data() << d2.data();

    for (InfoBase *node : std::as_const(affected_nodes))
    {
        for (SumTypeIfc *sum : std::as_const(sum_types))
        {
            const SumTypeHash hash = sum->getTypeHash();
            const InfoBase::SumStateCount cnt = node->getSumStateCount(hash);

            auto it = cnt.constBegin();
            while (it != cnt.constEnd())
            {
                if (it.key() != CheckSumState::UNKNOWN)
                {
                    const unsigned long expected_count = it.key() == TEST_STATE ?
                                                           1 : 0;  // one sum type per node

                    ASSERT_EQ(it.value(), expected_count) << "at: " << node->getName().toStdString()
                                                          << ", for: " << sum->getName().toStdString()
                                                          << ", state: " << CheckSumState::toString(it.key()).toStdString();
                }
                ++it;
            }
        }
    }
}

// add a new file to the data tree
TEST_F(SumTreeCalcTest, AddFile)
{
    const SumTypeHash hash = all_sums.first()->getTypeHash();
    f2->setSumState(hash, CheckSumState::EXISTS);
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 1);

    FileInfo::ptr f3 = addFileChild(d2.data(), "f3");
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 1);

    f3->setSumState(hash, CheckSumState::EXISTS);
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 2);
}

// remove one file from the data tree
TEST_F(SumTreeCalcTest, RemoveFile)
{
    const SumTypeHash hash = all_sums.first()->getTypeHash();
    f2->setSumState(hash, CheckSumState::EXISTS);
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 1);

    d1->removeChild(f2.data());
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 0);
}

// add a new directory (with one file) to the data tree
TEST_F(SumTreeCalcTest, AddDir)
{
    const SumTypeHash hash = all_sums.first()->getTypeHash();
    f2->setSumState(hash, CheckSumState::EXISTS);
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 1);

    DirInfo::ptr d3 = addDirChild(d2.data(), "d3");
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 1);

    FileInfo::ptr f3 = addFileChild(d3.data(), "f3");
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 1);
}

// remove one directory from the data tree
TEST_F(SumTreeCalcTest, RemoveDir)
{
    const SumTypeHash hash = all_sums.first()->getTypeHash();
    f2->setSumState(hash, CheckSumState::EXISTS);
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 1);

    r->removeChild(d1.data());
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::EXISTS), 0);
}

// initial file states are unknown; see them getting known when a file state is set
TEST_F(SumTreeCalcTest, Unknown)
{
    for (const SumTypeIfc *sum : std::as_const(all_sums))
    {
        const SumTypeHash hash = sum->getTypeHash();

        ASSERT_EQ(d2->getSumCount(hash, CheckSumState::UNKNOWN), 0) << "for: " << sum->getName().toStdString();
        ASSERT_EQ(d1->getSumCount(hash, CheckSumState::UNKNOWN), 1) << "for: " << sum->getName().toStdString();
        ASSERT_EQ(r->getSumCount(hash, CheckSumState::UNKNOWN), 2) << "for: " << sum->getName().toStdString();
    }

    const SumTypeIfc *sum = all_sums.first();
    const SumTypeHash hash = sum->getTypeHash();

    f2->setSumState(hash, CheckSumState::EXISTS);
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::UNKNOWN), 1);
    ASSERT_EQ(d1->getSumCount(hash, CheckSumState::UNKNOWN), 0);
    ASSERT_EQ(d2->getSumCount(hash, CheckSumState::UNKNOWN), 0);

    f1->setSumState(hash, CheckSumState::EXISTS);
    ASSERT_EQ(r->getSumCount(hash, CheckSumState::UNKNOWN), 0);
    ASSERT_EQ(d1->getSumCount(hash, CheckSumState::UNKNOWN), 0);
    ASSERT_EQ(d2->getSumCount(hash, CheckSumState::UNKNOWN), 0);
}
