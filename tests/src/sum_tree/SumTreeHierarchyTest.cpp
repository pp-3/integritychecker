// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SumTreeHierarchyTest.h"

void SumTreeHierarchyTest::SetUp()
{
    root = std::make_unique<DirInfoRoot>(QDir());
}

void SumTreeHierarchyTest::TearDown()
{
    root.reset();
}

FileInfo::ptr SumTreeHierarchyTest::addFileChild(InfoParent *parent, const QString &name)
{
    FileInfo::ptr f = FileInfo::ptr::create(name);
    parent->registerChild(f);
    return f;
}

DirInfo::ptr SumTreeHierarchyTest::addDirChild(InfoParent *parent, const QString &name)
{
    DirInfo::ptr d = DirInfo::ptr::create(name);
    parent->registerChild(d);
    return d;
}


TEST_F(SumTreeHierarchyTest, Empty)
{
    DirInfo::ptr r = root->getRoot();

    ASSERT_TRUE(r);
    EXPECT_TRUE(r->getChildren().isEmpty());
    EXPECT_TRUE(r->getSubPath().isEmpty());
    EXPECT_EQ(r->getSubPath(), QString());
}

TEST_F(SumTreeHierarchyTest, AddFile)
{
    DirInfo::ptr r = root->getRoot();

    FileInfo::ptr f = addFileChild(r.data(), f1name);

    EXPECT_EQ(f->getName(), f1name);
    EXPECT_FALSE(f->isDir());
    EXPECT_EQ(r->getChildren().size(), 1);
    EXPECT_EQ(f->getSubPath(), f1name);
    EXPECT_EQ(root->searchChild(f.data()), f);
    EXPECT_EQ(f->getParent(), r.data());
}

TEST_F(SumTreeHierarchyTest, AddDir)
{
    DirInfo::ptr r = root->getRoot();

    DirInfo::ptr d = addDirChild(r.data(), d1name);

    EXPECT_EQ(d->getName(), d1name);
    EXPECT_TRUE(d->isDir());
    EXPECT_EQ(r->getChildren().size(), 1);
    EXPECT_EQ(d->getSubPath(), d1name);
    EXPECT_EQ(root->searchChild(d.data()), d);
    EXPECT_EQ(d->getParent(), r.data());
    EXPECT_NE(d->getInfoBase(), nullptr);
}

TEST_F(SumTreeHierarchyTest, RemoveFile)
{
    DirInfo::ptr r = root->getRoot();

    FileInfo::ptr f = addFileChild(r.data(), f1name);
    r->removeChild(f.data());

    ASSERT_TRUE(r->getChildren().isEmpty());
}

TEST_F(SumTreeHierarchyTest, AddHierarchy)
{
    DirInfo::ptr r = root->getRoot();

    FileInfo::ptr f1 = addFileChild(r.data(), f1name);
    DirInfo::ptr d1 = addDirChild(r.data(), d1name);

    FileInfo::ptr f2 = addFileChild(d1.data(), f2name);
    DirInfo::ptr d2 = addDirChild(d1.data(), d2name);

    // root dir
    EXPECT_EQ(r->getChildren().size(), 2);
    EXPECT_EQ(d1->getParent(), r.data());
    EXPECT_EQ(f1->getParent(), r.data());

    // d1 sub dir
    EXPECT_EQ(d1->getChildren().size(), 2);
    EXPECT_EQ(d2->getParent(), d1.data());
    EXPECT_EQ(f2->getParent(), d1.data());

    // search
    EXPECT_EQ(root->searchChild(f2.data()), f2);
    EXPECT_EQ(r->searchChild(f2.data()), f2);
    EXPECT_EQ(d1->searchChild(f2.data()), f2);

    // path
    EXPECT_EQ(d2->getSubPath(), QString(d1name) + InfoBase::DirSeparator + d2name);
    EXPECT_EQ(f2->getSubPath(), QString(d1name) + InfoBase::DirSeparator + f2name);
}

TEST_F(SumTreeHierarchyTest, RemoveHierarchy)
{
    DirInfo::ptr r = root->getRoot();

    FileInfo::ptr f1 = addFileChild(r.data(), f1name);
    DirInfo::ptr d1 = addDirChild(r.data(), d1name);

    FileInfo::ptr f2 = addFileChild(d1.data(), f2name);
    DirInfo::ptr d2 = addDirChild(d1.data(), d2name);

    // clear shared pointer
    DirInfo *d1p = d1.data();
    DirInfo *d2p = d2.data();

    FileInfo::weak_ptr f1w = f1;
    FileInfo::weak_ptr f2w = f2;
    DirInfo::weak_ptr d1w = d1;
    DirInfo::weak_ptr d2w = d2;

    f1.reset();
    f2.reset();
    d1.reset();
    d2.reset();

    // remove wrong sub directory
    r->removeChild(d2p);
    EXPECT_TRUE(d1w.lock());
    EXPECT_TRUE(d2w.lock());
    EXPECT_TRUE(f1w.lock());
    EXPECT_TRUE(f2w.lock());

    // remove existing sub directory
    r->removeChild(d1p);
    EXPECT_FALSE(d1w.lock());
    EXPECT_FALSE(d2w.lock());
    EXPECT_TRUE(f1w.lock());
    EXPECT_FALSE(f2w.lock());
    EXPECT_EQ(r->getChildren().size(), 1);
}

