// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <memory>

#include <QDir>

#include "sum_tree/DirInfo.h"
#include "sum_tree/DirInfoRoot.h"
#include "sum_tree/FileInfo.h"

class SumTreeHierarchyTest : public ::testing::Test
{
protected:
    static constexpr const char *f1name = "f1";
    static constexpr const char *f2name = "f2";
    static constexpr const char *d1name = "d1";
    static constexpr const char *d2name = "d2";

    std::unique_ptr<DirInfoRoot> root;

    void SetUp() override;
    void TearDown() override;

    FileInfo::ptr addFileChild(InfoParent *parent, const QString &name);
    DirInfo::ptr addDirChild(InfoParent *parent, const QString &name);
};
