// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "sum_tree/TreeUtil.h"

#include "SumTreeHierarchyTest.h"

class TreeUtilTest : public SumTreeHierarchyTest
{
protected:
    DirInfo::ptr r;
    DirInfo::ptr d1;
    DirInfo::ptr d2;
    FileInfo::ptr f2;

    void SetUp() override
    {
        SumTreeHierarchyTest::SetUp();

        r = root->getRoot();
        d1 = addDirChild(r.data(), d1name);
        d2 = addDirChild(d1.data(), d2name);
        f2 = addFileChild(d2.data(), f2name);
    }

    void TearDown() override
    {
        r.reset();
        d1.reset();
        d2.reset();
        f2.reset();
        SumTreeHierarchyTest::TearDown();
    }
};

TEST_F(TreeUtilTest, CheckFile)
{
    InfoBase *ib = f2.get();
    ASSERT_NE(ib, nullptr);

    QList<DirInfo *> result = TreeUtil::getDirHierarchy(ib);
    EXPECT_TRUE(result.isEmpty());
}

TEST_F(TreeUtilTest, CheckDirs)
{
    // single hierarchy
    InfoBase *ib1 = d1->getInfoBase();
    ASSERT_NE(ib1, nullptr);

    QList<DirInfo *> result = TreeUtil::getDirHierarchy(ib1);
    EXPECT_EQ(result.size(), 2);
    EXPECT_EQ(result, QList<DirInfo *>() << d1.get() << r.get());

    // double hierarchy
    InfoBase *ib2 = d2.get();
    ASSERT_NE(ib2, nullptr);

    result = TreeUtil::getDirHierarchy(ib2);
    EXPECT_EQ(result.size(), 3);
    EXPECT_EQ(result, QList<DirInfo *>() << d2.get() << d1.get() << r.get());
}

TEST_F(TreeUtilTest, FileSubPath)
{
    DirInfo::ptr r = root->getRoot();

    // invalid file
    FileInfo::ptr f1 = addFileChild(r.data(), f1name);
    FileInfo::weak_ptr weakf1(f1);
    r->removeChild(f1.get());
    f1.reset();
    EXPECT_FALSE(TreeUtil::getParentSubPath(weakf1));

    // file without parent
    f1 = addFileChild(r.data(), f1name);
    std::optional<QString> path = TreeUtil::getParentSubPath(f1);
    ASSERT_TRUE(path);
    EXPECT_EQ(*path, QString());

    // file with parent
    DirInfo::ptr d1 = addDirChild(r.data(), d1name);
    FileInfo::ptr f2 = addFileChild(d1.data(), f2name);
    path = TreeUtil::getParentSubPath(f2);
    ASSERT_TRUE(path);
    EXPECT_EQ(*path, d1name);

    // file with two parent
    DirInfo::ptr d2 = addDirChild(d1.data(), d2name);
    f2 = addFileChild(d2.data(), f2name);
    path = TreeUtil::getParentSubPath(f2);
    ASSERT_TRUE(path);
    EXPECT_EQ(*path, QString(d1name) + InfoBase::DirSeparator + d2name);
}

TEST_F(TreeUtilTest, CommonDir)
{
    DirInfo::ptr r = root->getRoot();

    DirInfo::ptr d1 = addDirChild(r.data(), d1name);
    DirInfo::ptr d2 = addDirChild(r.data(), d2name);

    DirInfo::ptr d11 = addDirChild(d1.data(), "d11");
    DirInfo::ptr d12 = addDirChild(d1.data(), "d12");

    DirInfo::ptr d21 = addDirChild(d2.data(), "d21");
    DirInfo::ptr d22 = addDirChild(d2.data(), "d22");

    DirInfo::ptr d221 = addDirChild(d22.data(), "d221");
    DirInfo::ptr d222 = addDirChild(d22.data(), "d222");

    EXPECT_EQ(TreeUtil::findCommonParent(nullptr, nullptr), nullptr);
    EXPECT_EQ(TreeUtil::findCommonParent(d1.get(), nullptr), d1.get());
    EXPECT_EQ(TreeUtil::findCommonParent(nullptr, d2.get()), d2.get());

    EXPECT_EQ(TreeUtil::findCommonParent(d1.get(), d2.get()), r.get());
    EXPECT_EQ(TreeUtil::findCommonParent(d1.get(), d222.get()), r.get());

    EXPECT_EQ(TreeUtil::findCommonParent(d11.get(), d12.get()), d1.get());
    EXPECT_EQ(TreeUtil::findCommonParent(d21.get(), d221.get()), d2.get());
    EXPECT_EQ(TreeUtil::findCommonParent(d222.get(), d221.get()), d22.get());
}
