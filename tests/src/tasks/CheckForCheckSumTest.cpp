// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <QCoreApplication>

#include "checksums/SumTypeIfc.h"
#include "checksums/SumTypeManager.h"
#include "model/TaskDispatcher.h"
#include "sum_tree/DirInfo.h"
#include "sum_tree/DirInfoRoot.h"
#include "tasks/CheckForCheckSum.h"
#include "tasks/ScanFsTask.h"
#include "worker/StaticInitializer.h"
#include "thread_util/threadcheck.h"

#include "data/TestData.h"
#include "data/TestFsUtils.h"
#include "ifc/ErrorPostMock.h"
#include "ifc/WorkerSettingsMock.h"
#include "shared/DataCache.h"
#include "shared/UserSettingsManagerIfcMock.h"

#include "TaskExecutionMock.h"

class CheckForCheckSumTest : public ::testing::Test
{
protected:
    ErrorPostMock errors;
    WorkerSettingsMock worker_settings;
    TestData test_data;
    QString test_data_root_path;
    std::unique_ptr<TaskDispatcher> task_dispatcher;
    MainThreadResetter keep_main_thread;
    DirInfoRoot::ptr root;
    SumTypeHashList all_sum_hashes;
    UserSettingsManagerIfcMock user_settings_manager;
    std::unique_ptr<DataCache> caching;
    constexpr static unsigned int cache_size = 10000000u;

    using AsyncTreeResult = TaskResult<InfoBase::weak_ptr>::ptr;
    using AsyncTreeResultList = QList<AsyncTreeResult>;
    using AsyncTreeResultListResult = TaskResult<AsyncTreeResultList>::ptr;

    void SetUp() override
    {
        const bool b = test_data.create();
        if (!b)
            GTEST_SKIP() << "Test data unpacking failed";

        keep_main_thread = setMainThread(QCoreApplication::instance());  // keep QCoreApplication main thread, as no running event loop is needed for these tests
        task_dispatcher.reset(new TaskDispatcher(&worker_settings, std::make_unique<TaskExecutionMock>()));

        caching = std::make_unique<DataCache>(&user_settings_manager, task_dispatcher.get());
        StaticInitializer::getInstance().populateSettings(&worker_settings, caching.get(), &errors);

        // scan test data from file system
        test_data_root_path = test_data.rootPath();
        QSharedPointer<ScanFsTask> task = QSharedPointer<ScanFsTask>::create(test_data_root_path, &errors);
        task_dispatcher->addTask(task);
        root = task->getTaskResult()->getResult();
        ASSERT_TRUE(root);

        all_sum_hashes = SumTypeManager::getInstance().getRegisteredHashes();

        user_settings_manager.SetUp();

        EXPECT_CALL(*user_settings_manager.profile_settings, getCacheSize).WillRepeatedly(testing::Return(cache_size));

        EXPECT_CALL(worker_settings, getOutputDirSelection).WillRepeatedly(testing::Return(WorkerSettingsIfc::OUTPUT_SELECTION::AS_INPUT));
        EXPECT_CALL(worker_settings, getInputDir).WillRepeatedly(testing::Return(test_data_root_path));
        EXPECT_CALL(worker_settings, getOutputDir).WillRepeatedly(testing::Return(test_data_root_path));
        EXPECT_CALL(worker_settings, isInputDirValid).WillRepeatedly(testing::Return(true));
        EXPECT_CALL(worker_settings, isOutputDirValid).WillRepeatedly(testing::Return(true));
        EXPECT_CALL(worker_settings, areDirsValid).WillRepeatedly(testing::Return(true));
        EXPECT_CALL(worker_settings, createOutputDir).WillRepeatedly(testing::Return(true));
        EXPECT_CALL(worker_settings, getMinMaxWorkerThreads).WillRepeatedly(testing::Return(QPair<unsigned int, unsigned int>(1, 8)));
        EXPECT_CALL(worker_settings, getCacheSize).WillRepeatedly(testing::Return(cache_size));
        EXPECT_CALL(worker_settings, getEnabledCalcHashes).WillRepeatedly(testing::Return(all_sum_hashes));

        EXPECT_CALL(errors, addError).Times(0); // no errors expected
    }

    void TearDown() override
    {
        user_settings_manager.TearDown();
        caching.reset();
        task_dispatcher->shutdown();
        task_dispatcher.reset();
        keep_main_thread.reset();
        root.clear();
        test_data.destroy();
    }
};


// search for all files with an existing checksum
TEST_F(CheckForCheckSumTest, CheckFoundSumStates)
{
    ASSERT_GE(all_sum_hashes.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    DirInfo::ptr r = root->getRoot();
    QSet<FileInfo *> all_files = TestFsUtils::getAllFiles(r.data(), true);

    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), r, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    ASSERT_GT(check->getName().size(), 0);
    TaskLifeCycle::ptr lfc = task_dispatcher->addTask(check);
    bool canceled = true;
    ASSERT_TRUE(lfc->isFinishedOrCanceled(&canceled));
    ASSERT_FALSE(canceled);

    AsyncTreeResultListResult result = check->getTaskResult();
    ASSERT_TRUE(result);
    EXPECT_TRUE(result->hasResult());
    AsyncTreeResultList found_files = result->getResult();

    while (!found_files.isEmpty())
    {
        TaskResult<InfoBase::weak_ptr>::ptr tr = found_files.takeFirst();
        ASSERT_TRUE(tr);
        if (!tr->hasResult())
            continue;

        InfoBase::weak_ptr ibw = tr->getResult();
        InfoBase::ptr ib = ibw.toStrongRef();
        ASSERT_TRUE(ib);

        EXPECT_TRUE(!ib->isDir());  // only files are collected

        // check found files are in the data tree
        FileInfo *fi = FileInfo::cast(ib.data());
        ASSERT_TRUE(fi);
        all_files.remove(fi); // found_files may contain each file multiple times (one per checksum)
                              // thus only the first one will remove a file here

        const QString name = ib->getName();

        const bool expect_checksum = (TestData::hasAtLeastOneValidChecksum(name) ||
                                      TestData::hasAtLeastOneErrorChecksum(name));

        // check found file has at least one check sum
        const bool has_checksum = std::any_of(all_sum_hashes.cbegin(),
                                              all_sum_hashes.cend(),
                                              [&ib](const SumTypeHash &hash) -> bool {
                                                 return ib->getSumCount(hash, CheckSumState::EXISTS) > 0;
                                              });
        EXPECT_EQ(has_checksum, expect_checksum);
    }

    // verify rest of files in the data tree don't provide a check sum
    for (FileInfo *fi : all_files)
    {
        const QString name = fi->getName();
        const bool is_missing = TestData::hasNoChecksums(name);
        const bool with_checksum = (TestData::hasAtLeastOneValidChecksum(name) ||
                                    TestData::hasAtLeastOneErrorChecksum(name));
        EXPECT_TRUE(is_missing || !with_checksum);
    }
}

// scan one file with all checksums
TEST_F(CheckForCheckSumTest, OneFileWithAllCheckSums)
{
    ASSERT_GE(all_sum_hashes.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString file_name = TestData::getFileNamesWithOnlyCheckSums().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // check for file
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);

    // fetch result list
    AsyncTreeResultListResult result = check->getTaskResult();
    ASSERT_TRUE(result);
    ASSERT_TRUE(result->hasResult());
    const AsyncTreeResultList found_checksums = result->getResult();
    ASSERT_EQ(found_checksums.size(), all_sum_hashes.size());

    for (const AsyncTreeResult &atr : found_checksums)
    {
        // get result
        ASSERT_TRUE(atr);
        ASSERT_TRUE(atr->hasResult());

        // check for file
        InfoBase::weak_ptr ibw = atr->getResult();
        InfoBase::ptr ib = ibw.toStrongRef();
        ASSERT_TRUE(ib);
        EXPECT_TRUE(!ib->isDir());  // only files are collected
    }

    // check for all checksums
    for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
    {
        EXPECT_EQ(test_file->getSumCount(hash, CheckSumState::States::EXISTS), 1);
    }
}

// scan one file with all checksums directly via CheckFileForCheckSum
TEST_F(CheckForCheckSumTest, OneFileWithAllCheckSums_CheckFileForCheckSum)
{
    const SumTypeIfc *sum = SumTypeManager::getInstance().getRegisteredTypes().constFirst();
    const QString file_name = TestData::getFileNamesWithOnlyCheckSums().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);
    const FileInfo::ptr fi = FileInfo::cast(test_file_ptr);
    ASSERT_TRUE(fi);

    // check for file
    QSharedPointer<CheckFileForCheckSum> check =
        QSharedPointer<CheckFileForCheckSum>::create(test_data.rootPath(),
                                                     fi,
                                                     sum,
                                                     CheckForCheckSum::EXISTS);
    ASSERT_GT(check->getName().size(), 0);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);

    // fetch result list
    AsyncTreeResult result = check->getTaskResult();
    ASSERT_TRUE(result);
    ASSERT_TRUE(result->hasResult());
    // get result
    InfoBase::weak_ptr ibw = result->getResult();
    InfoBase::ptr ib = ibw.toStrongRef();
    ASSERT_TRUE(ib);
    EXPECT_TRUE(!ib->isDir()); // only files are collected

    // check for checksum
    EXPECT_EQ(test_file->getSumCount(sum->getTypeHash(), CheckSumState::States::EXISTS), 1);
}

// scan one file with less than all checksums
TEST_F(CheckForCheckSumTest, OneFileWithPartlyCheckSums)
{
    ASSERT_GE(all_sum_hashes.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString file_name = TestData::getFileNamesWithSingleCheckSum().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // check for file checksums
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);

    // fetch result list (with one result)
    const AsyncTreeResultList found_checksums = check->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), 1);  // at least one result

    const AsyncTreeResult atr = found_checksums.front();
    // get result
    ASSERT_TRUE(atr);
    ASSERT_TRUE(atr->hasResult());

    // check for file
    InfoBase::weak_ptr ibw = atr->getResult();
    InfoBase::ptr ib = ibw.toStrongRef();
    ASSERT_TRUE(ib);
    EXPECT_TRUE(!ib->isDir()); // only files are collected
}

// scan an empty directory
TEST_F(CheckForCheckSumTest, DirWithoutCheckSums)
{
    const QString dir_name = TestData::getEmptyDirs().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_dir = TestFsUtils::findChild(r.get(), dir_name, true);
    ASSERT_NE(test_dir, nullptr);
    const InfoBase::ptr test_dir_ptr = root->searchChild(test_dir);
    ASSERT_TRUE(test_dir_ptr);

    // check for dir checksums
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_dir_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);

    // fetch result list
    const AsyncTreeResultList found_checksums = check->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), 0);
}

// scan one directory with valid checksums and errors with all check sum types
TEST_F(CheckForCheckSumTest, OneDirWithCheckSumsAndErrors)
{
    ASSERT_GE(all_sum_hashes.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString dir_name = TestData::getDirNamesWithMixedCheckSumsAndErrors().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_dir = TestFsUtils::findChild(r.get(), dir_name, true);
    ASSERT_NE(test_dir, nullptr);
    const InfoBase::ptr test_dir_ptr = root->searchChild(test_dir);
    ASSERT_TRUE(test_dir_ptr);

    // check for dir checksums
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_dir_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);

    // fetch result list
    const AsyncTreeResultList found_checksums = check->getAvailableTaskResults();
    ASSERT_GT(found_checksums.size(), 0);  // at least one result

    // get unique files (files appear multiple times, if they have multiple check sums)
    QSet<InfoBase::ptr> found_files;
    for (const AsyncTreeResult &atr : found_checksums)
    {
        // get result
        ASSERT_TRUE(atr);
        ASSERT_TRUE(atr->hasResult());

        // check for files
        InfoBase::weak_ptr ibw = atr->getResult();
        InfoBase::ptr ib = ibw.toStrongRef();
        ASSERT_TRUE(ib);

        if (!ib->isDir())
            found_files << ib;
    }

    unsigned long cnt_in_files = 0;
    unsigned long cnt_in_dir = 0;

    // check for found checksums
    for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
    {
        // count all files
        for (const InfoBase::ptr &ib : std::as_const(found_files))
        {
            cnt_in_files += ib->getSumCount(hash, CheckSumState::States::EXISTS);
        }

        // count in searched directory
        cnt_in_dir += test_dir->getSumCount(hash, CheckSumState::States::EXISTS);
    }

    EXPECT_GT(cnt_in_files, 0);
    EXPECT_EQ(cnt_in_files, cnt_in_dir);
}


// scan one directory with valid checksums and errors with one check sum type
TEST_F(CheckForCheckSumTest, OneDirWithCheckSumsAndErrorsSingleType)
{
    const QString dir_name = TestData::getDirNamesWithMixedCheckSumsAndErrors().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_dir = TestFsUtils::findChild(r.get(), dir_name, true);
    ASSERT_NE(test_dir, nullptr);
    const InfoBase::ptr test_dir_ptr = root->searchChild(test_dir);
    ASSERT_TRUE(test_dir_ptr);

    // check for dir checksums
    const SumTypeHashList single_check{*all_sum_hashes.constBegin()};
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_dir_ptr, single_check, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(check);

    // fetch result list
    const AsyncTreeResultList found_checksums = check->getAvailableTaskResults();
    ASSERT_GT(found_checksums.size(), 0);  // at least one result

    unsigned long cnt_in_files = 0;
    unsigned long cnt_in_dir = 0;

    // check for found checksums
    for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
    {
        // count all files
        for (const AsyncTreeResult &atr : found_checksums)
        {
            // get result
            ASSERT_TRUE(atr);
            ASSERT_TRUE(atr->hasResult());

            // check for files
            InfoBase::weak_ptr ibw = atr->getResult();
            InfoBase::ptr ib = ibw.toStrongRef();
            ASSERT_TRUE(ib);

            if (!ib->isDir())
                cnt_in_files += ib->getSumCount(hash, CheckSumState::States::EXISTS);
        }

        // count in searched directory
        cnt_in_dir += test_dir->getSumCount(hash, CheckSumState::States::EXISTS);
    }

    EXPECT_GT(cnt_in_files, 0);
    EXPECT_EQ(cnt_in_files, cnt_in_dir);
}

TEST_F(CheckForCheckSumTest, CompareOneFileWithCheckSums)
{
    ASSERT_GE(all_sum_hashes.size(), 6);  // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString file_name = TestData::getFileNamesWithOnlyCheckSums().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(precheck);
    // compare file with available checksums
    QSharedPointer<CheckForCheckSum> compare =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::COMPARE, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(compare);

    // fetch result list
    const AsyncTreeResultList found_checksums = compare->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), all_sum_hashes.size());

    for (const AsyncTreeResult &atr : found_checksums)
    {
        // get result
        ASSERT_TRUE(atr);
        ASSERT_TRUE(atr->hasResult());

        // check for file
        InfoBase::weak_ptr ibw = atr->getResult();
        InfoBase::ptr ib = ibw.toStrongRef();
        ASSERT_TRUE(ib);
        EXPECT_TRUE(!ib->isDir()); // only files are collected
    }

    // check for all checksums
    for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
    {
        EXPECT_EQ(test_file->getSumCount(hash, CheckSumState::States::VERIFIED), 1);
    }
}

TEST_F(CheckForCheckSumTest, CompareOneFileWithError)
{
    ASSERT_GE(all_sum_hashes.size(), 6);  // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString file_name = TestData::getFileNamesWithOnlyErrors().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(precheck);
    // compare file with available checksums
    QSharedPointer<CheckForCheckSum> compare =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::COMPARE, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(compare);

    // fetch result list (with one result for each check sum)
    const AsyncTreeResultList found_checksums = compare->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), all_sum_hashes.size());

    for (const AsyncTreeResult &atr : found_checksums)
    {
        // get result
        ASSERT_TRUE(atr);
        ASSERT_TRUE(atr->hasResult());

        // check for file
        InfoBase::weak_ptr ibw = atr->getResult();
        InfoBase::ptr ib = ibw.toStrongRef();
        ASSERT_TRUE(ib);
        EXPECT_TRUE(!ib->isDir()); // only files are collected
    }

    // check for all checksums
    for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
    {
        EXPECT_EQ(test_file->getSumCount(hash, CheckSumState::States::CHECK_ERROR), 1);
    }
}

TEST_F(CheckForCheckSumTest, CompareOneDirWithCheckSumsAndErrors)
{
    const QString dir_name = TestData::getDirNamesWithMixedCheckSumsAndErrors().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_dir = TestFsUtils::findChild(r.get(), dir_name, true);
    ASSERT_NE(test_dir, nullptr);
    const InfoBase::ptr test_dir_ptr = root->searchChild(test_dir);
    ASSERT_TRUE(test_dir_ptr);

    // check for dir checksums
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_dir_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(check);
    // compare dir with available checksums
    QSharedPointer<CheckForCheckSum> compare =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_dir_ptr, all_sum_hashes, true, CheckForCheckSum::COMPARE, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(compare);

    // fetch result list
    const AsyncTreeResultList found_checksums = compare->getAvailableTaskResults();
    ASSERT_GT(found_checksums.size(), all_sum_hashes.size());

    for (const AsyncTreeResult &atr : found_checksums)
    {
        // get result
        ASSERT_TRUE(atr);
        ASSERT_TRUE(atr->hasResult());

        // check for file
        InfoBase::weak_ptr ibw = atr->getResult();
        InfoBase::ptr ib = ibw.toStrongRef();
        ASSERT_TRUE(ib);

        // check for found checksums
        for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
        {
            const unsigned long errors = ib->getSumCount(hash, CheckSumState::States::CHECK_ERROR);
            const unsigned long valid = ib->getSumCount(hash, CheckSumState::States::VERIFIED);
            if (ib->isDir())
            {
                EXPECT_TRUE(errors > 0 || valid > 0);
            }
            else
            {
                EXPECT_TRUE((errors == 1 || valid == 1) && (errors + valid == 1));
            }
        }
    }
}

TEST_F(CheckForCheckSumTest, CompareTopDir)
{
    DirInfo::ptr r = root->getRoot();

    // check for dir checksums
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), r, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(check);
    // compare dir with available checksums
    QSharedPointer<CheckForCheckSum> compare =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), r, all_sum_hashes, true, CheckForCheckSum::COMPARE, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(compare);

    // fetch result list
    const AsyncTreeResultList found_checksums = compare->getAvailableTaskResults();
    ASSERT_GT(found_checksums.size(), all_sum_hashes.size());

    // check state for file in the tree
    const QSet<FileInfo *> all_files = TestFsUtils::getAllFiles(r.data(), true);
    for (FileInfo *fi : all_files)
    {
        if (fi->isDir())
            continue;

        const QString name = fi->getName();
        const bool checksum_missing = TestData::hasNoChecksums(name);
        const bool has_valid_checksum = TestData::hasAtLeastOneValidChecksum(name);
        const bool has_failed_checksum = TestData::hasAtLeastOneErrorChecksum(name);

        if (checksum_missing)
        {
            const bool missing = std::all_of(all_sum_hashes.constBegin(), all_sum_hashes.constEnd(), [&fi](const SumTypeHash &hash) -> bool {
                const InfoBase::SumStateCount ssc = fi->getSumStateCount(hash);

                return ssc[CheckSumState::States::EXISTS] == 0 &&
                       ssc[CheckSumState::States::CHECK_ERROR] == 0 &&
                       ssc[CheckSumState::States::VERIFIED] == 0;
            });
            EXPECT_TRUE(missing);
        }

        if (has_valid_checksum)
        {
            const bool valid = std::any_of(all_sum_hashes.constBegin(), all_sum_hashes.constEnd(), [&fi](const SumTypeHash &hash) -> bool {
                const InfoBase::SumStateCount ssc = fi->getSumStateCount(hash);

                return ssc[CheckSumState::States::EXISTS] == 1 ||
                       (ssc[CheckSumState::States::CHECK_ERROR] == 0 &&
                        ssc[CheckSumState::States::VERIFIED] == 1);
            });
            EXPECT_TRUE(valid);
        }

        if (has_failed_checksum)
        {
            const bool error = std::any_of(all_sum_hashes.constBegin(), all_sum_hashes.constEnd(), [&fi](const SumTypeHash &hash) -> bool {
                const InfoBase::SumStateCount ssc = fi->getSumStateCount(hash);

                return ssc[CheckSumState::States::EXISTS] == 1 ||
                       (ssc[CheckSumState::States::CHECK_ERROR] == 1 &&
                        ssc[CheckSumState::States::VERIFIED] == 0);
            });
            EXPECT_TRUE(error);
        }
    }
}

TEST_F(CheckForCheckSumTest, ReCalculateValidFile)
{
    ASSERT_GE(all_sum_hashes.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString file_name = TestData::getFileNamesWithOnlyCheckSums().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(precheck);
    // calculate file checksums
    QSharedPointer<CheckForCheckSum> calc =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::CALCULATE, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(calc);

    // fetch result list
    const AsyncTreeResultList found_checksums = calc->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), all_sum_hashes.size());

    for (const AsyncTreeResult &atr : found_checksums)
    {
        // get result
        ASSERT_TRUE(atr);
        ASSERT_TRUE(atr->hasResult());

        // check for file
        InfoBase::weak_ptr ibw = atr->getResult();
        InfoBase::ptr ib = ibw.toStrongRef();
        ASSERT_TRUE(ib);
        EXPECT_TRUE(!ib->isDir()); // only files are collected
    }

    // check for all checksums
    for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
    {
        EXPECT_EQ(test_file->getSumCount(hash, CheckSumState::States::VERIFIED), 1);
    }
}

TEST_F(CheckForCheckSumTest, CalculateErrorFile)
{
    ASSERT_GE(all_sum_hashes.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString file_name = TestData::getFileNamesWithOnlyErrors().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(precheck);
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::COMPARE, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);

    // recalculate each check sum separately
    SumTypeHashList errored_hashes = all_sum_hashes;
    SumTypeHashList recalculated_hashes;

    while (!errored_hashes.isEmpty())
    {
        const SumTypeHash hash = *errored_hashes.constBegin();
        errored_hashes.remove(hash);
        recalculated_hashes.insert(hash);

        QSharedPointer<CheckForCheckSum> calc = QSharedPointer<CheckForCheckSum>::create(
            test_data.rootPath(), test_file_ptr, SumTypeHashList({hash}), false, CheckForCheckSum::CALCULATE, &errors);
        /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(calc);

        // fetch result list (with one result)
        const AsyncTreeResultList found_checksums = calc->getAvailableTaskResults();
        ASSERT_EQ(found_checksums.size(), 1);

        // check recalculated checksum
        const bool valid = std::all_of(recalculated_hashes.constBegin(),
                                       recalculated_hashes.constEnd(),
                                       [&test_file](const SumTypeHash &hash) -> bool {
                                         const InfoBase::SumStateCount ssc = test_file->getSumStateCount(hash);

                                         return ssc[CheckSumState::States::EXISTS] == 0 &&
                                                ssc[CheckSumState::States::CHECK_ERROR] == 0 &&
                                                ssc[CheckSumState::States::VERIFIED] == 1;
                                       });
        EXPECT_TRUE(valid);

        // all other checksums are still errored
        const bool error = std::all_of(errored_hashes.constBegin(),
                                       errored_hashes.constEnd(),
                                       [&test_file](const SumTypeHash &hash) -> bool {
                                         const InfoBase::SumStateCount ssc = test_file->getSumStateCount(hash);

                                         return ssc[CheckSumState::States::EXISTS] == 0 &&
                                                ssc[CheckSumState::States::CHECK_ERROR] == 1 &&
                                                ssc[CheckSumState::States::VERIFIED] == 0;
                                       });
        EXPECT_TRUE(error);
    }
}

TEST_F(CheckForCheckSumTest, CalculateMissingFile)
{
    ASSERT_GE(all_sum_hashes.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString file_name = TestData::getFileNamesWithMissingCheckSums().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(precheck);

    // recalculate each check sum separately
    SumTypeHashList missing_hashes = all_sum_hashes;
    SumTypeHashList recalculated_hashes;

    while (!missing_hashes.isEmpty())
    {
        const SumTypeHash hash = *missing_hashes.constBegin();
        missing_hashes.remove(hash);
        recalculated_hashes.insert(hash);

        QSharedPointer<CheckForCheckSum> calc = QSharedPointer<CheckForCheckSum>::create(
            test_data.rootPath(), test_file_ptr, SumTypeHashList({hash}), false, CheckForCheckSum::CALCULATE, &errors);
        /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(calc);

        // fetch result list (with one result)
        const AsyncTreeResultList found_checksums = calc->getAvailableTaskResults();
        ASSERT_EQ(found_checksums.size(), 1);

        // check recalculated checksum
        const bool valid = std::all_of(recalculated_hashes.constBegin(),
                                       recalculated_hashes.constEnd(),
                                       [&test_file](const SumTypeHash &hash) -> bool {
                                         const InfoBase::SumStateCount ssc = test_file->getSumStateCount(hash);

                                         return ssc[CheckSumState::States::EXISTS] == 0 &&
                                                ssc[CheckSumState::States::CHECK_ERROR] == 0 &&
                                                ssc[CheckSumState::States::VERIFIED] == 1;
                                       });
        EXPECT_TRUE(valid);

        // all other checksums are still missing
        const bool missing = std::all_of(missing_hashes.constBegin(),
                                         missing_hashes.constEnd(),
                                         [&test_file](const SumTypeHash &hash) -> bool {
                                           const InfoBase::SumStateCount ssc = test_file->getSumStateCount(hash);

                                           return ssc[CheckSumState::States::EXISTS] == 0 &&
                                                  ssc[CheckSumState::States::CHECK_ERROR] == 0 &&
                                                  ssc[CheckSumState::States::VERIFIED] == 0;
                                         });
        EXPECT_TRUE(missing);
    }
}

TEST_F(CheckForCheckSumTest, CalculateMixedFile)
{
    ASSERT_GE(all_sum_hashes.size(), 6); // MD5, SHA1, SHA256, SHA512, SHA3-256, SHA3-512

    const QString file_name = TestData::getFileNamesWithMixedCheckSumsAndErrors().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(precheck);
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::COMPARE, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);

    // recalculate all errored check sums
    SumTypeHashList errored_hashes;
    for (const SumTypeHash &hash : std::as_const(all_sum_hashes))
    {
        if (test_file->getSumCount(hash, CheckSumState::States::CHECK_ERROR) > 0)
            errored_hashes.insert(hash);
    }

    ASSERT_FALSE(errored_hashes.isEmpty());

    QSharedPointer<CheckForCheckSum> calc =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, errored_hashes, false, CheckForCheckSum::CALCULATE, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(calc);

    // fetch result list
    const AsyncTreeResultList found_checksums = calc->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), errored_hashes.size());

    // expect all checksums being valid now
    const bool valid = std::all_of(all_sum_hashes.constBegin(),
                                   all_sum_hashes.constEnd(),
                                   [&test_file](const SumTypeHash &hash) -> bool {
                                     const InfoBase::SumStateCount ssc = test_file->getSumStateCount(hash);

                                     return ssc[CheckSumState::States::EXISTS] == 0 &&
                                            ssc[CheckSumState::States::CHECK_ERROR] == 0 &&
                                            ssc[CheckSumState::States::VERIFIED] == 1;
                                   });
    EXPECT_TRUE(valid);
}

TEST_F(CheckForCheckSumTest, CalculateTopDir)
{
    DirInfo::ptr r = root->getRoot();

    // check for dir checksums
    QSharedPointer<CheckForCheckSum> check =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), r, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);
    // calculate top dir with all available checksums
    QSharedPointer<CheckForCheckSum> calc =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), r, all_sum_hashes, true, CheckForCheckSum::CALCULATE, &errors);
    /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(calc);

    // fetch result list
    const AsyncTreeResultList found_checksums = calc->getAvailableTaskResults();
    ASSERT_GT(found_checksums.size(), all_sum_hashes.size());

    // check state for files in the tree
    QSet<FileInfo *> all_files = TestFsUtils::getAllFiles(r.data(), true);

    // remove checksum files itself from the check
    const QList<SumTypeIfc *> all_sums = SumTypeManager::getInstance().getRegisteredTypes();
    for (auto it=all_files.begin() ; it!=all_files.end() ;)
    {
        FileInfo *const fi = *it;
        const InfoBase::ptr ib_ptr = root->searchChild(fi);
        ASSERT_TRUE(ib_ptr);
        const FileInfo::ptr fi_ptr = FileInfo::cast(ib_ptr);
        ASSERT_TRUE(fi_ptr);

        const bool is_checksumfile = std::any_of(all_sums.constBegin(),
                                                 all_sums.constEnd(),
                                                 [fi_ptr](SumTypeIfc *ifc) -> bool {
                                                   return ifc->getSumFileManager()->isCheckSumFile(fi_ptr);
                                                 });

        if (is_checksumfile)
            it = all_files.erase(it);
        else
            ++it;
    }

    // check files
    for (FileInfo *fi : all_files)
    {
        if (fi->isDir())
            continue;

        // expect all checksums being valid now
        const bool valid = std::all_of(all_sum_hashes.constBegin(),
                                       all_sum_hashes.constEnd(),
                                       [&fi](const SumTypeHash &hash) -> bool {
                                         const InfoBase::SumStateCount ssc = fi->getSumStateCount(hash);

                                         return ssc[CheckSumState::States::EXISTS] == 0 &&
                                                ssc[CheckSumState::States::CHECK_ERROR] == 0 &&
                                                ssc[CheckSumState::States::VERIFIED] == 1;
                                       });
        EXPECT_TRUE(valid);
    }
}

// if the shared pointer to file gets removed the background task can't calculate a checksum for it
TEST_F(CheckForCheckSumTest, MissingSharedPtr)
{
    const QString file_name = TestData::getFileNamesWithOnlyCheckSums().constLast();

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::weak_ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(precheck);

    // delete all shared pointers
    r.reset();
    root.reset();

    // calculate file checksums
    QSharedPointer<CheckForCheckSum> calc =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::CALCULATE, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(calc);

    // fetch result list
    const AsyncTreeResultList found_checksums = calc->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), 0); // calculation failed, because of missing file ptr
}

// if a file gets removed during calculation, the calculation fails
TEST_F(CheckForCheckSumTest, FileGone)
{
    const QStringList dir = {"dir1"};
    const QString file_name = "file1";

    ASSERT_TRUE(test_data.existsDir(dir));
    ASSERT_TRUE(test_data.existsFile(dir, file_name));

    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(precheck);

    // delete file from fs
    ASSERT_TRUE(test_data.removeFile(dir, file_name));

    EXPECT_CALL(errors, addWarning).Times(testing::AtLeast(1)); // at least one error expected

    // calculate file checksums
    QSharedPointer<CheckForCheckSum> calc =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, all_sum_hashes, true, CheckForCheckSum::CALCULATE, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(calc);

    // fetch result list
    const AsyncTreeResultList found_checksums = calc->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), 0); // calculation failed, because of missing file
}

// writing of a checksum file fails
TEST_F(CheckForCheckSumTest, BlockChecksumWrite)
{
    if (QCoreApplication::applicationDirPath().startsWith("/builds"))
    {
        // simple detector of a run in the CI
        GTEST_SKIP() << "Test relies on removing the write permission for a file. This is disallowed in the CI. Workaround: skip the test there";
    }

    const QStringList dir = {"dir1"};
    const QString file_name = "file1";

    ASSERT_TRUE(test_data.existsDir(dir));
    ASSERT_TRUE(test_data.existsFile(dir, file_name));

    // related md5 check sum data
    SumTypeIfc *md5 = SumTypeManager::getInstance().fromName("MD5");
    ASSERT_NE(md5, nullptr);
    const SumTypeHash md5hash = md5->getTypeHash();
    const SumTypeHashList md5hash_aslist = {md5hash};
    const QString md5_sum_filename("MD5SUMS");
    ASSERT_TRUE(test_data.existsFile(dir, md5_sum_filename));

    // find file in tree
    DirInfo::ptr r = root->getRoot();
    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    const InfoBase::ptr test_file_ptr = root->searchChild(test_file);
    ASSERT_TRUE(test_file_ptr);

    // first check file for checksums
    QSharedPointer<CheckForCheckSum> precheck =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, md5hash_aslist, true, CheckForCheckSum::EXISTS, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(precheck);

    // set checksum file to not accessible
    ASSERT_TRUE(test_data.setLastDirPermissions(dir, {}));

    EXPECT_CALL(errors, addWarning).Times(testing::AtLeast(1)); // at least one error expected

    // calculate file checksums
    QSharedPointer<CheckForCheckSum> calc =
        QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), test_file_ptr, md5hash_aslist, true, CheckForCheckSum::CALCULATE, &errors);
    /* TaskLifeCycle::ptr lfc = */ task_dispatcher->addTask(calc);

    // revert read-only settings, in order to allow deletion of test data
    ASSERT_TRUE(test_data.setLastDirPermissions(dir, QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ExeUser));

    // fetch result list
    const AsyncTreeResultList found_checksums = calc->getAvailableTaskResults();
    ASSERT_EQ(found_checksums.size(), 0); // calculation failed, because of missing file
}

TEST_F(CheckForCheckSumTest, CleanTopDir)
{
  DirInfo::ptr r = root->getRoot();

  // check for dir checksums
  QSharedPointer<CheckForCheckSum> check =
      QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), r, all_sum_hashes, true, CheckForCheckSum::EXISTS, &errors);
  /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(check);
  // calculate top dir with all available checksums
  QSharedPointer<CheckForCheckSum> calc =
      QSharedPointer<CheckForCheckSum>::create(test_data.rootPath(), r, all_sum_hashes, true, CheckForCheckSum::CALCULATE, &errors);
  /* TaskLifeCycle::ptr lfc = */task_dispatcher->addTask(calc);

  // remove single hashes
  SumTypeHashList keep_hashes = all_sum_hashes;
  const QList<CheckSumState::States> states = CheckSumState::getStateList();

  while (!keep_hashes.isEmpty())
  {
      const SumTypeHash to_remove = *keep_hashes.begin();
      keep_hashes.remove(to_remove);

      InfoBase::SumStateCount sums = r->getSumStateCount(to_remove);
      bool b = std::any_of(states.cbegin(),
                           states.cend(),
                           [&sums](const CheckSumState::States &state) -> bool
                           {
                               return sums[state] > 0;
                           });
      EXPECT_TRUE(b);

      root->cleanFromUnusedIntegrityTypes(keep_hashes);

      sums = r->getSumStateCount(to_remove);
      b = std::any_of(states.cbegin(),
                      states.cend(),
                      [&sums](const CheckSumState::States &state) -> bool
                      {
                          return sums[state] > 0;
                      });
      EXPECT_FALSE(b);
  }
}
