// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ScanFsTaskTest.h"

#include "checksums/SumTypeManager.h"
#include "tasks/ReScanFsTask.h"
#include "tasks/ScanFsTask.h"

#include "data/TestFsUtils.h"
#include "shared/DataCacheMock.h"

class ReScanFsTaskTest : public ScanFsTaskTest
{
protected:
    DataCacheMock cache;
    DirInfoRoot::ptr root;
    DirInfo::ptr r;
    QString test_data_root_path;
    SumTypeHashList single_sum_hash;

    void SetUp() override
    {
        ScanFsTaskTest::SetUp();
        test_data_root_path = test_data.rootPath();

        StaticInitializer::getInstance().populateSettings(&worker_settings, &cache, &errors);

        // initial scan
        QSharedPointer<ScanFsTask> task = QSharedPointer<ScanFsTask>::create(test_data.rootPath(), &errors);
        task_dispatcher->addTask(task);
        QSharedPointer<TaskResult<QSharedPointer<DirInfoRoot>>> result = task->getTaskResult();
        root = result->getResult();
        r = root->getRoot();

        // supported check sums
        {
            const QList<SumTypeIfc *> all_sums = SumTypeManager::getInstance().getRegisteredTypes();
            single_sum_hash << all_sums.first()->getTypeHash();
        }

        EXPECT_CALL(worker_settings, getOutputDirSelection).WillRepeatedly(testing::Return(WorkerSettingsIfc::OUTPUT_SELECTION::AS_INPUT));
        EXPECT_CALL(worker_settings, getInputDir).WillRepeatedly(testing::Return(test_data_root_path));
        EXPECT_CALL(worker_settings, getOutputDir).WillRepeatedly(testing::Return(test_data_root_path));
        EXPECT_CALL(worker_settings, isInputDirValid).WillRepeatedly(testing::Return(true));
        EXPECT_CALL(worker_settings, isOutputDirValid).WillRepeatedly(testing::Return(true));
        EXPECT_CALL(worker_settings, areDirsValid).WillRepeatedly(testing::Return(true));
        EXPECT_CALL(worker_settings, createOutputDir).WillRepeatedly(testing::Return(false));
        EXPECT_CALL(worker_settings, getMinMaxWorkerThreads).WillRepeatedly(testing::Return(QPair<unsigned int, unsigned int>(1, 1)));
        EXPECT_CALL(worker_settings, getEnabledCalcHashes).WillRepeatedly(testing::Return(single_sum_hash));

        EXPECT_CALL(errors, addError).Times(0); // no errors expected
        EXPECT_CALL(errors, addWarning).WillRepeatedly(testing::Return());

        EXPECT_CALL(cache, hasData).WillRepeatedly(testing::Return(false));
        EXPECT_CALL(cache, getData).WillRepeatedly(testing::Return(nullptr));
        EXPECT_CALL(cache, replaceData(::testing::_, ::testing::_)).WillRepeatedly(testing::Return(nullptr));
    }

    void TearDown() override
    {
        r.reset();
        root.reset();

        ScanFsTaskTest::TearDown();
    }
};

TEST_F(ReScanFsTaskTest, NoChange)
{
    // rescan
    QSharedPointer<ReScanFsTask> task = QSharedPointer<ReScanFsTask>::create(root,
                                                                             test_data.rootPath(),
                                                                             r,
                                                                             true,
                                                                             single_sum_hash,
                                                                             &errors);
    task_dispatcher->addTask(task);
    QSharedPointer<TaskResult<QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>>>> result = task->getTaskResult();
    ASSERT_TRUE(result);

    const QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>> &changed_files = task->getAvailableTaskResults();
    ASSERT_TRUE(changed_files.isEmpty());
}

TEST_F(ReScanFsTaskTest, AddRootFile)
{
    const QStringList dir;
    const QString file_name = "new_file1";
    const QByteArray content = {"abc"};

    // create new file
    ASSERT_TRUE(test_data.createFile(dir, file_name, content));

    // rescan
    QSharedPointer<ReScanFsTask> task = QSharedPointer<ReScanFsTask>::create(root,
                                                                             test_data.rootPath(),
                                                                             r,
                                                                             true,
                                                                             single_sum_hash,
                                                                             &errors);
    task_dispatcher->addTask(task);
    QSharedPointer<TaskResult<QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>>>> result = task->getTaskResult();
    ASSERT_TRUE(result);

    const QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>> &changed_files = task->getAvailableTaskResults();
    ASSERT_EQ(changed_files.size(), 1);

    QSharedPointer<TaskResult<QWeakPointer<InfoBase>>> result2 = changed_files.constFirst();
    ASSERT_TRUE(result2->hasResult());
    ASSERT_TRUE(result2->getResult().toStrongRef());
    ASSERT_EQ(result2->getResult().toStrongRef().get(), r.get());
}

TEST_F(ReScanFsTaskTest, AddSubDirFileInEmptyDir)
{
    const QStringList dir = {"dir2", "dir3", "dir6"};
    const QString file_name = "new_file1";
    const QByteArray content = {"abc"};
    const DirInfo *subdir = TestFsUtils::findDir(dir, r.get());
    ASSERT_NE(subdir, nullptr);

    // create new file
    ASSERT_TRUE(test_data.createFile(dir, file_name, content));

    // rescan
    QSharedPointer<ReScanFsTask> task = QSharedPointer<ReScanFsTask>::create(root,
                                                                             test_data.rootPath(),
                                                                             r,
                                                                             true,
                                                                             single_sum_hash,
                                                                             &errors);
    task_dispatcher->addTask(task);
    QSharedPointer<TaskResult<QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>>>> result = task->getTaskResult();
    ASSERT_TRUE(result);

    const QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>> &changed_files = task->getAvailableTaskResults();
    ASSERT_EQ(changed_files.size(), 1);

    QSharedPointer<TaskResult<QWeakPointer<InfoBase>>> result2 = changed_files.constFirst();
    ASSERT_TRUE(result2->hasResult());
    ASSERT_TRUE(result2->getResult().toStrongRef());
    ASSERT_EQ(result2->getResult().toStrongRef().get(), subdir);
}

TEST_F(ReScanFsTaskTest, AddSubDirFileInNonEmptyDir)
{
    const QStringList dir = {"dir2", "dir3", "dir5"};
    const QString file_name = "new_file1";
    const QByteArray content = {"abc"};
    const DirInfo *subdir = TestFsUtils::findDir(dir, r.get());
    ASSERT_NE(subdir, nullptr);

           // create new file
    ASSERT_TRUE(test_data.createFile(dir, file_name, content));

           // rescan
    QSharedPointer<ReScanFsTask> task = QSharedPointer<ReScanFsTask>::create(root,
                                                                             test_data.rootPath(),
                                                                             r,
                                                                             true,
                                                                             single_sum_hash,
                                                                             &errors);
    task_dispatcher->addTask(task);
    QSharedPointer<TaskResult<QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>>>> result = task->getTaskResult();
    ASSERT_TRUE(result);

    const QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>> &changed_files = task->getAvailableTaskResults();
    ASSERT_EQ(changed_files.size(), 1);

    QSharedPointer<TaskResult<QWeakPointer<InfoBase>>> result2 = changed_files.constFirst();
    ASSERT_TRUE(result2->hasResult());
    ASSERT_TRUE(result2->getResult().toStrongRef());
    ASSERT_EQ(result2->getResult().toStrongRef().get(), subdir);
}

TEST_F(ReScanFsTaskTest, RemoveFile)
{
    const QStringList dir = {"dir1"};
    const QString file_name = "file1";

    InfoBase *const test_file = TestFsUtils::findChild(r.get(), file_name, true);
    ASSERT_NE(test_file, nullptr);
    InfoParent *const test_parent = test_file->getParent();
    ASSERT_NE(test_parent, nullptr);

    // delete file from fs
    ASSERT_TRUE(test_data.removeFile(dir, file_name));

    // rescan
    QSharedPointer<ReScanFsTask> task = QSharedPointer<ReScanFsTask>::create(root,
                                                                             test_data.rootPath(),
                                                                             r,
                                                                             true,
                                                                             single_sum_hash,
                                                                             &errors);
    task_dispatcher->addTask(task);
    QSharedPointer<TaskResult<QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>>>> result = task->getTaskResult();
    ASSERT_TRUE(result);

    const QList<QSharedPointer<TaskResult<QWeakPointer<InfoBase>>>> &changed_files = task->getAvailableTaskResults();
    ASSERT_EQ(changed_files.size(), 1);

    InfoBase::weak_ptr lost_file = changed_files.constFirst()->getResult();
    InfoBase::ptr ib = lost_file.toStrongRef();
    ASSERT_EQ(test_parent->getInfoBase(), ib.get());
}
