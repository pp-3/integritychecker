// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ScanFsTaskTest.h"

#include <QCoreApplication>

#include "tasks/ScanFsTask.h"

#include "data/TestFsUtils.h"

#include "TaskExecutionMock.h"

void ScanFsTaskTest::SetUp()
{
    const bool b = test_data.create();
    if (!b)
        GTEST_SKIP() << "Test data unpacking failed";

    keep_main_thread = setMainThread(QCoreApplication::instance());  // keep QCoreApplication main thread, as no running event loop is needed for these tests
    task_dispatcher.reset(new TaskDispatcher(&worker_settings, std::make_unique<TaskExecutionMock>()));
}

void ScanFsTaskTest::TearDown()
{
    task_dispatcher->shutdown();
    task_dispatcher.reset();
    keep_main_thread.reset();
    test_data.destroy();
}

void ScanFsTaskTest::cmpDir(const QStringList &dir_paths, DirInfo *dir)
    {
        QDir d(test_data.rootPath() + "/" + dir_paths.join("/"));
        const QStringList entries = d.entryList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::NoSymLinks);

        for (const QString &it : entries)
        {
            InfoBase *ib = TestFsUtils::findChild(dir, it, false);
            ASSERT_NE(ib, nullptr) << "File not found: " << it.toStdString() << " in " << dir->getName().toStdString();

            QFileInfo fi(d, it);

            if (!fi.isFile())
            {
                ASSERT_TRUE(ib->isDir()) << "for: " << it.toStdString() << " in " << dir->getName().toStdString();
                DirInfo *di = DirInfo::cast(ib);
                ASSERT_NE(di, nullptr) << "for: " << it.toStdString() << " in " << dir->getName().toStdString();

                QStringList sub_dir(dir_paths);
                sub_dir << it;
                return cmpDir(sub_dir, di);
            }
            else
            {
                ASSERT_FALSE(ib->isDir()) << "for: " << it.toStdString() << " in " << dir->getName().toStdString();
            }
        }
    }


TEST_F(ScanFsTaskTest, DoTask)
{
    QSharedPointer<ScanFsTask> task = QSharedPointer<ScanFsTask>::create(test_data.rootPath(), &errors);
    task_dispatcher->addTask(task);
    QSharedPointer<TaskResult<QSharedPointer<DirInfoRoot>>> result = task->getTaskResult();
    ASSERT_TRUE(result);
    ASSERT_TRUE(result->hasResult());
}

TEST_F(ScanFsTaskTest, CmpTree)
{
    QSharedPointer<ScanFsTask> task = QSharedPointer<ScanFsTask>::create(test_data.rootPath(), &errors);
    task_dispatcher->addTask(task);
    QSharedPointer<TaskResult<QSharedPointer<DirInfoRoot>>> result = task->getTaskResult();
    DirInfoRoot::ptr root = result->getResult();
    DirInfo::ptr r = root->getRoot();
    ASSERT_TRUE(r);

    cmpDir(QStringList(), r.get());
}
