// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "model/TaskDispatcher.h"

#include "data/TestData.h"
#include "ifc/ErrorPostMock.h"
#include "ifc/WorkerSettingsMock.h"
#include "sum_tree/DirInfo.h"
#include "thread_util/threadcheck.h"

class ScanFsTaskTest : public ::testing::Test
{
protected:
    TestData test_data;
    ErrorPostMock errors;
    WorkerSettingsMock worker_settings;
    std::unique_ptr<TaskDispatcher> task_dispatcher;
    MainThreadResetter keep_main_thread;

    void SetUp() override;

    void TearDown() override;

    /**
     * Compares recursively the contents of two paths. The first is in the file system. The second in the parsed tree.
     * If the contents of two related directories are not equal the test case fails.
     * @param dir_paths The paths in the current root directory as list of separate folders. An empty list denotes the root directory.
     * @param dir The related instance in the parsed tree.
     */
    void cmpDir(const QStringList &dir_paths, DirInfo *dir);
};
