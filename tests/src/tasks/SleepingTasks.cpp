// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "SleepingTasks.h"

#include <QThread>

SleepingSingleTask::SleepingSingleTask(size_t sleepInMs) :
    sleepInMs(sleepInMs)
{
}

QString SleepingSingleTask::getName() const
{
    return "SleepingSingleTask";
}

void SleepingSingleTask::doTask()
{
    QThread::usleep(sleepInMs * 1000);
}

SleepingSingleTaskWithResult::SleepingSingleTaskWithResult(size_t sleepInMs) :
    TaskWithResult<bool>(),
    sleepInMs(sleepInMs)
{
}

QString SleepingSingleTaskWithResult::getName() const
{
    return "SleepingSingleTaskWithResult";
}

void SleepingSingleTaskWithResult::doTask()
{
    QThread::usleep(sleepInMs * 1000);
    setResult(true);
}

SleepingParentTask::SleepingParentTask(size_t childrenCount, bool cancelOnly, size_t sleepInMs) :
    ParentTaskIfc(nullptr),
    childrenCount(childrenCount),
    cancelOnly(cancelOnly),
    sleepInMs(sleepInMs)
{
}

QString SleepingParentTask::getName() const
{
    return "SleepingParentTask";
}

void SleepingParentTask::doParentTask()
{
    for (size_t i=0 ; i<childrenCount ; ++i)
    {
        QSharedPointer<TaskWithResult<bool>> task;
        if (cancelOnly)
            task = QSharedPointer<SleepingSingleTaskCancelOnly>::create();
        else
            task = QSharedPointer<SleepingSingleTaskWithResult>::create();

        startSubTask(task);
    }
}

SleepingSingleTaskCancelOnly::SleepingSingleTaskCancelOnly() :
    TaskWithResult<bool>()
{
}

QString SleepingSingleTaskCancelOnly::getName() const
{
    return "SleepingSingleTaskCancelOnly";
}

void SleepingSingleTaskCancelOnly::doTask()
{
    while (!shouldCancel())
        QThread::usleep(10 * 1000);

    setResult(true);
}
