// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SLEEPINGTASKS_HPP
#define SLEEPINGTASKS_HPP

#include "tasks/ParentTaskIfc.h"
#include "tasks/TaskIfc.h"
#include "tasks/TaskWithResult.h"

/**
 * @brief The SleepingSingleTask class is intended for testing the task execution interfaces. It just sleeps.
 */
class SleepingSingleTask : public TaskIfc
{
public:
    SleepingSingleTask(size_t sleepInMs = 200);

    QString getName() const override;
    void doTask() override;

private:
    size_t sleepInMs;
};

/**
 * @brief The SleepingSingleTaskWithResult class also just sleeps but sets a "true" result at the end of the sleep.
 */
class SleepingSingleTaskWithResult : public TaskWithResult<bool>
{
public:
    SleepingSingleTaskWithResult(size_t sleepInMs = 200);

    QString getName() const override;
    void doTask() override;

private:
    size_t sleepInMs;
};

/**
 * @brief The SleepingParentTask class creates sleeping sub tasks.
 */
class SleepingParentTask : public ParentTaskIfc<bool>
{
public:
    SleepingParentTask(size_t childrenCount, bool cancelOnly, size_t sleepInMs = 200);

    QString getName() const override;
    void doParentTask() override;

private:
    size_t childrenCount;
    bool cancelOnly;
    size_t sleepInMs;
};

/**
 * @brief The SleepingSingleTaskCancelOnly class sleeps indefinitely until it's canceled.
 */
class SleepingSingleTaskCancelOnly : public TaskWithResult<bool>
{
public:
    SleepingSingleTaskCancelOnly();

    QString getName() const override;
    void doTask() override;
};

#endif // SLEEPINGTASKS_HPP
