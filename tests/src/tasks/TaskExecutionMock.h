// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <memory>

#include <gmock/gmock.h>

#include "ifc/TaskExecutionIfc.h"

/**
 * The TaskExecutionMock executes tasks in the tests. In opposite of the ThreadPoolExecution all tasks are immediately
 * executed and not dispatched to background worker threads.
 */
class TaskExecutionMock : public TaskExecutionIfc
{
public:
    TaskExecutionMock()
    {
        EXPECT_CALL(*this, setMaxThreadPoolSize).Times(testing::AnyNumber());  // ignore pool size
        EXPECT_CALL(*this, shutdown).Times(1);
    }

    void executeTask(QRunnable *r) override
    {
        std::unique_ptr<QRunnable> task(r);
        task->run();
    }

    MOCK_METHOD(void, setMaxThreadPoolSize, (size_t s), (override));
    MOCK_METHOD(void, shutdown, (), (override));
};
