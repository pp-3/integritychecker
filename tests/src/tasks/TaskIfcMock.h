// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gmock/gmock.h>

#ifndef TASKIFCMOCK_H
#define TASKIFCMOCK_H

#include "tasks/TaskIfc.h"

class TaskIfcMock : public TaskIfc
{
public:
    MOCK_METHOD(QString, getName, (), (const, override));
    MOCK_METHOD(void, cancel, (), (override));
    MOCK_METHOD(bool, hasSubTasks, (), (const, override));
    MOCK_METHOD(size_t, getTaskCount, (), (const, override));
    MOCK_METHOD(bool, isInternal, (), (const, override));
    MOCK_METHOD(void, doTask, (), (override));
};

#endif // TASKIFCMOCK_H
