// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "tasks/TaskLifeCycle.h"

#include "TaskIfcMock.h"
#include "TaskLifeCycleRunStateMock.h"
#include "qt/TemporaryEventLoop.h"

class TaskLifeCycleTest : public ::testing::Test
{
protected:
    QSharedPointer<TaskLifeCycle> tlc;
    QSharedPointer<TaskIfcMock> task_mock;
    TaskLifeCycleRunStateMock runstate_mock;
    QMetaObject::Connection started_connection;
    QMetaObject::Connection finished_connection;

    void SetUp() override
    {
        task_mock = QSharedPointer<TaskIfcMock>::create();
        tlc = QSharedPointer<TaskLifeCycle>::create(task_mock);
        connectRunStateMock();

        ASSERT_EQ(tlc->getState(), TaskLifeCycle::RunState::INIT);


        EXPECT_CALL(*task_mock, cancel).WillOnce(::testing::Return());  // destructor call
    }

    void TearDown() override
    {
        disconnectRunStateMock();
        tlc.reset();
        task_mock.reset();
    }

    void connectRunStateMock()
    {
        started_connection = QObject::connect(tlc.get(), &TaskLifeCycle::t_TaskStarted, tlc.get(), [this]() {
                runstate_mock.updateRunState(TaskLifeCycle::RunState::STARTED);
            }, Qt::DirectConnection);
        EXPECT_TRUE(started_connection);

        finished_connection = QObject::connect(tlc.get(), &TaskLifeCycle::t_TaskFinished, tlc.get(), [this](bool canceled) {
                if (canceled)
                    runstate_mock.updateRunState(TaskLifeCycle::RunState::CANCELED);
                else
                    runstate_mock.updateRunState(TaskLifeCycle::RunState::FINISHED);
            }, Qt::DirectConnection);
        EXPECT_TRUE(finished_connection);
    }

    void disconnectRunStateMock()
    {
        QObject::disconnect(finished_connection);
        QObject::disconnect(started_connection);
    }
};

// checks, if the TaskLifeCycle signals all state changes
TEST_F(TaskLifeCycleTest, CheckStatesFinished)
{
    {
        testing::InSequence seq;
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(1);
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::FINISHED)).Times(1);
    }

    bool canceled;
    SingleTaskLifeCycleState state(tlc);
    ASSERT_EQ(tlc->getState(), TaskLifeCycle::RunState::INIT);
    ASSERT_GT(tlc->getStateStr().size(), 0);
    ASSERT_FALSE(tlc->isRunning());
    canceled = true;
    ASSERT_FALSE(tlc->isFinishedOrCanceled(&canceled));
    ASSERT_FALSE(canceled);

    state.gotoCreated();
    ASSERT_EQ(tlc->getState(), TaskLifeCycle::RunState::CREATED);
    ASSERT_GT(tlc->getStateStr().size(), 0);
    ASSERT_FALSE(tlc->isRunning());
    canceled = true;
    ASSERT_FALSE(tlc->isFinishedOrCanceled(&canceled));
    ASSERT_FALSE(canceled);

    state.gotoStarted();
    ASSERT_EQ(tlc->getState(), TaskLifeCycle::RunState::STARTED);
    ASSERT_GT(tlc->getStateStr().size(), 0);
    ASSERT_TRUE(tlc->isRunning());
    canceled = true;
    ASSERT_FALSE(tlc->isFinishedOrCanceled(&canceled));
    ASSERT_FALSE(canceled);

    state.gotoFinished(false);
    ASSERT_EQ(tlc->getState(), TaskLifeCycle::RunState::FINISHED);
    ASSERT_GT(tlc->getStateStr().size(), 0);
    ASSERT_FALSE(tlc->isRunning());
    canceled = true;
    ASSERT_TRUE(tlc->isFinishedOrCanceled(&canceled));
    ASSERT_FALSE(canceled);
}

TEST_F(TaskLifeCycleTest, CheckStatesCanceled)
{
    {
        testing::InSequence seq;
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(1);
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::CANCELED)).Times(1);
    }

    SingleTaskLifeCycleState state(tlc);
    state.gotoCreated();
    state.gotoStarted();
    state.gotoFinished(true);
    ASSERT_EQ(tlc->getState(), TaskLifeCycle::RunState::CANCELED);
    ASSERT_GT(tlc->getStateStr().size(), 0);
    ASSERT_FALSE(tlc->isRunning());
    bool canceled = false;
    ASSERT_TRUE(tlc->isFinishedOrCanceled(&canceled));
    ASSERT_TRUE(canceled);
}

// for next tests to run an event loop needs to run

TEST_F(TaskLifeCycleTest, CheckStatesCallbacks)
{
    // create temporary event loop to process signals
    QList<QObject *> moveToEventLoopThread;
    moveToEventLoopThread << tlc.get();
    std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(moveToEventLoopThread);

    const TaskLifeCycle::BoolWithConnection already_running = tlc->addStartedCallback([this]() {
        runstate_mock.updateRunState(TaskLifeCycle::RunState::STARTED);
    }, tlc.get());
    ASSERT_FALSE(already_running.first);
    ASSERT_TRUE(already_running.second);

    const TaskLifeCycle::BoolWithConnection already_finished = tlc->addFinishedCallback([this](bool canceled)
        {
            if (canceled)
                runstate_mock.updateRunState(TaskLifeCycle::RunState::CANCELED);
            else
                runstate_mock.updateRunState(TaskLifeCycle::RunState::FINISHED);
        }, tlc.get());
    ASSERT_FALSE(already_running.first);
    ASSERT_TRUE(already_running.second);

    {
        testing::InSequence seq;
        // direct signals + queued signals (from SetUp)
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(1);
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::FINISHED)).Times(1);
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(1);
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::FINISHED)).Times(1);
    }

    SingleTaskLifeCycleState state(tlc);
    state.gotoCreated();
    state.gotoStarted();
    state.gotoFinished(false);

    tel->waitForEvents();
    QObject::disconnect(already_running.second.value());
    QObject::disconnect(already_finished.second.value());
}

TEST_F(TaskLifeCycleTest, CheckStatesCallbacksAlreadyStarted)
{
    // create temporary event loop to process signals
    QList<QObject *> moveToEventLoopThread;
    moveToEventLoopThread << tlc.get();
    std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(moveToEventLoopThread);

    {
        testing::InSequence seq;
        // direct signals + queued signals (from SetUp)
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(2);
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::FINISHED)).Times(2);
    }

    SingleTaskLifeCycleState state(tlc);
    state.gotoCreated();
    state.gotoStarted();

    const TaskLifeCycle::BoolWithConnection already_running = tlc->addStartedCallback(
        [this]()
        {
            runstate_mock.updateRunState(TaskLifeCycle::RunState::STARTED);
        },
        tlc.get());
    ASSERT_TRUE(already_running.first);  // started
    ASSERT_FALSE(already_running.second);  // no further callback

    const TaskLifeCycle::BoolWithConnection already_finished = tlc->addFinishedCallback(
        [this](bool canceled)
        {
            if (canceled)
                runstate_mock.updateRunState(TaskLifeCycle::RunState::CANCELED);
            else
                runstate_mock.updateRunState(TaskLifeCycle::RunState::FINISHED);
        },
        tlc.get());
    ASSERT_FALSE(already_finished.first);  // but not finished
    ASSERT_TRUE(already_finished.second);  // callback setup

    state.gotoFinished(false);

    tel->waitForEvents();
}

TEST_F(TaskLifeCycleTest, CheckStatesCallbacksAlreadyFinished)
{
    // create temporary event loop to process signals
    QList<QObject *> moveToEventLoopThread;
    moveToEventLoopThread << tlc.get();
    std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(moveToEventLoopThread);

    {
        testing::InSequence seq;
        // queued signal (from SetUp)
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(1);
        // direct signals + queued signals (from SetUp)
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::FINISHED)).Times(2);
    }

    SingleTaskLifeCycleState state(tlc);
    state.gotoCreated();
    state.gotoStarted();
    state.gotoFinished(false);

    const TaskLifeCycle::BoolWithConnection is_running = tlc->addStartedCallback(
        [this]()
        {
            runstate_mock.updateRunState(TaskLifeCycle::RunState::STARTED);
        },
        tlc.get());
    ASSERT_FALSE(is_running.first);  // already finished -> not running
    ASSERT_FALSE(is_running.second);  // already finished -> no callback

    const TaskLifeCycle::BoolWithConnection already_finished = tlc->addFinishedCallback(
        [this](bool canceled)
        {
            if (canceled)
                runstate_mock.updateRunState(TaskLifeCycle::RunState::CANCELED);
            else
                runstate_mock.updateRunState(TaskLifeCycle::RunState::FINISHED);
        },
        tlc.get());
    ASSERT_TRUE(already_finished.first);
    ASSERT_FALSE(already_finished.second);  // already finished -> no callback

    tel->waitForEvents();
}

TEST_F(TaskLifeCycleTest, CheckProgessSingleTask)
{
    disconnectRunStateMock();

    // progress callback
    ::testing::MockFunction<void(int, int)> progressMockDirect, progressMockQueued;
    auto progressCallbackDirect = progressMockDirect.AsStdFunction();
    auto progressCallbackQueued = progressMockQueued.AsStdFunction();

    // create temporary event loop to process signals
    QList<QObject *> moveToEventLoopThread;
    moveToEventLoopThread << tlc.get();
    std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(moveToEventLoopThread);

    connectRunStateMock();  // from temporary event loop

    // connect signal
    QMetaObject::Connection connection;
    connection = QObject::connect(tlc.get(), &TaskLifeCycle::t_TaskProgress, tlc.get(), progressCallbackDirect, Qt::DirectConnection);
    EXPECT_TRUE(connection);

    // connect direct callback
    const TaskLifeCycle::BoolWithConnection is_not_finished = tlc->addTaskProgressCallback(progressCallbackQueued, tlc.get());
    ASSERT_TRUE(is_not_finished.first);
    ASSERT_TRUE(is_not_finished.second);  // callback setup

    // task progress
    ResultWait<bool> create_delay_barrier(false);
    {
        testing::InSequence seq;
        EXPECT_CALL(progressMockDirect, Call(0, 1))
            .Times(1)
            .WillOnce(
                [&create_delay_barrier]()
                {
                    create_delay_barrier.setResult(true);
                });
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(1);
        EXPECT_CALL(progressMockDirect, Call(1, 1)).Times(1);
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::FINISHED)).Times(1);
    }
    {
        testing::InSequence seq;
        EXPECT_CALL(progressMockQueued, Call(0, 1)).Times(1);
        EXPECT_CALL(progressMockQueued, Call(1, 1)).Times(1);
    }

    SingleTaskLifeCycleState state(tlc);
    state.gotoCreated();
    ASSERT_TRUE(create_delay_barrier.wait(10000));
    state.gotoStarted();
    state.gotoFinished(false);

    tel->waitForEvents();
    QObject::disconnect(is_not_finished.second.value());
}

TEST_F(TaskLifeCycleTest, CheckProgessFinishedTask)
{
    disconnectRunStateMock();

    // progress callback
    ::testing::MockFunction<void(int, int)> progressMock;
    auto func = progressMock.AsStdFunction();

    // create temporary event loop to process signals
    QList<QObject *> moveToEventLoopThread;
    moveToEventLoopThread << tlc.get();
    std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(moveToEventLoopThread);

    connectRunStateMock();  // from temporary event loop

    EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(1);
    EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::FINISHED)).Times(1);

    SingleTaskLifeCycleState state(tlc);
    state.gotoCreated();
    state.gotoStarted();
    state.gotoFinished(false);
    QThread::msleep(100);

    // task progress
    EXPECT_CALL(progressMock, Call(::testing::_, ::testing::_)).Times(0);

    // connect signal
    QMetaObject::Connection connection;
    connection = QObject::connect(tlc.get(), &TaskLifeCycle::t_TaskProgress, tlc.get(), func, Qt::DirectConnection);
    EXPECT_TRUE(connection);

    // connect direct callback
    const TaskLifeCycle::BoolWithConnection is_not_finished = tlc->addTaskProgressCallback(func, tlc.get());
    ASSERT_FALSE(is_not_finished.first);
    ASSERT_FALSE(is_not_finished.second);
}

TEST_F(TaskLifeCycleTest, CheckProgessMultipleTasks)
{
    disconnectRunStateMock();

    // progress callback
    ::testing::MockFunction<void(int, int)> progressMock;
    auto progressCallback = progressMock.AsStdFunction();

    // create temporary event loop to process signals
    QList<QObject *> moveToEventLoopThread;
    moveToEventLoopThread << tlc.get();
    std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(moveToEventLoopThread);

    connectRunStateMock();  // from temporary event loop

    // connect signal
    const QMetaObject::Connection connection = QObject::connect(tlc.get(), &TaskLifeCycle::t_TaskProgress,
                                                                tlc.get(), progressCallback,
                                                                Qt::DirectConnection);
    EXPECT_TRUE(connection);

    // parent task with 5 sub tasks
    static const size_t COUNT = 5;
    static const char *NAME = "TEST";

    // prepare task
    EXPECT_CALL(*task_mock, getTaskCount).WillRepeatedly(::testing::Return(COUNT));
    EXPECT_CALL(*task_mock, getName).WillRepeatedly(::testing::Return(QString(NAME)));
    EXPECT_EQ(tlc->getName(), QString(NAME));

    // task progress
    {
        testing::InSequence seq;
        EXPECT_CALL(progressMock, Call(0, COUNT)).Times(1);
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::STARTED)).Times(1);
        for (size_t i = 1 ; i <= COUNT ; ++i)
        {
            EXPECT_CALL(progressMock, Call(i, COUNT)).Times(1);
        }
        EXPECT_CALL(runstate_mock, updateRunState(TaskLifeCycle::RunState::FINISHED)).Times(1);
    }

    SubTasksCollectingLifeCycleState state(tlc);
    for (size_t i = 1 ; i <= COUNT ; ++i)
        state.gotoCreated();
    QThread::msleep(10);
    for (size_t i = 1 ; i <= COUNT ; ++i)
        state.gotoStarted();
    for (size_t i = 1 ; i <= COUNT ; ++i)
    {
        state.gotoFinished(false);
        // progress is updated delayed / throttled down
        QThread::sleep(1);
    }
}
