// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "thread_util/AsyncExec.h"

#include "qt/TemporaryEventLoop.h"

TEST(AsyncExec, ExecuteInOtherLoop)
{
    QThread *current = QThread::currentThread();
    QThread *app = QCoreApplication::instance()->thread();

    // tests are running in main application thread
    EXPECT_EQ(current, app);

    // create separate loop
    QList<QObject *> temporaryObjects;
    std::unique_ptr<TemporaryEventLoop> tel = TemporaryEventLoop::create(temporaryObjects);
    QThread *other = tel->thread();

    // run in other thread loop
    QThread *other2 = nullptr;
    auto f = [&other2]() -> bool
    {
        other2 = QThread::currentThread();
        return true;
    };

    QFuture<bool> res = AsyncExec::execIn(tel.get(), f);
    res.waitForFinished();
    EXPECT_TRUE(res.result());
    EXPECT_EQ(other, other2);
    EXPECT_NE(app, other2);
}
