// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <atomic>

#include "tasks/ThreadPoolExecution.h"
#include "thread_util/ResultWait.h"

class ResultWaitTest : public ::testing::Test
{
protected:
    static constexpr int DEFAULT_RESULT = 1;
    ResultWait<int> wait{int(DEFAULT_RESULT)};
    QMutex mutex;
    std::atomic_uint result_count{0};

    class WaitingRunnable : public QRunnable
    {
    private:
        ResultWait<int> &wait_for;
        QList<int> &results;
        QMutex &mutex;
        std::atomic_uint &result_count;

    public:
        WaitingRunnable(ResultWait<int>  &wait_for,
                        QList<int>       &results,
                        QMutex           &mutex,
                        std::atomic_uint &result_count) :
            wait_for(wait_for),
            results(results),
            mutex(mutex),
            result_count(result_count)
        {
        }

    protected:
        void run() override
        {
            const int result = wait_for.wait();
            {
                QMutexLocker ml(&mutex);
                results << result;
            }
            
            ++result_count;
        }
    };

    bool waitForResultCount(unsigned int cnt)
    {
        for (size_t i = 0 ; i<100 ; ++i)
        {
            if (result_count.load() == cnt)
                return true;

            QThread::usleep(100 * 1000);
        }

        return false;
    }
};

TEST_F(ResultWaitTest, NoResult)
{
    EXPECT_FALSE(wait.hasResult());
}

TEST_F(ResultWaitTest, Timeout)
{
    bool result_set = true;

    const int result = wait.wait(100, &result_set);
    EXPECT_FALSE(result_set);
    EXPECT_EQ(result, DEFAULT_RESULT);
}

TEST_F(ResultWaitTest, SingleWait)
{
    ThreadPoolExecution pool;
    pool.setMaxThreadPoolSize(8);

    QList<int> results;
    pool.executeTask(new WaitingRunnable(wait, results, mutex, result_count));

    QThread::usleep(100 * 1000);
    EXPECT_FALSE(wait.hasResult());
    EXPECT_EQ(result_count.load(), 0);

    wait.setResult(2);
    ASSERT_TRUE(waitForResultCount(1));
    ASSERT_FALSE(results.isEmpty());
    EXPECT_EQ(results.front(), 2);
}

TEST_F(ResultWaitTest, MultipleWait)
{
    constexpr size_t PARALLEL_WAITS = 100;
    ThreadPoolExecution pool;
    pool.setMaxThreadPoolSize(8);

    QList<int> results;
    for (size_t i = 0; i < PARALLEL_WAITS; ++i)
        pool.executeTask(new WaitingRunnable(wait, results, mutex, result_count));

    QThread::usleep(100 * 1000);
    EXPECT_FALSE(wait.hasResult());
    EXPECT_EQ(result_count.load(), 0);

    wait.setResult(3);
    EXPECT_TRUE(waitForResultCount(PARALLEL_WAITS));
    EXPECT_EQ(results.size(), PARALLEL_WAITS);
    EXPECT_EQ(std::count(results.cbegin(),
                         results.cend(),
                         3),
              PARALLEL_WAITS);
}
