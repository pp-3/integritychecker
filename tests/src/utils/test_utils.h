// Copyright 2023 Peter Peters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <ostream>

#include <QSet>
#include <QStringList>

/**
 * "Prints" a QStringList.
 * @param out The stream to write to.
 * @param l The list to print.
 */
std::ostream& operator<<(std::ostream& out, const QStringList &l);

template <typename T>
QSet<T> toSet(const QList<T> &l)
{
    return QSet<T>(l.cbegin(), l.cend());
}

template <typename T>
QList<T> toList(const QSet<T> &s)
{
    return QList<T>(s.cbegin(), s.cend());
}

QStringList toList(const QSet<QString> &s);
